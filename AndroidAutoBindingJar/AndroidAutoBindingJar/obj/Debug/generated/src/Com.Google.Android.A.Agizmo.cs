using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']"
	[global::Android.Runtime.Register ("com/google/android/a/agizmo", DoNotGenerateAcw=true)]
	public partial class Agizmo : global::Java.Lang.Object, global::Android.OS.IInterface {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/a/agizmo", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Agizmo); }
		}

		protected Agizmo (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_os_IBinder_Ljava_lang_String_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/constructor[@name='agizmo' and count(parameter)=2 and parameter[1][@type='android.os.IBinder'] and parameter[2][@type='java.lang.String']]"
		[Register (".ctor", "(Landroid/os/IBinder;Ljava/lang/String;)V", "")]
		protected unsafe Agizmo (global::Android.OS.IBinder p0, string p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				if (((object) this).GetType () != typeof (Agizmo)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/os/IBinder;Ljava/lang/String;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/os/IBinder;Ljava/lang/String;)V", __args);
					return;
				}

				if (id_ctor_Landroid_os_IBinder_Ljava_lang_String_ == IntPtr.Zero)
					id_ctor_Landroid_os_IBinder_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/os/IBinder;Ljava/lang/String;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_os_IBinder_Ljava_lang_String_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_os_IBinder_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_a_ILandroid_os_Parcel_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Parcel']]"
		[Register ("a", "(ILandroid/os/Parcel;)Landroid/os/Parcel;", "")]
		protected unsafe global::Android.OS.Parcel A (int p0, global::Android.OS.Parcel p1)
		{
			if (id_a_ILandroid_os_Parcel_ == IntPtr.Zero)
				id_a_ILandroid_os_Parcel_ = JNIEnv.GetMethodID (class_ref, "a", "(ILandroid/os/Parcel;)Landroid/os/Parcel;");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				global::Android.OS.Parcel __ret = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_ILandroid_os_Parcel_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_a_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/method[@name='a_' and count(parameter)=0]"
		[Register ("a_", "()Landroid/os/Parcel;", "")]
		protected unsafe global::Android.OS.Parcel A_ ()
		{
			if (id_a_ == IntPtr.Zero)
				id_a_ = JNIEnv.GetMethodID (class_ref, "a_", "()Landroid/os/Parcel;");
			try {
				return global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_asBinder;
#pragma warning disable 0169
		static Delegate GetAsBinderHandler ()
		{
			if (cb_asBinder == null)
				cb_asBinder = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AsBinder);
			return cb_asBinder;
		}

		static IntPtr n_AsBinder (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.A.Agizmo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.A.Agizmo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.AsBinder ());
		}
#pragma warning restore 0169

		static IntPtr id_asBinder;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/method[@name='asBinder' and count(parameter)=0]"
		[Register ("asBinder", "()Landroid/os/IBinder;", "GetAsBinderHandler")]
		public virtual unsafe global::Android.OS.IBinder AsBinder ()
		{
			if (id_asBinder == IntPtr.Zero)
				id_asBinder = JNIEnv.GetMethodID (class_ref, "asBinder", "()Landroid/os/IBinder;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_asBinder), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "asBinder", "()Landroid/os/IBinder;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_b_ILandroid_os_Parcel_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/method[@name='b' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Parcel']]"
		[Register ("b", "(ILandroid/os/Parcel;)V", "")]
		protected unsafe void B (int p0, global::Android.OS.Parcel p1)
		{
			if (id_b_ILandroid_os_Parcel_ == IntPtr.Zero)
				id_b_ILandroid_os_Parcel_ = JNIEnv.GetMethodID (class_ref, "b", "(ILandroid/os/Parcel;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_b_ILandroid_os_Parcel_, __args);
			} finally {
			}
		}

		static IntPtr id_c_ILandroid_os_Parcel_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='agizmo']/method[@name='c' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Parcel']]"
		[Register ("c", "(ILandroid/os/Parcel;)V", "")]
		protected unsafe void C (int p0, global::Android.OS.Parcel p1)
		{
			if (id_c_ILandroid_os_Parcel_ == IntPtr.Zero)
				id_c_ILandroid_os_Parcel_ = JNIEnv.GetMethodID (class_ref, "c", "(ILandroid/os/Parcel;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_c_ILandroid_os_Parcel_, __args);
			} finally {
			}
		}

	}
}
