using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpMenuAdapter']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/AlphaJumpMenuAdapter", DoNotGenerateAcw=true)]
	public abstract partial class AlphaJumpMenuAdapter : global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter {


		static IntPtr SUPPORTED_CHARACTER_LIST_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpMenuAdapter']/field[@name='SUPPORTED_CHARACTER_LIST']"
		[Register ("SUPPORTED_CHARACTER_LIST")]
		protected static global::System.Collections.IList SupportedCharacterList {
			get {
				if (SUPPORTED_CHARACTER_LIST_jfieldId == IntPtr.Zero)
					SUPPORTED_CHARACTER_LIST_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "SUPPORTED_CHARACTER_LIST", "Ljava/util/List;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, SUPPORTED_CHARACTER_LIST_jfieldId);
				return global::Android.Runtime.JavaList.FromJniHandle (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr SUPPORTED_CHARACTER_SET_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpMenuAdapter']/field[@name='SUPPORTED_CHARACTER_SET']"
		[Register ("SUPPORTED_CHARACTER_SET")]
		protected static global::System.Collections.ICollection SupportedCharacterSet {
			get {
				if (SUPPORTED_CHARACTER_SET_jfieldId == IntPtr.Zero)
					SUPPORTED_CHARACTER_SET_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "SUPPORTED_CHARACTER_SET", "Ljava/util/Set;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, SUPPORTED_CHARACTER_SET_jfieldId);
				return global::Android.Runtime.JavaSet.FromJniHandle (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr mAlphaJumpKeyItemList_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpMenuAdapter']/field[@name='mAlphaJumpKeyItemList']"
		[Register ("mAlphaJumpKeyItemList")]
		protected global::System.Collections.IList MAlphaJumpKeyItemList {
			get {
				if (mAlphaJumpKeyItemList_jfieldId == IntPtr.Zero)
					mAlphaJumpKeyItemList_jfieldId = JNIEnv.GetFieldID (class_ref, "mAlphaJumpKeyItemList", "Ljava/util/List;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mAlphaJumpKeyItemList_jfieldId);
				return global::Android.Runtime.JavaList.FromJniHandle (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mAlphaJumpKeyItemList_jfieldId == IntPtr.Zero)
					mAlphaJumpKeyItemList_jfieldId = JNIEnv.GetFieldID (class_ref, "mAlphaJumpKeyItemList", "Ljava/util/List;");
				IntPtr native_value = global::Android.Runtime.JavaList.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mAlphaJumpKeyItemList_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/AlphaJumpMenuAdapter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AlphaJumpMenuAdapter); }
		}

		protected AlphaJumpMenuAdapter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpMenuAdapter']/constructor[@name='AlphaJumpMenuAdapter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe AlphaJumpMenuAdapter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (AlphaJumpMenuAdapter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/AlphaJumpMenuAdapter", DoNotGenerateAcw=true)]
	internal partial class AlphaJumpMenuAdapterInvoker : AlphaJumpMenuAdapter {

		public AlphaJumpMenuAdapterInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (AlphaJumpMenuAdapterInvoker); }
		}

		static IntPtr id_getMenuItemCount;
		public override unsafe int MenuItemCount {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItemCount' and count(parameter)=0]"
			[Register ("getMenuItemCount", "()I", "GetGetMenuItemCountHandler")]
			get {
				if (id_getMenuItemCount == IntPtr.Zero)
					id_getMenuItemCount = JNIEnv.GetMethodID (class_ref, "getMenuItemCount", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMenuItemCount);
				} finally {
				}
			}
		}

		static IntPtr id_getMenuItem_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItem' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getMenuItem", "(I)Lcom/google/android/apps/auto/sdk/MenuItem;", "GetGetMenuItem_IHandler")]
		public override unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem GetMenuItem (int p0)
		{
			if (id_getMenuItem_I == IntPtr.Zero)
				id_getMenuItem_I = JNIEnv.GetMethodID (class_ref, "getMenuItem", "(I)Lcom/google/android/apps/auto/sdk/MenuItem;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getMenuItem_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}

}
