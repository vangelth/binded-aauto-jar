using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager.CarMessageListener']"
	[Register ("com/google/android/gms/car/CarMessageManager$CarMessageListener", "", "Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerInvoker")]
	public partial interface ICarMessageManagerCarMessageListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager.CarMessageListener']/method[@name='onIntegerMessage' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("onIntegerMessage", "(III)V", "GetOnIntegerMessage_IIIHandler:Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerInvoker, AndroidAutoBindingJar")]
		void OnIntegerMessage (int p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager.CarMessageListener']/method[@name='onOwnershipLost' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onOwnershipLost", "(I)V", "GetOnOwnershipLost_IHandler:Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerInvoker, AndroidAutoBindingJar")]
		void OnOwnershipLost (int p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarMessageManager$CarMessageListener", DoNotGenerateAcw=true)]
	internal class ICarMessageManagerCarMessageListenerInvoker : global::Java.Lang.Object, ICarMessageManagerCarMessageListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarMessageManager$CarMessageListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarMessageManagerCarMessageListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarMessageManagerCarMessageListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarMessageManagerCarMessageListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarMessageManager.CarMessageListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarMessageManagerCarMessageListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onIntegerMessage_III;
#pragma warning disable 0169
		static Delegate GetOnIntegerMessage_IIIHandler ()
		{
			if (cb_onIntegerMessage_III == null)
				cb_onIntegerMessage_III = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int>) n_OnIntegerMessage_III);
			return cb_onIntegerMessage_III;
		}

		static void n_OnIntegerMessage_III (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnIntegerMessage (p0, p1, p2);
		}
#pragma warning restore 0169

		IntPtr id_onIntegerMessage_III;
		public unsafe void OnIntegerMessage (int p0, int p1, int p2)
		{
			if (id_onIntegerMessage_III == IntPtr.Zero)
				id_onIntegerMessage_III = JNIEnv.GetMethodID (class_ref, "onIntegerMessage", "(III)V");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onIntegerMessage_III, __args);
		}

		static Delegate cb_onOwnershipLost_I;
#pragma warning disable 0169
		static Delegate GetOnOwnershipLost_IHandler ()
		{
			if (cb_onOwnershipLost_I == null)
				cb_onOwnershipLost_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnOwnershipLost_I);
			return cb_onOwnershipLost_I;
		}

		static void n_OnOwnershipLost_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnOwnershipLost (p0);
		}
#pragma warning restore 0169

		IntPtr id_onOwnershipLost_I;
		public unsafe void OnOwnershipLost (int p0)
		{
			if (id_onOwnershipLost_I == IntPtr.Zero)
				id_onOwnershipLost_I = JNIEnv.GetMethodID (class_ref, "onOwnershipLost", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onOwnershipLost_I, __args);
		}

	}

	public partial class IntegerMessageEventArgs : global::System.EventArgs {

		public IntegerMessageEventArgs (int p0, int p1, int p2)
		{
			this.p0 = p0;
			this.p1 = p1;
			this.p2 = p2;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}

		int p1;
		public int P1 {
			get { return p1; }
		}

		int p2;
		public int P2 {
			get { return p2; }
		}
	}

	public partial class OwnershipLostEventArgs : global::System.EventArgs {

		public OwnershipLostEventArgs (int p0)
		{
			this.p0 = p0;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarMessageManager_CarMessageListenerImplementor")]
	internal sealed partial class ICarMessageManagerCarMessageListenerImplementor : global::Java.Lang.Object, ICarMessageManagerCarMessageListener {

		object sender;

		public ICarMessageManagerCarMessageListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarMessageManager_CarMessageListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<IntegerMessageEventArgs> OnIntegerMessageHandler;
#pragma warning restore 0649

		public void OnIntegerMessage (int p0, int p1, int p2)
		{
			var __h = OnIntegerMessageHandler;
			if (__h != null)
				__h (sender, new IntegerMessageEventArgs (p0, p1, p2));
		}
#pragma warning disable 0649
		public EventHandler<OwnershipLostEventArgs> OnOwnershipLostHandler;
#pragma warning restore 0649

		public void OnOwnershipLost (int p0)
		{
			var __h = OnOwnershipLostHandler;
			if (__h != null)
				__h (sender, new OwnershipLostEventArgs (p0));
		}

		internal static bool __IsEmpty (ICarMessageManagerCarMessageListenerImplementor value)
		{
			return value.OnIntegerMessageHandler == null && value.OnOwnershipLostHandler == null;
		}
	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']"
	[Register ("com/google/android/gms/car/CarMessageManager", "", "Com.Google.Android.Gms.Car.ICarMessageManagerInvoker")]
	public partial interface ICarMessageManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='acquireCategory' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("acquireCategory", "(I)Z", "GetAcquireCategory_IHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		bool AcquireCategory (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='getLastIntegerMessage' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register ("getLastIntegerMessage", "(II)Ljava/lang/Integer;", "GetGetLastIntegerMessage_IIHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		global::Java.Lang.Integer GetLastIntegerMessage (int p0, int p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='registerMessageListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarMessageManager.CarMessageListener']]"
		[Register ("registerMessageListener", "(Lcom/google/android/gms/car/CarMessageManager$CarMessageListener;)V", "GetRegisterMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_Handler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		void RegisterMessageListener (global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='releaseAllCategories' and count(parameter)=0]"
		[Register ("releaseAllCategories", "()V", "GetReleaseAllCategoriesHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		void ReleaseAllCategories ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='releaseCategory' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("releaseCategory", "(I)V", "GetReleaseCategory_IHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		void ReleaseCategory (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='sendIntegerMessage' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("sendIntegerMessage", "(III)V", "GetSendIntegerMessage_IIIHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		void SendIntegerMessage (int p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarMessageManager']/method[@name='unregisterMessageListener' and count(parameter)=0]"
		[Register ("unregisterMessageListener", "()V", "GetUnregisterMessageListenerHandler:Com.Google.Android.Gms.Car.ICarMessageManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterMessageListener ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarMessageManager", DoNotGenerateAcw=true)]
	internal class ICarMessageManagerInvoker : global::Java.Lang.Object, ICarMessageManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarMessageManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarMessageManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarMessageManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarMessageManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarMessageManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarMessageManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_acquireCategory_I;
#pragma warning disable 0169
		static Delegate GetAcquireCategory_IHandler ()
		{
			if (cb_acquireCategory_I == null)
				cb_acquireCategory_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_AcquireCategory_I);
			return cb_acquireCategory_I;
		}

		static bool n_AcquireCategory_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AcquireCategory (p0);
		}
#pragma warning restore 0169

		IntPtr id_acquireCategory_I;
		public unsafe bool AcquireCategory (int p0)
		{
			if (id_acquireCategory_I == IntPtr.Zero)
				id_acquireCategory_I = JNIEnv.GetMethodID (class_ref, "acquireCategory", "(I)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_acquireCategory_I, __args);
		}

		static Delegate cb_getLastIntegerMessage_II;
#pragma warning disable 0169
		static Delegate GetGetLastIntegerMessage_IIHandler ()
		{
			if (cb_getLastIntegerMessage_II == null)
				cb_getLastIntegerMessage_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, IntPtr>) n_GetLastIntegerMessage_II);
			return cb_getLastIntegerMessage_II;
		}

		static IntPtr n_GetLastIntegerMessage_II (IntPtr jnienv, IntPtr native__this, int p0, int p1)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLastIntegerMessage (p0, p1));
		}
#pragma warning restore 0169

		IntPtr id_getLastIntegerMessage_II;
		public unsafe global::Java.Lang.Integer GetLastIntegerMessage (int p0, int p1)
		{
			if (id_getLastIntegerMessage_II == IntPtr.Zero)
				id_getLastIntegerMessage_II = JNIEnv.GetMethodID (class_ref, "getLastIntegerMessage", "(II)Ljava/lang/Integer;");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLastIntegerMessage_II, __args), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_;
#pragma warning disable 0169
		static Delegate GetRegisterMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_Handler ()
		{
			if (cb_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_ == null)
				cb_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RegisterMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_);
			return cb_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_;
		}

		static void n_RegisterMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener p0 = (global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegisterMessageListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_;
		public unsafe void RegisterMessageListener (global::Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListener p0)
		{
			if (id_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_ == IntPtr.Zero)
				id_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_ = JNIEnv.GetMethodID (class_ref, "registerMessageListener", "(Lcom/google/android/gms/car/CarMessageManager$CarMessageListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerMessageListener_Lcom_google_android_gms_car_CarMessageManager_CarMessageListener_, __args);
		}

		static Delegate cb_releaseAllCategories;
#pragma warning disable 0169
		static Delegate GetReleaseAllCategoriesHandler ()
		{
			if (cb_releaseAllCategories == null)
				cb_releaseAllCategories = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ReleaseAllCategories);
			return cb_releaseAllCategories;
		}

		static void n_ReleaseAllCategories (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ReleaseAllCategories ();
		}
#pragma warning restore 0169

		IntPtr id_releaseAllCategories;
		public unsafe void ReleaseAllCategories ()
		{
			if (id_releaseAllCategories == IntPtr.Zero)
				id_releaseAllCategories = JNIEnv.GetMethodID (class_ref, "releaseAllCategories", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_releaseAllCategories);
		}

		static Delegate cb_releaseCategory_I;
#pragma warning disable 0169
		static Delegate GetReleaseCategory_IHandler ()
		{
			if (cb_releaseCategory_I == null)
				cb_releaseCategory_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_ReleaseCategory_I);
			return cb_releaseCategory_I;
		}

		static void n_ReleaseCategory_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ReleaseCategory (p0);
		}
#pragma warning restore 0169

		IntPtr id_releaseCategory_I;
		public unsafe void ReleaseCategory (int p0)
		{
			if (id_releaseCategory_I == IntPtr.Zero)
				id_releaseCategory_I = JNIEnv.GetMethodID (class_ref, "releaseCategory", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_releaseCategory_I, __args);
		}

		static Delegate cb_sendIntegerMessage_III;
#pragma warning disable 0169
		static Delegate GetSendIntegerMessage_IIIHandler ()
		{
			if (cb_sendIntegerMessage_III == null)
				cb_sendIntegerMessage_III = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int>) n_SendIntegerMessage_III);
			return cb_sendIntegerMessage_III;
		}

		static void n_SendIntegerMessage_III (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SendIntegerMessage (p0, p1, p2);
		}
#pragma warning restore 0169

		IntPtr id_sendIntegerMessage_III;
		public unsafe void SendIntegerMessage (int p0, int p1, int p2)
		{
			if (id_sendIntegerMessage_III == IntPtr.Zero)
				id_sendIntegerMessage_III = JNIEnv.GetMethodID (class_ref, "sendIntegerMessage", "(III)V");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendIntegerMessage_III, __args);
		}

		static Delegate cb_unregisterMessageListener;
#pragma warning disable 0169
		static Delegate GetUnregisterMessageListenerHandler ()
		{
			if (cb_unregisterMessageListener == null)
				cb_unregisterMessageListener = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_UnregisterMessageListener);
			return cb_unregisterMessageListener;
		}

		static void n_UnregisterMessageListener (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarMessageManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarMessageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterMessageListener ();
		}
#pragma warning restore 0169

		IntPtr id_unregisterMessageListener;
		public unsafe void UnregisterMessageListener ()
		{
			if (id_unregisterMessageListener == IntPtr.Zero)
				id_unregisterMessageListener = JNIEnv.GetMethodID (class_ref, "unregisterMessageListener", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterMessageListener);
		}

	}

}
