using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/StatusBarController", DoNotGenerateAcw=true)]
	public partial class StatusBarController : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/StatusBarController", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (StatusBarController); }
		}

		protected StatusBarController (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_isTitleVisible;
#pragma warning disable 0169
		static Delegate GetIsTitleVisibleHandler ()
		{
			if (cb_isTitleVisible == null)
				cb_isTitleVisible = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsTitleVisible);
			return cb_isTitleVisible;
		}

		static bool n_IsTitleVisible (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsTitleVisible;
		}
#pragma warning restore 0169

		static IntPtr id_isTitleVisible;
		public virtual unsafe bool IsTitleVisible {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='isTitleVisible' and count(parameter)=0]"
			[Register ("isTitleVisible", "()Z", "GetIsTitleVisibleHandler")]
			get {
				if (id_isTitleVisible == IntPtr.Zero)
					id_isTitleVisible = JNIEnv.GetMethodID (class_ref, "isTitleVisible", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isTitleVisible);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isTitleVisible", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_hideAppHeader;
#pragma warning disable 0169
		static Delegate GetHideAppHeaderHandler ()
		{
			if (cb_hideAppHeader == null)
				cb_hideAppHeader = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideAppHeader);
			return cb_hideAppHeader;
		}

		static void n_HideAppHeader (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideAppHeader ();
		}
#pragma warning restore 0169

		static IntPtr id_hideAppHeader;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideAppHeader' and count(parameter)=0]"
		[Register ("hideAppHeader", "()V", "GetHideAppHeaderHandler")]
		public virtual unsafe void HideAppHeader ()
		{
			if (id_hideAppHeader == IntPtr.Zero)
				id_hideAppHeader = JNIEnv.GetMethodID (class_ref, "hideAppHeader", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideAppHeader);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideAppHeader", "()V"));
			} finally {
			}
		}

		static Delegate cb_hideBatteryLevel;
#pragma warning disable 0169
		static Delegate GetHideBatteryLevelHandler ()
		{
			if (cb_hideBatteryLevel == null)
				cb_hideBatteryLevel = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideBatteryLevel);
			return cb_hideBatteryLevel;
		}

		static void n_HideBatteryLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideBatteryLevel ();
		}
#pragma warning restore 0169

		static IntPtr id_hideBatteryLevel;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideBatteryLevel' and count(parameter)=0]"
		[Register ("hideBatteryLevel", "()V", "GetHideBatteryLevelHandler")]
		public virtual unsafe void HideBatteryLevel ()
		{
			if (id_hideBatteryLevel == IntPtr.Zero)
				id_hideBatteryLevel = JNIEnv.GetMethodID (class_ref, "hideBatteryLevel", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideBatteryLevel);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideBatteryLevel", "()V"));
			} finally {
			}
		}

		static Delegate cb_hideClock;
#pragma warning disable 0169
		static Delegate GetHideClockHandler ()
		{
			if (cb_hideClock == null)
				cb_hideClock = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideClock);
			return cb_hideClock;
		}

		static void n_HideClock (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideClock ();
		}
#pragma warning restore 0169

		static IntPtr id_hideClock;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideClock' and count(parameter)=0]"
		[Register ("hideClock", "()V", "GetHideClockHandler")]
		public virtual unsafe void HideClock ()
		{
			if (id_hideClock == IntPtr.Zero)
				id_hideClock = JNIEnv.GetMethodID (class_ref, "hideClock", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideClock);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideClock", "()V"));
			} finally {
			}
		}

		static Delegate cb_hideConnectivityLevel;
#pragma warning disable 0169
		static Delegate GetHideConnectivityLevelHandler ()
		{
			if (cb_hideConnectivityLevel == null)
				cb_hideConnectivityLevel = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideConnectivityLevel);
			return cb_hideConnectivityLevel;
		}

		static void n_HideConnectivityLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideConnectivityLevel ();
		}
#pragma warning restore 0169

		static IntPtr id_hideConnectivityLevel;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideConnectivityLevel' and count(parameter)=0]"
		[Register ("hideConnectivityLevel", "()V", "GetHideConnectivityLevelHandler")]
		public virtual unsafe void HideConnectivityLevel ()
		{
			if (id_hideConnectivityLevel == IntPtr.Zero)
				id_hideConnectivityLevel = JNIEnv.GetMethodID (class_ref, "hideConnectivityLevel", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideConnectivityLevel);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideConnectivityLevel", "()V"));
			} finally {
			}
		}

		static Delegate cb_hideMicButton;
#pragma warning disable 0169
		static Delegate GetHideMicButtonHandler ()
		{
			if (cb_hideMicButton == null)
				cb_hideMicButton = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideMicButton);
			return cb_hideMicButton;
		}

		static void n_HideMicButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideMicButton ();
		}
#pragma warning restore 0169

		static IntPtr id_hideMicButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideMicButton' and count(parameter)=0]"
		[Register ("hideMicButton", "()V", "GetHideMicButtonHandler")]
		public virtual unsafe void HideMicButton ()
		{
			if (id_hideMicButton == IntPtr.Zero)
				id_hideMicButton = JNIEnv.GetMethodID (class_ref, "hideMicButton", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideMicButton);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideMicButton", "()V"));
			} finally {
			}
		}

		static Delegate cb_hideTitle;
#pragma warning disable 0169
		static Delegate GetHideTitleHandler ()
		{
			if (cb_hideTitle == null)
				cb_hideTitle = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideTitle);
			return cb_hideTitle;
		}

		static void n_HideTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideTitle ();
		}
#pragma warning restore 0169

		static IntPtr id_hideTitle;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='hideTitle' and count(parameter)=0]"
		[Register ("hideTitle", "()V", "GetHideTitleHandler")]
		public virtual unsafe void HideTitle ()
		{
			if (id_hideTitle == IntPtr.Zero)
				id_hideTitle = JNIEnv.GetMethodID (class_ref, "hideTitle", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideTitle);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideTitle", "()V"));
			} finally {
			}
		}

		static Delegate cb_setAppBarAlpha_F;
#pragma warning disable 0169
		static Delegate GetSetAppBarAlpha_FHandler ()
		{
			if (cb_setAppBarAlpha_F == null)
				cb_setAppBarAlpha_F = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, float>) n_SetAppBarAlpha_F);
			return cb_setAppBarAlpha_F;
		}

		static void n_SetAppBarAlpha_F (IntPtr jnienv, IntPtr native__this, float p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetAppBarAlpha (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAppBarAlpha_F;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='setAppBarAlpha' and count(parameter)=1 and parameter[1][@type='float']]"
		[Register ("setAppBarAlpha", "(F)V", "GetSetAppBarAlpha_FHandler")]
		public virtual unsafe void SetAppBarAlpha (float p0)
		{
			if (id_setAppBarAlpha_F == IntPtr.Zero)
				id_setAppBarAlpha_F = JNIEnv.GetMethodID (class_ref, "setAppBarAlpha", "(F)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setAppBarAlpha_F, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAppBarAlpha", "(F)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAppBarBackgroundColor_I;
#pragma warning disable 0169
		static Delegate GetSetAppBarBackgroundColor_IHandler ()
		{
			if (cb_setAppBarBackgroundColor_I == null)
				cb_setAppBarBackgroundColor_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetAppBarBackgroundColor_I);
			return cb_setAppBarBackgroundColor_I;
		}

		static void n_SetAppBarBackgroundColor_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetAppBarBackgroundColor (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAppBarBackgroundColor_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='setAppBarBackgroundColor' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setAppBarBackgroundColor", "(I)V", "GetSetAppBarBackgroundColor_IHandler")]
		public virtual unsafe void SetAppBarBackgroundColor (int p0)
		{
			if (id_setAppBarBackgroundColor_I == IntPtr.Zero)
				id_setAppBarBackgroundColor_I = JNIEnv.GetMethodID (class_ref, "setAppBarBackgroundColor", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setAppBarBackgroundColor_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAppBarBackgroundColor", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setDayNightStyle_I;
#pragma warning disable 0169
		static Delegate GetSetDayNightStyle_IHandler ()
		{
			if (cb_setDayNightStyle_I == null)
				cb_setDayNightStyle_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetDayNightStyle_I);
			return cb_setDayNightStyle_I;
		}

		static void n_SetDayNightStyle_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDayNightStyle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setDayNightStyle_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='setDayNightStyle' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setDayNightStyle", "(I)V", "GetSetDayNightStyle_IHandler")]
		public virtual unsafe void SetDayNightStyle (int p0)
		{
			if (id_setDayNightStyle_I == IntPtr.Zero)
				id_setDayNightStyle_I = JNIEnv.GetMethodID (class_ref, "setDayNightStyle", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDayNightStyle_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDayNightStyle", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setTitle_Ljava_lang_CharSequence_;
#pragma warning disable 0169
		static Delegate GetSetTitle_Ljava_lang_CharSequence_Handler ()
		{
			if (cb_setTitle_Ljava_lang_CharSequence_ == null)
				cb_setTitle_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_CharSequence_);
			return cb_setTitle_Ljava_lang_CharSequence_;
		}

		static void n_SetTitle_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetTitle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setTitle_Ljava_lang_CharSequence_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
		[Register ("setTitle", "(Ljava/lang/CharSequence;)V", "GetSetTitle_Ljava_lang_CharSequence_Handler")]
		public virtual unsafe void SetTitle (global::Java.Lang.ICharSequence p0)
		{
			if (id_setTitle_Ljava_lang_CharSequence_ == IntPtr.Zero)
				id_setTitle_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/CharSequence;)V");
			IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setTitle_Ljava_lang_CharSequence_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/CharSequence;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		public void SetTitle (string p0)
		{
			global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
			SetTitle (jls_p0);
			jls_p0?.Dispose ();
		}

		static Delegate cb_showAppHeader;
#pragma warning disable 0169
		static Delegate GetShowAppHeaderHandler ()
		{
			if (cb_showAppHeader == null)
				cb_showAppHeader = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowAppHeader);
			return cb_showAppHeader;
		}

		static void n_ShowAppHeader (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowAppHeader ();
		}
#pragma warning restore 0169

		static IntPtr id_showAppHeader;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showAppHeader' and count(parameter)=0]"
		[Register ("showAppHeader", "()V", "GetShowAppHeaderHandler")]
		public virtual unsafe void ShowAppHeader ()
		{
			if (id_showAppHeader == IntPtr.Zero)
				id_showAppHeader = JNIEnv.GetMethodID (class_ref, "showAppHeader", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showAppHeader);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showAppHeader", "()V"));
			} finally {
			}
		}

		static Delegate cb_showBatteryLevel;
#pragma warning disable 0169
		static Delegate GetShowBatteryLevelHandler ()
		{
			if (cb_showBatteryLevel == null)
				cb_showBatteryLevel = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowBatteryLevel);
			return cb_showBatteryLevel;
		}

		static void n_ShowBatteryLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowBatteryLevel ();
		}
#pragma warning restore 0169

		static IntPtr id_showBatteryLevel;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showBatteryLevel' and count(parameter)=0]"
		[Register ("showBatteryLevel", "()V", "GetShowBatteryLevelHandler")]
		public virtual unsafe void ShowBatteryLevel ()
		{
			if (id_showBatteryLevel == IntPtr.Zero)
				id_showBatteryLevel = JNIEnv.GetMethodID (class_ref, "showBatteryLevel", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showBatteryLevel);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showBatteryLevel", "()V"));
			} finally {
			}
		}

		static Delegate cb_showClock;
#pragma warning disable 0169
		static Delegate GetShowClockHandler ()
		{
			if (cb_showClock == null)
				cb_showClock = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowClock);
			return cb_showClock;
		}

		static void n_ShowClock (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowClock ();
		}
#pragma warning restore 0169

		static IntPtr id_showClock;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showClock' and count(parameter)=0]"
		[Register ("showClock", "()V", "GetShowClockHandler")]
		public virtual unsafe void ShowClock ()
		{
			if (id_showClock == IntPtr.Zero)
				id_showClock = JNIEnv.GetMethodID (class_ref, "showClock", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showClock);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showClock", "()V"));
			} finally {
			}
		}

		static Delegate cb_showConnectivityLevel;
#pragma warning disable 0169
		static Delegate GetShowConnectivityLevelHandler ()
		{
			if (cb_showConnectivityLevel == null)
				cb_showConnectivityLevel = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowConnectivityLevel);
			return cb_showConnectivityLevel;
		}

		static void n_ShowConnectivityLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowConnectivityLevel ();
		}
#pragma warning restore 0169

		static IntPtr id_showConnectivityLevel;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showConnectivityLevel' and count(parameter)=0]"
		[Register ("showConnectivityLevel", "()V", "GetShowConnectivityLevelHandler")]
		public virtual unsafe void ShowConnectivityLevel ()
		{
			if (id_showConnectivityLevel == IntPtr.Zero)
				id_showConnectivityLevel = JNIEnv.GetMethodID (class_ref, "showConnectivityLevel", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showConnectivityLevel);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showConnectivityLevel", "()V"));
			} finally {
			}
		}

		static Delegate cb_showMicButton;
#pragma warning disable 0169
		static Delegate GetShowMicButtonHandler ()
		{
			if (cb_showMicButton == null)
				cb_showMicButton = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowMicButton);
			return cb_showMicButton;
		}

		static void n_ShowMicButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowMicButton ();
		}
#pragma warning restore 0169

		static IntPtr id_showMicButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showMicButton' and count(parameter)=0]"
		[Register ("showMicButton", "()V", "GetShowMicButtonHandler")]
		public virtual unsafe void ShowMicButton ()
		{
			if (id_showMicButton == IntPtr.Zero)
				id_showMicButton = JNIEnv.GetMethodID (class_ref, "showMicButton", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showMicButton);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showMicButton", "()V"));
			} finally {
			}
		}

		static Delegate cb_showTitle;
#pragma warning disable 0169
		static Delegate GetShowTitleHandler ()
		{
			if (cb_showTitle == null)
				cb_showTitle = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowTitle);
			return cb_showTitle;
		}

		static void n_ShowTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.StatusBarController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowTitle ();
		}
#pragma warning restore 0169

		static IntPtr id_showTitle;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='StatusBarController']/method[@name='showTitle' and count(parameter)=0]"
		[Register ("showTitle", "()V", "GetShowTitleHandler")]
		public virtual unsafe void ShowTitle ()
		{
			if (id_showTitle == IntPtr.Zero)
				id_showTitle = JNIEnv.GetMethodID (class_ref, "showTitle", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showTitle);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showTitle", "()V"));
			} finally {
			}
		}

	}
}
