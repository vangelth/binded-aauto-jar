using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='Car']"
	[global::Android.Runtime.Register ("android/support/car/Car", DoNotGenerateAcw=true)]
	public partial class Car : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='APP_FOCUS_SERVICE']"
		[Register ("APP_FOCUS_SERVICE")]
		public const string AppFocusService = (string) "app_focus";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='AUDIO_SERVICE']"
		[Register ("AUDIO_SERVICE")]
		public const string AudioService = (string) "audio";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CAR_NAVIGATION_SERVICE']"
		[Register ("CAR_NAVIGATION_SERVICE")]
		public const string CarNavigationService = (string) "car_navigation_service";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_ADB_EMULATOR']"
		[Register ("CONNECTION_TYPE_ADB_EMULATOR")]
		public const int ConnectionTypeAdbEmulator = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_EMBEDDED']"
		[Register ("CONNECTION_TYPE_EMBEDDED")]
		public const int ConnectionTypeEmbedded = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_EMULATOR']"
		[Register ("CONNECTION_TYPE_EMULATOR")]
		public const int ConnectionTypeEmulator = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_ON_DEVICE_EMULATOR']"
		[Register ("CONNECTION_TYPE_ON_DEVICE_EMULATOR")]
		public const int ConnectionTypeOnDeviceEmulator = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_UNKNOWN']"
		[Register ("CONNECTION_TYPE_UNKNOWN")]
		public const int ConnectionTypeUnknown = (int) -1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_USB']"
		[Register ("CONNECTION_TYPE_USB")]
		public const int ConnectionTypeUsb = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='CONNECTION_TYPE_WIFI']"
		[Register ("CONNECTION_TYPE_WIFI")]
		public const int ConnectionTypeWifi = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='INFO_SERVICE']"
		[Register ("INFO_SERVICE")]
		public const string InfoService = (string) "info";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='NAVIGATION_STATUS_SERVICE']"
		[Register ("NAVIGATION_STATUS_SERVICE")]
		public const string NavigationStatusService = (string) "car_navigation_service";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PACKAGE_SERVICE']"
		[Register ("PACKAGE_SERVICE")]
		public const string PackageService = (string) "package";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_CAR_CONTROL_AUDIO_VOLUME']"
		[Register ("PERMISSION_CAR_CONTROL_AUDIO_VOLUME")]
		public const string PermissionCarControlAudioVolume = (string) "android.car.permission.CAR_CONTROL_AUDIO_VOLUME";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_CAR_NAVIGATION_MANAGER']"
		[Register ("PERMISSION_CAR_NAVIGATION_MANAGER")]
		public const string PermissionCarNavigationManager = (string) "android.car.permission.PERMISSION_CAR_NAVIGATION_MANAGER";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_FUEL']"
		[Register ("PERMISSION_FUEL")]
		public const string PermissionFuel = (string) "android.car.permission.CAR_FUEL";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_MILEAGE']"
		[Register ("PERMISSION_MILEAGE")]
		public const string PermissionMileage = (string) "android.car.permission.CAR_MILEAGE";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_SPEED']"
		[Register ("PERMISSION_SPEED")]
		public const string PermissionSpeed = (string) "android.car.permission.CAR_SPEED";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_VEHICLE_DYNAMICS_STATE']"
		[Register ("PERMISSION_VEHICLE_DYNAMICS_STATE")]
		public const string PermissionVehicleDynamicsState = (string) "android.car.permission.VEHICLE_DYNAMICS_STATE";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='PERMISSION_VENDOR_EXTENSION']"
		[Register ("PERMISSION_VENDOR_EXTENSION")]
		public const string PermissionVendorExtension = (string) "android.car.permission.CAR_VENDOR_EXTENSION";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='Car']/field[@name='SENSOR_SERVICE']"
		[Register ("SENSOR_SERVICE")]
		public const string SensorService = (string) "sensor";
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='Car.ConnectionType']"
		[Register ("android/support/car/Car$ConnectionType", "", "Android.Support.Car.Car/IConnectionTypeInvoker")]
		public partial interface IConnectionType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/Car$ConnectionType", DoNotGenerateAcw=true)]
		internal class IConnectionTypeInvoker : global::Java.Lang.Object, IConnectionType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/Car$ConnectionType");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IConnectionTypeInvoker); }
			}

			IntPtr class_ref;

			public static IConnectionType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IConnectionType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.Car.ConnectionType"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IConnectionTypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Car.IConnectionType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car.IConnectionType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Car.IConnectionType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car.IConnectionType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Car.IConnectionType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car.IConnectionType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Car.IConnectionType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car.IConnectionType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/Car", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Car); }
		}

		protected Car (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_support_car_CarServiceLoader_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='Car']/constructor[@name='Car' and count(parameter)=1 and parameter[1][@type='android.support.car.CarServiceLoader']]"
		[Register (".ctor", "(Landroid/support/car/CarServiceLoader;)V", "")]
		public unsafe Car (global::Android.Support.Car.CarServiceLoader p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (Car)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/support/car/CarServiceLoader;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/support/car/CarServiceLoader;)V", __args);
					return;
				}

				if (id_ctor_Landroid_support_car_CarServiceLoader_ == IntPtr.Zero)
					id_ctor_Landroid_support_car_CarServiceLoader_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/support/car/CarServiceLoader;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_support_car_CarServiceLoader_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_support_car_CarServiceLoader_, __args);
			} finally {
			}
		}

		static Delegate cb_getCarConnectionType;
#pragma warning disable 0169
		static Delegate GetGetCarConnectionTypeHandler ()
		{
			if (cb_getCarConnectionType == null)
				cb_getCarConnectionType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetCarConnectionType);
			return cb_getCarConnectionType;
		}

		static int n_GetCarConnectionType (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CarConnectionType;
		}
#pragma warning restore 0169

		static IntPtr id_getCarConnectionType;
		public virtual unsafe int CarConnectionType {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='getCarConnectionType' and count(parameter)=0]"
			[Register ("getCarConnectionType", "()I", "GetGetCarConnectionTypeHandler")]
			get {
				if (id_getCarConnectionType == IntPtr.Zero)
					id_getCarConnectionType = JNIEnv.GetMethodID (class_ref, "getCarConnectionType", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getCarConnectionType);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarConnectionType", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_isConnected;
#pragma warning disable 0169
		static Delegate GetIsConnectedHandler ()
		{
			if (cb_isConnected == null)
				cb_isConnected = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConnected);
			return cb_isConnected;
		}

		static bool n_IsConnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsConnected;
		}
#pragma warning restore 0169

		static IntPtr id_isConnected;
		public virtual unsafe bool IsConnected {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='isConnected' and count(parameter)=0]"
			[Register ("isConnected", "()Z", "GetIsConnectedHandler")]
			get {
				if (id_isConnected == IntPtr.Zero)
					id_isConnected = JNIEnv.GetMethodID (class_ref, "isConnected", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConnected);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isConnected", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isConnecting;
#pragma warning disable 0169
		static Delegate GetIsConnectingHandler ()
		{
			if (cb_isConnecting == null)
				cb_isConnecting = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConnecting);
			return cb_isConnecting;
		}

		static bool n_IsConnecting (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsConnecting;
		}
#pragma warning restore 0169

		static IntPtr id_isConnecting;
		public virtual unsafe bool IsConnecting {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='isConnecting' and count(parameter)=0]"
			[Register ("isConnecting", "()Z", "GetIsConnectingHandler")]
			get {
				if (id_isConnecting == IntPtr.Zero)
					id_isConnecting = JNIEnv.GetMethodID (class_ref, "isConnecting", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConnecting);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isConnecting", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_connect;
#pragma warning disable 0169
		static Delegate GetConnectHandler ()
		{
			if (cb_connect == null)
				cb_connect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Connect);
			return cb_connect;
		}

		static void n_Connect (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Connect ();
		}
#pragma warning restore 0169

		static IntPtr id_connect;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='connect' and count(parameter)=0]"
		[Register ("connect", "()V", "GetConnectHandler")]
		public virtual unsafe void Connect ()
		{
			if (id_connect == IntPtr.Zero)
				id_connect = JNIEnv.GetMethodID (class_ref, "connect", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_connect);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "connect", "()V"));
			} finally {
			}
		}

		static IntPtr id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='createCar' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.support.car.CarConnectionCallback']]"
		[Register ("createCar", "(Landroid/content/Context;Landroid/support/car/CarConnectionCallback;)Landroid/support/car/Car;", "")]
		public static unsafe global::Android.Support.Car.Car CreateCar (global::Android.Content.Context p0, global::Android.Support.Car.CarConnectionCallback p1)
		{
			if (id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_ == IntPtr.Zero)
				id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_ = JNIEnv.GetStaticMethodID (class_ref, "createCar", "(Landroid/content/Context;Landroid/support/car/CarConnectionCallback;)Landroid/support/car/Car;");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				global::Android.Support.Car.Car __ret = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (JNIEnv.CallStaticObjectMethod  (class_ref, id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_Landroid_os_Handler_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='createCar' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.support.car.CarConnectionCallback'] and parameter[3][@type='android.os.Handler']]"
		[Register ("createCar", "(Landroid/content/Context;Landroid/support/car/CarConnectionCallback;Landroid/os/Handler;)Landroid/support/car/Car;", "")]
		public static unsafe global::Android.Support.Car.Car CreateCar (global::Android.Content.Context p0, global::Android.Support.Car.CarConnectionCallback p1, global::Android.OS.Handler p2)
		{
			if (id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_Landroid_os_Handler_ == IntPtr.Zero)
				id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_Landroid_os_Handler_ = JNIEnv.GetStaticMethodID (class_ref, "createCar", "(Landroid/content/Context;Landroid/support/car/CarConnectionCallback;Landroid/os/Handler;)Landroid/support/car/Car;");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				global::Android.Support.Car.Car __ret = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (JNIEnv.CallStaticObjectMethod  (class_ref, id_createCar_Landroid_content_Context_Landroid_support_car_CarConnectionCallback_Landroid_os_Handler_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_disconnect;
#pragma warning disable 0169
		static Delegate GetDisconnectHandler ()
		{
			if (cb_disconnect == null)
				cb_disconnect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Disconnect);
			return cb_disconnect;
		}

		static void n_Disconnect (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Disconnect ();
		}
#pragma warning restore 0169

		static IntPtr id_disconnect;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='disconnect' and count(parameter)=0]"
		[Register ("disconnect", "()V", "GetDisconnectHandler")]
		public virtual unsafe void Disconnect ()
		{
			if (id_disconnect == IntPtr.Zero)
				id_disconnect = JNIEnv.GetMethodID (class_ref, "disconnect", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_disconnect);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "disconnect", "()V"));
			} finally {
			}
		}

		static Delegate cb_getCarManager_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_Class_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_Class_ == null)
				cb_getCarManager_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_Class_);
			return cb_getCarManager_Ljava_lang_Class_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getCarManager_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;T&gt;']]"
		[Register ("getCarManager", "(Ljava/lang/Class;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_Class_Handler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"T"})]
		public virtual unsafe global::Java.Lang.Object GetCarManager (global::Java.Lang.Class p0)
		{
			if (id_getCarManager_Ljava_lang_Class_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/Class;)Ljava/lang/Object;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Java.Lang.Object __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarManager", "(Ljava/lang/Class;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_getCarManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_String_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_String_ == null)
				cb_getCarManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_String_);
			return cb_getCarManager_Ljava_lang_String_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Car __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getCarManager_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='Car']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler")]
		public virtual unsafe global::Java.Lang.Object GetCarManager (string p0)
		{
			if (id_getCarManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				global::Java.Lang.Object __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
