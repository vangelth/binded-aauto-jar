using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarVendorExtensionManagerLoader']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/CarVendorExtensionManagerLoader", DoNotGenerateAcw=true)]
	public partial class CarVendorExtensionManagerLoader : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarVendorExtensionManagerLoader']/field[@name='VENDOR_EXTENSION_LOADER_SERVICE']"
		[Register ("VENDOR_EXTENSION_LOADER_SERVICE")]
		public const string VendorExtensionLoaderService = (string) "vec_loader";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/CarVendorExtensionManagerLoader", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarVendorExtensionManagerLoader); }
		}

		protected CarVendorExtensionManagerLoader (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarApi_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarVendorExtensionManagerLoader']/constructor[@name='CarVendorExtensionManagerLoader' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarApi']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarApi;)V", "")]
		protected unsafe CarVendorExtensionManagerLoader (global::Com.Google.Android.Gms.Car.ICarApi p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (CarVendorExtensionManagerLoader)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarApi;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarApi;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarApi_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarApi_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarApi;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarApi_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarApi_, __args);
			} finally {
			}
		}

		static Delegate cb_getManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetManager_Ljava_lang_String_Handler ()
		{
			if (cb_getManager_Ljava_lang_String_ == null)
				cb_getManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetManager_Ljava_lang_String_);
			return cb_getManager_Ljava_lang_String_;
		}

		static IntPtr n_GetManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarVendorExtensionManagerLoader __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarVendorExtensionManagerLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getManager_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarVendorExtensionManagerLoader']/method[@name='getManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getManager", "(Ljava/lang/String;)Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager;", "GetGetManager_Ljava_lang_String_Handler")]
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManager GetManager (string p0)
		{
			if (id_getManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getManager", "(Ljava/lang/String;)Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManager __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManager> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManager> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getManager", "(Ljava/lang/String;)Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
