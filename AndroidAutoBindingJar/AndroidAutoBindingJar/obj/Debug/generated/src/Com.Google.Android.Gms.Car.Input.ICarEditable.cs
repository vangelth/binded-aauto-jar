using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car.Input {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='CarEditable']"
	[Register ("com/google/android/gms/car/input/CarEditable", "", "Com.Google.Android.Gms.Car.Input.ICarEditableInvoker")]
	public partial interface ICarEditable : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='CarEditable']/method[@name='onCreateInputConnection' and count(parameter)=1 and parameter[1][@type='android.view.inputmethod.EditorInfo']]"
		[Register ("onCreateInputConnection", "(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;", "GetOnCreateInputConnection_Landroid_view_inputmethod_EditorInfo_Handler:Com.Google.Android.Gms.Car.Input.ICarEditableInvoker, AndroidAutoBindingJar")]
		global::Android.Views.InputMethods.IInputConnection OnCreateInputConnection (global::Android.Views.InputMethods.EditorInfo p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='CarEditable']/method[@name='setCarEditableListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.input.CarEditableListener']]"
		[Register ("setCarEditableListener", "(Lcom/google/android/gms/car/input/CarEditableListener;)V", "GetSetCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_Handler:Com.Google.Android.Gms.Car.Input.ICarEditableInvoker, AndroidAutoBindingJar")]
		void SetCarEditableListener (global::Com.Google.Android.Gms.Car.Input.ICarEditableListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='CarEditable']/method[@name='setInputEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setInputEnabled", "(Z)V", "GetSetInputEnabled_ZHandler:Com.Google.Android.Gms.Car.Input.ICarEditableInvoker, AndroidAutoBindingJar")]
		void SetInputEnabled (bool p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/input/CarEditable", DoNotGenerateAcw=true)]
	internal class ICarEditableInvoker : global::Java.Lang.Object, ICarEditable {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/input/CarEditable");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarEditableInvoker); }
		}

		IntPtr class_ref;

		public static ICarEditable GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarEditable> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.input.CarEditable"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarEditableInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_;
#pragma warning disable 0169
		static Delegate GetOnCreateInputConnection_Landroid_view_inputmethod_EditorInfo_Handler ()
		{
			if (cb_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_ == null)
				cb_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnCreateInputConnection_Landroid_view_inputmethod_EditorInfo_);
			return cb_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_;
		}

		static IntPtr n_OnCreateInputConnection_Landroid_view_inputmethod_EditorInfo_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.Input.ICarEditable __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.InputMethods.EditorInfo p0 = global::Java.Lang.Object.GetObject<global::Android.Views.InputMethods.EditorInfo> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.OnCreateInputConnection (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_;
		public unsafe global::Android.Views.InputMethods.IInputConnection OnCreateInputConnection (global::Android.Views.InputMethods.EditorInfo p0)
		{
			if (id_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_ == IntPtr.Zero)
				id_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_ = JNIEnv.GetMethodID (class_ref, "onCreateInputConnection", "(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			global::Android.Views.InputMethods.IInputConnection __ret = global::Java.Lang.Object.GetObject<global::Android.Views.InputMethods.IInputConnection> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onCreateInputConnection_Landroid_view_inputmethod_EditorInfo_, __args), JniHandleOwnership.TransferLocalRef);
			return __ret;
		}

		static Delegate cb_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_;
#pragma warning disable 0169
		static Delegate GetSetCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_Handler ()
		{
			if (cb_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_ == null)
				cb_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_);
			return cb_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_;
		}

		static void n_SetCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.Input.ICarEditable __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.Input.ICarEditableListener p0 = (global::Com.Google.Android.Gms.Car.Input.ICarEditableListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditableListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetCarEditableListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_;
		public unsafe void SetCarEditableListener (global::Com.Google.Android.Gms.Car.Input.ICarEditableListener p0)
		{
			if (id_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_ == IntPtr.Zero)
				id_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_ = JNIEnv.GetMethodID (class_ref, "setCarEditableListener", "(Lcom/google/android/gms/car/input/CarEditableListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setCarEditableListener_Lcom_google_android_gms_car_input_CarEditableListener_, __args);
		}

		static Delegate cb_setInputEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetInputEnabled_ZHandler ()
		{
			if (cb_setInputEnabled_Z == null)
				cb_setInputEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetInputEnabled_Z);
			return cb_setInputEnabled_Z;
		}

		static void n_SetInputEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Gms.Car.Input.ICarEditable __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetInputEnabled (p0);
		}
#pragma warning restore 0169

		IntPtr id_setInputEnabled_Z;
		public unsafe void SetInputEnabled (bool p0)
		{
			if (id_setInputEnabled_Z == IntPtr.Zero)
				id_setInputEnabled_Z = JNIEnv.GetMethodID (class_ref, "setInputEnabled", "(Z)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setInputEnabled_Z, __args);
		}

	}

}
