using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']"
	[global::Android.Runtime.Register ("android/support/car/CarInfoManager", DoNotGenerateAcw=true)]
	public abstract partial class CarInfoManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/field[@name='DRIVER_SIDE_CENTER']"
		[Register ("DRIVER_SIDE_CENTER")]
		public const int DriverSideCenter = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/field[@name='DRIVER_SIDE_LEFT']"
		[Register ("DRIVER_SIDE_LEFT")]
		public const int DriverSideLeft = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/field[@name='DRIVER_SIDE_RIGHT']"
		[Register ("DRIVER_SIDE_RIGHT")]
		public const int DriverSideRight = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/field[@name='DRIVER_SIDE_UNKNOWN']"
		[Register ("DRIVER_SIDE_UNKNOWN")]
		public const int DriverSideUnknown = (int) 0;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarInfoManager.DriverSide']"
		[Register ("android/support/car/CarInfoManager$DriverSide", "", "Android.Support.Car.CarInfoManager/IDriverSideInvoker")]
		public partial interface IDriverSide : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/CarInfoManager$DriverSide", DoNotGenerateAcw=true)]
		internal class IDriverSideInvoker : global::Java.Lang.Object, IDriverSide {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarInfoManager$DriverSide");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IDriverSideInvoker); }
			}

			IntPtr class_ref;

			public static IDriverSide GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IDriverSide> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarInfoManager.DriverSide"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IDriverSideInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarInfoManager.IDriverSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager.IDriverSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.CarInfoManager.IDriverSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager.IDriverSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarInfoManager.IDriverSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager.IDriverSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarInfoManager.IDriverSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager.IDriverSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarInfoManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInfoManager); }
		}

		protected CarInfoManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/constructor[@name='CarInfoManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarInfoManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarInfoManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getDriverPosition;
#pragma warning disable 0169
		static Delegate GetGetDriverPositionHandler ()
		{
			if (cb_getDriverPosition == null)
				cb_getDriverPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetDriverPosition);
			return cb_getDriverPosition;
		}

		static int n_GetDriverPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DriverPosition;
		}
#pragma warning restore 0169

		public abstract int DriverPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getDriverPosition' and count(parameter)=0]"
			[Register ("getDriverPosition", "()I", "GetGetDriverPositionHandler")] get;
		}

		static Delegate cb_getHeadunitManufacturer;
#pragma warning disable 0169
		static Delegate GetGetHeadunitManufacturerHandler ()
		{
			if (cb_getHeadunitManufacturer == null)
				cb_getHeadunitManufacturer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitManufacturer);
			return cb_getHeadunitManufacturer;
		}

		static IntPtr n_GetHeadunitManufacturer (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitManufacturer);
		}
#pragma warning restore 0169

		public abstract string HeadunitManufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitManufacturer' and count(parameter)=0]"
			[Register ("getHeadunitManufacturer", "()Ljava/lang/String;", "GetGetHeadunitManufacturerHandler")] get;
		}

		static Delegate cb_getHeadunitModel;
#pragma warning disable 0169
		static Delegate GetGetHeadunitModelHandler ()
		{
			if (cb_getHeadunitModel == null)
				cb_getHeadunitModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitModel);
			return cb_getHeadunitModel;
		}

		static IntPtr n_GetHeadunitModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitModel);
		}
#pragma warning restore 0169

		public abstract string HeadunitModel {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitModel' and count(parameter)=0]"
			[Register ("getHeadunitModel", "()Ljava/lang/String;", "GetGetHeadunitModelHandler")] get;
		}

		static Delegate cb_getHeadunitSoftwareBuild;
#pragma warning disable 0169
		static Delegate GetGetHeadunitSoftwareBuildHandler ()
		{
			if (cb_getHeadunitSoftwareBuild == null)
				cb_getHeadunitSoftwareBuild = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitSoftwareBuild);
			return cb_getHeadunitSoftwareBuild;
		}

		static IntPtr n_GetHeadunitSoftwareBuild (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitSoftwareBuild);
		}
#pragma warning restore 0169

		public abstract string HeadunitSoftwareBuild {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitSoftwareBuild' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareBuild", "()Ljava/lang/String;", "GetGetHeadunitSoftwareBuildHandler")] get;
		}

		static Delegate cb_getHeadunitSoftwareVersion;
#pragma warning disable 0169
		static Delegate GetGetHeadunitSoftwareVersionHandler ()
		{
			if (cb_getHeadunitSoftwareVersion == null)
				cb_getHeadunitSoftwareVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitSoftwareVersion);
			return cb_getHeadunitSoftwareVersion;
		}

		static IntPtr n_GetHeadunitSoftwareVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitSoftwareVersion);
		}
#pragma warning restore 0169

		public abstract string HeadunitSoftwareVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitSoftwareVersion' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareVersion", "()Ljava/lang/String;", "GetGetHeadunitSoftwareVersionHandler")] get;
		}

		static Delegate cb_getManufacturer;
#pragma warning disable 0169
		static Delegate GetGetManufacturerHandler ()
		{
			if (cb_getManufacturer == null)
				cb_getManufacturer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetManufacturer);
			return cb_getManufacturer;
		}

		static IntPtr n_GetManufacturer (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Manufacturer);
		}
#pragma warning restore 0169

		public abstract string Manufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getManufacturer' and count(parameter)=0]"
			[Register ("getManufacturer", "()Ljava/lang/String;", "GetGetManufacturerHandler")] get;
		}

		static Delegate cb_getModel;
#pragma warning disable 0169
		static Delegate GetGetModelHandler ()
		{
			if (cb_getModel == null)
				cb_getModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModel);
			return cb_getModel;
		}

		static IntPtr n_GetModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Model);
		}
#pragma warning restore 0169

		public abstract string Model {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getModel' and count(parameter)=0]"
			[Register ("getModel", "()Ljava/lang/String;", "GetGetModelHandler")] get;
		}

		static Delegate cb_getModelYear;
#pragma warning disable 0169
		static Delegate GetGetModelYearHandler ()
		{
			if (cb_getModelYear == null)
				cb_getModelYear = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModelYear);
			return cb_getModelYear;
		}

		static IntPtr n_GetModelYear (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ModelYear);
		}
#pragma warning restore 0169

		public abstract string ModelYear {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getModelYear' and count(parameter)=0]"
			[Register ("getModelYear", "()Ljava/lang/String;", "GetGetModelYearHandler")] get;
		}

		static Delegate cb_getVehicleId;
#pragma warning disable 0169
		static Delegate GetGetVehicleIdHandler ()
		{
			if (cb_getVehicleId == null)
				cb_getVehicleId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVehicleId);
			return cb_getVehicleId;
		}

		static IntPtr n_GetVehicleId (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.VehicleId);
		}
#pragma warning restore 0169

		public abstract string VehicleId {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getVehicleId' and count(parameter)=0]"
			[Register ("getVehicleId", "()Ljava/lang/String;", "GetGetVehicleIdHandler")] get;
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/CarInfoManager", DoNotGenerateAcw=true)]
	internal partial class CarInfoManagerInvoker : CarInfoManager {

		public CarInfoManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInfoManagerInvoker); }
		}

		static IntPtr id_getDriverPosition;
		public override unsafe int DriverPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getDriverPosition' and count(parameter)=0]"
			[Register ("getDriverPosition", "()I", "GetGetDriverPositionHandler")]
			get {
				if (id_getDriverPosition == IntPtr.Zero)
					id_getDriverPosition = JNIEnv.GetMethodID (class_ref, "getDriverPosition", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getDriverPosition);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitManufacturer;
		public override unsafe string HeadunitManufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitManufacturer' and count(parameter)=0]"
			[Register ("getHeadunitManufacturer", "()Ljava/lang/String;", "GetGetHeadunitManufacturerHandler")]
			get {
				if (id_getHeadunitManufacturer == IntPtr.Zero)
					id_getHeadunitManufacturer = JNIEnv.GetMethodID (class_ref, "getHeadunitManufacturer", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitManufacturer), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitModel;
		public override unsafe string HeadunitModel {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitModel' and count(parameter)=0]"
			[Register ("getHeadunitModel", "()Ljava/lang/String;", "GetGetHeadunitModelHandler")]
			get {
				if (id_getHeadunitModel == IntPtr.Zero)
					id_getHeadunitModel = JNIEnv.GetMethodID (class_ref, "getHeadunitModel", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitModel), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitSoftwareBuild;
		public override unsafe string HeadunitSoftwareBuild {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitSoftwareBuild' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareBuild", "()Ljava/lang/String;", "GetGetHeadunitSoftwareBuildHandler")]
			get {
				if (id_getHeadunitSoftwareBuild == IntPtr.Zero)
					id_getHeadunitSoftwareBuild = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareBuild", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareBuild), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitSoftwareVersion;
		public override unsafe string HeadunitSoftwareVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getHeadunitSoftwareVersion' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareVersion", "()Ljava/lang/String;", "GetGetHeadunitSoftwareVersionHandler")]
			get {
				if (id_getHeadunitSoftwareVersion == IntPtr.Zero)
					id_getHeadunitSoftwareVersion = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareVersion", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareVersion), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getManufacturer;
		public override unsafe string Manufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getManufacturer' and count(parameter)=0]"
			[Register ("getManufacturer", "()Ljava/lang/String;", "GetGetManufacturerHandler")]
			get {
				if (id_getManufacturer == IntPtr.Zero)
					id_getManufacturer = JNIEnv.GetMethodID (class_ref, "getManufacturer", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getManufacturer), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getModel;
		public override unsafe string Model {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getModel' and count(parameter)=0]"
			[Register ("getModel", "()Ljava/lang/String;", "GetGetModelHandler")]
			get {
				if (id_getModel == IntPtr.Zero)
					id_getModel = JNIEnv.GetMethodID (class_ref, "getModel", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModel), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getModelYear;
		public override unsafe string ModelYear {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getModelYear' and count(parameter)=0]"
			[Register ("getModelYear", "()Ljava/lang/String;", "GetGetModelYearHandler")]
			get {
				if (id_getModelYear == IntPtr.Zero)
					id_getModelYear = JNIEnv.GetMethodID (class_ref, "getModelYear", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModelYear), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getVehicleId;
		public override unsafe string VehicleId {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManager']/method[@name='getVehicleId' and count(parameter)=0]"
			[Register ("getVehicleId", "()Ljava/lang/String;", "GetGetVehicleIdHandler")]
			get {
				if (id_getVehicleId == IntPtr.Zero)
					id_getVehicleId = JNIEnv.GetMethodID (class_ref, "getVehicleId", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getVehicleId), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
