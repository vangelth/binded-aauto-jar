using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/a/a", DoNotGenerateAcw=true)]
	public sealed partial class A : global::Android.Support.Car.Media.CarAudioManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/a/a", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (A); }
		}

		internal A (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_media_AudioManager_Lcom_google_android_gms_car_CarAudioManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/constructor[@name='a' and count(parameter)=2 and parameter[1][@type='android.media.AudioManager'] and parameter[2][@type='com.google.android.gms.car.CarAudioManager']]"
		[Register (".ctor", "(Landroid/media/AudioManager;Lcom/google/android/gms/car/CarAudioManager;)V", "")]
		public unsafe A (global::Android.Media.AudioManager p0, global::Com.Google.Android.Gms.Car.ICarAudioManager p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (A)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/media/AudioManager;Lcom/google/android/gms/car/CarAudioManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/media/AudioManager;Lcom/google/android/gms/car/CarAudioManager;)V", __args);
					return;
				}

				if (id_ctor_Landroid_media_AudioManager_Lcom_google_android_gms_car_CarAudioManager_ == IntPtr.Zero)
					id_ctor_Landroid_media_AudioManager_Lcom_google_android_gms_car_CarAudioManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/media/AudioManager;Lcom/google/android/gms/car/CarAudioManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_media_AudioManager_Lcom_google_android_gms_car_CarAudioManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_media_AudioManager_Lcom_google_android_gms_car_CarAudioManager_, __args);
			} finally {
			}
		}

		static IntPtr id_getAudioRecordAudioFormat;
		public override unsafe global::Android.Media.AudioFormat AudioRecordAudioFormat {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='getAudioRecordAudioFormat' and count(parameter)=0]"
			[Register ("getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;", "GetGetAudioRecordAudioFormatHandler")]
			get {
				if (id_getAudioRecordAudioFormat == IntPtr.Zero)
					id_getAudioRecordAudioFormat = JNIEnv.GetMethodID (class_ref, "getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;");
				try {
					return global::Java.Lang.Object.GetObject<global::Android.Media.AudioFormat> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordAudioFormat), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getAudioRecordMaxBufferSize;
		public override unsafe int AudioRecordMaxBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='getAudioRecordMaxBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMaxBufferSize", "()I", "GetGetAudioRecordMaxBufferSizeHandler")]
			get {
				if (id_getAudioRecordMaxBufferSize == IntPtr.Zero)
					id_getAudioRecordMaxBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMaxBufferSize", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMaxBufferSize);
				} finally {
				}
			}
		}

		static IntPtr id_getAudioRecordMinBufferSize;
		public override unsafe int AudioRecordMinBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='getAudioRecordMinBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMinBufferSize", "()I", "GetGetAudioRecordMinBufferSizeHandler")]
			get {
				if (id_getAudioRecordMinBufferSize == IntPtr.Zero)
					id_getAudioRecordMinBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMinBufferSize", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMinBufferSize);
				} finally {
				}
			}
		}

		static IntPtr id_isAudioRecordSupported;
		public override unsafe bool IsAudioRecordSupported {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='isAudioRecordSupported' and count(parameter)=0]"
			[Register ("isAudioRecordSupported", "()Z", "GetIsAudioRecordSupportedHandler")]
			get {
				if (id_isAudioRecordSupported == IntPtr.Zero)
					id_isAudioRecordSupported = JNIEnv.GetMethodID (class_ref, "isAudioRecordSupported", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isAudioRecordSupported);
				} finally {
				}
			}
		}

		static IntPtr id_isMediaMuted;
		public override unsafe bool IsMediaMuted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='isMediaMuted' and count(parameter)=0]"
			[Register ("isMediaMuted", "()Z", "GetIsMediaMutedHandler")]
			get {
				if (id_isMediaMuted == IntPtr.Zero)
					id_isMediaMuted = JNIEnv.GetMethodID (class_ref, "isMediaMuted", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isMediaMuted);
				} finally {
				}
			}
		}

		static IntPtr id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='abandonAudioFocus' and count(parameter)=2 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes']]"
		[Register ("abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V", "")]
		public override sealed unsafe void AbandonAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1)
		{
			if (id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ == IntPtr.Zero)
				id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ = JNIEnv.GetMethodID (class_ref, "abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_, __args);
			} finally {
			}
		}

		static IntPtr id_createCarAudioRecord_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='createCarAudioRecord' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;", "")]
		public override sealed unsafe global::Android.Support.Car.Media.CarAudioRecord CreateCarAudioRecord (int p0)
		{
			if (id_createCarAudioRecord_I == IntPtr.Zero)
				id_createCarAudioRecord_I = JNIEnv.GetMethodID (class_ref, "createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecord> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_createCarAudioRecord_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getAudioAttributesForCarUsage_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='getAudioAttributesForCarUsage' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;", "")]
		public override sealed unsafe global::Android.Media.AudioAttributes GetAudioAttributesForCarUsage (int p0)
		{
			if (id_getAudioAttributesForCarUsage_I == IntPtr.Zero)
				id_getAudioAttributesForCarUsage_I = JNIEnv.GetMethodID (class_ref, "getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioAttributesForCarUsage_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "")]
		public override sealed unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='requestAudioFocus' and count(parameter)=3 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I", "")]
		public override sealed unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='requestAudioFocus' and count(parameter)=4 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I", "")]
		public override sealed unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2, int p3)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_setMediaMute_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='a']/method[@name='setMediaMute' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setMediaMute", "(Z)Z", "")]
		public override sealed unsafe bool SetMediaMute (bool p0)
		{
			if (id_setMediaMute_Z == IntPtr.Zero)
				id_setMediaMute_Z = JNIEnv.GetMethodID (class_ref, "setMediaMute", "(Z)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_setMediaMute_Z, __args);
			} finally {
			}
		}

	}
}
