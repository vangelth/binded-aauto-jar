using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Hardware {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']"
	[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManager", DoNotGenerateAcw=true)]
	public abstract partial class CarSensorManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_RATE_FAST']"
		[Register ("SENSOR_RATE_FAST")]
		public const int SensorRateFast = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_RATE_FASTEST']"
		[Register ("SENSOR_RATE_FASTEST")]
		public const int SensorRateFastest = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_RATE_NORMAL']"
		[Register ("SENSOR_RATE_NORMAL")]
		public const int SensorRateNormal = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_RATE_UI']"
		[Register ("SENSOR_RATE_UI")]
		public const int SensorRateUi = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_ABS_ACTIVE']"
		[Register ("SENSOR_TYPE_ABS_ACTIVE")]
		public const int SensorTypeAbsActive = (int) 24;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_ACCELEROMETER']"
		[Register ("SENSOR_TYPE_ACCELEROMETER")]
		public const int SensorTypeAccelerometer = (int) 14;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_CAR_SPEED']"
		[Register ("SENSOR_TYPE_CAR_SPEED")]
		public const int SensorTypeCarSpeed = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_COMPASS']"
		[Register ("SENSOR_TYPE_COMPASS")]
		public const int SensorTypeCompass = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_DRIVING_STATUS']"
		[Register ("SENSOR_TYPE_DRIVING_STATUS")]
		public const int SensorTypeDrivingStatus = (int) 11;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_ENVIRONMENT']"
		[Register ("SENSOR_TYPE_ENVIRONMENT")]
		public const int SensorTypeEnvironment = (int) 12;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_FUEL_LEVEL']"
		[Register ("SENSOR_TYPE_FUEL_LEVEL")]
		public const int SensorTypeFuelLevel = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_GEAR']"
		[Register ("SENSOR_TYPE_GEAR")]
		public const int SensorTypeGear = (int) 7;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_GPS_SATELLITE']"
		[Register ("SENSOR_TYPE_GPS_SATELLITE")]
		public const int SensorTypeGpsSatellite = (int) 17;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_GYROSCOPE']"
		[Register ("SENSOR_TYPE_GYROSCOPE")]
		public const int SensorTypeGyroscope = (int) 18;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_LOCATION']"
		[Register ("SENSOR_TYPE_LOCATION")]
		public const int SensorTypeLocation = (int) 10;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_NIGHT']"
		[Register ("SENSOR_TYPE_NIGHT")]
		public const int SensorTypeNight = (int) 9;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_ODOMETER']"
		[Register ("SENSOR_TYPE_ODOMETER")]
		public const int SensorTypeOdometer = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_PARKING_BRAKE']"
		[Register ("SENSOR_TYPE_PARKING_BRAKE")]
		public const int SensorTypeParkingBrake = (int) 6;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED13']"
		[Register ("SENSOR_TYPE_RESERVED13")]
		public const int SensorTypeReserved13 = (int) 13;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED15']"
		[Register ("SENSOR_TYPE_RESERVED15")]
		public const int SensorTypeReserved15 = (int) 15;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED16']"
		[Register ("SENSOR_TYPE_RESERVED16")]
		public const int SensorTypeReserved16 = (int) 16;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED19']"
		[Register ("SENSOR_TYPE_RESERVED19")]
		public const int SensorTypeReserved19 = (int) 19;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED20']"
		[Register ("SENSOR_TYPE_RESERVED20")]
		public const int SensorTypeReserved20 = (int) 20;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED21']"
		[Register ("SENSOR_TYPE_RESERVED21")]
		public const int SensorTypeReserved21 = (int) 21;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED22']"
		[Register ("SENSOR_TYPE_RESERVED22")]
		public const int SensorTypeReserved22 = (int) 22;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RESERVED8']"
		[Register ("SENSOR_TYPE_RESERVED8")]
		public const int SensorTypeReserved8 = (int) 8;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_RPM']"
		[Register ("SENSOR_TYPE_RPM")]
		public const int SensorTypeRpm = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_TRACTION_CONTROL_ACTIVE']"
		[Register ("SENSOR_TYPE_TRACTION_CONTROL_ACTIVE")]
		public const int SensorTypeTractionControlActive = (int) 25;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_VENDOR_EXTENSION_END']"
		[Register ("SENSOR_TYPE_VENDOR_EXTENSION_END")]
		public const int SensorTypeVendorExtensionEnd = (int) 1879048191;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_VENDOR_EXTENSION_START']"
		[Register ("SENSOR_TYPE_VENDOR_EXTENSION_START")]
		public const int SensorTypeVendorExtensionStart = (int) 1610612736;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/field[@name='SENSOR_TYPE_WHEEL_TICK_DISTANCE']"
		[Register ("SENSOR_TYPE_WHEEL_TICK_DISTANCE")]
		public const int SensorTypeWheelTickDistance = (int) 23;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.hardware']/interface[@name='CarSensorManager.OnSensorChangedListener']"
		[Register ("android/support/car/hardware/CarSensorManager$OnSensorChangedListener", "", "Android.Support.Car.Hardware.CarSensorManager/IOnSensorChangedListenerInvoker")]
		public partial interface IOnSensorChangedListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/interface[@name='CarSensorManager.OnSensorChangedListener']/method[@name='onSensorChanged' and count(parameter)=2 and parameter[1][@type='android.support.car.hardware.CarSensorManager'] and parameter[2][@type='android.support.car.hardware.CarSensorEvent']]"
			[Register ("onSensorChanged", "(Landroid/support/car/hardware/CarSensorManager;Landroid/support/car/hardware/CarSensorEvent;)V", "GetOnSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_Handler:Android.Support.Car.Hardware.CarSensorManager/IOnSensorChangedListenerInvoker, AndroidAutoBindingJar")]
			void OnSensorChanged (global::Android.Support.Car.Hardware.CarSensorManager p0, global::Android.Support.Car.Hardware.CarSensorEvent p1);

		}

		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManager$OnSensorChangedListener", DoNotGenerateAcw=true)]
		internal class IOnSensorChangedListenerInvoker : global::Java.Lang.Object, IOnSensorChangedListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/hardware/CarSensorManager$OnSensorChangedListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnSensorChangedListenerInvoker); }
			}

			IntPtr class_ref;

			public static IOnSensorChangedListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnSensorChangedListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.hardware.CarSensorManager.OnSensorChangedListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnSensorChangedListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_;
#pragma warning disable 0169
			static Delegate GetOnSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_Handler ()
			{
				if (cb_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_ == null)
					cb_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_);
				return cb_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_;
			}

			static void n_OnSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.Hardware.CarSensorManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.Hardware.CarSensorEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
				__this.OnSensorChanged (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_;
			public unsafe void OnSensorChanged (global::Android.Support.Car.Hardware.CarSensorManager p0, global::Android.Support.Car.Hardware.CarSensorEvent p1)
			{
				if (id_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_ == IntPtr.Zero)
					id_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_ = JNIEnv.GetMethodID (class_ref, "onSensorChanged", "(Landroid/support/car/hardware/CarSensorManager;Landroid/support/car/hardware/CarSensorEvent;)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_, __args);
			}

		}

		public partial class SensorChangedEventArgs : global::System.EventArgs {

			public SensorChangedEventArgs (global::Android.Support.Car.Hardware.CarSensorManager p0, global::Android.Support.Car.Hardware.CarSensorEvent p1)
			{
				this.p0 = p0;
				this.p1 = p1;
			}

			global::Android.Support.Car.Hardware.CarSensorManager p0;
			public global::Android.Support.Car.Hardware.CarSensorManager P0 {
				get { return p0; }
			}

			global::Android.Support.Car.Hardware.CarSensorEvent p1;
			public global::Android.Support.Car.Hardware.CarSensorEvent P1 {
				get { return p1; }
			}
		}

		[global::Android.Runtime.Register ("mono/android/support/car/hardware/CarSensorManager_OnSensorChangedListenerImplementor")]
		internal sealed partial class IOnSensorChangedListenerImplementor : global::Java.Lang.Object, IOnSensorChangedListener {

			object sender;

			public IOnSensorChangedListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/android/support/car/hardware/CarSensorManager_OnSensorChangedListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<SensorChangedEventArgs> Handler;
#pragma warning restore 0649

			public void OnSensorChanged (global::Android.Support.Car.Hardware.CarSensorManager p0, global::Android.Support.Car.Hardware.CarSensorEvent p1)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new SensorChangedEventArgs (p0, p1));
			}

			internal static bool __IsEmpty (IOnSensorChangedListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.hardware']/interface[@name='CarSensorManager.SensorRate']"
		[Register ("android/support/car/hardware/CarSensorManager$SensorRate", "", "Android.Support.Car.Hardware.CarSensorManager/ISensorRateInvoker")]
		public partial interface ISensorRate : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManager$SensorRate", DoNotGenerateAcw=true)]
		internal class ISensorRateInvoker : global::Java.Lang.Object, ISensorRate {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/hardware/CarSensorManager$SensorRate");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ISensorRateInvoker); }
			}

			IntPtr class_ref;

			public static ISensorRate GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ISensorRate> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.hardware.CarSensorManager.SensorRate"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ISensorRateInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorRate> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.hardware']/interface[@name='CarSensorManager.SensorType']"
		[Register ("android/support/car/hardware/CarSensorManager$SensorType", "", "Android.Support.Car.Hardware.CarSensorManager/ISensorTypeInvoker")]
		public partial interface ISensorType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManager$SensorType", DoNotGenerateAcw=true)]
		internal class ISensorTypeInvoker : global::Java.Lang.Object, ISensorType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/hardware/CarSensorManager$SensorType");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ISensorTypeInvoker); }
			}

			IntPtr class_ref;

			public static ISensorType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ISensorType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.hardware.CarSensorManager.SensorType"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ISensorTypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorManager.ISensorType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.ISensorType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/hardware/CarSensorManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorManager); }
		}

		protected CarSensorManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/constructor[@name='CarSensorManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarSensorManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarSensorManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
#pragma warning disable 0169
		static Delegate GetAddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IIHandler ()
		{
			if (cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II == null)
				cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, bool>) n_AddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II);
			return cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
		}

		static bool n_AddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.AddListener (p0, p1, p2);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='addListener' and count(parameter)=3 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z", "GetAddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IIHandler")]
		public abstract bool AddListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1, int p2);

		static Delegate cb_getLatestSensorEvent_I;
#pragma warning disable 0169
		static Delegate GetGetLatestSensorEvent_IHandler ()
		{
			if (cb_getLatestSensorEvent_I == null)
				cb_getLatestSensorEvent_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetLatestSensorEvent_I);
			return cb_getLatestSensorEvent_I;
		}

		static IntPtr n_GetLatestSensorEvent_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLatestSensorEvent (p0));
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getLatestSensorEvent' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;", "GetGetLatestSensorEvent_IHandler")]
		public abstract global::Android.Support.Car.Hardware.CarSensorEvent GetLatestSensorEvent (int p0);

		static Delegate cb_getSensorConfig_I;
#pragma warning disable 0169
		static Delegate GetGetSensorConfig_IHandler ()
		{
			if (cb_getSensorConfig_I == null)
				cb_getSensorConfig_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetSensorConfig_I);
			return cb_getSensorConfig_I;
		}

		static IntPtr n_GetSensorConfig_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetSensorConfig (p0));
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getSensorConfig' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;", "GetGetSensorConfig_IHandler")]
		public abstract global::Android.Support.Car.Hardware.CarSensorConfig GetSensorConfig (int p0);

		static Delegate cb_getSupportedSensors;
#pragma warning disable 0169
		static Delegate GetGetSupportedSensorsHandler ()
		{
			if (cb_getSupportedSensors == null)
				cb_getSupportedSensors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSupportedSensors);
			return cb_getSupportedSensors;
		}

		static IntPtr n_GetSupportedSensors (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetSupportedSensors ());
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getSupportedSensors' and count(parameter)=0]"
		[Register ("getSupportedSensors", "()[I", "GetGetSupportedSensorsHandler")]
		public abstract int[] GetSupportedSensors ();

		static Delegate cb_isSensorSupported_I;
#pragma warning disable 0169
		static Delegate GetIsSensorSupported_IHandler ()
		{
			if (cb_isSensorSupported_I == null)
				cb_isSensorSupported_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_IsSensorSupported_I);
			return cb_isSensorSupported_I;
		}

		static bool n_IsSensorSupported_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsSensorSupported (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='isSensorSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isSensorSupported", "(I)Z", "GetIsSensorSupported_IHandler")]
		public abstract bool IsSensorSupported (int p0);

		static Delegate cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_Handler ()
		{
			if (cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ == null)
				cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_);
			return cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
		}

		static void n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_Handler")]
		public abstract void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0);

		static Delegate cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IHandler ()
		{
			if (cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I == null)
				cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I);
			return cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
		}

		static void n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='removeListener' and count(parameter)=2 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IHandler")]
		public abstract void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1);

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManager", DoNotGenerateAcw=true)]
	internal partial class CarSensorManagerInvoker : CarSensorManager {

		public CarSensorManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorManagerInvoker); }
		}

		static IntPtr id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='addListener' and count(parameter)=3 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z", "GetAddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IIHandler")]
		public override unsafe bool AddListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1, int p2)
		{
			if (id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II == IntPtr.Zero)
				id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_getLatestSensorEvent_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getLatestSensorEvent' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;", "GetGetLatestSensorEvent_IHandler")]
		public override unsafe global::Android.Support.Car.Hardware.CarSensorEvent GetLatestSensorEvent (int p0)
		{
			if (id_getLatestSensorEvent_I == IntPtr.Zero)
				id_getLatestSensorEvent_I = JNIEnv.GetMethodID (class_ref, "getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLatestSensorEvent_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getSensorConfig_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getSensorConfig' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;", "GetGetSensorConfig_IHandler")]
		public override unsafe global::Android.Support.Car.Hardware.CarSensorConfig GetSensorConfig (int p0)
		{
			if (id_getSensorConfig_I == IntPtr.Zero)
				id_getSensorConfig_I = JNIEnv.GetMethodID (class_ref, "getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSensorConfig_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getSupportedSensors;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='getSupportedSensors' and count(parameter)=0]"
		[Register ("getSupportedSensors", "()[I", "GetGetSupportedSensorsHandler")]
		public override unsafe int[] GetSupportedSensors ()
		{
			if (id_getSupportedSensors == IntPtr.Zero)
				id_getSupportedSensors = JNIEnv.GetMethodID (class_ref, "getSupportedSensors", "()[I");
			try {
				return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSupportedSensors), JniHandleOwnership.TransferLocalRef, typeof (int));
			} finally {
			}
		}

		static IntPtr id_isSensorSupported_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='isSensorSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isSensorSupported", "(I)Z", "GetIsSensorSupported_IHandler")]
		public override unsafe bool IsSensorSupported (int p0)
		{
			if (id_isSensorSupported_I == IntPtr.Zero)
				id_isSensorSupported_I = JNIEnv.GetMethodID (class_ref, "isSensorSupported", "(I)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isSensorSupported_I, __args);
			} finally {
			}
		}

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_Handler")]
		public override unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_, __args);
			} finally {
			}
		}

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManager']/method[@name='removeListener' and count(parameter)=2 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IHandler")]
		public override unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I, __args);
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
