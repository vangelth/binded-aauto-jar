using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.UI {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedListView", DoNotGenerateAcw=true)]
	public partial class PagedListView : global::Android.Widget.FrameLayout {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/field[@name='DEFAULT_MAX_CLICKS']"
		[Register ("DEFAULT_MAX_CLICKS")]
		public const int DefaultMaxClicks = (int) 6;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/field[@name='PAGINATION_HOLD_DELAY_MS']"
		[Register ("PAGINATION_HOLD_DELAY_MS")]
		protected const int PaginationHoldDelayMs = (int) 400;

		static IntPtr mHandler_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/field[@name='mHandler']"
		[Register ("mHandler")]
		protected global::Android.OS.Handler MHandler {
			get {
				if (mHandler_jfieldId == IntPtr.Zero)
					mHandler_jfieldId = JNIEnv.GetFieldID (class_ref, "mHandler", "Landroid/os/Handler;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mHandler_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.Handler> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mHandler_jfieldId == IntPtr.Zero)
					mHandler_jfieldId = JNIEnv.GetFieldID (class_ref, "mHandler", "Landroid/os/Handler;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mHandler_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr mOnScrollListener_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/field[@name='mOnScrollListener']"
		[Register ("mOnScrollListener")]
		protected global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener MOnScrollListener {
			get {
				if (mOnScrollListener_jfieldId == IntPtr.Zero)
					mOnScrollListener_jfieldId = JNIEnv.GetFieldID (class_ref, "mOnScrollListener", "Lcom/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mOnScrollListener_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mOnScrollListener_jfieldId == IntPtr.Zero)
					mOnScrollListener_jfieldId = JNIEnv.GetFieldID (class_ref, "mOnScrollListener", "Lcom/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mOnScrollListener_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr mPaginationRunnable_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/field[@name='mPaginationRunnable']"
		[Register ("mPaginationRunnable")]
		protected global::Java.Lang.IRunnable MPaginationRunnable {
			get {
				if (mPaginationRunnable_jfieldId == IntPtr.Zero)
					mPaginationRunnable_jfieldId = JNIEnv.GetFieldID (class_ref, "mPaginationRunnable", "Ljava/lang/Runnable;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mPaginationRunnable_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Java.Lang.IRunnable> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mPaginationRunnable_jfieldId == IntPtr.Zero)
					mPaginationRunnable_jfieldId = JNIEnv.GetFieldID (class_ref, "mPaginationRunnable", "Ljava/lang/Runnable;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mPaginationRunnable_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}
		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedListView.ItemCap']"
		[Register ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemCap", "", "Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView/IItemCapInvoker")]
		public partial interface IItemCap : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedListView.ItemCap']/method[@name='setMaxItems' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setMaxItems", "(I)V", "GetSetMaxItems_IHandler:Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView/IItemCapInvoker, AndroidAutoBindingJar")]
			void SetMaxItems (int p0);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemCap", DoNotGenerateAcw=true)]
		internal class IItemCapInvoker : global::Java.Lang.Object, IItemCap {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemCap");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IItemCapInvoker); }
			}

			IntPtr class_ref;

			public static IItemCap GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IItemCap> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.ui.PagedListView.ItemCap"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IItemCapInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_setMaxItems_I;
#pragma warning disable 0169
			static Delegate GetSetMaxItems_IHandler ()
			{
				if (cb_setMaxItems_I == null)
					cb_setMaxItems_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetMaxItems_I);
				return cb_setMaxItems_I;
			}

			static void n_SetMaxItems_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.IItemCap __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.IItemCap> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.SetMaxItems (p0);
			}
#pragma warning restore 0169

			IntPtr id_setMaxItems_I;
			public unsafe void SetMaxItems (int p0)
			{
				if (id_setMaxItems_I == IntPtr.Zero)
					id_setMaxItems_I = JNIEnv.GetMethodID (class_ref, "setMaxItems", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setMaxItems_I, __args);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedListView.ItemPositionOffset']"
		[Register ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemPositionOffset", "", "Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView/IItemPositionOffsetInvoker")]
		public partial interface IItemPositionOffset : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedListView.ItemPositionOffset']/method[@name='setPositionOffset' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setPositionOffset", "(I)V", "GetSetPositionOffset_IHandler:Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView/IItemPositionOffsetInvoker, AndroidAutoBindingJar")]
			void SetPositionOffset (int p0);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemPositionOffset", DoNotGenerateAcw=true)]
		internal class IItemPositionOffsetInvoker : global::Java.Lang.Object, IItemPositionOffset {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedListView$ItemPositionOffset");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IItemPositionOffsetInvoker); }
			}

			IntPtr class_ref;

			public static IItemPositionOffset GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IItemPositionOffset> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.ui.PagedListView.ItemPositionOffset"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IItemPositionOffsetInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_setPositionOffset_I;
#pragma warning disable 0169
			static Delegate GetSetPositionOffset_IHandler ()
			{
				if (cb_setPositionOffset_I == null)
					cb_setPositionOffset_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetPositionOffset_I);
				return cb_setPositionOffset_I;
			}

			static void n_SetPositionOffset_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.IItemPositionOffset __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.IItemPositionOffset> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.SetPositionOffset (p0);
			}
#pragma warning restore 0169

			IntPtr id_setPositionOffset_I;
			public unsafe void SetPositionOffset (int p0)
			{
				if (id_setPositionOffset_I == IntPtr.Zero)
					id_setPositionOffset_I = JNIEnv.GetMethodID (class_ref, "setPositionOffset", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setPositionOffset_I, __args);
			}

		}


		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener", DoNotGenerateAcw=true)]
		public partial class OnScrollListener : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (OnScrollListener); }
			}

			protected OnScrollListener (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/constructor[@name='PagedListView.OnScrollListener' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe OnScrollListener ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (OnScrollListener)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_onGestureDown;
#pragma warning disable 0169
			static Delegate GetOnGestureDownHandler ()
			{
				if (cb_onGestureDown == null)
					cb_onGestureDown = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnGestureDown);
				return cb_onGestureDown;
			}

			static void n_OnGestureDown (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnGestureDown ();
			}
#pragma warning restore 0169

			static IntPtr id_onGestureDown;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onGestureDown' and count(parameter)=0]"
			[Register ("onGestureDown", "()V", "GetOnGestureDownHandler")]
			public virtual unsafe void OnGestureDown ()
			{
				if (id_onGestureDown == IntPtr.Zero)
					id_onGestureDown = JNIEnv.GetMethodID (class_ref, "onGestureDown", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onGestureDown);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onGestureDown", "()V"));
				} finally {
				}
			}

			static Delegate cb_onGestureUp;
#pragma warning disable 0169
			static Delegate GetOnGestureUpHandler ()
			{
				if (cb_onGestureUp == null)
					cb_onGestureUp = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnGestureUp);
				return cb_onGestureUp;
			}

			static void n_OnGestureUp (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnGestureUp ();
			}
#pragma warning restore 0169

			static IntPtr id_onGestureUp;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onGestureUp' and count(parameter)=0]"
			[Register ("onGestureUp", "()V", "GetOnGestureUpHandler")]
			public virtual unsafe void OnGestureUp ()
			{
				if (id_onGestureUp == IntPtr.Zero)
					id_onGestureUp = JNIEnv.GetMethodID (class_ref, "onGestureUp", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onGestureUp);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onGestureUp", "()V"));
				} finally {
				}
			}

			static Delegate cb_onLeaveBottom;
#pragma warning disable 0169
			static Delegate GetOnLeaveBottomHandler ()
			{
				if (cb_onLeaveBottom == null)
					cb_onLeaveBottom = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnLeaveBottom);
				return cb_onLeaveBottom;
			}

			static void n_OnLeaveBottom (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnLeaveBottom ();
			}
#pragma warning restore 0169

			static IntPtr id_onLeaveBottom;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onLeaveBottom' and count(parameter)=0]"
			[Register ("onLeaveBottom", "()V", "GetOnLeaveBottomHandler")]
			public virtual unsafe void OnLeaveBottom ()
			{
				if (id_onLeaveBottom == IntPtr.Zero)
					id_onLeaveBottom = JNIEnv.GetMethodID (class_ref, "onLeaveBottom", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onLeaveBottom);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onLeaveBottom", "()V"));
				} finally {
				}
			}

			static Delegate cb_onPageDown;
#pragma warning disable 0169
			static Delegate GetOnPageDownHandler ()
			{
				if (cb_onPageDown == null)
					cb_onPageDown = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPageDown);
				return cb_onPageDown;
			}

			static void n_OnPageDown (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnPageDown ();
			}
#pragma warning restore 0169

			static IntPtr id_onPageDown;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onPageDown' and count(parameter)=0]"
			[Register ("onPageDown", "()V", "GetOnPageDownHandler")]
			public virtual unsafe void OnPageDown ()
			{
				if (id_onPageDown == IntPtr.Zero)
					id_onPageDown = JNIEnv.GetMethodID (class_ref, "onPageDown", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPageDown);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPageDown", "()V"));
				} finally {
				}
			}

			static Delegate cb_onPageUp;
#pragma warning disable 0169
			static Delegate GetOnPageUpHandler ()
			{
				if (cb_onPageUp == null)
					cb_onPageUp = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPageUp);
				return cb_onPageUp;
			}

			static void n_OnPageUp (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnPageUp ();
			}
#pragma warning restore 0169

			static IntPtr id_onPageUp;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onPageUp' and count(parameter)=0]"
			[Register ("onPageUp", "()V", "GetOnPageUpHandler")]
			public virtual unsafe void OnPageUp ()
			{
				if (id_onPageUp == IntPtr.Zero)
					id_onPageUp = JNIEnv.GetMethodID (class_ref, "onPageUp", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPageUp);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPageUp", "()V"));
				} finally {
				}
			}

			static Delegate cb_onReachBottom;
#pragma warning disable 0169
			static Delegate GetOnReachBottomHandler ()
			{
				if (cb_onReachBottom == null)
					cb_onReachBottom = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnReachBottom);
				return cb_onReachBottom;
			}

			static void n_OnReachBottom (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnReachBottom ();
			}
#pragma warning restore 0169

			static IntPtr id_onReachBottom;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onReachBottom' and count(parameter)=0]"
			[Register ("onReachBottom", "()V", "GetOnReachBottomHandler")]
			public virtual unsafe void OnReachBottom ()
			{
				if (id_onReachBottom == IntPtr.Zero)
					id_onReachBottom = JNIEnv.GetMethodID (class_ref, "onReachBottom", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onReachBottom);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onReachBottom", "()V"));
				} finally {
				}
			}

			static Delegate cb_onScrollDownButtonClicked;
#pragma warning disable 0169
			static Delegate GetOnScrollDownButtonClickedHandler ()
			{
				if (cb_onScrollDownButtonClicked == null)
					cb_onScrollDownButtonClicked = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnScrollDownButtonClicked);
				return cb_onScrollDownButtonClicked;
			}

			static void n_OnScrollDownButtonClicked (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnScrollDownButtonClicked ();
			}
#pragma warning restore 0169

			static IntPtr id_onScrollDownButtonClicked;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onScrollDownButtonClicked' and count(parameter)=0]"
			[Register ("onScrollDownButtonClicked", "()V", "GetOnScrollDownButtonClickedHandler")]
			public virtual unsafe void OnScrollDownButtonClicked ()
			{
				if (id_onScrollDownButtonClicked == IntPtr.Zero)
					id_onScrollDownButtonClicked = JNIEnv.GetMethodID (class_ref, "onScrollDownButtonClicked", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onScrollDownButtonClicked);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onScrollDownButtonClicked", "()V"));
				} finally {
				}
			}

			static Delegate cb_onScrollUpButtonClicked;
#pragma warning disable 0169
			static Delegate GetOnScrollUpButtonClickedHandler ()
			{
				if (cb_onScrollUpButtonClicked == null)
					cb_onScrollUpButtonClicked = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnScrollUpButtonClicked);
				return cb_onScrollUpButtonClicked;
			}

			static void n_OnScrollUpButtonClicked (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnScrollUpButtonClicked ();
			}
#pragma warning restore 0169

			static IntPtr id_onScrollUpButtonClicked;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView.OnScrollListener']/method[@name='onScrollUpButtonClicked' and count(parameter)=0]"
			[Register ("onScrollUpButtonClicked", "()V", "GetOnScrollUpButtonClickedHandler")]
			public virtual unsafe void OnScrollUpButtonClicked ()
			{
				if (id_onScrollUpButtonClicked == IntPtr.Zero)
					id_onScrollUpButtonClicked = JNIEnv.GetMethodID (class_ref, "onScrollUpButtonClicked", "()V");
				try {

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onScrollUpButtonClicked);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onScrollUpButtonClicked", "()V"));
				} finally {
				}
			}

		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedListView", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PagedListView); }
		}

		protected PagedListView (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/constructor[@name='PagedListView' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "")]
		public unsafe PagedListView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (PagedListView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/constructor[@name='PagedListView' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "")]
		public unsafe PagedListView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (PagedListView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/constructor[@name='PagedListView' and count(parameter)=4 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", "")]
		public unsafe PagedListView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2, int p3)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				if (((object) this).GetType () != typeof (PagedListView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_III;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/constructor[@name='PagedListView' and count(parameter)=5 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;III)V", "")]
		public unsafe PagedListView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2, int p3, int p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				if (((object) this).GetType () != typeof (PagedListView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;III)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;III)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_III == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_III = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;III)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_III, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_III, __args);
			} finally {
			}
		}

		static Delegate cb_getDefaultMaxPages;
#pragma warning disable 0169
		static Delegate GetGetDefaultMaxPagesHandler ()
		{
			if (cb_getDefaultMaxPages == null)
				cb_getDefaultMaxPages = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetDefaultMaxPages);
			return cb_getDefaultMaxPages;
		}

		static int n_GetDefaultMaxPages (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DefaultMaxPages;
		}
#pragma warning restore 0169

		static IntPtr id_getDefaultMaxPages;
		protected virtual unsafe int DefaultMaxPages {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getDefaultMaxPages' and count(parameter)=0]"
			[Register ("getDefaultMaxPages", "()I", "GetGetDefaultMaxPagesHandler")]
			get {
				if (id_getDefaultMaxPages == IntPtr.Zero)
					id_getDefaultMaxPages = JNIEnv.GetMethodID (class_ref, "getDefaultMaxPages", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getDefaultMaxPages);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDefaultMaxPages", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getFirstFullyVisibleChildPosition;
#pragma warning disable 0169
		static Delegate GetGetFirstFullyVisibleChildPositionHandler ()
		{
			if (cb_getFirstFullyVisibleChildPosition == null)
				cb_getFirstFullyVisibleChildPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetFirstFullyVisibleChildPosition);
			return cb_getFirstFullyVisibleChildPosition;
		}

		static int n_GetFirstFullyVisibleChildPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.FirstFullyVisibleChildPosition;
		}
#pragma warning restore 0169

		static IntPtr id_getFirstFullyVisibleChildPosition;
		public virtual unsafe int FirstFullyVisibleChildPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getFirstFullyVisibleChildPosition' and count(parameter)=0]"
			[Register ("getFirstFullyVisibleChildPosition", "()I", "GetGetFirstFullyVisibleChildPositionHandler")]
			get {
				if (id_getFirstFullyVisibleChildPosition == IntPtr.Zero)
					id_getFirstFullyVisibleChildPosition = JNIEnv.GetMethodID (class_ref, "getFirstFullyVisibleChildPosition", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getFirstFullyVisibleChildPosition);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFirstFullyVisibleChildPosition", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getLastFullyVisibleChildPosition;
#pragma warning disable 0169
		static Delegate GetGetLastFullyVisibleChildPositionHandler ()
		{
			if (cb_getLastFullyVisibleChildPosition == null)
				cb_getLastFullyVisibleChildPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetLastFullyVisibleChildPosition);
			return cb_getLastFullyVisibleChildPosition;
		}

		static int n_GetLastFullyVisibleChildPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.LastFullyVisibleChildPosition;
		}
#pragma warning restore 0169

		static IntPtr id_getLastFullyVisibleChildPosition;
		public virtual unsafe int LastFullyVisibleChildPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getLastFullyVisibleChildPosition' and count(parameter)=0]"
			[Register ("getLastFullyVisibleChildPosition", "()I", "GetGetLastFullyVisibleChildPositionHandler")]
			get {
				if (id_getLastFullyVisibleChildPosition == IntPtr.Zero)
					id_getLastFullyVisibleChildPosition = JNIEnv.GetMethodID (class_ref, "getLastFullyVisibleChildPosition", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getLastFullyVisibleChildPosition);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLastFullyVisibleChildPosition", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getMaxPages;
#pragma warning disable 0169
		static Delegate GetGetMaxPagesHandler ()
		{
			if (cb_getMaxPages == null)
				cb_getMaxPages = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMaxPages);
			return cb_getMaxPages;
		}

		static int n_GetMaxPages (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MaxPages;
		}
#pragma warning restore 0169

		static Delegate cb_setMaxPages_I;
#pragma warning disable 0169
		static Delegate GetSetMaxPages_IHandler ()
		{
			if (cb_setMaxPages_I == null)
				cb_setMaxPages_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetMaxPages_I);
			return cb_setMaxPages_I;
		}

		static void n_SetMaxPages_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.MaxPages = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMaxPages;
		static IntPtr id_setMaxPages_I;
		public virtual unsafe int MaxPages {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getMaxPages' and count(parameter)=0]"
			[Register ("getMaxPages", "()I", "GetGetMaxPagesHandler")]
			get {
				if (id_getMaxPages == IntPtr.Zero)
					id_getMaxPages = JNIEnv.GetMethodID (class_ref, "getMaxPages", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMaxPages);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMaxPages", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setMaxPages' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setMaxPages", "(I)V", "GetSetMaxPages_IHandler")]
			set {
				if (id_setMaxPages_I == IntPtr.Zero)
					id_setMaxPages_I = JNIEnv.GetMethodID (class_ref, "setMaxPages", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setMaxPages_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMaxPages", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getRowsPerPage;
#pragma warning disable 0169
		static Delegate GetGetRowsPerPageHandler ()
		{
			if (cb_getRowsPerPage == null)
				cb_getRowsPerPage = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetRowsPerPage);
			return cb_getRowsPerPage;
		}

		static int n_GetRowsPerPage (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.RowsPerPage;
		}
#pragma warning restore 0169

		static IntPtr id_getRowsPerPage;
		public virtual unsafe int RowsPerPage {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getRowsPerPage' and count(parameter)=0]"
			[Register ("getRowsPerPage", "()I", "GetGetRowsPerPageHandler")]
			get {
				if (id_getRowsPerPage == IntPtr.Zero)
					id_getRowsPerPage = JNIEnv.GetMethodID (class_ref, "getRowsPerPage", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getRowsPerPage);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRowsPerPage", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_calculateMaxItemCount;
#pragma warning disable 0169
		static Delegate GetCalculateMaxItemCountHandler ()
		{
			if (cb_calculateMaxItemCount == null)
				cb_calculateMaxItemCount = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_CalculateMaxItemCount);
			return cb_calculateMaxItemCount;
		}

		static int n_CalculateMaxItemCount (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CalculateMaxItemCount ();
		}
#pragma warning restore 0169

		static IntPtr id_calculateMaxItemCount;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='calculateMaxItemCount' and count(parameter)=0]"
		[Register ("calculateMaxItemCount", "()I", "GetCalculateMaxItemCountHandler")]
		protected virtual unsafe int CalculateMaxItemCount ()
		{
			if (id_calculateMaxItemCount == IntPtr.Zero)
				id_calculateMaxItemCount = JNIEnv.GetMethodID (class_ref, "calculateMaxItemCount", "()I");
			try {

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_calculateMaxItemCount);
				else
					return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "calculateMaxItemCount", "()I"));
			} finally {
			}
		}

		static Delegate cb_findViewByPosition_I;
#pragma warning disable 0169
		static Delegate GetFindViewByPosition_IHandler ()
		{
			if (cb_findViewByPosition_I == null)
				cb_findViewByPosition_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_FindViewByPosition_I);
			return cb_findViewByPosition_I;
		}

		static IntPtr n_FindViewByPosition_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FindViewByPosition (p0));
		}
#pragma warning restore 0169

		static IntPtr id_findViewByPosition_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='findViewByPosition' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("findViewByPosition", "(I)Landroid/view/View;", "GetFindViewByPosition_IHandler")]
		public virtual unsafe global::Android.Views.View FindViewByPosition (int p0)
		{
			if (id_findViewByPosition_I == IntPtr.Zero)
				id_findViewByPosition_I = JNIEnv.GetMethodID (class_ref, "findViewByPosition", "(I)Landroid/view/View;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_findViewByPosition_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "findViewByPosition", "(I)Landroid/view/View;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getPage_I;
#pragma warning disable 0169
		static Delegate GetGetPage_IHandler ()
		{
			if (cb_getPage_I == null)
				cb_getPage_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int>) n_GetPage_I);
			return cb_getPage_I;
		}

		static int n_GetPage_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.GetPage (p0);
		}
#pragma warning restore 0169

		static IntPtr id_getPage_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='getPage' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getPage", "(I)I", "GetGetPage_IHandler")]
		public virtual unsafe int GetPage (int p0)
		{
			if (id_getPage_I == IntPtr.Zero)
				id_getPage_I = JNIEnv.GetMethodID (class_ref, "getPage", "(I)I");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getPage_I, __args);
				else
					return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPage", "(I)I"), __args);
			} finally {
			}
		}

		static Delegate cb_onLayout_ZIIII;
#pragma warning disable 0169
		static Delegate GetOnLayout_ZIIIIHandler ()
		{
			if (cb_onLayout_ZIIII == null)
				cb_onLayout_ZIIII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool, int, int, int, int>) n_OnLayout_ZIIII);
			return cb_onLayout_ZIIII;
		}

		static void n_OnLayout_ZIIII (IntPtr jnienv, IntPtr native__this, bool p0, int p1, int p2, int p3, int p4)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnLayout (p0, p1, p2, p3, p4);
		}
#pragma warning restore 0169

		static IntPtr id_onLayout_ZIIII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='onLayout' and count(parameter)=5 and parameter[1][@type='boolean'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("onLayout", "(ZIIII)V", "GetOnLayout_ZIIIIHandler")]
		protected override unsafe void OnLayout (bool p0, int p1, int p2, int p3, int p4)
		{
			if (id_onLayout_ZIIII == IntPtr.Zero)
				id_onLayout_ZIIII = JNIEnv.GetMethodID (class_ref, "onLayout", "(ZIIII)V");
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onLayout_ZIIII, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onLayout", "(ZIIII)V"), __args);
			} finally {
			}
		}

		static Delegate cb_positionOf_Landroid_view_View_;
#pragma warning disable 0169
		static Delegate GetPositionOf_Landroid_view_View_Handler ()
		{
			if (cb_positionOf_Landroid_view_View_ == null)
				cb_positionOf_Landroid_view_View_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int>) n_PositionOf_Landroid_view_View_);
			return cb_positionOf_Landroid_view_View_;
		}

		static int n_PositionOf_Landroid_view_View_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.PositionOf (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_positionOf_Landroid_view_View_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='positionOf' and count(parameter)=1 and parameter[1][@type='android.view.View']]"
		[Register ("positionOf", "(Landroid/view/View;)I", "GetPositionOf_Landroid_view_View_Handler")]
		public virtual unsafe int PositionOf (global::Android.Views.View p0)
		{
			if (id_positionOf_Landroid_view_View_ == IntPtr.Zero)
				id_positionOf_Landroid_view_View_ = JNIEnv.GetMethodID (class_ref, "positionOf", "(Landroid/view/View;)I");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_positionOf_Landroid_view_View_, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "positionOf", "(Landroid/view/View;)I"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_removeDefaultItemDecoration;
#pragma warning disable 0169
		static Delegate GetRemoveDefaultItemDecorationHandler ()
		{
			if (cb_removeDefaultItemDecoration == null)
				cb_removeDefaultItemDecoration = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_RemoveDefaultItemDecoration);
			return cb_removeDefaultItemDecoration;
		}

		static void n_RemoveDefaultItemDecoration (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RemoveDefaultItemDecoration ();
		}
#pragma warning restore 0169

		static IntPtr id_removeDefaultItemDecoration;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='removeDefaultItemDecoration' and count(parameter)=0]"
		[Register ("removeDefaultItemDecoration", "()V", "GetRemoveDefaultItemDecorationHandler")]
		public virtual unsafe void RemoveDefaultItemDecoration ()
		{
			if (id_removeDefaultItemDecoration == IntPtr.Zero)
				id_removeDefaultItemDecoration = JNIEnv.GetMethodID (class_ref, "removeDefaultItemDecoration", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeDefaultItemDecoration);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeDefaultItemDecoration", "()V"));
			} finally {
			}
		}

		static Delegate cb_resetMaxPages;
#pragma warning disable 0169
		static Delegate GetResetMaxPagesHandler ()
		{
			if (cb_resetMaxPages == null)
				cb_resetMaxPages = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ResetMaxPages);
			return cb_resetMaxPages;
		}

		static void n_ResetMaxPages (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ResetMaxPages ();
		}
#pragma warning restore 0169

		static IntPtr id_resetMaxPages;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='resetMaxPages' and count(parameter)=0]"
		[Register ("resetMaxPages", "()V", "GetResetMaxPagesHandler")]
		public virtual unsafe void ResetMaxPages ()
		{
			if (id_resetMaxPages == IntPtr.Zero)
				id_resetMaxPages = JNIEnv.GetMethodID (class_ref, "resetMaxPages", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_resetMaxPages);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "resetMaxPages", "()V"));
			} finally {
			}
		}

		static Delegate cb_scrollToPosition_I;
#pragma warning disable 0169
		static Delegate GetScrollToPosition_IHandler ()
		{
			if (cb_scrollToPosition_I == null)
				cb_scrollToPosition_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_ScrollToPosition_I);
			return cb_scrollToPosition_I;
		}

		static void n_ScrollToPosition_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ScrollToPosition (p0);
		}
#pragma warning restore 0169

		static IntPtr id_scrollToPosition_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='scrollToPosition' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("scrollToPosition", "(I)V", "GetScrollToPosition_IHandler")]
		public virtual unsafe void ScrollToPosition (int p0)
		{
			if (id_scrollToPosition_I == IntPtr.Zero)
				id_scrollToPosition_I = JNIEnv.GetMethodID (class_ref, "scrollToPosition", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_scrollToPosition_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "scrollToPosition", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAutoDayNightMode;
#pragma warning disable 0169
		static Delegate GetSetAutoDayNightModeHandler ()
		{
			if (cb_setAutoDayNightMode == null)
				cb_setAutoDayNightMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetAutoDayNightMode);
			return cb_setAutoDayNightMode;
		}

		static void n_SetAutoDayNightMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetAutoDayNightMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setAutoDayNightMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setAutoDayNightMode' and count(parameter)=0]"
		[Register ("setAutoDayNightMode", "()V", "GetSetAutoDayNightModeHandler")]
		public virtual unsafe void SetAutoDayNightMode ()
		{
			if (id_setAutoDayNightMode == IntPtr.Zero)
				id_setAutoDayNightMode = JNIEnv.GetMethodID (class_ref, "setAutoDayNightMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setAutoDayNightMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAutoDayNightMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setDarkMode;
#pragma warning disable 0169
		static Delegate GetSetDarkModeHandler ()
		{
			if (cb_setDarkMode == null)
				cb_setDarkMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetDarkMode);
			return cb_setDarkMode;
		}

		static void n_SetDarkMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDarkMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setDarkMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setDarkMode' and count(parameter)=0]"
		[Register ("setDarkMode", "()V", "GetSetDarkModeHandler")]
		public virtual unsafe void SetDarkMode ()
		{
			if (id_setDarkMode == IntPtr.Zero)
				id_setDarkMode = JNIEnv.GetMethodID (class_ref, "setDarkMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDarkMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDarkMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setDayNightStyle_I;
#pragma warning disable 0169
		static Delegate GetSetDayNightStyle_IHandler ()
		{
			if (cb_setDayNightStyle_I == null)
				cb_setDayNightStyle_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetDayNightStyle_I);
			return cb_setDayNightStyle_I;
		}

		static void n_SetDayNightStyle_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDayNightStyle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setDayNightStyle_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setDayNightStyle' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setDayNightStyle", "(I)V", "GetSetDayNightStyle_IHandler")]
		public virtual unsafe void SetDayNightStyle (int p0)
		{
			if (id_setDayNightStyle_I == IntPtr.Zero)
				id_setDayNightStyle_I = JNIEnv.GetMethodID (class_ref, "setDayNightStyle", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDayNightStyle_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDayNightStyle", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setDefaultMaxPages_I;
#pragma warning disable 0169
		static Delegate GetSetDefaultMaxPages_IHandler ()
		{
			if (cb_setDefaultMaxPages_I == null)
				cb_setDefaultMaxPages_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetDefaultMaxPages_I);
			return cb_setDefaultMaxPages_I;
		}

		static void n_SetDefaultMaxPages_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDefaultMaxPages (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setDefaultMaxPages_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setDefaultMaxPages' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setDefaultMaxPages", "(I)V", "GetSetDefaultMaxPages_IHandler")]
		public virtual unsafe void SetDefaultMaxPages (int p0)
		{
			if (id_setDefaultMaxPages_I == IntPtr.Zero)
				id_setDefaultMaxPages_I = JNIEnv.GetMethodID (class_ref, "setDefaultMaxPages", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDefaultMaxPages_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDefaultMaxPages", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setLightMode;
#pragma warning disable 0169
		static Delegate GetSetLightModeHandler ()
		{
			if (cb_setLightMode == null)
				cb_setLightMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetLightMode);
			return cb_setLightMode;
		}

		static void n_SetLightMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetLightMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setLightMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setLightMode' and count(parameter)=0]"
		[Register ("setLightMode", "()V", "GetSetLightModeHandler")]
		public virtual unsafe void SetLightMode ()
		{
			if (id_setLightMode == IntPtr.Zero)
				id_setLightMode = JNIEnv.GetMethodID (class_ref, "setLightMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setLightMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLightMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setListViewStartEndPadding_II;
#pragma warning disable 0169
		static Delegate GetSetListViewStartEndPadding_IIHandler ()
		{
			if (cb_setListViewStartEndPadding_II == null)
				cb_setListViewStartEndPadding_II = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int>) n_SetListViewStartEndPadding_II);
			return cb_setListViewStartEndPadding_II;
		}

		static void n_SetListViewStartEndPadding_II (IntPtr jnienv, IntPtr native__this, int p0, int p1)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetListViewStartEndPadding (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_setListViewStartEndPadding_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setListViewStartEndPadding' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register ("setListViewStartEndPadding", "(II)V", "GetSetListViewStartEndPadding_IIHandler")]
		public virtual unsafe void SetListViewStartEndPadding (int p0, int p1)
		{
			if (id_setListViewStartEndPadding_II == IntPtr.Zero)
				id_setListViewStartEndPadding_II = JNIEnv.GetMethodID (class_ref, "setListViewStartEndPadding", "(II)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setListViewStartEndPadding_II, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setListViewStartEndPadding", "(II)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_;
#pragma warning disable 0169
		static Delegate GetSetOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_Handler ()
		{
			if (cb_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_ == null)
				cb_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_);
			return cb_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_;
		}

		static void n_SetOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOnScrollListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='setOnScrollListener' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.ui.PagedListView.OnScrollListener']]"
		[Register ("setOnScrollListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener;)V", "GetSetOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_Handler")]
		public virtual unsafe void SetOnScrollListener (global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView.OnScrollListener p0)
		{
			if (id_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_ == IntPtr.Zero)
				id_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_ = JNIEnv.GetMethodID (class_ref, "setOnScrollListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setOnScrollListener_Lcom_google_android_apps_auto_sdk_ui_PagedListView_OnScrollListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOnScrollListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedListView$OnScrollListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_shouldEnablePageDownButton;
#pragma warning disable 0169
		static Delegate GetShouldEnablePageDownButtonHandler ()
		{
			if (cb_shouldEnablePageDownButton == null)
				cb_shouldEnablePageDownButton = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_ShouldEnablePageDownButton);
			return cb_shouldEnablePageDownButton;
		}

		static bool n_ShouldEnablePageDownButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ShouldEnablePageDownButton ();
		}
#pragma warning restore 0169

		static IntPtr id_shouldEnablePageDownButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='shouldEnablePageDownButton' and count(parameter)=0]"
		[Register ("shouldEnablePageDownButton", "()Z", "GetShouldEnablePageDownButtonHandler")]
		protected virtual unsafe bool ShouldEnablePageDownButton ()
		{
			if (id_shouldEnablePageDownButton == IntPtr.Zero)
				id_shouldEnablePageDownButton = JNIEnv.GetMethodID (class_ref, "shouldEnablePageDownButton", "()Z");
			try {

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_shouldEnablePageDownButton);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "shouldEnablePageDownButton", "()Z"));
			} finally {
			}
		}

		static Delegate cb_shouldEnablePageUpButton;
#pragma warning disable 0169
		static Delegate GetShouldEnablePageUpButtonHandler ()
		{
			if (cb_shouldEnablePageUpButton == null)
				cb_shouldEnablePageUpButton = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_ShouldEnablePageUpButton);
			return cb_shouldEnablePageUpButton;
		}

		static bool n_ShouldEnablePageUpButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ShouldEnablePageUpButton ();
		}
#pragma warning restore 0169

		static IntPtr id_shouldEnablePageUpButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='shouldEnablePageUpButton' and count(parameter)=0]"
		[Register ("shouldEnablePageUpButton", "()Z", "GetShouldEnablePageUpButtonHandler")]
		protected virtual unsafe bool ShouldEnablePageUpButton ()
		{
			if (id_shouldEnablePageUpButton == IntPtr.Zero)
				id_shouldEnablePageUpButton = JNIEnv.GetMethodID (class_ref, "shouldEnablePageUpButton", "()Z");
			try {

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_shouldEnablePageUpButton);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "shouldEnablePageUpButton", "()Z"));
			} finally {
			}
		}

		static Delegate cb_updateMaxItems;
#pragma warning disable 0169
		static Delegate GetUpdateMaxItemsHandler ()
		{
			if (cb_updateMaxItems == null)
				cb_updateMaxItems = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_UpdateMaxItems);
			return cb_updateMaxItems;
		}

		static void n_UpdateMaxItems (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UpdateMaxItems ();
		}
#pragma warning restore 0169

		static IntPtr id_updateMaxItems;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='updateMaxItems' and count(parameter)=0]"
		[Register ("updateMaxItems", "()V", "GetUpdateMaxItemsHandler")]
		protected virtual unsafe void UpdateMaxItems ()
		{
			if (id_updateMaxItems == IntPtr.Zero)
				id_updateMaxItems = JNIEnv.GetMethodID (class_ref, "updateMaxItems", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_updateMaxItems);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "updateMaxItems", "()V"));
			} finally {
			}
		}

		static Delegate cb_updatePaginationButtons_Z;
#pragma warning disable 0169
		static Delegate GetUpdatePaginationButtons_ZHandler ()
		{
			if (cb_updatePaginationButtons_Z == null)
				cb_updatePaginationButtons_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_UpdatePaginationButtons_Z);
			return cb_updatePaginationButtons_Z;
		}

		static void n_UpdatePaginationButtons_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UpdatePaginationButtons (p0);
		}
#pragma warning restore 0169

		static IntPtr id_updatePaginationButtons_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='updatePaginationButtons' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("updatePaginationButtons", "(Z)V", "GetUpdatePaginationButtons_ZHandler")]
		protected virtual unsafe void UpdatePaginationButtons (bool p0)
		{
			if (id_updatePaginationButtons_Z == IntPtr.Zero)
				id_updatePaginationButtons_Z = JNIEnv.GetMethodID (class_ref, "updatePaginationButtons", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_updatePaginationButtons_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "updatePaginationButtons", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_updateRowsPerPage;
#pragma warning disable 0169
		static Delegate GetUpdateRowsPerPageHandler ()
		{
			if (cb_updateRowsPerPage == null)
				cb_updateRowsPerPage = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_UpdateRowsPerPage);
			return cb_updateRowsPerPage;
		}

		static void n_UpdateRowsPerPage (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedListView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UpdateRowsPerPage ();
		}
#pragma warning restore 0169

		static IntPtr id_updateRowsPerPage;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedListView']/method[@name='updateRowsPerPage' and count(parameter)=0]"
		[Register ("updateRowsPerPage", "()V", "GetUpdateRowsPerPageHandler")]
		protected virtual unsafe void UpdateRowsPerPage ()
		{
			if (id_updateRowsPerPage == IntPtr.Zero)
				id_updateRowsPerPage = JNIEnv.GetMethodID (class_ref, "updateRowsPerPage", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_updateRowsPerPage);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "updateRowsPerPage", "()V"));
			} finally {
			}
		}

	}
}
