using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/d", DoNotGenerateAcw=true)]
	public partial class D : global::Com.Google.Android.A.Bgizmo {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/d", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (D); }
		}

		protected D (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/constructor[@name='d' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe D ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (D)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_a;
#pragma warning disable 0169
		static Delegate GetAHandler ()
		{
			if (cb_a == null)
				cb_a = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_A);
			return cb_a;
		}

		static IntPtr n_A (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.A ());
		}
#pragma warning restore 0169

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()Lcom/google/android/apps/auto/sdk/nav/NavigationProviderConfig;", "GetAHandler")]
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()Lcom/google/android/apps/auto/sdk/nav/NavigationProviderConfig;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "a", "()Lcom/google/android/apps/auto/sdk/nav/NavigationProviderConfig;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
#pragma warning disable 0169
		static Delegate GetA_Lcom_google_android_apps_auto_sdk_nav_ClientMode_Handler ()
		{
			if (cb_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ == null)
				cb_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_A_Lcom_google_android_apps_auto_sdk_nav_ClientMode_);
			return cb_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
		}

		static void n_A_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.A (p0);
		}
#pragma warning restore 0169

		static IntPtr id_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.ClientMode']]"
		[Register ("a", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V", "GetA_Lcom_google_android_apps_auto_sdk_nav_ClientMode_Handler")]
		public virtual unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode p0)
		{
			if (id_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ == IntPtr.Zero)
				id_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Lcom_google_android_apps_auto_sdk_nav_ClientMode_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "a", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_;
#pragma warning disable 0169
		static Delegate GetA_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_Handler ()
		{
			if (cb_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_ == null)
				cb_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_A_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_);
			return cb_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_;
		}

		static void n_A_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.A (p0);
		}
#pragma warning restore 0169

		static IntPtr id_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.NavigationClientConfig']]"
		[Register ("a", "(Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;)V", "GetA_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_Handler")]
		public virtual unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig p0)
		{
			if (id_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_ == IntPtr.Zero)
				id_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Lcom_google_android_apps_auto_sdk_nav_NavigationClientConfig_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "a", "(Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_b;
#pragma warning disable 0169
		static Delegate GetBHandler ()
		{
			if (cb_b == null)
				cb_b = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_B);
			return cb_b;
		}

		static void n_B (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.B ();
		}
#pragma warning restore 0169

		static IntPtr id_b;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/method[@name='b' and count(parameter)=0]"
		[Register ("b", "()V", "GetBHandler")]
		public virtual unsafe void B ()
		{
			if (id_b == IntPtr.Zero)
				id_b = JNIEnv.GetMethodID (class_ref, "b", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_b);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "b", "()V"));
			} finally {
			}
		}

		static Delegate cb_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I;
#pragma warning disable 0169
		static Delegate GetOnTransact_ILandroid_os_Parcel_Landroid_os_Parcel_IHandler ()
		{
			if (cb_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I == null)
				cb_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, IntPtr, int, bool>) n_OnTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I);
			return cb_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I;
		}

		static bool n_OnTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, IntPtr native_p2, int p3)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Parcel p1 = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (native_p1, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Parcel p2 = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (native_p2, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnTransact (p0, p1, p2, p3);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='d']/method[@name='onTransact' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='android.os.Parcel'] and parameter[3][@type='android.os.Parcel'] and parameter[4][@type='int']]"
		[Register ("onTransact", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z", "GetOnTransact_ILandroid_os_Parcel_Landroid_os_Parcel_IHandler")]
		public virtual unsafe bool OnTransact (int p0, global::Android.OS.Parcel p1, global::Android.OS.Parcel p2, int p3)
		{
			if (id_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I == IntPtr.Zero)
				id_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I = JNIEnv.GetMethodID (class_ref, "onTransact", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onTransact_ILandroid_os_Parcel_Landroid_os_Parcel_I, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onTransact", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z"), __args);
				return __ret;
			} finally {
			}
		}

	}
}
