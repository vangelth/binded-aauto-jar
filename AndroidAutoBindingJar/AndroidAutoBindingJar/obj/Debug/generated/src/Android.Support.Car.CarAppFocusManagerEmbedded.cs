using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/CarAppFocusManagerEmbedded", DoNotGenerateAcw=true)]
	public partial class CarAppFocusManagerEmbedded : global::Android.Support.Car.CarAppFocusManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarAppFocusManagerEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAppFocusManagerEmbedded); }
		}

		protected CarAppFocusManagerEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static void n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAppFocus (p0);
		}
#pragma warning restore 0169

		static IntPtr id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='abandonAppFocus' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0)
		{
			if (id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
#pragma warning disable 0169
		static Delegate GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_IHandler ()
		{
			if (cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I == null)
				cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I);
			return cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
		}

		static void n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAppFocus (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='abandonAppFocus' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback'] and parameter[2][@type='int']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_IHandler")]
		public override unsafe void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0, int p1)
		{
			if (id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I == IntPtr.Zero)
				id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I = JNIEnv.GetMethodID (class_ref, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
#pragma warning disable 0169
		static Delegate GetAddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler ()
		{
			if (cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == null)
				cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_AddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I);
			return cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		}

		static void n_AddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddFocusListener (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='addFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetAddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public override unsafe void AddFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1)
		{
			if (id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == IntPtr.Zero)
				id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNIEnv.GetMethodID (class_ref, "addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetIsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_IsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static bool n_IsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsOwningFocus (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='isOwningFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z", "GetIsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe bool IsOwningFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1)
		{
			if (id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCarDisconnected", "()V"));
			} finally {
			}
		}

		static Delegate cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
#pragma warning disable 0169
		static Delegate GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_Handler ()
		{
			if (cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ == null)
				cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_);
			return cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
		}

		static void n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveFocusListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='removeFocusListener' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_Handler")]
		public override unsafe void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0)
		{
			if (id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ == IntPtr.Zero)
				id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ = JNIEnv.GetMethodID (class_ref, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
#pragma warning disable 0169
		static Delegate GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler ()
		{
			if (cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == null)
				cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I);
			return cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		}

		static void n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveFocusListener (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='removeFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public override unsafe void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1)
		{
			if (id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == IntPtr.Zero)
				id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNIEnv.GetMethodID (class_ref, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetRequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, int>) n_RequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static int n_RequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.CarAppFocusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAppFocus (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManagerEmbedded']/method[@name='requestAppFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I", "GetRequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe int RequestAppFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1)
		{
			if (id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I"), __args);
				return __ret;
			} finally {
			}
		}

	}
}
