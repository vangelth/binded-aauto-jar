using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Vanagon {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient", DoNotGenerateAcw=true)]
	public partial class PhoneSysUiClient : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_HOME']"
		[Register ("FACET_HOME")]
		public const int FacetHome = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_MAPS']"
		[Register ("FACET_MAPS")]
		public const int FacetMaps = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_MEDIA']"
		[Register ("FACET_MEDIA")]
		public const int FacetMedia = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_OTHER']"
		[Register ("FACET_OTHER")]
		public const int FacetOther = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_PHONE']"
		[Register ("FACET_PHONE")]
		public const int FacetPhone = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='FACET_UNKNOWN']"
		[Register ("FACET_UNKNOWN")]
		public const int FacetUnknown = (int) 0;

		static IntPtr sTestOnlyPackageNameOverride_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='sTestOnlyPackageNameOverride']"
		[Register ("sTestOnlyPackageNameOverride")]
		public static string STestOnlyPackageNameOverride {
			get {
				if (sTestOnlyPackageNameOverride_jfieldId == IntPtr.Zero)
					sTestOnlyPackageNameOverride_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlyPackageNameOverride", "Ljava/lang/String;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, sTestOnlyPackageNameOverride_jfieldId);
				return JNIEnv.GetString (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (sTestOnlyPackageNameOverride_jfieldId == IntPtr.Zero)
					sTestOnlyPackageNameOverride_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlyPackageNameOverride", "Ljava/lang/String;");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JNIEnv.SetStaticField (class_ref, sTestOnlyPackageNameOverride_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr sTestOnlySystemUIClassName_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='sTestOnlySystemUIClassName']"
		[Register ("sTestOnlySystemUIClassName")]
		public static string STestOnlySystemUIClassName {
			get {
				if (sTestOnlySystemUIClassName_jfieldId == IntPtr.Zero)
					sTestOnlySystemUIClassName_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlySystemUIClassName", "Ljava/lang/String;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, sTestOnlySystemUIClassName_jfieldId);
				return JNIEnv.GetString (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (sTestOnlySystemUIClassName_jfieldId == IntPtr.Zero)
					sTestOnlySystemUIClassName_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlySystemUIClassName", "Ljava/lang/String;");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JNIEnv.SetStaticField (class_ref, sTestOnlySystemUIClassName_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr sTestOnlyUseThreadClassLoader_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/field[@name='sTestOnlyUseThreadClassLoader']"
		[Register ("sTestOnlyUseThreadClassLoader")]
		public static bool STestOnlyUseThreadClassLoader {
			get {
				if (sTestOnlyUseThreadClassLoader_jfieldId == IntPtr.Zero)
					sTestOnlyUseThreadClassLoader_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlyUseThreadClassLoader", "Z");
				return JNIEnv.GetStaticBooleanField (class_ref, sTestOnlyUseThreadClassLoader_jfieldId);
			}
			set {
				if (sTestOnlyUseThreadClassLoader_jfieldId == IntPtr.Zero)
					sTestOnlyUseThreadClassLoader_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "sTestOnlyUseThreadClassLoader", "Z");
				try {
					JNIEnv.SetStaticField (class_ref, sTestOnlyUseThreadClassLoader_jfieldId, value);
				} finally {
				}
			}
		}
		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.AndroidAutoStateCallback']"
		[Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback", "", "Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IAndroidAutoStateCallbackInvoker")]
		public partial interface IAndroidAutoStateCallback : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.AndroidAutoStateCallback']/method[@name='onEnterPhoneMode' and count(parameter)=0]"
			[Register ("onEnterPhoneMode", "()V", "GetOnEnterPhoneModeHandler:Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IAndroidAutoStateCallbackInvoker, AndroidAutoBindingJar")]
			void OnEnterPhoneMode ();

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.AndroidAutoStateCallback']/method[@name='onExitPhoneMode' and count(parameter)=0]"
			[Register ("onExitPhoneMode", "()V", "GetOnExitPhoneModeHandler:Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IAndroidAutoStateCallbackInvoker, AndroidAutoBindingJar")]
			void OnExitPhoneMode ();

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback", DoNotGenerateAcw=true)]
		internal class IAndroidAutoStateCallbackInvoker : global::Java.Lang.Object, IAndroidAutoStateCallback {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IAndroidAutoStateCallbackInvoker); }
			}

			IntPtr class_ref;

			public static IAndroidAutoStateCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IAndroidAutoStateCallback> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.AndroidAutoStateCallback"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IAndroidAutoStateCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onEnterPhoneMode;
#pragma warning disable 0169
			static Delegate GetOnEnterPhoneModeHandler ()
			{
				if (cb_onEnterPhoneMode == null)
					cb_onEnterPhoneMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnEnterPhoneMode);
				return cb_onEnterPhoneMode;
			}

			static void n_OnEnterPhoneMode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnEnterPhoneMode ();
			}
#pragma warning restore 0169

			IntPtr id_onEnterPhoneMode;
			public unsafe void OnEnterPhoneMode ()
			{
				if (id_onEnterPhoneMode == IntPtr.Zero)
					id_onEnterPhoneMode = JNIEnv.GetMethodID (class_ref, "onEnterPhoneMode", "()V");
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onEnterPhoneMode);
			}

			static Delegate cb_onExitPhoneMode;
#pragma warning disable 0169
			static Delegate GetOnExitPhoneModeHandler ()
			{
				if (cb_onExitPhoneMode == null)
					cb_onExitPhoneMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnExitPhoneMode);
				return cb_onExitPhoneMode;
			}

			static void n_OnExitPhoneMode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnExitPhoneMode ();
			}
#pragma warning restore 0169

			IntPtr id_onExitPhoneMode;
			public unsafe void OnExitPhoneMode ()
			{
				if (id_onExitPhoneMode == IntPtr.Zero)
					id_onExitPhoneMode = JNIEnv.GetMethodID (class_ref, "onExitPhoneMode", "()V");
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onExitPhoneMode);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.ScreenshotProvider.OnCompleteListener']"
		[Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider$OnCompleteListener", "", "Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IScreenshotProviderOnCompleteListenerInvoker")]
		public partial interface IScreenshotProviderOnCompleteListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.ScreenshotProvider.OnCompleteListener']/method[@name='onScreenshotComplete' and count(parameter)=1 and parameter[1][@type='android.graphics.Bitmap']]"
			[Register ("onScreenshotComplete", "(Landroid/graphics/Bitmap;)V", "GetOnScreenshotComplete_Landroid_graphics_Bitmap_Handler:Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IScreenshotProviderOnCompleteListenerInvoker, AndroidAutoBindingJar")]
			void OnScreenshotComplete (global::Android.Graphics.Bitmap p0);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider$OnCompleteListener", DoNotGenerateAcw=true)]
		internal class IScreenshotProviderOnCompleteListenerInvoker : global::Java.Lang.Object, IScreenshotProviderOnCompleteListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider$OnCompleteListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IScreenshotProviderOnCompleteListenerInvoker); }
			}

			IntPtr class_ref;

			public static IScreenshotProviderOnCompleteListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IScreenshotProviderOnCompleteListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.ScreenshotProvider.OnCompleteListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IScreenshotProviderOnCompleteListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onScreenshotComplete_Landroid_graphics_Bitmap_;
#pragma warning disable 0169
			static Delegate GetOnScreenshotComplete_Landroid_graphics_Bitmap_Handler ()
			{
				if (cb_onScreenshotComplete_Landroid_graphics_Bitmap_ == null)
					cb_onScreenshotComplete_Landroid_graphics_Bitmap_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnScreenshotComplete_Landroid_graphics_Bitmap_);
				return cb_onScreenshotComplete_Landroid_graphics_Bitmap_;
			}

			static void n_OnScreenshotComplete_Landroid_graphics_Bitmap_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Graphics.Bitmap p0 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnScreenshotComplete (p0);
			}
#pragma warning restore 0169

			IntPtr id_onScreenshotComplete_Landroid_graphics_Bitmap_;
			public unsafe void OnScreenshotComplete (global::Android.Graphics.Bitmap p0)
			{
				if (id_onScreenshotComplete_Landroid_graphics_Bitmap_ == IntPtr.Zero)
					id_onScreenshotComplete_Landroid_graphics_Bitmap_ = JNIEnv.GetMethodID (class_ref, "onScreenshotComplete", "(Landroid/graphics/Bitmap;)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onScreenshotComplete_Landroid_graphics_Bitmap_, __args);
			}

		}

		public partial class ScreenshotProviderOnCompleteEventArgs : global::System.EventArgs {

			public ScreenshotProviderOnCompleteEventArgs (global::Android.Graphics.Bitmap p0)
			{
				this.p0 = p0;
			}

			global::Android.Graphics.Bitmap p0;
			public global::Android.Graphics.Bitmap P0 {
				get { return p0; }
			}
		}

		[global::Android.Runtime.Register ("mono/com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor")]
		internal sealed partial class IScreenshotProviderOnCompleteListenerImplementor : global::Java.Lang.Object, IScreenshotProviderOnCompleteListener {

			object sender;

			public IScreenshotProviderOnCompleteListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<ScreenshotProviderOnCompleteEventArgs> Handler;
#pragma warning restore 0649

			public void OnScreenshotComplete (global::Android.Graphics.Bitmap p0)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new ScreenshotProviderOnCompleteEventArgs (p0));
			}

			internal static bool __IsEmpty (IScreenshotProviderOnCompleteListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.ScreenshotProvider']"
		[Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider", "", "Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IScreenshotProviderInvoker")]
		public partial interface IScreenshotProvider : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/interface[@name='PhoneSysUiClient.ScreenshotProvider']/method[@name='getScreenshot' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.ScreenshotProvider.OnCompleteListener']]"
			[Register ("getScreenshot", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider$OnCompleteListener;)V", "GetGetScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_Handler:Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IScreenshotProviderInvoker, AndroidAutoBindingJar")]
			void GetScreenshot (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener p0);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider", DoNotGenerateAcw=true)]
		internal class IScreenshotProviderInvoker : global::Java.Lang.Object, IScreenshotProvider {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IScreenshotProviderInvoker); }
			}

			IntPtr class_ref;

			public static IScreenshotProvider GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IScreenshotProvider> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.ScreenshotProvider"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IScreenshotProviderInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_;
#pragma warning disable 0169
			static Delegate GetGetScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_Handler ()
			{
				if (cb_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_ == null)
					cb_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_GetScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_);
				return cb_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_;
			}

			static void n_GetScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener p0 = (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.GetScreenshot (p0);
			}
#pragma warning restore 0169

			IntPtr id_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_;
			public unsafe void GetScreenshot (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProviderOnCompleteListener p0)
			{
				if (id_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_ == IntPtr.Zero)
					id_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_ = JNIEnv.GetMethodID (class_ref, "getScreenshot", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider$OnCompleteListener;)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_getScreenshot_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_OnCompleteListener_, __args);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PhoneSysUiClient); }
		}

		protected PhoneSysUiClient (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_app_Activity_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/constructor[@name='PhoneSysUiClient' and count(parameter)=1 and parameter[1][@type='android.app.Activity']]"
		[Register (".ctor", "(Landroid/app/Activity;)V", "")]
		public unsafe PhoneSysUiClient (global::Android.App.Activity p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (PhoneSysUiClient)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/app/Activity;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/app/Activity;)V", __args);
					return;
				}

				if (id_ctor_Landroid_app_Activity_ == IntPtr.Zero)
					id_ctor_Landroid_app_Activity_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/app/Activity;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_app_Activity_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_app_Activity_, __args);
			} finally {
			}
		}

		static Delegate cb_getActivityRootView;
#pragma warning disable 0169
		static Delegate GetGetActivityRootViewHandler ()
		{
			if (cb_getActivityRootView == null)
				cb_getActivityRootView = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetActivityRootView);
			return cb_getActivityRootView;
		}

		static IntPtr n_GetActivityRootView (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ActivityRootView);
		}
#pragma warning restore 0169

		static IntPtr id_getActivityRootView;
		public virtual unsafe global::Android.Views.ViewGroup ActivityRootView {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='getActivityRootView' and count(parameter)=0]"
			[Register ("getActivityRootView", "()Landroid/view/ViewGroup;", "GetGetActivityRootViewHandler")]
			get {
				if (id_getActivityRootView == IntPtr.Zero)
					id_getActivityRootView = JNIEnv.GetMethodID (class_ref, "getActivityRootView", "()Landroid/view/ViewGroup;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Views.ViewGroup> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getActivityRootView), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Views.ViewGroup> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getActivityRootView", "()Landroid/view/ViewGroup;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getSystemRoot;
#pragma warning disable 0169
		static Delegate GetGetSystemRootHandler ()
		{
			if (cb_getSystemRoot == null)
				cb_getSystemRoot = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSystemRoot);
			return cb_getSystemRoot;
		}

		static IntPtr n_GetSystemRoot (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.SystemRoot);
		}
#pragma warning restore 0169

		static IntPtr id_getSystemRoot;
		public virtual unsafe global::Android.Views.View SystemRoot {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='getSystemRoot' and count(parameter)=0]"
			[Register ("getSystemRoot", "()Landroid/view/View;", "GetGetSystemRootHandler")]
			get {
				if (id_getSystemRoot == IntPtr.Zero)
					id_getSystemRoot = JNIEnv.GetMethodID (class_ref, "getSystemRoot", "()Landroid/view/View;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSystemRoot), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSystemRoot", "()Landroid/view/View;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_isAndroidAutoRunning_Landroid_content_Context_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='isAndroidAutoRunning' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("isAndroidAutoRunning", "(Landroid/content/Context;)Z", "")]
		public static unsafe bool IsAndroidAutoRunning (global::Android.Content.Context p0)
		{
			if (id_isAndroidAutoRunning_Landroid_content_Context_ == IntPtr.Zero)
				id_isAndroidAutoRunning_Landroid_content_Context_ = JNIEnv.GetStaticMethodID (class_ref, "isAndroidAutoRunning", "(Landroid/content/Context;)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				bool __ret = JNIEnv.CallStaticBooleanMethod  (class_ref, id_isAndroidAutoRunning_Landroid_content_Context_, __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_onConfigurationChanged_Landroid_content_res_Configuration_;
#pragma warning disable 0169
		static Delegate GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler ()
		{
			if (cb_onConfigurationChanged_Landroid_content_res_Configuration_ == null)
				cb_onConfigurationChanged_Landroid_content_res_Configuration_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnConfigurationChanged_Landroid_content_res_Configuration_);
			return cb_onConfigurationChanged_Landroid_content_res_Configuration_;
		}

		static void n_OnConfigurationChanged_Landroid_content_res_Configuration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Res.Configuration p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Res.Configuration> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnConfigurationChanged (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onConfigurationChanged_Landroid_content_res_Configuration_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onConfigurationChanged' and count(parameter)=1 and parameter[1][@type='android.content.res.Configuration']]"
		[Register ("onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler")]
		public virtual unsafe void OnConfigurationChanged (global::Android.Content.Res.Configuration p0)
		{
			if (id_onConfigurationChanged_Landroid_content_res_Configuration_ == IntPtr.Zero)
				id_onConfigurationChanged_Landroid_content_res_Configuration_ = JNIEnv.GetMethodID (class_ref, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConfigurationChanged_Landroid_content_res_Configuration_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onCreate_Z;
#pragma warning disable 0169
		static Delegate GetOnCreate_ZHandler ()
		{
			if (cb_onCreate_Z == null)
				cb_onCreate_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, IntPtr>) n_OnCreate_Z);
			return cb_onCreate_Z;
		}

		static IntPtr n_OnCreate_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OnCreate (p0));
		}
#pragma warning restore 0169

		static IntPtr id_onCreate_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onCreate' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("onCreate", "(Z)Landroid/view/ViewGroup;", "GetOnCreate_ZHandler")]
		public virtual unsafe global::Android.Views.ViewGroup OnCreate (bool p0)
		{
			if (id_onCreate_Z == IntPtr.Zero)
				id_onCreate_Z = JNIEnv.GetMethodID (class_ref, "onCreate", "(Z)Landroid/view/ViewGroup;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Views.ViewGroup> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onCreate_Z, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Views.ViewGroup> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCreate", "(Z)Landroid/view/ViewGroup;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_onDestroy;
#pragma warning disable 0169
		static Delegate GetOnDestroyHandler ()
		{
			if (cb_onDestroy == null)
				cb_onDestroy = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDestroy);
			return cb_onDestroy;
		}

		static void n_OnDestroy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDestroy ();
		}
#pragma warning restore 0169

		static IntPtr id_onDestroy;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onDestroy' and count(parameter)=0]"
		[Register ("onDestroy", "()V", "GetOnDestroyHandler")]
		public virtual unsafe void OnDestroy ()
		{
			if (id_onDestroy == IntPtr.Zero)
				id_onDestroy = JNIEnv.GetMethodID (class_ref, "onDestroy", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDestroy);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDestroy", "()V"));
			} finally {
			}
		}

		static Delegate cb_onPause;
#pragma warning disable 0169
		static Delegate GetOnPauseHandler ()
		{
			if (cb_onPause == null)
				cb_onPause = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPause);
			return cb_onPause;
		}

		static void n_OnPause (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPause ();
		}
#pragma warning restore 0169

		static IntPtr id_onPause;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onPause' and count(parameter)=0]"
		[Register ("onPause", "()V", "GetOnPauseHandler")]
		public virtual unsafe void OnPause ()
		{
			if (id_onPause == IntPtr.Zero)
				id_onPause = JNIEnv.GetMethodID (class_ref, "onPause", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPause);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPause", "()V"));
			} finally {
			}
		}

		static Delegate cb_onResume;
#pragma warning disable 0169
		static Delegate GetOnResumeHandler ()
		{
			if (cb_onResume == null)
				cb_onResume = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnResume);
			return cb_onResume;
		}

		static void n_OnResume (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnResume ();
		}
#pragma warning restore 0169

		static IntPtr id_onResume;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onResume' and count(parameter)=0]"
		[Register ("onResume", "()V", "GetOnResumeHandler")]
		public virtual unsafe void OnResume ()
		{
			if (id_onResume == IntPtr.Zero)
				id_onResume = JNIEnv.GetMethodID (class_ref, "onResume", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onResume);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onResume", "()V"));
			} finally {
			}
		}

		static Delegate cb_onStart;
#pragma warning disable 0169
		static Delegate GetOnStartHandler ()
		{
			if (cb_onStart == null)
				cb_onStart = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStart);
			return cb_onStart;
		}

		static void n_OnStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStart ();
		}
#pragma warning restore 0169

		static IntPtr id_onStart;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onStart' and count(parameter)=0]"
		[Register ("onStart", "()V", "GetOnStartHandler")]
		public virtual unsafe void OnStart ()
		{
			if (id_onStart == IntPtr.Zero)
				id_onStart = JNIEnv.GetMethodID (class_ref, "onStart", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStart);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onStart", "()V"));
			} finally {
			}
		}

		static Delegate cb_onStop;
#pragma warning disable 0169
		static Delegate GetOnStopHandler ()
		{
			if (cb_onStop == null)
				cb_onStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStop);
			return cb_onStop;
		}

		static void n_OnStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStop ();
		}
#pragma warning restore 0169

		static IntPtr id_onStop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onStop' and count(parameter)=0]"
		[Register ("onStop", "()V", "GetOnStopHandler")]
		public virtual unsafe void OnStop ()
		{
			if (id_onStop == IntPtr.Zero)
				id_onStop = JNIEnv.GetMethodID (class_ref, "onStop", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStop);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onStop", "()V"));
			} finally {
			}
		}

		static Delegate cb_onWindowFocusChanged_Z;
#pragma warning disable 0169
		static Delegate GetOnWindowFocusChanged_ZHandler ()
		{
			if (cb_onWindowFocusChanged_Z == null)
				cb_onWindowFocusChanged_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_OnWindowFocusChanged_Z);
			return cb_onWindowFocusChanged_Z;
		}

		static void n_OnWindowFocusChanged_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnWindowFocusChanged (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onWindowFocusChanged_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='onWindowFocusChanged' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("onWindowFocusChanged", "(Z)V", "GetOnWindowFocusChanged_ZHandler")]
		public virtual unsafe void OnWindowFocusChanged (bool p0)
		{
			if (id_onWindowFocusChanged_Z == IntPtr.Zero)
				id_onWindowFocusChanged_Z = JNIEnv.GetMethodID (class_ref, "onWindowFocusChanged", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onWindowFocusChanged_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onWindowFocusChanged", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_;
#pragma warning disable 0169
		static Delegate GetSetAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_Handler ()
		{
			if (cb_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_ == null)
				cb_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_);
			return cb_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_;
		}

		static void n_SetAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback p0 = (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAndroidAutoStateListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setAndroidAutoStateListener' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.AndroidAutoStateCallback']]"
		[Register ("setAndroidAutoStateListener", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback;)V", "GetSetAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_Handler")]
		public virtual unsafe void SetAndroidAutoStateListener (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IAndroidAutoStateCallback p0)
		{
			if (id_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_ == IntPtr.Zero)
				id_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_ = JNIEnv.GetMethodID (class_ref, "setAndroidAutoStateListener", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setAndroidAutoStateListener_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_AndroidAutoStateCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAndroidAutoStateListener", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$AndroidAutoStateCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetEnabled_ZHandler ()
		{
			if (cb_setEnabled_Z == null)
				cb_setEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetEnabled_Z);
			return cb_setEnabled_Z;
		}

		static void n_SetEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetEnabled (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setEnabled_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setEnabled", "(Z)V", "GetSetEnabled_ZHandler")]
		public virtual unsafe void SetEnabled (bool p0)
		{
			if (id_setEnabled_Z == IntPtr.Zero)
				id_setEnabled_Z = JNIEnv.GetMethodID (class_ref, "setEnabled", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setEnabled_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEnabled", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setPrettyImmersiveStickyTransitions_Z;
#pragma warning disable 0169
		static Delegate GetSetPrettyImmersiveStickyTransitions_ZHandler ()
		{
			if (cb_setPrettyImmersiveStickyTransitions_Z == null)
				cb_setPrettyImmersiveStickyTransitions_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetPrettyImmersiveStickyTransitions_Z);
			return cb_setPrettyImmersiveStickyTransitions_Z;
		}

		static void n_SetPrettyImmersiveStickyTransitions_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetPrettyImmersiveStickyTransitions (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setPrettyImmersiveStickyTransitions_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setPrettyImmersiveStickyTransitions' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setPrettyImmersiveStickyTransitions", "(Z)V", "GetSetPrettyImmersiveStickyTransitions_ZHandler")]
		public virtual unsafe void SetPrettyImmersiveStickyTransitions (bool p0)
		{
			if (id_setPrettyImmersiveStickyTransitions_Z == IntPtr.Zero)
				id_setPrettyImmersiveStickyTransitions_Z = JNIEnv.GetMethodID (class_ref, "setPrettyImmersiveStickyTransitions", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setPrettyImmersiveStickyTransitions_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPrettyImmersiveStickyTransitions", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_;
#pragma warning disable 0169
		static Delegate GetSetScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_Handler ()
		{
			if (cb_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_ == null)
				cb_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_);
			return cb_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_;
		}

		static void n_SetScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider p0 = (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetScreenshotProvider (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setScreenshotProvider' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.ScreenshotProvider']]"
		[Register ("setScreenshotProvider", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider;)V", "GetSetScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_Handler")]
		public virtual unsafe void SetScreenshotProvider (global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient.IScreenshotProvider p0)
		{
			if (id_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_ == IntPtr.Zero)
				id_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_ = JNIEnv.GetMethodID (class_ref, "setScreenshotProvider", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setScreenshotProvider_Lcom_google_android_apps_auto_sdk_vanagon_PhoneSysUiClient_ScreenshotProvider_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setScreenshotProvider", "(Lcom/google/android/apps/auto/sdk/vanagon/PhoneSysUiClient$ScreenshotProvider;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setSystemUiVisibility_I;
#pragma warning disable 0169
		static Delegate GetSetSystemUiVisibility_IHandler ()
		{
			if (cb_setSystemUiVisibility_I == null)
				cb_setSystemUiVisibility_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetSystemUiVisibility_I);
			return cb_setSystemUiVisibility_I;
		}

		static void n_SetSystemUiVisibility_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetSystemUiVisibility (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setSystemUiVisibility_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setSystemUiVisibility' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setSystemUiVisibility", "(I)V", "GetSetSystemUiVisibility_IHandler")]
		public virtual unsafe void SetSystemUiVisibility (int p0)
		{
			if (id_setSystemUiVisibility_I == IntPtr.Zero)
				id_setSystemUiVisibility_I = JNIEnv.GetMethodID (class_ref, "setSystemUiVisibility", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSystemUiVisibility_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSystemUiVisibility", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setTintStatusBarIcons_Z;
#pragma warning disable 0169
		static Delegate GetSetTintStatusBarIcons_ZHandler ()
		{
			if (cb_setTintStatusBarIcons_Z == null)
				cb_setTintStatusBarIcons_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetTintStatusBarIcons_Z);
			return cb_setTintStatusBarIcons_Z;
		}

		static void n_SetTintStatusBarIcons_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetTintStatusBarIcons (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setTintStatusBarIcons_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='setTintStatusBarIcons' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setTintStatusBarIcons", "(Z)V", "GetSetTintStatusBarIcons_ZHandler")]
		public virtual unsafe void SetTintStatusBarIcons (bool p0)
		{
			if (id_setTintStatusBarIcons_Z == IntPtr.Zero)
				id_setTintStatusBarIcons_Z = JNIEnv.GetMethodID (class_ref, "setTintStatusBarIcons", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setTintStatusBarIcons_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTintStatusBarIcons", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_showDownButton_Z;
#pragma warning disable 0169
		static Delegate GetShowDownButton_ZHandler ()
		{
			if (cb_showDownButton_Z == null)
				cb_showDownButton_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_ShowDownButton_Z);
			return cb_showDownButton_Z;
		}

		static void n_ShowDownButton_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowDownButton (p0);
		}
#pragma warning restore 0169

		static IntPtr id_showDownButton_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='showDownButton' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("showDownButton", "(Z)V", "GetShowDownButton_ZHandler")]
		public virtual unsafe void ShowDownButton (bool p0)
		{
			if (id_showDownButton_Z == IntPtr.Zero)
				id_showDownButton_Z = JNIEnv.GetMethodID (class_ref, "showDownButton", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showDownButton_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showDownButton", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_showFacetNavigation_Z;
#pragma warning disable 0169
		static Delegate GetShowFacetNavigation_ZHandler ()
		{
			if (cb_showFacetNavigation_Z == null)
				cb_showFacetNavigation_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_ShowFacetNavigation_Z);
			return cb_showFacetNavigation_Z;
		}

		static void n_ShowFacetNavigation_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowFacetNavigation (p0);
		}
#pragma warning restore 0169

		static IntPtr id_showFacetNavigation_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='showFacetNavigation' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("showFacetNavigation", "(Z)V", "GetShowFacetNavigation_ZHandler")]
		public virtual unsafe void ShowFacetNavigation (bool p0)
		{
			if (id_showFacetNavigation_Z == IntPtr.Zero)
				id_showFacetNavigation_Z = JNIEnv.GetMethodID (class_ref, "showFacetNavigation", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showFacetNavigation_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showFacetNavigation", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_showFacetNavigation_ZII;
#pragma warning disable 0169
		static Delegate GetShowFacetNavigation_ZIIHandler ()
		{
			if (cb_showFacetNavigation_ZII == null)
				cb_showFacetNavigation_ZII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool, int, int>) n_ShowFacetNavigation_ZII);
			return cb_showFacetNavigation_ZII;
		}

		static void n_ShowFacetNavigation_ZII (IntPtr jnienv, IntPtr native__this, bool p0, int p1, int p2)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowFacetNavigation (p0, p1, p2);
		}
#pragma warning restore 0169

		static IntPtr id_showFacetNavigation_ZII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='showFacetNavigation' and count(parameter)=3 and parameter[1][@type='boolean'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("showFacetNavigation", "(ZII)V", "GetShowFacetNavigation_ZIIHandler")]
		public virtual unsafe void ShowFacetNavigation (bool p0, int p1, int p2)
		{
			if (id_showFacetNavigation_ZII == IntPtr.Zero)
				id_showFacetNavigation_ZII = JNIEnv.GetMethodID (class_ref, "showFacetNavigation", "(ZII)V");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showFacetNavigation_ZII, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showFacetNavigation", "(ZII)V"), __args);
			} finally {
			}
		}

		static Delegate cb_suppressHomeButtonExit_Z;
#pragma warning disable 0169
		static Delegate GetSuppressHomeButtonExit_ZHandler ()
		{
			if (cb_suppressHomeButtonExit_Z == null)
				cb_suppressHomeButtonExit_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SuppressHomeButtonExit_Z);
			return cb_suppressHomeButtonExit_Z;
		}

		static void n_SuppressHomeButtonExit_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SuppressHomeButtonExit (p0);
		}
#pragma warning restore 0169

		static IntPtr id_suppressHomeButtonExit_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.vanagon']/class[@name='PhoneSysUiClient']/method[@name='suppressHomeButtonExit' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("suppressHomeButtonExit", "(Z)V", "GetSuppressHomeButtonExit_ZHandler")]
		public virtual unsafe void SuppressHomeButtonExit (bool p0)
		{
			if (id_suppressHomeButtonExit_Z == IntPtr.Zero)
				id_suppressHomeButtonExit_Z = JNIEnv.GetMethodID (class_ref, "suppressHomeButtonExit", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_suppressHomeButtonExit_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "suppressHomeButtonExit", "(Z)V"), __args);
			} finally {
			}
		}

	}
}
