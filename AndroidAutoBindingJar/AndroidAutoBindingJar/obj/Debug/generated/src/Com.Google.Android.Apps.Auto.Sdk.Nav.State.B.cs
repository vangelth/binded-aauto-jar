using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.State {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='b']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/b", DoNotGenerateAcw=true)]
	public sealed partial class B : global::Com.Google.Android.A.Agizmo {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/b", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (B); }
		}

		internal B (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='b']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()Lcom/google/android/apps/auto/sdk/nav/state/CarInstrumentClusterConfig;", "")]
		public unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()Lcom/google/android/apps/auto/sdk/nav/state/CarInstrumentClusterConfig;");
			try {
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_a_Lcom_google_android_apps_auto_sdk_nav_state_NavigationSummary_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='b']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.state.NavigationSummary']]"
		[Register ("a", "(Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary;)V", "")]
		public unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary p0)
		{
			if (id_a_Lcom_google_android_apps_auto_sdk_nav_state_NavigationSummary_ == IntPtr.Zero)
				id_a_Lcom_google_android_apps_auto_sdk_nav_state_NavigationSummary_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Lcom_google_android_apps_auto_sdk_nav_state_NavigationSummary_, __args);
			} finally {
			}
		}

		static IntPtr id_a_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='b']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.state.TurnEvent']]"
		[Register ("a", "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V", "")]
		public unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent p0)
		{
			if (id_a_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_ == IntPtr.Zero)
				id_a_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_, __args);
			} finally {
			}
		}

	}
}
