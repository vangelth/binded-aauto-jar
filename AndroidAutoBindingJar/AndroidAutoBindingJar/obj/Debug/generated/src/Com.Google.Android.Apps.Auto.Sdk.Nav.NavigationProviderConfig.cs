using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/NavigationProviderConfig", DoNotGenerateAcw=true)]
	public partial class NavigationProviderConfig : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/NavigationProviderConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (NavigationProviderConfig); }
		}

		protected NavigationProviderConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/constructor[@name='NavigationProviderConfig' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe NavigationProviderConfig ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (NavigationProviderConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_ctor_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/constructor[@name='NavigationProviderConfig' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register (".ctor", "(II)V", "")]
		public unsafe NavigationProviderConfig (int p0, int p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (NavigationProviderConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(II)V", __args);
					return;
				}

				if (id_ctor_II == IntPtr.Zero)
					id_ctor_II = JNIEnv.GetMethodID (class_ref, "<init>", "(II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_II, __args);
			} finally {
			}
		}

		static Delegate cb_getMaxVersion;
#pragma warning disable 0169
		static Delegate GetGetMaxVersionHandler ()
		{
			if (cb_getMaxVersion == null)
				cb_getMaxVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMaxVersion);
			return cb_getMaxVersion;
		}

		static int n_GetMaxVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MaxVersion;
		}
#pragma warning restore 0169

		static IntPtr id_getMaxVersion;
		public virtual unsafe int MaxVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/method[@name='getMaxVersion' and count(parameter)=0]"
			[Register ("getMaxVersion", "()I", "GetGetMaxVersionHandler")]
			get {
				if (id_getMaxVersion == IntPtr.Zero)
					id_getMaxVersion = JNIEnv.GetMethodID (class_ref, "getMaxVersion", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMaxVersion);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMaxVersion", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getMinVersion;
#pragma warning disable 0169
		static Delegate GetGetMinVersionHandler ()
		{
			if (cb_getMinVersion == null)
				cb_getMinVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMinVersion);
			return cb_getMinVersion;
		}

		static int n_GetMinVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MinVersion;
		}
#pragma warning restore 0169

		static IntPtr id_getMinVersion;
		public virtual unsafe int MinVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/method[@name='getMinVersion' and count(parameter)=0]"
			[Register ("getMinVersion", "()I", "GetGetMinVersionHandler")]
			get {
				if (id_getMinVersion == IntPtr.Zero)
					id_getMinVersion = JNIEnv.GetMethodID (class_ref, "getMinVersion", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMinVersion);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMinVersion", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderConfig']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
