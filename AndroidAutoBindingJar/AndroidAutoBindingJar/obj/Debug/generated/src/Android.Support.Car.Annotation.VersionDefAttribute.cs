using System;

namespace Android.Support.Car.Annotation {

	[global::Android.Runtime.Annotation ("android.support.car.annotation.VersionDef")]
	public partial class VersionDefAttribute : Attribute
	{
		[global::Android.Runtime.Register ("version")]
		public int Version { get; set; }

	}
}
