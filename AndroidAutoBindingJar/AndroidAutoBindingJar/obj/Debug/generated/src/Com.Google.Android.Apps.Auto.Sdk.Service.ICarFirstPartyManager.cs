using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager.ScreenshotResultCallback']"
	[Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback", "", "Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallbackInvoker")]
	public partial interface ICarFirstPartyManagerScreenshotResultCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager.ScreenshotResultCallback']/method[@name='onScreenshotResult' and count(parameter)=1 and parameter[1][@type='android.graphics.Bitmap']]"
		[Register ("onScreenshotResult", "(Landroid/graphics/Bitmap;)V", "GetOnScreenshotResult_Landroid_graphics_Bitmap_Handler:Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallbackInvoker, AndroidAutoBindingJar")]
		void OnScreenshotResult (global::Android.Graphics.Bitmap p0);

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback", DoNotGenerateAcw=true)]
	internal class ICarFirstPartyManagerScreenshotResultCallbackInvoker : global::Java.Lang.Object, ICarFirstPartyManagerScreenshotResultCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarFirstPartyManagerScreenshotResultCallbackInvoker); }
		}

		IntPtr class_ref;

		public static ICarFirstPartyManagerScreenshotResultCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarFirstPartyManagerScreenshotResultCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.service.CarFirstPartyManager.ScreenshotResultCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarFirstPartyManagerScreenshotResultCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onScreenshotResult_Landroid_graphics_Bitmap_;
#pragma warning disable 0169
		static Delegate GetOnScreenshotResult_Landroid_graphics_Bitmap_Handler ()
		{
			if (cb_onScreenshotResult_Landroid_graphics_Bitmap_ == null)
				cb_onScreenshotResult_Landroid_graphics_Bitmap_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnScreenshotResult_Landroid_graphics_Bitmap_);
			return cb_onScreenshotResult_Landroid_graphics_Bitmap_;
		}

		static void n_OnScreenshotResult_Landroid_graphics_Bitmap_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Graphics.Bitmap p0 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnScreenshotResult (p0);
		}
#pragma warning restore 0169

		IntPtr id_onScreenshotResult_Landroid_graphics_Bitmap_;
		public unsafe void OnScreenshotResult (global::Android.Graphics.Bitmap p0)
		{
			if (id_onScreenshotResult_Landroid_graphics_Bitmap_ == IntPtr.Zero)
				id_onScreenshotResult_Landroid_graphics_Bitmap_ = JNIEnv.GetMethodID (class_ref, "onScreenshotResult", "(Landroid/graphics/Bitmap;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onScreenshotResult_Landroid_graphics_Bitmap_, __args);
		}

	}


	[Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager", DoNotGenerateAcw=true)]
	public abstract class CarFirstPartyManager : Java.Lang.Object {

		internal CarFirstPartyManager ()
		{
		}

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager']/field[@name='SERVICE_NAME']"
		[Register ("SERVICE_NAME")]
		public const string ServiceName = (string) "car_1p";
	}

	[Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager", DoNotGenerateAcw=true)]
	[global::System.Obsolete ("Use the 'CarFirstPartyManager' type. This type will be removed in a future release.")]
	public abstract class CarFirstPartyManagerConsts : CarFirstPartyManager {

		private CarFirstPartyManagerConsts ()
		{
		}
	}

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager']"
	[Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager", "", "Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerInvoker")]
	public partial interface ICarFirstPartyManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager']/method[@name='captureScreenshot' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.service.CarFirstPartyManager.ScreenshotResultCallback']]"
		[Register ("captureScreenshot", "(Lcom/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback;)V", "GetCaptureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_Handler:Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void CaptureScreenshot (global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager']/method[@name='startCarActivity' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("startCarActivity", "(Landroid/content/Intent;)V", "GetStartCarActivity_Landroid_content_Intent_Handler:Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void StartCarActivity (global::Android.Content.Intent p0);

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager", DoNotGenerateAcw=true)]
	internal class ICarFirstPartyManagerInvoker : global::Java.Lang.Object, ICarFirstPartyManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/CarFirstPartyManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarFirstPartyManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarFirstPartyManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarFirstPartyManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.service.CarFirstPartyManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarFirstPartyManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_;
#pragma warning disable 0169
		static Delegate GetCaptureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_Handler ()
		{
			if (cb_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ == null)
				cb_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_CaptureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_);
			return cb_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_;
		}

		static void n_CaptureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback p0 = (global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CaptureScreenshot (p0);
		}
#pragma warning restore 0169

		IntPtr id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_;
		public unsafe void CaptureScreenshot (global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback p0)
		{
			if (id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ == IntPtr.Zero)
				id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ = JNIEnv.GetMethodID (class_ref, "captureScreenshot", "(Lcom/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_, __args);
		}

		static Delegate cb_startCarActivity_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetStartCarActivity_Landroid_content_Intent_Handler ()
		{
			if (cb_startCarActivity_Landroid_content_Intent_ == null)
				cb_startCarActivity_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartCarActivity_Landroid_content_Intent_);
			return cb_startCarActivity_Landroid_content_Intent_;
		}

		static void n_StartCarActivity_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartCarActivity (p0);
		}
#pragma warning restore 0169

		IntPtr id_startCarActivity_Landroid_content_Intent_;
		public unsafe void StartCarActivity (global::Android.Content.Intent p0)
		{
			if (id_startCarActivity_Landroid_content_Intent_ == IntPtr.Zero)
				id_startCarActivity_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "startCarActivity", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startCarActivity_Landroid_content_Intent_, __args);
		}

	}

}
