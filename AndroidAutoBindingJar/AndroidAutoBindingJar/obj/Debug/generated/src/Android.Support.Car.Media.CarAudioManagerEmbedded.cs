using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Media {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/media/CarAudioManagerEmbedded", DoNotGenerateAcw=true)]
	public partial class CarAudioManagerEmbedded : global::Android.Support.Car.Media.CarAudioManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/media/CarAudioManagerEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioManagerEmbedded); }
		}

		protected CarAudioManagerEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Ljava_lang_Object_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/constructor[@name='CarAudioManagerEmbedded' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register (".ctor", "(Ljava/lang/Object;)V", "")]
		public unsafe CarAudioManagerEmbedded (global::Java.Lang.Object p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (CarAudioManagerEmbedded)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Ljava/lang/Object;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Ljava/lang/Object;)V", __args);
					return;
				}

				if (id_ctor_Ljava_lang_Object_ == IntPtr.Zero)
					id_ctor_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/lang/Object;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_lang_Object_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Ljava_lang_Object_, __args);
			} finally {
			}
		}

		static Delegate cb_getAudioRecordAudioFormat;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordAudioFormatHandler ()
		{
			if (cb_getAudioRecordAudioFormat == null)
				cb_getAudioRecordAudioFormat = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAudioRecordAudioFormat);
			return cb_getAudioRecordAudioFormat;
		}

		static IntPtr n_GetAudioRecordAudioFormat (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.AudioRecordAudioFormat);
		}
#pragma warning restore 0169

		static IntPtr id_getAudioRecordAudioFormat;
		public override unsafe global::Android.Media.AudioFormat AudioRecordAudioFormat {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='getAudioRecordAudioFormat' and count(parameter)=0]"
			[Register ("getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;", "GetGetAudioRecordAudioFormatHandler")]
			get {
				if (id_getAudioRecordAudioFormat == IntPtr.Zero)
					id_getAudioRecordAudioFormat = JNIEnv.GetMethodID (class_ref, "getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Media.AudioFormat> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordAudioFormat), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Media.AudioFormat> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getAudioRecordMaxBufferSize;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordMaxBufferSizeHandler ()
		{
			if (cb_getAudioRecordMaxBufferSize == null)
				cb_getAudioRecordMaxBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetAudioRecordMaxBufferSize);
			return cb_getAudioRecordMaxBufferSize;
		}

		static int n_GetAudioRecordMaxBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AudioRecordMaxBufferSize;
		}
#pragma warning restore 0169

		static IntPtr id_getAudioRecordMaxBufferSize;
		public override unsafe int AudioRecordMaxBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='getAudioRecordMaxBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMaxBufferSize", "()I", "GetGetAudioRecordMaxBufferSizeHandler")]
			get {
				if (id_getAudioRecordMaxBufferSize == IntPtr.Zero)
					id_getAudioRecordMaxBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMaxBufferSize", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMaxBufferSize);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAudioRecordMaxBufferSize", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getAudioRecordMinBufferSize;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordMinBufferSizeHandler ()
		{
			if (cb_getAudioRecordMinBufferSize == null)
				cb_getAudioRecordMinBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetAudioRecordMinBufferSize);
			return cb_getAudioRecordMinBufferSize;
		}

		static int n_GetAudioRecordMinBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AudioRecordMinBufferSize;
		}
#pragma warning restore 0169

		static IntPtr id_getAudioRecordMinBufferSize;
		public override unsafe int AudioRecordMinBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='getAudioRecordMinBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMinBufferSize", "()I", "GetGetAudioRecordMinBufferSizeHandler")]
			get {
				if (id_getAudioRecordMinBufferSize == IntPtr.Zero)
					id_getAudioRecordMinBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMinBufferSize", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMinBufferSize);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAudioRecordMinBufferSize", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_isAudioRecordSupported;
#pragma warning disable 0169
		static Delegate GetIsAudioRecordSupportedHandler ()
		{
			if (cb_isAudioRecordSupported == null)
				cb_isAudioRecordSupported = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsAudioRecordSupported);
			return cb_isAudioRecordSupported;
		}

		static bool n_IsAudioRecordSupported (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsAudioRecordSupported;
		}
#pragma warning restore 0169

		static IntPtr id_isAudioRecordSupported;
		public override unsafe bool IsAudioRecordSupported {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='isAudioRecordSupported' and count(parameter)=0]"
			[Register ("isAudioRecordSupported", "()Z", "GetIsAudioRecordSupportedHandler")]
			get {
				if (id_isAudioRecordSupported == IntPtr.Zero)
					id_isAudioRecordSupported = JNIEnv.GetMethodID (class_ref, "isAudioRecordSupported", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isAudioRecordSupported);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAudioRecordSupported", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isMediaMuted;
#pragma warning disable 0169
		static Delegate GetIsMediaMutedHandler ()
		{
			if (cb_isMediaMuted == null)
				cb_isMediaMuted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsMediaMuted);
			return cb_isMediaMuted;
		}

		static bool n_IsMediaMuted (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsMediaMuted;
		}
#pragma warning restore 0169

		static IntPtr id_isMediaMuted;
		public override unsafe bool IsMediaMuted {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='isMediaMuted' and count(parameter)=0]"
			[Register ("isMediaMuted", "()Z", "GetIsMediaMutedHandler")]
			get {
				if (id_isMediaMuted == IntPtr.Zero)
					id_isMediaMuted = JNIEnv.GetMethodID (class_ref, "isMediaMuted", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isMediaMuted);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isMediaMuted", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
#pragma warning disable 0169
		static Delegate GetAbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_Handler ()
		{
			if (cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ == null)
				cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_);
			return cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
		}

		static void n_AbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAudioFocus (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='abandonAudioFocus' and count(parameter)=2 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes']]"
		[Register ("abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V", "GetAbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_Handler")]
		public override unsafe void AbandonAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1)
		{
			if (id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ == IntPtr.Zero)
				id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ = JNIEnv.GetMethodID (class_ref, "abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_createCarAudioRecord_I;
#pragma warning disable 0169
		static Delegate GetCreateCarAudioRecord_IHandler ()
		{
			if (cb_createCarAudioRecord_I == null)
				cb_createCarAudioRecord_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_CreateCarAudioRecord_I);
			return cb_createCarAudioRecord_I;
		}

		static IntPtr n_CreateCarAudioRecord_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CreateCarAudioRecord (p0));
		}
#pragma warning restore 0169

		static IntPtr id_createCarAudioRecord_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='createCarAudioRecord' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;", "GetCreateCarAudioRecord_IHandler")]
		public override unsafe global::Android.Support.Car.Media.CarAudioRecord CreateCarAudioRecord (int p0)
		{
			if (id_createCarAudioRecord_I == IntPtr.Zero)
				id_createCarAudioRecord_I = JNIEnv.GetMethodID (class_ref, "createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecord> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_createCarAudioRecord_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecord> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getAudioAttributesForCarUsage_I;
#pragma warning disable 0169
		static Delegate GetGetAudioAttributesForCarUsage_IHandler ()
		{
			if (cb_getAudioAttributesForCarUsage_I == null)
				cb_getAudioAttributesForCarUsage_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetAudioAttributesForCarUsage_I);
			return cb_getAudioAttributesForCarUsage_I;
		}

		static IntPtr n_GetAudioAttributesForCarUsage_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetAudioAttributesForCarUsage (p0));
		}
#pragma warning restore 0169

		static IntPtr id_getAudioAttributesForCarUsage_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='getAudioAttributesForCarUsage' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;", "GetGetAudioAttributesForCarUsage_IHandler")]
		public override unsafe global::Android.Media.AudioAttributes GetAudioAttributesForCarUsage (int p0)
		{
			if (id_getAudioAttributesForCarUsage_I == IntPtr.Zero)
				id_getAudioAttributesForCarUsage_I = JNIEnv.GetMethodID (class_ref, "getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioAttributesForCarUsage_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCarDisconnected", "()V"));
			} finally {
			}
		}

		static Delegate cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
#pragma warning disable 0169
		static Delegate GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IHandler ()
		{
			if (cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I == null)
				cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, int>) n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I);
			return cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
		}

		static int n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAudioFocus (p0, p1, p2);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='requestAudioFocus' and count(parameter)=3 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IHandler")]
		public override unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
#pragma warning disable 0169
		static Delegate GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IIHandler ()
		{
			if (cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II == null)
				cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, int, int>) n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II);
			return cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
		}

		static int n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2, int p3)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAudioFocus (p0, p1, p2, p3);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='requestAudioFocus' and count(parameter)=4 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IIHandler")]
		public override unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2, int p3)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_setMediaMute_Z;
#pragma warning disable 0169
		static Delegate GetSetMediaMute_ZHandler ()
		{
			if (cb_setMediaMute_Z == null)
				cb_setMediaMute_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, bool>) n_SetMediaMute_Z);
			return cb_setMediaMute_Z;
		}

		static bool n_SetMediaMute_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Android.Support.Car.Media.CarAudioManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SetMediaMute (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setMediaMute_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManagerEmbedded']/method[@name='setMediaMute' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setMediaMute", "(Z)Z", "GetSetMediaMute_ZHandler")]
		public override unsafe bool SetMediaMute (bool p0)
		{
			if (id_setMediaMute_Z == IntPtr.Zero)
				id_setMediaMute_Z = JNIEnv.GetMethodID (class_ref, "setMediaMute", "(Z)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_setMediaMute_Z, __args);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMediaMute", "(Z)Z"), __args);
			} finally {
			}
		}

	}
}
