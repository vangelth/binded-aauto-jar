using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.State {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/CarInstrumentClusterConfig", DoNotGenerateAcw=true)]
	public partial class CarInstrumentClusterConfig : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/CarInstrumentClusterConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInstrumentClusterConfig); }
		}

		protected CarInstrumentClusterConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/constructor[@name='CarInstrumentClusterConfig' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarInstrumentClusterConfig ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarInstrumentClusterConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getImageDepthBits;
#pragma warning disable 0169
		static Delegate GetGetImageDepthBitsHandler ()
		{
			if (cb_getImageDepthBits == null)
				cb_getImageDepthBits = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageDepthBits);
			return cb_getImageDepthBits;
		}

		static int n_GetImageDepthBits (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageDepthBits;
		}
#pragma warning restore 0169

		static IntPtr id_getImageDepthBits;
		public virtual unsafe int ImageDepthBits {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='getImageDepthBits' and count(parameter)=0]"
			[Register ("getImageDepthBits", "()I", "GetGetImageDepthBitsHandler")]
			get {
				if (id_getImageDepthBits == IntPtr.Zero)
					id_getImageDepthBits = JNIEnv.GetMethodID (class_ref, "getImageDepthBits", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageDepthBits);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageDepthBits", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getImageHeight;
#pragma warning disable 0169
		static Delegate GetGetImageHeightHandler ()
		{
			if (cb_getImageHeight == null)
				cb_getImageHeight = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageHeight);
			return cb_getImageHeight;
		}

		static int n_GetImageHeight (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageHeight;
		}
#pragma warning restore 0169

		static IntPtr id_getImageHeight;
		public virtual unsafe int ImageHeight {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='getImageHeight' and count(parameter)=0]"
			[Register ("getImageHeight", "()I", "GetGetImageHeightHandler")]
			get {
				if (id_getImageHeight == IntPtr.Zero)
					id_getImageHeight = JNIEnv.GetMethodID (class_ref, "getImageHeight", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageHeight);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageHeight", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getImageWidth;
#pragma warning disable 0169
		static Delegate GetGetImageWidthHandler ()
		{
			if (cb_getImageWidth == null)
				cb_getImageWidth = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageWidth);
			return cb_getImageWidth;
		}

		static int n_GetImageWidth (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageWidth;
		}
#pragma warning restore 0169

		static IntPtr id_getImageWidth;
		public virtual unsafe int ImageWidth {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='getImageWidth' and count(parameter)=0]"
			[Register ("getImageWidth", "()I", "GetGetImageWidthHandler")]
			get {
				if (id_getImageWidth == IntPtr.Zero)
					id_getImageWidth = JNIEnv.GetMethodID (class_ref, "getImageWidth", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageWidth);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageWidth", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getMinMessageIntervalMs;
#pragma warning disable 0169
		static Delegate GetGetMinMessageIntervalMsHandler ()
		{
			if (cb_getMinMessageIntervalMs == null)
				cb_getMinMessageIntervalMs = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMinMessageIntervalMs);
			return cb_getMinMessageIntervalMs;
		}

		static int n_GetMinMessageIntervalMs (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MinMessageIntervalMs;
		}
#pragma warning restore 0169

		static IntPtr id_getMinMessageIntervalMs;
		public virtual unsafe int MinMessageIntervalMs {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='getMinMessageIntervalMs' and count(parameter)=0]"
			[Register ("getMinMessageIntervalMs", "()I", "GetGetMinMessageIntervalMsHandler")]
			get {
				if (id_getMinMessageIntervalMs == IntPtr.Zero)
					id_getMinMessageIntervalMs = JNIEnv.GetMethodID (class_ref, "getMinMessageIntervalMs", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMinMessageIntervalMs);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMinMessageIntervalMs", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_supportsImages;
#pragma warning disable 0169
		static Delegate GetSupportsImagesHandler ()
		{
			if (cb_supportsImages == null)
				cb_supportsImages = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_SupportsImages);
			return cb_supportsImages;
		}

		static bool n_SupportsImages (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SupportsImages ();
		}
#pragma warning restore 0169

		static IntPtr id_supportsImages;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='supportsImages' and count(parameter)=0]"
		[Register ("supportsImages", "()Z", "GetSupportsImagesHandler")]
		public virtual unsafe bool SupportsImages ()
		{
			if (id_supportsImages == IntPtr.Zero)
				id_supportsImages = JNIEnv.GetMethodID (class_ref, "supportsImages", "()Z");
			try {

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_supportsImages);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "supportsImages", "()Z"));
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.CarInstrumentClusterConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='CarInstrumentClusterConfig']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
