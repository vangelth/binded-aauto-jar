using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='c']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/c", DoNotGenerateAcw=true)]
	public sealed partial class C : global::Java.Lang.Object, global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManager {


		public static class InterfaceConsts {

			// The following are fields from: com.google.android.apps.auto.sdk.service.CarFirstPartyManager

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/interface[@name='CarFirstPartyManager']/field[@name='SERVICE_NAME']"
			[Register ("SERVICE_NAME")]
			public const string ServiceName = (string) "car_1p";
		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/c", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (C); }
		}

		internal C (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarFirstPartyManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='c']/constructor[@name='c' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarFirstPartyManager']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarFirstPartyManager;)V", "")]
		public unsafe C (global::Com.Google.Android.Gms.Car.ICarFirstPartyManager p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (C)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarFirstPartyManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarFirstPartyManager;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarFirstPartyManager_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarFirstPartyManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarFirstPartyManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarFirstPartyManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarFirstPartyManager_, __args);
			} finally {
			}
		}

		static IntPtr id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='c']/method[@name='captureScreenshot' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.service.CarFirstPartyManager.ScreenshotResultCallback']]"
		[Register ("captureScreenshot", "(Lcom/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback;)V", "")]
		public unsafe void CaptureScreenshot (global::Com.Google.Android.Apps.Auto.Sdk.Service.ICarFirstPartyManagerScreenshotResultCallback p0)
		{
			if (id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ == IntPtr.Zero)
				id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_ = JNIEnv.GetMethodID (class_ref, "captureScreenshot", "(Lcom/google/android/apps/auto/sdk/service/CarFirstPartyManager$ScreenshotResultCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_captureScreenshot_Lcom_google_android_apps_auto_sdk_service_CarFirstPartyManager_ScreenshotResultCallback_, __args);
			} finally {
			}
		}

		static IntPtr id_startCarActivity_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='c']/method[@name='startCarActivity' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("startCarActivity", "(Landroid/content/Intent;)V", "")]
		public unsafe void StartCarActivity (global::Android.Content.Intent p0)
		{
			if (id_startCarActivity_Landroid_content_Intent_ == IntPtr.Zero)
				id_startCarActivity_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "startCarActivity", "(Landroid/content/Intent;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startCarActivity_Landroid_content_Intent_, __args);
			} finally {
			}
		}

	}
}
