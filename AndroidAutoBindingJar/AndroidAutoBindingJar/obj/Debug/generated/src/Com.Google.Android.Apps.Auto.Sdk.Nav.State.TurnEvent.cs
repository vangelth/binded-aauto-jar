using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.State {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent", DoNotGenerateAcw=true)]
	public partial class TurnEvent : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder", DoNotGenerateAcw=true)]
		public partial class Builder : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Builder); }
			}

			protected Builder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/constructor[@name='TurnEvent.Builder' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe Builder ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static IntPtr id_ctor_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/constructor[@name='TurnEvent.Builder' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.state.TurnEvent']]"
			[Register (".ctor", "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V", "")]
			public unsafe Builder (global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent p0)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V", __args);
						return;
					}

					if (id_ctor_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_ == IntPtr.Zero)
						id_ctor_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_apps_auto_sdk_nav_state_TurnEvent_, __args);
				} finally {
				}
			}

			static Delegate cb_build;
#pragma warning disable 0169
			static Delegate GetBuildHandler ()
			{
				if (cb_build == null)
					cb_build = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Build);
				return cb_build;
			}

			static IntPtr n_Build (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.Build ());
			}
#pragma warning restore 0169

			static IntPtr id_build;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='build' and count(parameter)=0]"
			[Register ("build", "()Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;", "GetBuildHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent Build ()
			{
				if (id_build == IntPtr.Zero)
					id_build = JNIEnv.GetMethodID (class_ref, "build", "()Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_build), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "build", "()Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setRoundaboutTurnEvent_II;
#pragma warning disable 0169
			static Delegate GetSetRoundaboutTurnEvent_IIHandler ()
			{
				if (cb_setRoundaboutTurnEvent_II == null)
					cb_setRoundaboutTurnEvent_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, IntPtr>) n_SetRoundaboutTurnEvent_II);
				return cb_setRoundaboutTurnEvent_II;
			}

			static IntPtr n_SetRoundaboutTurnEvent_II (IntPtr jnienv, IntPtr native__this, int p0, int p1)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetRoundaboutTurnEvent (p0, p1));
			}
#pragma warning restore 0169

			static IntPtr id_setRoundaboutTurnEvent_II;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setRoundaboutTurnEvent' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
			[Register ("setRoundaboutTurnEvent", "(II)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetRoundaboutTurnEvent_IIHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetRoundaboutTurnEvent (int p0, int p1)
			{
				if (id_setRoundaboutTurnEvent_II == IntPtr.Zero)
					id_setRoundaboutTurnEvent_II = JNIEnv.GetMethodID (class_ref, "setRoundaboutTurnEvent", "(II)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setRoundaboutTurnEvent_II, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRoundaboutTurnEvent", "(II)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setSecondsUntilTurnEvent_I;
#pragma warning disable 0169
			static Delegate GetSetSecondsUntilTurnEvent_IHandler ()
			{
				if (cb_setSecondsUntilTurnEvent_I == null)
					cb_setSecondsUntilTurnEvent_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetSecondsUntilTurnEvent_I);
				return cb_setSecondsUntilTurnEvent_I;
			}

			static IntPtr n_SetSecondsUntilTurnEvent_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetSecondsUntilTurnEvent (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setSecondsUntilTurnEvent_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setSecondsUntilTurnEvent' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setSecondsUntilTurnEvent", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetSecondsUntilTurnEvent_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetSecondsUntilTurnEvent (int p0)
			{
				if (id_setSecondsUntilTurnEvent_I == IntPtr.Zero)
					id_setSecondsUntilTurnEvent_I = JNIEnv.GetMethodID (class_ref, "setSecondsUntilTurnEvent", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setSecondsUntilTurnEvent_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSecondsUntilTurnEvent", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTurnDisplayDistanceE3_I;
#pragma warning disable 0169
			static Delegate GetSetTurnDisplayDistanceE3_IHandler ()
			{
				if (cb_setTurnDisplayDistanceE3_I == null)
					cb_setTurnDisplayDistanceE3_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTurnDisplayDistanceE3_I);
				return cb_setTurnDisplayDistanceE3_I;
			}

			static IntPtr n_SetTurnDisplayDistanceE3_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTurnDisplayDistanceE3 (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTurnDisplayDistanceE3_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnDisplayDistanceE3' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTurnDisplayDistanceE3", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnDisplayDistanceE3_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnDisplayDistanceE3 (int p0)
			{
				if (id_setTurnDisplayDistanceE3_I == IntPtr.Zero)
					id_setTurnDisplayDistanceE3_I = JNIEnv.GetMethodID (class_ref, "setTurnDisplayDistanceE3", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnDisplayDistanceE3_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnDisplayDistanceE3", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTurnDistanceMeters_I;
#pragma warning disable 0169
			static Delegate GetSetTurnDistanceMeters_IHandler ()
			{
				if (cb_setTurnDistanceMeters_I == null)
					cb_setTurnDistanceMeters_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTurnDistanceMeters_I);
				return cb_setTurnDistanceMeters_I;
			}

			static IntPtr n_SetTurnDistanceMeters_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTurnDistanceMeters (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTurnDistanceMeters_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnDistanceMeters' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTurnDistanceMeters", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnDistanceMeters_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnDistanceMeters (int p0)
			{
				if (id_setTurnDistanceMeters_I == IntPtr.Zero)
					id_setTurnDistanceMeters_I = JNIEnv.GetMethodID (class_ref, "setTurnDistanceMeters", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnDistanceMeters_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnDistanceMeters", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTurnDistanceUnit_I;
#pragma warning disable 0169
			static Delegate GetSetTurnDistanceUnit_IHandler ()
			{
				if (cb_setTurnDistanceUnit_I == null)
					cb_setTurnDistanceUnit_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTurnDistanceUnit_I);
				return cb_setTurnDistanceUnit_I;
			}

			static IntPtr n_SetTurnDistanceUnit_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTurnDistanceUnit (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTurnDistanceUnit_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnDistanceUnit' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTurnDistanceUnit", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnDistanceUnit_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnDistanceUnit (int p0)
			{
				if (id_setTurnDistanceUnit_I == IntPtr.Zero)
					id_setTurnDistanceUnit_I = JNIEnv.GetMethodID (class_ref, "setTurnDistanceUnit", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnDistanceUnit_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnDistanceUnit", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTurnEventRoadName_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetTurnEventRoadName_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setTurnEventRoadName_Ljava_lang_CharSequence_ == null)
					cb_setTurnEventRoadName_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetTurnEventRoadName_Ljava_lang_CharSequence_);
				return cb_setTurnEventRoadName_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetTurnEventRoadName_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetTurnEventRoadName (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setTurnEventRoadName_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnEventRoadName' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setTurnEventRoadName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnEventRoadName_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnEventRoadName (global::Java.Lang.ICharSequence p0)
			{
				if (id_setTurnEventRoadName_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setTurnEventRoadName_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setTurnEventRoadName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnEventRoadName_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnEventRoadName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnEventRoadName (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __result = SetTurnEventRoadName (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setTurnEventType_I;
#pragma warning disable 0169
			static Delegate GetSetTurnEventType_IHandler ()
			{
				if (cb_setTurnEventType_I == null)
					cb_setTurnEventType_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTurnEventType_I);
				return cb_setTurnEventType_I;
			}

			static IntPtr n_SetTurnEventType_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTurnEventType (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTurnEventType_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnEventType' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTurnEventType", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnEventType_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnEventType (int p0)
			{
				if (id_setTurnEventType_I == IntPtr.Zero)
					id_setTurnEventType_I = JNIEnv.GetMethodID (class_ref, "setTurnEventType", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnEventType_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnEventType", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTurnImage_arrayB;
#pragma warning disable 0169
			static Delegate GetSetTurnImage_arrayBHandler ()
			{
				if (cb_setTurnImage_arrayB == null)
					cb_setTurnImage_arrayB = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetTurnImage_arrayB);
				return cb_setTurnImage_arrayB;
			}

			static IntPtr n_SetTurnImage_arrayB (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetTurnImage (p0));
				if (p0 != null)
					JNIEnv.CopyArray (p0, native_p0);
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setTurnImage_arrayB;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnImage' and count(parameter)=1 and parameter[1][@type='byte[]']]"
			[Register ("setTurnImage", "([B)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnImage_arrayBHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnImage (byte[] p0)
			{
				if (id_setTurnImage_arrayB == IntPtr.Zero)
					id_setTurnImage_arrayB = JNIEnv.GetMethodID (class_ref, "setTurnImage", "([B)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				IntPtr native_p0 = JNIEnv.NewArray (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnImage_arrayB, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnImage", "([B)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					if (p0 != null) {
						JNIEnv.CopyArray (native_p0, p0);
						JNIEnv.DeleteLocalRef (native_p0);
					}
				}
			}

			static Delegate cb_setTurnSide_I;
#pragma warning disable 0169
			static Delegate GetSetTurnSide_IHandler ()
			{
				if (cb_setTurnSide_I == null)
					cb_setTurnSide_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTurnSide_I);
				return cb_setTurnSide_I;
			}

			static IntPtr n_SetTurnSide_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTurnSide (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTurnSide_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent.Builder']/method[@name='setTurnSide' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTurnSide", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;", "GetSetTurnSide_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder SetTurnSide (int p0)
			{
				if (id_setTurnSide_I == IntPtr.Zero)
					id_setTurnSide_I = JNIEnv.GetMethodID (class_ref, "setTurnSide", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTurnSide_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTurnSide", "(I)Lcom/google/android/apps/auto/sdk/nav/state/TurnEvent$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$DistanceUnit", DoNotGenerateAcw=true)]
		public abstract class DistanceUnit : Java.Lang.Object {

			internal DistanceUnit ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='FEET']"
			[Register ("FEET")]
			public const int Feet = (int) 6;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='KILOMETERS']"
			[Register ("KILOMETERS")]
			public const int Kilometers = (int) 2;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='KILOMETERS_P1']"
			[Register ("KILOMETERS_P1")]
			public const int KilometersP1 = (int) 3;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='METERS']"
			[Register ("METERS")]
			public const int Meters = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='MILES']"
			[Register ("MILES")]
			public const int Miles = (int) 4;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='MILES_P1']"
			[Register ("MILES_P1")]
			public const int MilesP1 = (int) 5;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='UNKNOWN']"
			[Register ("UNKNOWN")]
			public const int Unknown = (int) 0;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']/field[@name='YARDS']"
			[Register ("YARDS")]
			public const int Yards = (int) 7;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$DistanceUnit", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'DistanceUnit' type. This type will be removed in a future release.")]
		public abstract class DistanceUnitConsts : DistanceUnit {

			private DistanceUnitConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.DistanceUnit']"
		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$DistanceUnit", "", "Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent/IDistanceUnitInvoker")]
		public partial interface IDistanceUnit : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$DistanceUnit", DoNotGenerateAcw=true)]
		internal class IDistanceUnitInvoker : global::Java.Lang.Object, IDistanceUnit {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$DistanceUnit");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IDistanceUnitInvoker); }
			}

			IntPtr class_ref;

			public static IDistanceUnit GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IDistanceUnit> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.nav.state.TurnEvent.DistanceUnit"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IDistanceUnitInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnEventType", DoNotGenerateAcw=true)]
		public abstract class TurnEventType : Java.Lang.Object {

			internal TurnEventType ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='DEPART']"
			[Register ("DEPART")]
			public const int Depart = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='DESTINATION']"
			[Register ("DESTINATION")]
			public const int Destination = (int) 19;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='FERRY_BOAT']"
			[Register ("FERRY_BOAT")]
			public const int FerryBoat = (int) 16;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='FERRY_TRAIN']"
			[Register ("FERRY_TRAIN")]
			public const int FerryTrain = (int) 17;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='FORK']"
			[Register ("FORK")]
			public const int Fork = (int) 9;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='MERGE']"
			[Register ("MERGE")]
			public const int Merge = (int) 10;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='NAME_CHANGE']"
			[Register ("NAME_CHANGE")]
			public const int NameChange = (int) 2;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='OFF_RAMP']"
			[Register ("OFF_RAMP")]
			public const int OffRamp = (int) 8;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='ON_RAMP']"
			[Register ("ON_RAMP")]
			public const int OnRamp = (int) 7;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='ROUNDABOUT_ENTER']"
			[Register ("ROUNDABOUT_ENTER")]
			public const int RoundaboutEnter = (int) 11;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='ROUNDABOUT_ENTER_AND_EXIT']"
			[Register ("ROUNDABOUT_ENTER_AND_EXIT")]
			public const int RoundaboutEnterAndExit = (int) 13;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='ROUNDABOUT_EXIT']"
			[Register ("ROUNDABOUT_EXIT")]
			public const int RoundaboutExit = (int) 12;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='SHARP_TURN']"
			[Register ("SHARP_TURN")]
			public const int SharpTurn = (int) 5;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='SLIGHT_TURN']"
			[Register ("SLIGHT_TURN")]
			public const int SlightTurn = (int) 3;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='STRAIGHT']"
			[Register ("STRAIGHT")]
			public const int Straight = (int) 14;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='TURN']"
			[Register ("TURN")]
			public const int Turn = (int) 4;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='UNKNOWN']"
			[Register ("UNKNOWN")]
			public const int Unknown = (int) 0;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']/field[@name='U_TURN']"
			[Register ("U_TURN")]
			public const int UTurn = (int) 6;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnEventType", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'TurnEventType' type. This type will be removed in a future release.")]
		public abstract class TurnEventTypeConsts : TurnEventType {

			private TurnEventTypeConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnEventType']"
		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnEventType", "", "Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent/ITurnEventTypeInvoker")]
		public partial interface ITurnEventType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnEventType", DoNotGenerateAcw=true)]
		internal class ITurnEventTypeInvoker : global::Java.Lang.Object, ITurnEventType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnEventType");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITurnEventTypeInvoker); }
			}

			IntPtr class_ref;

			public static ITurnEventType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ITurnEventType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.nav.state.TurnEvent.TurnEventType"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITurnEventTypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnEventType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnSide", DoNotGenerateAcw=true)]
		public abstract class TurnSide : Java.Lang.Object {

			internal TurnSide ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnSide']/field[@name='LEFT']"
			[Register ("LEFT")]
			public const int Left = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnSide']/field[@name='RIGHT']"
			[Register ("RIGHT")]
			public const int Right = (int) 2;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnSide']/field[@name='UNKNOWN']"
			[Register ("UNKNOWN")]
			public const int Unknown = (int) 0;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnSide", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'TurnSide' type. This type will be removed in a future release.")]
		public abstract class TurnSideConsts : TurnSide {

			private TurnSideConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='TurnEvent.TurnSide']"
		[Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnSide", "", "Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent/ITurnSideInvoker")]
		public partial interface ITurnSide : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnSide", DoNotGenerateAcw=true)]
		internal class ITurnSideInvoker : global::Java.Lang.Object, ITurnSide {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/TurnEvent$TurnSide");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITurnSideInvoker); }
			}

			IntPtr class_ref;

			public static ITurnSide GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ITurnSide> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.nav.state.TurnEvent.TurnSide"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITurnSideInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/TurnEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (TurnEvent); }
		}

		protected TurnEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/constructor[@name='TurnEvent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe TurnEvent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (TurnEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_hasSecondsUntilTurnEvent;
#pragma warning disable 0169
		static Delegate GetHasSecondsUntilTurnEventHandler ()
		{
			if (cb_hasSecondsUntilTurnEvent == null)
				cb_hasSecondsUntilTurnEvent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasSecondsUntilTurnEvent);
			return cb_hasSecondsUntilTurnEvent;
		}

		static bool n_HasSecondsUntilTurnEvent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasSecondsUntilTurnEvent;
		}
#pragma warning restore 0169

		static IntPtr id_hasSecondsUntilTurnEvent;
		public virtual unsafe bool HasSecondsUntilTurnEvent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='hasSecondsUntilTurnEvent' and count(parameter)=0]"
			[Register ("hasSecondsUntilTurnEvent", "()Z", "GetHasSecondsUntilTurnEventHandler")]
			get {
				if (id_hasSecondsUntilTurnEvent == IntPtr.Zero)
					id_hasSecondsUntilTurnEvent = JNIEnv.GetMethodID (class_ref, "hasSecondsUntilTurnEvent", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasSecondsUntilTurnEvent);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasSecondsUntilTurnEvent", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_hasTurnAngle;
#pragma warning disable 0169
		static Delegate GetHasTurnAngleHandler ()
		{
			if (cb_hasTurnAngle == null)
				cb_hasTurnAngle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTurnAngle);
			return cb_hasTurnAngle;
		}

		static bool n_HasTurnAngle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTurnAngle;
		}
#pragma warning restore 0169

		static IntPtr id_hasTurnAngle;
		public virtual unsafe bool HasTurnAngle {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='hasTurnAngle' and count(parameter)=0]"
			[Register ("hasTurnAngle", "()Z", "GetHasTurnAngleHandler")]
			get {
				if (id_hasTurnAngle == IntPtr.Zero)
					id_hasTurnAngle = JNIEnv.GetMethodID (class_ref, "hasTurnAngle", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTurnAngle);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasTurnAngle", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_hasTurnDisplayDistanceE3;
#pragma warning disable 0169
		static Delegate GetHasTurnDisplayDistanceE3Handler ()
		{
			if (cb_hasTurnDisplayDistanceE3 == null)
				cb_hasTurnDisplayDistanceE3 = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTurnDisplayDistanceE3);
			return cb_hasTurnDisplayDistanceE3;
		}

		static bool n_HasTurnDisplayDistanceE3 (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTurnDisplayDistanceE3;
		}
#pragma warning restore 0169

		static IntPtr id_hasTurnDisplayDistanceE3;
		public virtual unsafe bool HasTurnDisplayDistanceE3 {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='hasTurnDisplayDistanceE3' and count(parameter)=0]"
			[Register ("hasTurnDisplayDistanceE3", "()Z", "GetHasTurnDisplayDistanceE3Handler")]
			get {
				if (id_hasTurnDisplayDistanceE3 == IntPtr.Zero)
					id_hasTurnDisplayDistanceE3 = JNIEnv.GetMethodID (class_ref, "hasTurnDisplayDistanceE3", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTurnDisplayDistanceE3);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasTurnDisplayDistanceE3", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_hasTurnDistanceMeters;
#pragma warning disable 0169
		static Delegate GetHasTurnDistanceMetersHandler ()
		{
			if (cb_hasTurnDistanceMeters == null)
				cb_hasTurnDistanceMeters = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTurnDistanceMeters);
			return cb_hasTurnDistanceMeters;
		}

		static bool n_HasTurnDistanceMeters (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTurnDistanceMeters;
		}
#pragma warning restore 0169

		static IntPtr id_hasTurnDistanceMeters;
		public virtual unsafe bool HasTurnDistanceMeters {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='hasTurnDistanceMeters' and count(parameter)=0]"
			[Register ("hasTurnDistanceMeters", "()Z", "GetHasTurnDistanceMetersHandler")]
			get {
				if (id_hasTurnDistanceMeters == IntPtr.Zero)
					id_hasTurnDistanceMeters = JNIEnv.GetMethodID (class_ref, "hasTurnDistanceMeters", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTurnDistanceMeters);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasTurnDistanceMeters", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_hasTurnNumber;
#pragma warning disable 0169
		static Delegate GetHasTurnNumberHandler ()
		{
			if (cb_hasTurnNumber == null)
				cb_hasTurnNumber = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTurnNumber);
			return cb_hasTurnNumber;
		}

		static bool n_HasTurnNumber (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTurnNumber;
		}
#pragma warning restore 0169

		static IntPtr id_hasTurnNumber;
		public virtual unsafe bool HasTurnNumber {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='hasTurnNumber' and count(parameter)=0]"
			[Register ("hasTurnNumber", "()Z", "GetHasTurnNumberHandler")]
			get {
				if (id_hasTurnNumber == IntPtr.Zero)
					id_hasTurnNumber = JNIEnv.GetMethodID (class_ref, "hasTurnNumber", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTurnNumber);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasTurnNumber", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getSecondsUntilTurnEvent;
#pragma warning disable 0169
		static Delegate GetGetSecondsUntilTurnEventHandler ()
		{
			if (cb_getSecondsUntilTurnEvent == null)
				cb_getSecondsUntilTurnEvent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetSecondsUntilTurnEvent);
			return cb_getSecondsUntilTurnEvent;
		}

		static int n_GetSecondsUntilTurnEvent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SecondsUntilTurnEvent;
		}
#pragma warning restore 0169

		static IntPtr id_getSecondsUntilTurnEvent;
		public virtual unsafe int SecondsUntilTurnEvent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getSecondsUntilTurnEvent' and count(parameter)=0]"
			[Register ("getSecondsUntilTurnEvent", "()I", "GetGetSecondsUntilTurnEventHandler")]
			get {
				if (id_getSecondsUntilTurnEvent == IntPtr.Zero)
					id_getSecondsUntilTurnEvent = JNIEnv.GetMethodID (class_ref, "getSecondsUntilTurnEvent", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getSecondsUntilTurnEvent);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSecondsUntilTurnEvent", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnAngle;
#pragma warning disable 0169
		static Delegate GetGetTurnAngleHandler ()
		{
			if (cb_getTurnAngle == null)
				cb_getTurnAngle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTurnAngle);
			return cb_getTurnAngle;
		}

		static int n_GetTurnAngle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TurnAngle;
		}
#pragma warning restore 0169

		static IntPtr id_getTurnAngle;
		public virtual unsafe int TurnAngle {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnAngle' and count(parameter)=0]"
			[Register ("getTurnAngle", "()I", "GetGetTurnAngleHandler")]
			get {
				if (id_getTurnAngle == IntPtr.Zero)
					id_getTurnAngle = JNIEnv.GetMethodID (class_ref, "getTurnAngle", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTurnAngle);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnAngle", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnDisplayDistanceE3;
#pragma warning disable 0169
		static Delegate GetGetTurnDisplayDistanceE3Handler ()
		{
			if (cb_getTurnDisplayDistanceE3 == null)
				cb_getTurnDisplayDistanceE3 = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTurnDisplayDistanceE3);
			return cb_getTurnDisplayDistanceE3;
		}

		static int n_GetTurnDisplayDistanceE3 (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TurnDisplayDistanceE3;
		}
#pragma warning restore 0169

		static IntPtr id_getTurnDisplayDistanceE3;
		public virtual unsafe int TurnDisplayDistanceE3 {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnDisplayDistanceE3' and count(parameter)=0]"
			[Register ("getTurnDisplayDistanceE3", "()I", "GetGetTurnDisplayDistanceE3Handler")]
			get {
				if (id_getTurnDisplayDistanceE3 == IntPtr.Zero)
					id_getTurnDisplayDistanceE3 = JNIEnv.GetMethodID (class_ref, "getTurnDisplayDistanceE3", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTurnDisplayDistanceE3);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnDisplayDistanceE3", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnDistanceMeters;
#pragma warning disable 0169
		static Delegate GetGetTurnDistanceMetersHandler ()
		{
			if (cb_getTurnDistanceMeters == null)
				cb_getTurnDistanceMeters = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTurnDistanceMeters);
			return cb_getTurnDistanceMeters;
		}

		static int n_GetTurnDistanceMeters (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TurnDistanceMeters;
		}
#pragma warning restore 0169

		static IntPtr id_getTurnDistanceMeters;
		public virtual unsafe int TurnDistanceMeters {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnDistanceMeters' and count(parameter)=0]"
			[Register ("getTurnDistanceMeters", "()I", "GetGetTurnDistanceMetersHandler")]
			get {
				if (id_getTurnDistanceMeters == IntPtr.Zero)
					id_getTurnDistanceMeters = JNIEnv.GetMethodID (class_ref, "getTurnDistanceMeters", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTurnDistanceMeters);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnDistanceMeters", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnDistanceUnit;
#pragma warning disable 0169
		static Delegate GetGetTurnDistanceUnitHandler ()
		{
			if (cb_getTurnDistanceUnit == null)
				cb_getTurnDistanceUnit = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTurnDistanceUnit);
			return cb_getTurnDistanceUnit;
		}

		static int n_GetTurnDistanceUnit (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TurnDistanceUnit;
		}
#pragma warning restore 0169

		static IntPtr id_getTurnDistanceUnit;
		public virtual unsafe int TurnDistanceUnit {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnDistanceUnit' and count(parameter)=0]"
			[Register ("getTurnDistanceUnit", "()I", "GetGetTurnDistanceUnitHandler")]
			get {
				if (id_getTurnDistanceUnit == IntPtr.Zero)
					id_getTurnDistanceUnit = JNIEnv.GetMethodID (class_ref, "getTurnDistanceUnit", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTurnDistanceUnit);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnDistanceUnit", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnEventRoadName;
#pragma warning disable 0169
		static Delegate GetGetTurnEventRoadNameHandler ()
		{
			if (cb_getTurnEventRoadName == null)
				cb_getTurnEventRoadName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTurnEventRoadName);
			return cb_getTurnEventRoadName;
		}

		static IntPtr n_GetTurnEventRoadName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.TurnEventRoadNameFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getTurnEventRoadName;
		public virtual unsafe global::Java.Lang.ICharSequence TurnEventRoadNameFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnEventRoadName' and count(parameter)=0]"
			[Register ("getTurnEventRoadName", "()Ljava/lang/CharSequence;", "GetGetTurnEventRoadNameHandler")]
			get {
				if (id_getTurnEventRoadName == IntPtr.Zero)
					id_getTurnEventRoadName = JNIEnv.GetMethodID (class_ref, "getTurnEventRoadName", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getTurnEventRoadName), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnEventRoadName", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string TurnEventRoadName {
			get { return TurnEventRoadNameFormatted == null ? null : TurnEventRoadNameFormatted.ToString (); }
		}

		static Delegate cb_getTurnNumber;
#pragma warning disable 0169
		static Delegate GetGetTurnNumberHandler ()
		{
			if (cb_getTurnNumber == null)
				cb_getTurnNumber = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTurnNumber);
			return cb_getTurnNumber;
		}

		static int n_GetTurnNumber (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TurnNumber;
		}
#pragma warning restore 0169

		static IntPtr id_getTurnNumber;
		public virtual unsafe int TurnNumber {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnNumber' and count(parameter)=0]"
			[Register ("getTurnNumber", "()I", "GetGetTurnNumberHandler")]
			get {
				if (id_getTurnNumber == IntPtr.Zero)
					id_getTurnNumber = JNIEnv.GetMethodID (class_ref, "getTurnNumber", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTurnNumber);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnNumber", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getTurnImage;
#pragma warning disable 0169
		static Delegate GetGetTurnImageHandler ()
		{
			if (cb_getTurnImage == null)
				cb_getTurnImage = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTurnImage);
			return cb_getTurnImage;
		}

		static IntPtr n_GetTurnImage (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetTurnImage ());
		}
#pragma warning restore 0169

		static IntPtr id_getTurnImage;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='getTurnImage' and count(parameter)=0]"
		[Register ("getTurnImage", "()[B", "GetGetTurnImageHandler")]
		public virtual unsafe byte[] GetTurnImage ()
		{
			if (id_getTurnImage == IntPtr.Zero)
				id_getTurnImage = JNIEnv.GetMethodID (class_ref, "getTurnImage", "()[B");
			try {

				if (((object) this).GetType () == ThresholdType)
					return (byte[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getTurnImage), JniHandleOwnership.TransferLocalRef, typeof (byte));
				else
					return (byte[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTurnImage", "()[B")), JniHandleOwnership.TransferLocalRef, typeof (byte));
			} finally {
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.TurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='TurnEvent']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
