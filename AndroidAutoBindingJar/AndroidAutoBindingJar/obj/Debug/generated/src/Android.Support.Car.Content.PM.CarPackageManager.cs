using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Content.PM {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']"
	[global::Android.Runtime.Register ("android/support/car/content/pm/CarPackageManager", DoNotGenerateAcw=true)]
	public abstract partial class CarPackageManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/content/pm/CarPackageManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarPackageManager); }
		}

		protected CarPackageManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']/constructor[@name='CarPackageManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarPackageManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarPackageManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetIsActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler ()
		{
			if (cb_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ == null)
				cb_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, bool>) n_IsActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_);
			return cb_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
		}

		static bool n_IsActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Content.PM.CarPackageManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Content.PM.CarPackageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsActivityAllowedWhileDriving (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']/method[@name='isActivityAllowedWhileDriving' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("isActivityAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z", "GetIsActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler")]
		public abstract bool IsActivityAllowedWhileDriving (string p0, string p1);

		static Delegate cb_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetIsServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler ()
		{
			if (cb_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ == null)
				cb_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, bool>) n_IsServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_);
			return cb_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
		}

		static bool n_IsServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Content.PM.CarPackageManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Content.PM.CarPackageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsServiceAllowedWhileDriving (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']/method[@name='isServiceAllowedWhileDriving' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("isServiceAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z", "GetIsServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler")]
		public abstract bool IsServiceAllowedWhileDriving (string p0, string p1);

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Content.PM.CarPackageManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Content.PM.CarPackageManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/content/pm/CarPackageManager", DoNotGenerateAcw=true)]
	internal partial class CarPackageManagerInvoker : CarPackageManager {

		public CarPackageManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarPackageManagerInvoker); }
		}

		static IntPtr id_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']/method[@name='isActivityAllowedWhileDriving' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("isActivityAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z", "GetIsActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler")]
		public override unsafe bool IsActivityAllowedWhileDriving (string p0, string p1)
		{
			if (id_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "isActivityAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (native_p1);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isActivityAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_, __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.content.pm']/class[@name='CarPackageManager']/method[@name='isServiceAllowedWhileDriving' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("isServiceAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z", "GetIsServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_Handler")]
		public override unsafe bool IsServiceAllowedWhileDriving (string p0, string p1)
		{
			if (id_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "isServiceAllowedWhileDriving", "(Ljava/lang/String;Ljava/lang/String;)Z");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (native_p1);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isServiceAllowedWhileDriving_Ljava_lang_String_Ljava_lang_String_, __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
