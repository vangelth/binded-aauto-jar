using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']"
	[global::Android.Runtime.Register ("android/support/car/CarServiceLoader", DoNotGenerateAcw=true)]
	public abstract partial class CarServiceLoader : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']"
		[global::Android.Runtime.Register ("android/support/car/CarServiceLoader$CarConnectionCallbackProxy", DoNotGenerateAcw=true)]
		public abstract partial class CarConnectionCallbackProxy : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/CarServiceLoader$CarConnectionCallbackProxy", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarConnectionCallbackProxy); }
			}

			protected CarConnectionCallbackProxy (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']/constructor[@name='CarServiceLoader.CarConnectionCallbackProxy' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe CarConnectionCallbackProxy ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (CarConnectionCallbackProxy)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_onConnected;
#pragma warning disable 0169
			static Delegate GetOnConnectedHandler ()
			{
				if (cb_onConnected == null)
					cb_onConnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnConnected);
				return cb_onConnected;
			}

			static void n_OnConnected (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnConnected ();
			}
#pragma warning restore 0169

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']/method[@name='onConnected' and count(parameter)=0]"
			[Register ("onConnected", "()V", "GetOnConnectedHandler")]
			public abstract void OnConnected ();

			static Delegate cb_onDisconnected;
#pragma warning disable 0169
			static Delegate GetOnDisconnectedHandler ()
			{
				if (cb_onDisconnected == null)
					cb_onDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDisconnected);
				return cb_onDisconnected;
			}

			static void n_OnDisconnected (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnDisconnected ();
			}
#pragma warning restore 0169

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']/method[@name='onDisconnected' and count(parameter)=0]"
			[Register ("onDisconnected", "()V", "GetOnDisconnectedHandler")]
			public abstract void OnDisconnected ();

		}

		[global::Android.Runtime.Register ("android/support/car/CarServiceLoader$CarConnectionCallbackProxy", DoNotGenerateAcw=true)]
		internal partial class CarConnectionCallbackProxyInvoker : CarConnectionCallbackProxy {

			public CarConnectionCallbackProxyInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarConnectionCallbackProxyInvoker); }
			}

			static IntPtr id_onConnected;
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']/method[@name='onConnected' and count(parameter)=0]"
			[Register ("onConnected", "()V", "GetOnConnectedHandler")]
			public override unsafe void OnConnected ()
			{
				if (id_onConnected == IntPtr.Zero)
					id_onConnected = JNIEnv.GetMethodID (class_ref, "onConnected", "()V");
				try {
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnected);
				} finally {
				}
			}

			static IntPtr id_onDisconnected;
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader.CarConnectionCallbackProxy']/method[@name='onDisconnected' and count(parameter)=0]"
			[Register ("onDisconnected", "()V", "GetOnDisconnectedHandler")]
			public override unsafe void OnDisconnected ()
			{
				if (id_onDisconnected == IntPtr.Zero)
					id_onDisconnected = JNIEnv.GetMethodID (class_ref, "onDisconnected", "()V");
				try {
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDisconnected);
				} finally {
				}
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarServiceLoader", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarServiceLoader); }
		}

		protected CarServiceLoader (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/constructor[@name='CarServiceLoader' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.support.car.CarServiceLoader.CarConnectionCallbackProxy'] and parameter[3][@type='android.os.Handler']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", "")]
		public unsafe CarServiceLoader (global::Android.Content.Context p0, global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy p1, global::Android.OS.Handler p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (CarServiceLoader)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_, __args);
			} finally {
			}
		}

		static Delegate cb_getCarConnectionType;
#pragma warning disable 0169
		static Delegate GetGetCarConnectionTypeHandler ()
		{
			if (cb_getCarConnectionType == null)
				cb_getCarConnectionType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetCarConnectionType);
			return cb_getCarConnectionType;
		}

		static int n_GetCarConnectionType (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CarConnectionType;
		}
#pragma warning restore 0169

		public abstract int CarConnectionType {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getCarConnectionType' and count(parameter)=0]"
			[Register ("getCarConnectionType", "()I", "GetGetCarConnectionTypeHandler")] get;
		}

		static Delegate cb_getConnectionCallback;
#pragma warning disable 0169
		static Delegate GetGetConnectionCallbackHandler ()
		{
			if (cb_getConnectionCallback == null)
				cb_getConnectionCallback = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetConnectionCallback);
			return cb_getConnectionCallback;
		}

		static IntPtr n_GetConnectionCallback (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ConnectionCallback);
		}
#pragma warning restore 0169

		static IntPtr id_getConnectionCallback;
		protected virtual unsafe global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy ConnectionCallback {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getConnectionCallback' and count(parameter)=0]"
			[Register ("getConnectionCallback", "()Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;", "GetGetConnectionCallbackHandler")]
			get {
				if (id_getConnectionCallback == IntPtr.Zero)
					id_getConnectionCallback = JNIEnv.GetMethodID (class_ref, "getConnectionCallback", "()Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getConnectionCallback), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getConnectionCallback", "()Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getContext;
#pragma warning disable 0169
		static Delegate GetGetContextHandler ()
		{
			if (cb_getContext == null)
				cb_getContext = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContext);
			return cb_getContext;
		}

		static IntPtr n_GetContext (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Context);
		}
#pragma warning restore 0169

		static IntPtr id_getContext;
		protected virtual unsafe global::Android.Content.Context Context {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getContext' and count(parameter)=0]"
			[Register ("getContext", "()Landroid/content/Context;", "GetGetContextHandler")]
			get {
				if (id_getContext == IntPtr.Zero)
					id_getContext = JNIEnv.GetMethodID (class_ref, "getContext", "()Landroid/content/Context;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Content.Context> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getContext), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Content.Context> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContext", "()Landroid/content/Context;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getEventHandler;
#pragma warning disable 0169
		static Delegate GetGetEventHandlerHandler ()
		{
			if (cb_getEventHandler == null)
				cb_getEventHandler = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventHandler);
			return cb_getEventHandler;
		}

		static IntPtr n_GetEventHandler (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EventHandler);
		}
#pragma warning restore 0169

		static IntPtr id_getEventHandler;
		protected virtual unsafe global::Android.OS.Handler EventHandler {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getEventHandler' and count(parameter)=0]"
			[Register ("getEventHandler", "()Landroid/os/Handler;", "GetGetEventHandlerHandler")]
			get {
				if (id_getEventHandler == IntPtr.Zero)
					id_getEventHandler = JNIEnv.GetMethodID (class_ref, "getEventHandler", "()Landroid/os/Handler;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.OS.Handler> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getEventHandler), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.OS.Handler> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventHandler", "()Landroid/os/Handler;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_isConnected;
#pragma warning disable 0169
		static Delegate GetIsConnectedHandler ()
		{
			if (cb_isConnected == null)
				cb_isConnected = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConnected);
			return cb_isConnected;
		}

		static bool n_IsConnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsConnected;
		}
#pragma warning restore 0169

		public abstract bool IsConnected {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='isConnected' and count(parameter)=0]"
			[Register ("isConnected", "()Z", "GetIsConnectedHandler")] get;
		}

		static Delegate cb_connect;
#pragma warning disable 0169
		static Delegate GetConnectHandler ()
		{
			if (cb_connect == null)
				cb_connect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Connect);
			return cb_connect;
		}

		static void n_Connect (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Connect ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='connect' and count(parameter)=0]"
		[Register ("connect", "()V", "GetConnectHandler")]
		public abstract void Connect ();

		static Delegate cb_disconnect;
#pragma warning disable 0169
		static Delegate GetDisconnectHandler ()
		{
			if (cb_disconnect == null)
				cb_disconnect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Disconnect);
			return cb_disconnect;
		}

		static void n_Disconnect (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Disconnect ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='disconnect' and count(parameter)=0]"
		[Register ("disconnect", "()V", "GetDisconnectHandler")]
		public abstract void Disconnect ();

		static Delegate cb_getCarManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_String_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_String_ == null)
				cb_getCarManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_String_);
			return cb_getCarManager_Ljava_lang_String_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarServiceLoader __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarServiceLoader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler")]
		public abstract global::Java.Lang.Object GetCarManager (string p0);

	}

	[global::Android.Runtime.Register ("android/support/car/CarServiceLoader", DoNotGenerateAcw=true)]
	internal partial class CarServiceLoaderInvoker : CarServiceLoader {

		public CarServiceLoaderInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarServiceLoaderInvoker); }
		}

		static IntPtr id_getCarConnectionType;
		public override unsafe int CarConnectionType {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getCarConnectionType' and count(parameter)=0]"
			[Register ("getCarConnectionType", "()I", "GetGetCarConnectionTypeHandler")]
			get {
				if (id_getCarConnectionType == IntPtr.Zero)
					id_getCarConnectionType = JNIEnv.GetMethodID (class_ref, "getCarConnectionType", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getCarConnectionType);
				} finally {
				}
			}
		}

		static IntPtr id_isConnected;
		public override unsafe bool IsConnected {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='isConnected' and count(parameter)=0]"
			[Register ("isConnected", "()Z", "GetIsConnectedHandler")]
			get {
				if (id_isConnected == IntPtr.Zero)
					id_isConnected = JNIEnv.GetMethodID (class_ref, "isConnected", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConnected);
				} finally {
				}
			}
		}

		static IntPtr id_connect;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='connect' and count(parameter)=0]"
		[Register ("connect", "()V", "GetConnectHandler")]
		public override unsafe void Connect ()
		{
			if (id_connect == IntPtr.Zero)
				id_connect = JNIEnv.GetMethodID (class_ref, "connect", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_connect);
			} finally {
			}
		}

		static IntPtr id_disconnect;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='disconnect' and count(parameter)=0]"
		[Register ("disconnect", "()V", "GetDisconnectHandler")]
		public override unsafe void Disconnect ()
		{
			if (id_disconnect == IntPtr.Zero)
				id_disconnect = JNIEnv.GetMethodID (class_ref, "disconnect", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_disconnect);
			} finally {
			}
		}

		static IntPtr id_getCarManager_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarServiceLoader']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler")]
		public override unsafe global::Java.Lang.Object GetCarManager (string p0)
		{
			if (id_getCarManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Java.Lang.Object __ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}

}
