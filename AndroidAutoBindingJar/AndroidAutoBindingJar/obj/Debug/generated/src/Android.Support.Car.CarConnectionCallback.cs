using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']"
	[global::Android.Runtime.Register ("android/support/car/CarConnectionCallback", DoNotGenerateAcw=true)]
	public abstract partial class CarConnectionCallback : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarConnectionCallback", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarConnectionCallback); }
		}

		protected CarConnectionCallback (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']/constructor[@name='CarConnectionCallback' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarConnectionCallback ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarConnectionCallback)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onConnected_Landroid_support_car_Car_;
#pragma warning disable 0169
		static Delegate GetOnConnected_Landroid_support_car_Car_Handler ()
		{
			if (cb_onConnected_Landroid_support_car_Car_ == null)
				cb_onConnected_Landroid_support_car_Car_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnConnected_Landroid_support_car_Car_);
			return cb_onConnected_Landroid_support_car_Car_;
		}

		static void n_OnConnected_Landroid_support_car_Car_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Car p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnConnected (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']/method[@name='onConnected' and count(parameter)=1 and parameter[1][@type='android.support.car.Car']]"
		[Register ("onConnected", "(Landroid/support/car/Car;)V", "GetOnConnected_Landroid_support_car_Car_Handler")]
		public abstract void OnConnected (global::Android.Support.Car.Car p0);

		static Delegate cb_onDisconnected_Landroid_support_car_Car_;
#pragma warning disable 0169
		static Delegate GetOnDisconnected_Landroid_support_car_Car_Handler ()
		{
			if (cb_onDisconnected_Landroid_support_car_Car_ == null)
				cb_onDisconnected_Landroid_support_car_Car_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnDisconnected_Landroid_support_car_Car_);
			return cb_onDisconnected_Landroid_support_car_Car_;
		}

		static void n_OnDisconnected_Landroid_support_car_Car_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Car p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Car> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnDisconnected (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']/method[@name='onDisconnected' and count(parameter)=1 and parameter[1][@type='android.support.car.Car']]"
		[Register ("onDisconnected", "(Landroid/support/car/Car;)V", "GetOnDisconnected_Landroid_support_car_Car_Handler")]
		public abstract void OnDisconnected (global::Android.Support.Car.Car p0);

	}

	[global::Android.Runtime.Register ("android/support/car/CarConnectionCallback", DoNotGenerateAcw=true)]
	internal partial class CarConnectionCallbackInvoker : CarConnectionCallback {

		public CarConnectionCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarConnectionCallbackInvoker); }
		}

		static IntPtr id_onConnected_Landroid_support_car_Car_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']/method[@name='onConnected' and count(parameter)=1 and parameter[1][@type='android.support.car.Car']]"
		[Register ("onConnected", "(Landroid/support/car/Car;)V", "GetOnConnected_Landroid_support_car_Car_Handler")]
		public override unsafe void OnConnected (global::Android.Support.Car.Car p0)
		{
			if (id_onConnected_Landroid_support_car_Car_ == IntPtr.Zero)
				id_onConnected_Landroid_support_car_Car_ = JNIEnv.GetMethodID (class_ref, "onConnected", "(Landroid/support/car/Car;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnected_Landroid_support_car_Car_, __args);
			} finally {
			}
		}

		static IntPtr id_onDisconnected_Landroid_support_car_Car_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarConnectionCallback']/method[@name='onDisconnected' and count(parameter)=1 and parameter[1][@type='android.support.car.Car']]"
		[Register ("onDisconnected", "(Landroid/support/car/Car;)V", "GetOnDisconnected_Landroid_support_car_Car_Handler")]
		public override unsafe void OnDisconnected (global::Android.Support.Car.Car p0)
		{
			if (id_onDisconnected_Landroid_support_car_Car_ == IntPtr.Zero)
				id_onDisconnected_Landroid_support_car_Car_ = JNIEnv.GetMethodID (class_ref, "onDisconnected", "(Landroid/support/car/Car;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDisconnected_Landroid_support_car_Car_, __args);
			} finally {
			}
		}

	}

}
