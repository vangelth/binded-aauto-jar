using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.UI {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView", DoNotGenerateAcw=true)]
	public partial class PagedScrollBarView : global::Android.Widget.FrameLayout, global::Android.Views.View.IOnClickListener, global::Android.Views.View.IOnLongClickListener {

		[Register ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener", DoNotGenerateAcw=true)]
		public abstract class PaginationListener : Java.Lang.Object {

			internal PaginationListener ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedScrollBarView.PaginationListener']/field[@name='PAGE_DOWN']"
			[Register ("PAGE_DOWN")]
			public const int PageDown = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedScrollBarView.PaginationListener']/field[@name='PAGE_UP']"
			[Register ("PAGE_UP")]
			public const int PageUp = (int) 0;
		}

		[Register ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'PaginationListener' type. This type will be removed in a future release.")]
		public abstract class PaginationListenerConsts : PaginationListener {

			private PaginationListenerConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedScrollBarView.PaginationListener']"
		[Register ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener", "", "Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView/IPaginationListenerInvoker")]
		public partial interface IPaginationListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='PagedScrollBarView.PaginationListener']/method[@name='onPaginate' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("onPaginate", "(I)V", "GetOnPaginate_IHandler:Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView/IPaginationListenerInvoker, AndroidAutoBindingJar")]
			void OnPaginate (int p0);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener", DoNotGenerateAcw=true)]
		internal class IPaginationListenerInvoker : global::Java.Lang.Object, IPaginationListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IPaginationListenerInvoker); }
			}

			IntPtr class_ref;

			public static IPaginationListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IPaginationListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.ui.PagedScrollBarView.PaginationListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IPaginationListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onPaginate_I;
#pragma warning disable 0169
			static Delegate GetOnPaginate_IHandler ()
			{
				if (cb_onPaginate_I == null)
					cb_onPaginate_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnPaginate_I);
				return cb_onPaginate_I;
			}

			static void n_OnPaginate_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnPaginate (p0);
			}
#pragma warning restore 0169

			IntPtr id_onPaginate_I;
			public unsafe void OnPaginate (int p0)
			{
				if (id_onPaginate_I == IntPtr.Zero)
					id_onPaginate_I = JNIEnv.GetMethodID (class_ref, "onPaginate", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPaginate_I, __args);
			}

		}

		public partial class PaginationEventArgs : global::System.EventArgs {

			public PaginationEventArgs (int p0)
			{
				this.p0 = p0;
			}

			int p0;
			public int P0 {
				get { return p0; }
			}
		}

		[global::Android.Runtime.Register ("mono/com/google/android/apps/auto/sdk/ui/PagedScrollBarView_PaginationListenerImplementor")]
		internal sealed partial class IPaginationListenerImplementor : global::Java.Lang.Object, IPaginationListener {

			object sender;

			public IPaginationListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/apps/auto/sdk/ui/PagedScrollBarView_PaginationListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<PaginationEventArgs> Handler;
#pragma warning restore 0649

			public void OnPaginate (int p0)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new PaginationEventArgs (p0));
			}

			internal static bool __IsEmpty (IPaginationListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/PagedScrollBarView", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PagedScrollBarView); }
		}

		protected PagedScrollBarView (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/constructor[@name='PagedScrollBarView' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "")]
		public unsafe PagedScrollBarView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (PagedScrollBarView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/constructor[@name='PagedScrollBarView' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "")]
		public unsafe PagedScrollBarView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (PagedScrollBarView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/constructor[@name='PagedScrollBarView' and count(parameter)=4 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", "")]
		public unsafe PagedScrollBarView (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2, int p3)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				if (((object) this).GetType () != typeof (PagedScrollBarView)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args);
			} finally {
			}
		}

		static Delegate cb_isDownEnabled;
#pragma warning disable 0169
		static Delegate GetIsDownEnabledHandler ()
		{
			if (cb_isDownEnabled == null)
				cb_isDownEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsDownEnabled);
			return cb_isDownEnabled;
		}

		static bool n_IsDownEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DownEnabled;
		}
#pragma warning restore 0169

		static Delegate cb_setDownEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetDownEnabled_ZHandler ()
		{
			if (cb_setDownEnabled_Z == null)
				cb_setDownEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetDownEnabled_Z);
			return cb_setDownEnabled_Z;
		}

		static void n_SetDownEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.DownEnabled = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isDownEnabled;
		static IntPtr id_setDownEnabled_Z;
		public virtual unsafe bool DownEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='isDownEnabled' and count(parameter)=0]"
			[Register ("isDownEnabled", "()Z", "GetIsDownEnabledHandler")]
			get {
				if (id_isDownEnabled == IntPtr.Zero)
					id_isDownEnabled = JNIEnv.GetMethodID (class_ref, "isDownEnabled", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isDownEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isDownEnabled", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setDownEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setDownEnabled", "(Z)V", "GetSetDownEnabled_ZHandler")]
			set {
				if (id_setDownEnabled_Z == IntPtr.Zero)
					id_setDownEnabled_Z = JNIEnv.GetMethodID (class_ref, "setDownEnabled", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDownEnabled_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDownEnabled", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isDownPressed;
#pragma warning disable 0169
		static Delegate GetIsDownPressedHandler ()
		{
			if (cb_isDownPressed == null)
				cb_isDownPressed = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsDownPressed);
			return cb_isDownPressed;
		}

		static bool n_IsDownPressed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsDownPressed;
		}
#pragma warning restore 0169

		static IntPtr id_isDownPressed;
		public virtual unsafe bool IsDownPressed {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='isDownPressed' and count(parameter)=0]"
			[Register ("isDownPressed", "()Z", "GetIsDownPressedHandler")]
			get {
				if (id_isDownPressed == IntPtr.Zero)
					id_isDownPressed = JNIEnv.GetMethodID (class_ref, "isDownPressed", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isDownPressed);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isDownPressed", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isUpPressed;
#pragma warning disable 0169
		static Delegate GetIsUpPressedHandler ()
		{
			if (cb_isUpPressed == null)
				cb_isUpPressed = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsUpPressed);
			return cb_isUpPressed;
		}

		static bool n_IsUpPressed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsUpPressed;
		}
#pragma warning restore 0169

		static IntPtr id_isUpPressed;
		public virtual unsafe bool IsUpPressed {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='isUpPressed' and count(parameter)=0]"
			[Register ("isUpPressed", "()Z", "GetIsUpPressedHandler")]
			get {
				if (id_isUpPressed == IntPtr.Zero)
					id_isUpPressed = JNIEnv.GetMethodID (class_ref, "isUpPressed", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isUpPressed);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isUpPressed", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_onClick_Landroid_view_View_;
#pragma warning disable 0169
		static Delegate GetOnClick_Landroid_view_View_Handler ()
		{
			if (cb_onClick_Landroid_view_View_ == null)
				cb_onClick_Landroid_view_View_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnClick_Landroid_view_View_);
			return cb_onClick_Landroid_view_View_;
		}

		static void n_OnClick_Landroid_view_View_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnClick (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onClick_Landroid_view_View_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='onClick' and count(parameter)=1 and parameter[1][@type='android.view.View']]"
		[Register ("onClick", "(Landroid/view/View;)V", "GetOnClick_Landroid_view_View_Handler")]
		public virtual unsafe void OnClick (global::Android.Views.View p0)
		{
			if (id_onClick_Landroid_view_View_ == IntPtr.Zero)
				id_onClick_Landroid_view_View_ = JNIEnv.GetMethodID (class_ref, "onClick", "(Landroid/view/View;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onClick_Landroid_view_View_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onClick", "(Landroid/view/View;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onLongClick_Landroid_view_View_;
#pragma warning disable 0169
		static Delegate GetOnLongClick_Landroid_view_View_Handler ()
		{
			if (cb_onLongClick_Landroid_view_View_ == null)
				cb_onLongClick_Landroid_view_View_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_OnLongClick_Landroid_view_View_);
			return cb_onLongClick_Landroid_view_View_;
		}

		static bool n_OnLongClick_Landroid_view_View_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnLongClick (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onLongClick_Landroid_view_View_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='onLongClick' and count(parameter)=1 and parameter[1][@type='android.view.View']]"
		[Register ("onLongClick", "(Landroid/view/View;)Z", "GetOnLongClick_Landroid_view_View_Handler")]
		public virtual unsafe bool OnLongClick (global::Android.Views.View p0)
		{
			if (id_onLongClick_Landroid_view_View_ == IntPtr.Zero)
				id_onLongClick_Landroid_view_View_ = JNIEnv.GetMethodID (class_ref, "onLongClick", "(Landroid/view/View;)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onLongClick_Landroid_view_View_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onLongClick", "(Landroid/view/View;)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_setAutoDayNightMode;
#pragma warning disable 0169
		static Delegate GetSetAutoDayNightModeHandler ()
		{
			if (cb_setAutoDayNightMode == null)
				cb_setAutoDayNightMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetAutoDayNightMode);
			return cb_setAutoDayNightMode;
		}

		static void n_SetAutoDayNightMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetAutoDayNightMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setAutoDayNightMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setAutoDayNightMode' and count(parameter)=0]"
		[Register ("setAutoDayNightMode", "()V", "GetSetAutoDayNightModeHandler")]
		public virtual unsafe void SetAutoDayNightMode ()
		{
			if (id_setAutoDayNightMode == IntPtr.Zero)
				id_setAutoDayNightMode = JNIEnv.GetMethodID (class_ref, "setAutoDayNightMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setAutoDayNightMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAutoDayNightMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setDarkMode;
#pragma warning disable 0169
		static Delegate GetSetDarkModeHandler ()
		{
			if (cb_setDarkMode == null)
				cb_setDarkMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetDarkMode);
			return cb_setDarkMode;
		}

		static void n_SetDarkMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDarkMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setDarkMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setDarkMode' and count(parameter)=0]"
		[Register ("setDarkMode", "()V", "GetSetDarkModeHandler")]
		public virtual unsafe void SetDarkMode ()
		{
			if (id_setDarkMode == IntPtr.Zero)
				id_setDarkMode = JNIEnv.GetMethodID (class_ref, "setDarkMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDarkMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDarkMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setDayNightStyle_I;
#pragma warning disable 0169
		static Delegate GetSetDayNightStyle_IHandler ()
		{
			if (cb_setDayNightStyle_I == null)
				cb_setDayNightStyle_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetDayNightStyle_I);
			return cb_setDayNightStyle_I;
		}

		static void n_SetDayNightStyle_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetDayNightStyle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setDayNightStyle_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setDayNightStyle' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setDayNightStyle", "(I)V", "GetSetDayNightStyle_IHandler")]
		public virtual unsafe void SetDayNightStyle (int p0)
		{
			if (id_setDayNightStyle_I == IntPtr.Zero)
				id_setDayNightStyle_I = JNIEnv.GetMethodID (class_ref, "setDayNightStyle", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDayNightStyle_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDayNightStyle", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setLightMode;
#pragma warning disable 0169
		static Delegate GetSetLightModeHandler ()
		{
			if (cb_setLightMode == null)
				cb_setLightMode = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_SetLightMode);
			return cb_setLightMode;
		}

		static void n_SetLightMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetLightMode ();
		}
#pragma warning restore 0169

		static IntPtr id_setLightMode;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setLightMode' and count(parameter)=0]"
		[Register ("setLightMode", "()V", "GetSetLightModeHandler")]
		public virtual unsafe void SetLightMode ()
		{
			if (id_setLightMode == IntPtr.Zero)
				id_setLightMode = JNIEnv.GetMethodID (class_ref, "setLightMode", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setLightMode);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLightMode", "()V"));
			} finally {
			}
		}

		static Delegate cb_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_;
#pragma warning disable 0169
		static Delegate GetSetPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_Handler ()
		{
			if (cb_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_ == null)
				cb_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_);
			return cb_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_;
		}

		static void n_SetPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener p0 = (global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetPaginationListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setPaginationListener' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.ui.PagedScrollBarView.PaginationListener']]"
		[Register ("setPaginationListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener;)V", "GetSetPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_Handler")]
		public virtual unsafe void SetPaginationListener (global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener p0)
		{
			if (id_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_ == IntPtr.Zero)
				id_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_ = JNIEnv.GetMethodID (class_ref, "setPaginationListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setPaginationListener_Lcom_google_android_apps_auto_sdk_ui_PagedScrollBarView_PaginationListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPaginationListener", "(Lcom/google/android/apps/auto/sdk/ui/PagedScrollBarView$PaginationListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setParameters_IIIZ;
#pragma warning disable 0169
		static Delegate GetSetParameters_IIIZHandler ()
		{
			if (cb_setParameters_IIIZ == null)
				cb_setParameters_IIIZ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int, bool>) n_SetParameters_IIIZ);
			return cb_setParameters_IIIZ;
		}

		static void n_SetParameters_IIIZ (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, bool p3)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetParameters (p0, p1, p2, p3);
		}
#pragma warning restore 0169

		static IntPtr id_setParameters_IIIZ;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setParameters' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='boolean']]"
		[Register ("setParameters", "(IIIZ)V", "GetSetParameters_IIIZHandler")]
		public virtual unsafe void SetParameters (int p0, int p1, int p2, bool p3)
		{
			if (id_setParameters_IIIZ == IntPtr.Zero)
				id_setParameters_IIIZ = JNIEnv.GetMethodID (class_ref, "setParameters", "(IIIZ)V");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setParameters_IIIZ, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setParameters", "(IIIZ)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setUpEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetUpEnabled_ZHandler ()
		{
			if (cb_setUpEnabled_Z == null)
				cb_setUpEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetUpEnabled_Z);
			return cb_setUpEnabled_Z;
		}

		static void n_SetUpEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetUpEnabled (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setUpEnabled_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='PagedScrollBarView']/method[@name='setUpEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setUpEnabled", "(Z)V", "GetSetUpEnabled_ZHandler")]
		public virtual unsafe void SetUpEnabled (bool p0)
		{
			if (id_setUpEnabled_Z == IntPtr.Zero)
				id_setUpEnabled_Z = JNIEnv.GetMethodID (class_ref, "setUpEnabled", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setUpEnabled_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUpEnabled", "(Z)V"), __args);
			} finally {
			}
		}

#region "Event implementation for Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener"
		public event EventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.PaginationEventArgs> Pagination {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener, global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListenerImplementor>(
						ref weak_implementor_SetPaginationListener,
						__CreateIPaginationListenerImplementor,
						SetPaginationListener,
						__h => __h.Handler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListener, global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListenerImplementor>(
						ref weak_implementor_SetPaginationListener,
						global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListenerImplementor.__IsEmpty,
						__v => SetPaginationListener (null),
						__h => __h.Handler -= value);
			}
		}

		WeakReference weak_implementor_SetPaginationListener;

		global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListenerImplementor __CreateIPaginationListenerImplementor ()
		{
			return new global::Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView.IPaginationListenerImplementor (this);
		}
#endregion
	}
}
