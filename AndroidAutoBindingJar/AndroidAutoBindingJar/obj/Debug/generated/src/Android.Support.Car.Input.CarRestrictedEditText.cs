using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Input {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.input']/class[@name='CarRestrictedEditText']"
	[global::Android.Runtime.Register ("android/support/car/input/CarRestrictedEditText", DoNotGenerateAcw=true)]
	public partial class CarRestrictedEditText : global::Android.Widget.EditText, global::Android.Support.Car.Input.ICarEditable {

		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']"
		[Register ("android/support/car/input/CarRestrictedEditText$KeyListener", "", "Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker")]
		public partial interface IKeyListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']/method[@name='onCloseKeyboard' and count(parameter)=0]"
			[Register ("onCloseKeyboard", "()V", "GetOnCloseKeyboardHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar")]
			void OnCloseKeyboard ();

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']/method[@name='onCommitText' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("onCommitText", "(Ljava/lang/String;)V", "GetOnCommitText_Ljava_lang_String_Handler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar")]
			void OnCommitText (string p0);

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']/method[@name='onDelete' and count(parameter)=0]"
			[Register ("onDelete", "()V", "GetOnDeleteHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar")]
			void OnDelete ();

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']/method[@name='onKeyDown' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("onKeyDown", "(I)V", "GetOnKeyDown_IHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar")]
			void OnKeyDown (int p0);

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarRestrictedEditText.KeyListener']/method[@name='onKeyUp' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("onKeyUp", "(I)V", "GetOnKeyUp_IHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar")]
			void OnKeyUp (int p0);

		}

		[global::Android.Runtime.Register ("android/support/car/input/CarRestrictedEditText$KeyListener", DoNotGenerateAcw=true)]
		internal class IKeyListenerInvoker : global::Java.Lang.Object, IKeyListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/input/CarRestrictedEditText$KeyListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IKeyListenerInvoker); }
			}

			IntPtr class_ref;

			public static IKeyListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IKeyListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.input.CarRestrictedEditText.KeyListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IKeyListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onCloseKeyboard;
#pragma warning disable 0169
			static Delegate GetOnCloseKeyboardHandler ()
			{
				if (cb_onCloseKeyboard == null)
					cb_onCloseKeyboard = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCloseKeyboard);
				return cb_onCloseKeyboard;
			}

			static void n_OnCloseKeyboard (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnCloseKeyboard ();
			}
#pragma warning restore 0169

			IntPtr id_onCloseKeyboard;
			public unsafe void OnCloseKeyboard ()
			{
				if (id_onCloseKeyboard == IntPtr.Zero)
					id_onCloseKeyboard = JNIEnv.GetMethodID (class_ref, "onCloseKeyboard", "()V");
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCloseKeyboard);
			}

			static Delegate cb_onCommitText_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetOnCommitText_Ljava_lang_String_Handler ()
			{
				if (cb_onCommitText_Ljava_lang_String_ == null)
					cb_onCommitText_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnCommitText_Ljava_lang_String_);
				return cb_onCommitText_Ljava_lang_String_;
			}

			static void n_OnCommitText_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnCommitText (p0);
			}
#pragma warning restore 0169

			IntPtr id_onCommitText_Ljava_lang_String_;
			public unsafe void OnCommitText (string p0)
			{
				if (id_onCommitText_Ljava_lang_String_ == IntPtr.Zero)
					id_onCommitText_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "onCommitText", "(Ljava/lang/String;)V");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCommitText_Ljava_lang_String_, __args);
				JNIEnv.DeleteLocalRef (native_p0);
			}

			static Delegate cb_onDelete;
#pragma warning disable 0169
			static Delegate GetOnDeleteHandler ()
			{
				if (cb_onDelete == null)
					cb_onDelete = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDelete);
				return cb_onDelete;
			}

			static void n_OnDelete (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnDelete ();
			}
#pragma warning restore 0169

			IntPtr id_onDelete;
			public unsafe void OnDelete ()
			{
				if (id_onDelete == IntPtr.Zero)
					id_onDelete = JNIEnv.GetMethodID (class_ref, "onDelete", "()V");
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDelete);
			}

			static Delegate cb_onKeyDown_I;
#pragma warning disable 0169
			static Delegate GetOnKeyDown_IHandler ()
			{
				if (cb_onKeyDown_I == null)
					cb_onKeyDown_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnKeyDown_I);
				return cb_onKeyDown_I;
			}

			static void n_OnKeyDown_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnKeyDown (p0);
			}
#pragma warning restore 0169

			IntPtr id_onKeyDown_I;
			public unsafe void OnKeyDown (int p0)
			{
				if (id_onKeyDown_I == IntPtr.Zero)
					id_onKeyDown_I = JNIEnv.GetMethodID (class_ref, "onKeyDown", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onKeyDown_I, __args);
			}

			static Delegate cb_onKeyUp_I;
#pragma warning disable 0169
			static Delegate GetOnKeyUp_IHandler ()
			{
				if (cb_onKeyUp_I == null)
					cb_onKeyUp_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnKeyUp_I);
				return cb_onKeyUp_I;
			}

			static void n_OnKeyUp_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnKeyUp (p0);
			}
#pragma warning restore 0169

			IntPtr id_onKeyUp_I;
			public unsafe void OnKeyUp (int p0)
			{
				if (id_onKeyUp_I == IntPtr.Zero)
					id_onKeyUp_I = JNIEnv.GetMethodID (class_ref, "onKeyUp", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onKeyUp_I, __args);
			}

		}

		public partial class CommitTextEventArgs : global::System.EventArgs {

			public CommitTextEventArgs (string p0)
			{
				this.p0 = p0;
			}

			string p0;
			public string P0 {
				get { return p0; }
			}
		}

		public partial class KeyDownEventArgs : global::System.EventArgs {

			public KeyDownEventArgs (int p0)
			{
				this.p0 = p0;
			}

			int p0;
			public int P0 {
				get { return p0; }
			}
		}

		public partial class KeyUpEventArgs : global::System.EventArgs {

			public KeyUpEventArgs (int p0)
			{
				this.p0 = p0;
			}

			int p0;
			public int P0 {
				get { return p0; }
			}
		}

		[global::Android.Runtime.Register ("mono/android/support/car/input/CarRestrictedEditText_KeyListenerImplementor")]
		internal sealed partial class IKeyListenerImplementor : global::Java.Lang.Object, IKeyListener {

			object sender;

			public IKeyListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/android/support/car/input/CarRestrictedEditText_KeyListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler OnCloseKeyboardHandler;
#pragma warning restore 0649

			public void OnCloseKeyboard ()
			{
				var __h = OnCloseKeyboardHandler;
				if (__h != null)
					__h (sender, new EventArgs ());
			}
#pragma warning disable 0649
			public EventHandler<CommitTextEventArgs> OnCommitTextHandler;
#pragma warning restore 0649

			public void OnCommitText (string p0)
			{
				var __h = OnCommitTextHandler;
				if (__h != null)
					__h (sender, new CommitTextEventArgs (p0));
			}
#pragma warning disable 0649
			public EventHandler OnDeleteHandler;
#pragma warning restore 0649

			public void OnDelete ()
			{
				var __h = OnDeleteHandler;
				if (__h != null)
					__h (sender, new EventArgs ());
			}
#pragma warning disable 0649
			public EventHandler<KeyDownEventArgs> OnKeyDownHandler;
#pragma warning restore 0649

			public void OnKeyDown (int p0)
			{
				var __h = OnKeyDownHandler;
				if (__h != null)
					__h (sender, new KeyDownEventArgs (p0));
			}
#pragma warning disable 0649
			public EventHandler<KeyUpEventArgs> OnKeyUpHandler;
#pragma warning restore 0649

			public void OnKeyUp (int p0)
			{
				var __h = OnKeyUpHandler;
				if (__h != null)
					__h (sender, new KeyUpEventArgs (p0));
			}

			internal static bool __IsEmpty (IKeyListenerImplementor value)
			{
				return value.OnCloseKeyboardHandler == null && value.OnCommitTextHandler == null && value.OnDeleteHandler == null && value.OnKeyDownHandler == null && value.OnKeyUpHandler == null;
			}
		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/input/CarRestrictedEditText", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarRestrictedEditText); }
		}

		protected CarRestrictedEditText (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.input']/class[@name='CarRestrictedEditText']/constructor[@name='CarRestrictedEditText' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "")]
		public unsafe CarRestrictedEditText (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (CarRestrictedEditText)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args);
			} finally {
			}
		}

		static Delegate cb_setCarEditableListener_Landroid_support_car_input_CarEditableListener_;
#pragma warning disable 0169
		static Delegate GetSetCarEditableListener_Landroid_support_car_input_CarEditableListener_Handler ()
		{
			if (cb_setCarEditableListener_Landroid_support_car_input_CarEditableListener_ == null)
				cb_setCarEditableListener_Landroid_support_car_input_CarEditableListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCarEditableListener_Landroid_support_car_input_CarEditableListener_);
			return cb_setCarEditableListener_Landroid_support_car_input_CarEditableListener_;
		}

		static void n_SetCarEditableListener_Landroid_support_car_input_CarEditableListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Input.CarRestrictedEditText __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Input.ICarEditableListener p0 = (global::Android.Support.Car.Input.ICarEditableListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.ICarEditableListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetCarEditableListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setCarEditableListener_Landroid_support_car_input_CarEditableListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarRestrictedEditText']/method[@name='setCarEditableListener' and count(parameter)=1 and parameter[1][@type='android.support.car.input.CarEditableListener']]"
		[Register ("setCarEditableListener", "(Landroid/support/car/input/CarEditableListener;)V", "GetSetCarEditableListener_Landroid_support_car_input_CarEditableListener_Handler")]
		public virtual unsafe void SetCarEditableListener (global::Android.Support.Car.Input.ICarEditableListener p0)
		{
			if (id_setCarEditableListener_Landroid_support_car_input_CarEditableListener_ == IntPtr.Zero)
				id_setCarEditableListener_Landroid_support_car_input_CarEditableListener_ = JNIEnv.GetMethodID (class_ref, "setCarEditableListener", "(Landroid/support/car/input/CarEditableListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setCarEditableListener_Landroid_support_car_input_CarEditableListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCarEditableListener", "(Landroid/support/car/input/CarEditableListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setInputEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetInputEnabled_ZHandler ()
		{
			if (cb_setInputEnabled_Z == null)
				cb_setInputEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetInputEnabled_Z);
			return cb_setInputEnabled_Z;
		}

		static void n_SetInputEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Android.Support.Car.Input.CarRestrictedEditText __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetInputEnabled (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setInputEnabled_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarRestrictedEditText']/method[@name='setInputEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setInputEnabled", "(Z)V", "GetSetInputEnabled_ZHandler")]
		public virtual unsafe void SetInputEnabled (bool p0)
		{
			if (id_setInputEnabled_Z == IntPtr.Zero)
				id_setInputEnabled_Z = JNIEnv.GetMethodID (class_ref, "setInputEnabled", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setInputEnabled_Z, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setInputEnabled", "(Z)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_;
#pragma warning disable 0169
		static Delegate GetSetKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_Handler ()
		{
			if (cb_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_ == null)
				cb_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_);
			return cb_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_;
		}

		static void n_SetKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Input.CarRestrictedEditText __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener p0 = (global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetKeyListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarRestrictedEditText']/method[@name='setKeyListener' and count(parameter)=1 and parameter[1][@type='android.support.car.input.CarRestrictedEditText.KeyListener']]"
		[Register ("setKeyListener", "(Landroid/support/car/input/CarRestrictedEditText$KeyListener;)V", "GetSetKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_Handler")]
		public virtual unsafe void SetKeyListener (global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener p0)
		{
			if (id_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_ == IntPtr.Zero)
				id_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_ = JNIEnv.GetMethodID (class_ref, "setKeyListener", "(Landroid/support/car/input/CarRestrictedEditText$KeyListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setKeyListener_Landroid_support_car_input_CarRestrictedEditText_KeyListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKeyListener", "(Landroid/support/car/input/CarRestrictedEditText$KeyListener;)V"), __args);
			} finally {
			}
		}

#region "Event implementation for Android.Support.Car.Input.ICarEditableListener"
		public event EventHandler<global::Android.Support.Car.Input.CarEditableEventArgs> CarEditable {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.ICarEditableListener, global::Android.Support.Car.Input.ICarEditableListenerImplementor>(
						ref weak_implementor_SetCarEditableListener,
						__CreateICarEditableListenerImplementor,
						SetCarEditableListener,
						__h => __h.Handler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.ICarEditableListener, global::Android.Support.Car.Input.ICarEditableListenerImplementor>(
						ref weak_implementor_SetCarEditableListener,
						global::Android.Support.Car.Input.ICarEditableListenerImplementor.__IsEmpty,
						__v => SetCarEditableListener (null),
						__h => __h.Handler -= value);
			}
		}

		WeakReference weak_implementor_SetCarEditableListener;

		global::Android.Support.Car.Input.ICarEditableListenerImplementor __CreateICarEditableListenerImplementor ()
		{
			return new global::Android.Support.Car.Input.ICarEditableListenerImplementor (this);
		}
#endregion
#region "Event implementation for Android.Support.Car.Input.CarRestrictedEditText.IKeyListener"
		public event EventHandler CloseKeyboard {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						__CreateIKeyListenerImplementor,
						SetKeyListener,
						__h => __h.OnCloseKeyboardHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor.__IsEmpty,
						__v => SetKeyListener (null),
						__h => __h.OnCloseKeyboardHandler -= value);
			}
		}

		public event EventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.CommitTextEventArgs> CommitText {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						__CreateIKeyListenerImplementor,
						SetKeyListener,
						__h => __h.OnCommitTextHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor.__IsEmpty,
						__v => SetKeyListener (null),
						__h => __h.OnCommitTextHandler -= value);
			}
		}

		public event EventHandler Delete {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						__CreateIKeyListenerImplementor,
						SetKeyListener,
						__h => __h.OnDeleteHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor.__IsEmpty,
						__v => SetKeyListener (null),
						__h => __h.OnDeleteHandler -= value);
			}
		}

		public event EventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.KeyDownEventArgs> KeyDown {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						__CreateIKeyListenerImplementor,
						SetKeyListener,
						__h => __h.OnKeyDownHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor.__IsEmpty,
						__v => SetKeyListener (null),
						__h => __h.OnKeyDownHandler -= value);
			}
		}

		public event EventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.KeyUpEventArgs> KeyUp {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						__CreateIKeyListenerImplementor,
						SetKeyListener,
						__h => __h.OnKeyUpHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListener, global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor>(
						ref weak_implementor_SetKeyListener,
						global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor.__IsEmpty,
						__v => SetKeyListener (null),
						__h => __h.OnKeyUpHandler -= value);
			}
		}

		WeakReference weak_implementor_SetKeyListener;

		global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor __CreateIKeyListenerImplementor ()
		{
			return new global::Android.Support.Car.Input.CarRestrictedEditText.IKeyListenerImplementor (this);
		}
#endregion
	}
}
