using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='b']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/suggestion/b", DoNotGenerateAcw=true)]
	public sealed partial class B : global::Com.Google.Android.A.Agizmo {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/suggestion/b", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (B); }
		}

		internal B (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='b']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.suggestion.NavigationSuggestion[]']]"
		[Register ("a", "([Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion;)V", "")]
		public unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion[] p0)
		{
			if (id_a_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_ == IntPtr.Zero)
				id_a_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_ = JNIEnv.GetMethodID (class_ref, "a", "([Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion;)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_, __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

	}
}
