using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Media {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']"
	[global::Android.Runtime.Register ("android/support/car/media/CarAudioManager", DoNotGenerateAcw=true)]
	public abstract partial class CarAudioManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_ALARM']"
		[Register ("CAR_AUDIO_USAGE_ALARM")]
		public const int CarAudioUsageAlarm = (int) 6;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_DEFAULT']"
		[Register ("CAR_AUDIO_USAGE_DEFAULT")]
		public const int CarAudioUsageDefault = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_MAX']"
		[Register ("CAR_AUDIO_USAGE_MAX")]
		public const int CarAudioUsageMax = (int) 9;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_MUSIC']"
		[Register ("CAR_AUDIO_USAGE_MUSIC")]
		public const int CarAudioUsageMusic = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_NAVIGATION_GUIDANCE']"
		[Register ("CAR_AUDIO_USAGE_NAVIGATION_GUIDANCE")]
		public const int CarAudioUsageNavigationGuidance = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_NOTIFICATION']"
		[Register ("CAR_AUDIO_USAGE_NOTIFICATION")]
		public const int CarAudioUsageNotification = (int) 7;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_RADIO']"
		[Register ("CAR_AUDIO_USAGE_RADIO")]
		public const int CarAudioUsageRadio = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_SYSTEM_SAFETY_ALERT']"
		[Register ("CAR_AUDIO_USAGE_SYSTEM_SAFETY_ALERT")]
		public const int CarAudioUsageSystemSafetyAlert = (int) 9;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_SYSTEM_SOUND']"
		[Register ("CAR_AUDIO_USAGE_SYSTEM_SOUND")]
		public const int CarAudioUsageSystemSound = (int) 8;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_VOICE_CALL']"
		[Register ("CAR_AUDIO_USAGE_VOICE_CALL")]
		public const int CarAudioUsageVoiceCall = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/field[@name='CAR_AUDIO_USAGE_VOICE_COMMAND']"
		[Register ("CAR_AUDIO_USAGE_VOICE_COMMAND")]
		public const int CarAudioUsageVoiceCommand = (int) 5;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.media']/interface[@name='CarAudioManager.CarAudioUsage']"
		[Register ("android/support/car/media/CarAudioManager$CarAudioUsage", "", "Android.Support.Car.Media.CarAudioManager/ICarAudioUsageInvoker")]
		public partial interface ICarAudioUsage : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/media/CarAudioManager$CarAudioUsage", DoNotGenerateAcw=true)]
		internal class ICarAudioUsageInvoker : global::Java.Lang.Object, ICarAudioUsage {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/media/CarAudioManager$CarAudioUsage");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ICarAudioUsageInvoker); }
			}

			IntPtr class_ref;

			public static ICarAudioUsage GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ICarAudioUsage> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.media.CarAudioManager.CarAudioUsage"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ICarAudioUsageInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager.ICarAudioUsage> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/media/CarAudioManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioManager); }
		}

		protected CarAudioManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/constructor[@name='CarAudioManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarAudioManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarAudioManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAudioRecordAudioFormat;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordAudioFormatHandler ()
		{
			if (cb_getAudioRecordAudioFormat == null)
				cb_getAudioRecordAudioFormat = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAudioRecordAudioFormat);
			return cb_getAudioRecordAudioFormat;
		}

		static IntPtr n_GetAudioRecordAudioFormat (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.AudioRecordAudioFormat);
		}
#pragma warning restore 0169

		public abstract global::Android.Media.AudioFormat AudioRecordAudioFormat {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordAudioFormat' and count(parameter)=0]"
			[Register ("getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;", "GetGetAudioRecordAudioFormatHandler")] get;
		}

		static Delegate cb_getAudioRecordMaxBufferSize;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordMaxBufferSizeHandler ()
		{
			if (cb_getAudioRecordMaxBufferSize == null)
				cb_getAudioRecordMaxBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetAudioRecordMaxBufferSize);
			return cb_getAudioRecordMaxBufferSize;
		}

		static int n_GetAudioRecordMaxBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AudioRecordMaxBufferSize;
		}
#pragma warning restore 0169

		public abstract int AudioRecordMaxBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordMaxBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMaxBufferSize", "()I", "GetGetAudioRecordMaxBufferSizeHandler")] get;
		}

		static Delegate cb_getAudioRecordMinBufferSize;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordMinBufferSizeHandler ()
		{
			if (cb_getAudioRecordMinBufferSize == null)
				cb_getAudioRecordMinBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetAudioRecordMinBufferSize);
			return cb_getAudioRecordMinBufferSize;
		}

		static int n_GetAudioRecordMinBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AudioRecordMinBufferSize;
		}
#pragma warning restore 0169

		public abstract int AudioRecordMinBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordMinBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMinBufferSize", "()I", "GetGetAudioRecordMinBufferSizeHandler")] get;
		}

		static Delegate cb_isAudioRecordSupported;
#pragma warning disable 0169
		static Delegate GetIsAudioRecordSupportedHandler ()
		{
			if (cb_isAudioRecordSupported == null)
				cb_isAudioRecordSupported = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsAudioRecordSupported);
			return cb_isAudioRecordSupported;
		}

		static bool n_IsAudioRecordSupported (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsAudioRecordSupported;
		}
#pragma warning restore 0169

		public abstract bool IsAudioRecordSupported {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='isAudioRecordSupported' and count(parameter)=0]"
			[Register ("isAudioRecordSupported", "()Z", "GetIsAudioRecordSupportedHandler")] get;
		}

		static Delegate cb_isMediaMuted;
#pragma warning disable 0169
		static Delegate GetIsMediaMutedHandler ()
		{
			if (cb_isMediaMuted == null)
				cb_isMediaMuted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsMediaMuted);
			return cb_isMediaMuted;
		}

		static bool n_IsMediaMuted (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsMediaMuted;
		}
#pragma warning restore 0169

		public abstract bool IsMediaMuted {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='isMediaMuted' and count(parameter)=0]"
			[Register ("isMediaMuted", "()Z", "GetIsMediaMutedHandler")] get;
		}

		static Delegate cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
#pragma warning disable 0169
		static Delegate GetAbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_Handler ()
		{
			if (cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ == null)
				cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_);
			return cb_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
		}

		static void n_AbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAudioFocus (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='abandonAudioFocus' and count(parameter)=2 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes']]"
		[Register ("abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V", "GetAbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_Handler")]
		public abstract void AbandonAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1);

		static Delegate cb_createCarAudioRecord_I;
#pragma warning disable 0169
		static Delegate GetCreateCarAudioRecord_IHandler ()
		{
			if (cb_createCarAudioRecord_I == null)
				cb_createCarAudioRecord_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_CreateCarAudioRecord_I);
			return cb_createCarAudioRecord_I;
		}

		static IntPtr n_CreateCarAudioRecord_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CreateCarAudioRecord (p0));
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='createCarAudioRecord' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;", "GetCreateCarAudioRecord_IHandler")]
		public abstract global::Android.Support.Car.Media.CarAudioRecord CreateCarAudioRecord (int p0);

		static Delegate cb_getAudioAttributesForCarUsage_I;
#pragma warning disable 0169
		static Delegate GetGetAudioAttributesForCarUsage_IHandler ()
		{
			if (cb_getAudioAttributesForCarUsage_I == null)
				cb_getAudioAttributesForCarUsage_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetAudioAttributesForCarUsage_I);
			return cb_getAudioAttributesForCarUsage_I;
		}

		static IntPtr n_GetAudioAttributesForCarUsage_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetAudioAttributesForCarUsage (p0));
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioAttributesForCarUsage' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;", "GetGetAudioAttributesForCarUsage_IHandler")]
		public abstract global::Android.Media.AudioAttributes GetAudioAttributesForCarUsage (int p0);

		static Delegate cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
#pragma warning disable 0169
		static Delegate GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IHandler ()
		{
			if (cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I == null)
				cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, int>) n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I);
			return cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
		}

		static int n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAudioFocus (p0, p1, p2);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='requestAudioFocus' and count(parameter)=3 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IHandler")]
		public abstract int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2);

		static Delegate cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
#pragma warning disable 0169
		static Delegate GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IIHandler ()
		{
			if (cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II == null)
				cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, int, int>) n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II);
			return cb_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
		}

		static int n_RequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2, int p3)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0 = (global::Android.Media.AudioManager.IOnAudioFocusChangeListener)global::Java.Lang.Object.GetObject<global::Android.Media.AudioManager.IOnAudioFocusChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Media.AudioAttributes p1 = global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAudioFocus (p0, p1, p2, p3);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='requestAudioFocus' and count(parameter)=4 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IIHandler")]
		public abstract int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2, int p3);

		static Delegate cb_setMediaMute_Z;
#pragma warning disable 0169
		static Delegate GetSetMediaMute_ZHandler ()
		{
			if (cb_setMediaMute_Z == null)
				cb_setMediaMute_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, bool>) n_SetMediaMute_Z);
			return cb_setMediaMute_Z;
		}

		static bool n_SetMediaMute_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SetMediaMute (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='setMediaMute' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setMediaMute", "(Z)Z", "GetSetMediaMute_ZHandler")]
		public abstract bool SetMediaMute (bool p0);

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/media/CarAudioManager", DoNotGenerateAcw=true)]
	internal partial class CarAudioManagerInvoker : CarAudioManager {

		public CarAudioManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioManagerInvoker); }
		}

		static IntPtr id_getAudioRecordAudioFormat;
		public override unsafe global::Android.Media.AudioFormat AudioRecordAudioFormat {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordAudioFormat' and count(parameter)=0]"
			[Register ("getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;", "GetGetAudioRecordAudioFormatHandler")]
			get {
				if (id_getAudioRecordAudioFormat == IntPtr.Zero)
					id_getAudioRecordAudioFormat = JNIEnv.GetMethodID (class_ref, "getAudioRecordAudioFormat", "()Landroid/media/AudioFormat;");
				try {
					return global::Java.Lang.Object.GetObject<global::Android.Media.AudioFormat> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordAudioFormat), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getAudioRecordMaxBufferSize;
		public override unsafe int AudioRecordMaxBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordMaxBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMaxBufferSize", "()I", "GetGetAudioRecordMaxBufferSizeHandler")]
			get {
				if (id_getAudioRecordMaxBufferSize == IntPtr.Zero)
					id_getAudioRecordMaxBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMaxBufferSize", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMaxBufferSize);
				} finally {
				}
			}
		}

		static IntPtr id_getAudioRecordMinBufferSize;
		public override unsafe int AudioRecordMinBufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioRecordMinBufferSize' and count(parameter)=0]"
			[Register ("getAudioRecordMinBufferSize", "()I", "GetGetAudioRecordMinBufferSizeHandler")]
			get {
				if (id_getAudioRecordMinBufferSize == IntPtr.Zero)
					id_getAudioRecordMinBufferSize = JNIEnv.GetMethodID (class_ref, "getAudioRecordMinBufferSize", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMinBufferSize);
				} finally {
				}
			}
		}

		static IntPtr id_isAudioRecordSupported;
		public override unsafe bool IsAudioRecordSupported {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='isAudioRecordSupported' and count(parameter)=0]"
			[Register ("isAudioRecordSupported", "()Z", "GetIsAudioRecordSupportedHandler")]
			get {
				if (id_isAudioRecordSupported == IntPtr.Zero)
					id_isAudioRecordSupported = JNIEnv.GetMethodID (class_ref, "isAudioRecordSupported", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isAudioRecordSupported);
				} finally {
				}
			}
		}

		static IntPtr id_isMediaMuted;
		public override unsafe bool IsMediaMuted {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='isMediaMuted' and count(parameter)=0]"
			[Register ("isMediaMuted", "()Z", "GetIsMediaMutedHandler")]
			get {
				if (id_isMediaMuted == IntPtr.Zero)
					id_isMediaMuted = JNIEnv.GetMethodID (class_ref, "isMediaMuted", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isMediaMuted);
				} finally {
				}
			}
		}

		static IntPtr id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='abandonAudioFocus' and count(parameter)=2 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes']]"
		[Register ("abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V", "GetAbandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_Handler")]
		public override unsafe void AbandonAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1)
		{
			if (id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ == IntPtr.Zero)
				id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_ = JNIEnv.GetMethodID (class_ref, "abandonAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_, __args);
			} finally {
			}
		}

		static IntPtr id_createCarAudioRecord_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='createCarAudioRecord' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;", "GetCreateCarAudioRecord_IHandler")]
		public override unsafe global::Android.Support.Car.Media.CarAudioRecord CreateCarAudioRecord (int p0)
		{
			if (id_createCarAudioRecord_I == IntPtr.Zero)
				id_createCarAudioRecord_I = JNIEnv.GetMethodID (class_ref, "createCarAudioRecord", "(I)Landroid/support/car/media/CarAudioRecord;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecord> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_createCarAudioRecord_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getAudioAttributesForCarUsage_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='getAudioAttributesForCarUsage' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;", "GetGetAudioAttributesForCarUsage_IHandler")]
		public override unsafe global::Android.Media.AudioAttributes GetAudioAttributesForCarUsage (int p0)
		{
			if (id_getAudioAttributesForCarUsage_I == IntPtr.Zero)
				id_getAudioAttributesForCarUsage_I = JNIEnv.GetMethodID (class_ref, "getAudioAttributesForCarUsage", "(I)Landroid/media/AudioAttributes;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Media.AudioAttributes> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioAttributesForCarUsage_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='requestAudioFocus' and count(parameter)=3 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IHandler")]
		public override unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;I)I");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_I, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='requestAudioFocus' and count(parameter)=4 and parameter[1][@type='android.media.AudioManager.OnAudioFocusChangeListener'] and parameter[2][@type='android.media.AudioAttributes'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I", "GetRequestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_IIHandler")]
		public override unsafe int RequestAudioFocus (global::Android.Media.AudioManager.IOnAudioFocusChangeListener p0, global::Android.Media.AudioAttributes p1, int p2, int p3)
		{
			if (id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II == IntPtr.Zero)
				id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II = JNIEnv.GetMethodID (class_ref, "requestAudioFocus", "(Landroid/media/AudioManager$OnAudioFocusChangeListener;Landroid/media/AudioAttributes;II)I");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAudioFocus_Landroid_media_AudioManager_OnAudioFocusChangeListener_Landroid_media_AudioAttributes_II, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_setMediaMute_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioManager']/method[@name='setMediaMute' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setMediaMute", "(Z)Z", "GetSetMediaMute_ZHandler")]
		public override unsafe bool SetMediaMute (bool p0)
		{
			if (id_setMediaMute_Z == IntPtr.Zero)
				id_setMediaMute_Z = JNIEnv.GetMethodID (class_ref, "setMediaMute", "(Z)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_setMediaMute_Z, __args);
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
