using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Input {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']"
	[global::Android.Runtime.Register ("android/support/car/input/CarInputManager", DoNotGenerateAcw=true)]
	public abstract partial class CarInputManager : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/input/CarInputManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInputManager); }
		}

		protected CarInputManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/constructor[@name='CarInputManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarInputManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarInputManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_isInputActive;
#pragma warning disable 0169
		static Delegate GetIsInputActiveHandler ()
		{
			if (cb_isInputActive == null)
				cb_isInputActive = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsInputActive);
			return cb_isInputActive;
		}

		static bool n_IsInputActive (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Input.CarInputManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsInputActive;
		}
#pragma warning restore 0169

		public abstract bool IsInputActive {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isInputActive' and count(parameter)=0]"
			[Register ("isInputActive", "()Z", "GetIsInputActiveHandler")] get;
		}

		static Delegate cb_isValid;
#pragma warning disable 0169
		static Delegate GetIsValidHandler ()
		{
			if (cb_isValid == null)
				cb_isValid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsValid);
			return cb_isValid;
		}

		static bool n_IsValid (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Input.CarInputManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsValid;
		}
#pragma warning restore 0169

		public abstract bool IsValid {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isValid' and count(parameter)=0]"
			[Register ("isValid", "()Z", "GetIsValidHandler")] get;
		}

		static Delegate cb_isCurrentCarEditable_Landroid_widget_EditText_;
#pragma warning disable 0169
		static Delegate GetIsCurrentCarEditable_Landroid_widget_EditText_Handler ()
		{
			if (cb_isCurrentCarEditable_Landroid_widget_EditText_ == null)
				cb_isCurrentCarEditable_Landroid_widget_EditText_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_IsCurrentCarEditable_Landroid_widget_EditText_);
			return cb_isCurrentCarEditable_Landroid_widget_EditText_;
		}

		static bool n_IsCurrentCarEditable_Landroid_widget_EditText_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Input.CarInputManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Widget.EditText p0 = global::Java.Lang.Object.GetObject<global::Android.Widget.EditText> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsCurrentCarEditable (p0);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isCurrentCarEditable' and count(parameter)=1 and parameter[1][@type='android.widget.EditText']]"
		[Register ("isCurrentCarEditable", "(Landroid/widget/EditText;)Z", "GetIsCurrentCarEditable_Landroid_widget_EditText_Handler")]
		public abstract bool IsCurrentCarEditable (global::Android.Widget.EditText p0);

		static Delegate cb_startInput_Landroid_widget_EditText_;
#pragma warning disable 0169
		static Delegate GetStartInput_Landroid_widget_EditText_Handler ()
		{
			if (cb_startInput_Landroid_widget_EditText_ == null)
				cb_startInput_Landroid_widget_EditText_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartInput_Landroid_widget_EditText_);
			return cb_startInput_Landroid_widget_EditText_;
		}

		static void n_StartInput_Landroid_widget_EditText_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Input.CarInputManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Widget.EditText p0 = global::Java.Lang.Object.GetObject<global::Android.Widget.EditText> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartInput (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='startInput' and count(parameter)=1 and parameter[1][@type='android.widget.EditText']]"
		[Register ("startInput", "(Landroid/widget/EditText;)V", "GetStartInput_Landroid_widget_EditText_Handler")]
		public abstract void StartInput (global::Android.Widget.EditText p0);

		static Delegate cb_stopInput;
#pragma warning disable 0169
		static Delegate GetStopInputHandler ()
		{
			if (cb_stopInput == null)
				cb_stopInput = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopInput);
			return cb_stopInput;
		}

		static void n_StopInput (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Input.CarInputManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.CarInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopInput ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='stopInput' and count(parameter)=0]"
		[Register ("stopInput", "()V", "GetStopInputHandler")]
		public abstract void StopInput ();

	}

	[global::Android.Runtime.Register ("android/support/car/input/CarInputManager", DoNotGenerateAcw=true)]
	internal partial class CarInputManagerInvoker : CarInputManager {

		public CarInputManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInputManagerInvoker); }
		}

		static IntPtr id_isInputActive;
		public override unsafe bool IsInputActive {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isInputActive' and count(parameter)=0]"
			[Register ("isInputActive", "()Z", "GetIsInputActiveHandler")]
			get {
				if (id_isInputActive == IntPtr.Zero)
					id_isInputActive = JNIEnv.GetMethodID (class_ref, "isInputActive", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isInputActive);
				} finally {
				}
			}
		}

		static IntPtr id_isValid;
		public override unsafe bool IsValid {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isValid' and count(parameter)=0]"
			[Register ("isValid", "()Z", "GetIsValidHandler")]
			get {
				if (id_isValid == IntPtr.Zero)
					id_isValid = JNIEnv.GetMethodID (class_ref, "isValid", "()Z");
				try {
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isValid);
				} finally {
				}
			}
		}

		static IntPtr id_isCurrentCarEditable_Landroid_widget_EditText_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='isCurrentCarEditable' and count(parameter)=1 and parameter[1][@type='android.widget.EditText']]"
		[Register ("isCurrentCarEditable", "(Landroid/widget/EditText;)Z", "GetIsCurrentCarEditable_Landroid_widget_EditText_Handler")]
		public override unsafe bool IsCurrentCarEditable (global::Android.Widget.EditText p0)
		{
			if (id_isCurrentCarEditable_Landroid_widget_EditText_ == IntPtr.Zero)
				id_isCurrentCarEditable_Landroid_widget_EditText_ = JNIEnv.GetMethodID (class_ref, "isCurrentCarEditable", "(Landroid/widget/EditText;)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isCurrentCarEditable_Landroid_widget_EditText_, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_startInput_Landroid_widget_EditText_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='startInput' and count(parameter)=1 and parameter[1][@type='android.widget.EditText']]"
		[Register ("startInput", "(Landroid/widget/EditText;)V", "GetStartInput_Landroid_widget_EditText_Handler")]
		public override unsafe void StartInput (global::Android.Widget.EditText p0)
		{
			if (id_startInput_Landroid_widget_EditText_ == IntPtr.Zero)
				id_startInput_Landroid_widget_EditText_ = JNIEnv.GetMethodID (class_ref, "startInput", "(Landroid/widget/EditText;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startInput_Landroid_widget_EditText_, __args);
			} finally {
			}
		}

		static IntPtr id_stopInput;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/class[@name='CarInputManager']/method[@name='stopInput' and count(parameter)=0]"
		[Register ("stopInput", "()V", "GetStopInputHandler")]
		public override unsafe void StopInput ()
		{
			if (id_stopInput == IntPtr.Zero)
				id_stopInput = JNIEnv.GetMethodID (class_ref, "stopInput", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stopInput);
			} finally {
			}
		}

	}

}
