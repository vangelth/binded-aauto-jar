using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/c", DoNotGenerateAcw=true)]
	public partial class C : global::Android.Content.ContextWrapper, global::Android.Views.LayoutInflater.IFactory, global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/c", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (C); }
		}

		protected C (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/constructor[@name='c' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe C ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (C)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getIntent;
#pragma warning disable 0169
		static Delegate GetGetIntentHandler ()
		{
			if (cb_getIntent == null)
				cb_getIntent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIntent);
			return cb_getIntent;
		}

		static IntPtr n_GetIntent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Intent);
		}
#pragma warning restore 0169

		static Delegate cb_setIntent_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetSetIntent_Landroid_content_Intent_Handler ()
		{
			if (cb_setIntent_Landroid_content_Intent_ == null)
				cb_setIntent_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIntent_Landroid_content_Intent_);
			return cb_setIntent_Landroid_content_Intent_;
		}

		static void n_SetIntent_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Intent = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIntent;
		static IntPtr id_setIntent_Landroid_content_Intent_;
		public virtual unsafe global::Android.Content.Intent Intent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='getIntent' and count(parameter)=0]"
			[Register ("getIntent", "()Landroid/content/Intent;", "GetGetIntentHandler")]
			get {
				if (id_getIntent == IntPtr.Zero)
					id_getIntent = JNIEnv.GetMethodID (class_ref, "getIntent", "()Landroid/content/Intent;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getIntent), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIntent", "()Landroid/content/Intent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='setIntent' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
			[Register ("setIntent", "(Landroid/content/Intent;)V", "GetSetIntent_Landroid_content_Intent_Handler")]
			set {
				if (id_setIntent_Landroid_content_Intent_ == IntPtr.Zero)
					id_setIntent_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "setIntent", "(Landroid/content/Intent;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setIntent_Landroid_content_Intent_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIntent", "(Landroid/content/Intent;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLayoutInflater;
#pragma warning disable 0169
		static Delegate GetGetLayoutInflaterHandler ()
		{
			if (cb_getLayoutInflater == null)
				cb_getLayoutInflater = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLayoutInflater);
			return cb_getLayoutInflater;
		}

		static IntPtr n_GetLayoutInflater (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LayoutInflater);
		}
#pragma warning restore 0169

		static IntPtr id_getLayoutInflater;
		public virtual unsafe global::Android.Views.LayoutInflater LayoutInflater {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='getLayoutInflater' and count(parameter)=0]"
			[Register ("getLayoutInflater", "()Landroid/view/LayoutInflater;", "GetGetLayoutInflaterHandler")]
			get {
				if (id_getLayoutInflater == IntPtr.Zero)
					id_getLayoutInflater = JNIEnv.GetMethodID (class_ref, "getLayoutInflater", "()Landroid/view/LayoutInflater;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Views.LayoutInflater> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLayoutInflater), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Views.LayoutInflater> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLayoutInflater", "()Landroid/view/LayoutInflater;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()Lcom/google/android/gms/car/input/InputManager;", "")]
		public unsafe global::Com.Google.Android.Gms.Car.Input.IInputManager A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()Lcom/google/android/gms/car/input/InputManager;");
			try {
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_a_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='a' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("a", "(Ljava/lang/String;)Ljava/lang/Object;", "")]
		public unsafe global::Java.Lang.Object A (string p0)
		{
			if (id_a_Ljava_lang_String_ == IntPtr.Zero)
				id_a_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "a", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Java.Lang.Object __ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_attach_Lcom_google_android_gms_car_CarActivityHost_;
#pragma warning disable 0169
		static Delegate GetAttach_Lcom_google_android_gms_car_CarActivityHost_Handler ()
		{
			if (cb_attach_Lcom_google_android_gms_car_CarActivityHost_ == null)
				cb_attach_Lcom_google_android_gms_car_CarActivityHost_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Attach_Lcom_google_android_gms_car_CarActivityHost_);
			return cb_attach_Lcom_google_android_gms_car_CarActivityHost_;
		}

		static void n_Attach_Lcom_google_android_gms_car_CarActivityHost_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarActivityHost p0 = (global::Com.Google.Android.Gms.Car.ICarActivityHost)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attach (p0);
		}
#pragma warning restore 0169

		static IntPtr id_attach_Lcom_google_android_gms_car_CarActivityHost_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='attach' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarActivityHost']]"
		[Register ("attach", "(Lcom/google/android/gms/car/CarActivityHost;)V", "GetAttach_Lcom_google_android_gms_car_CarActivityHost_Handler")]
		public virtual unsafe void Attach (global::Com.Google.Android.Gms.Car.ICarActivityHost p0)
		{
			if (id_attach_Lcom_google_android_gms_car_CarActivityHost_ == IntPtr.Zero)
				id_attach_Lcom_google_android_gms_car_CarActivityHost_ = JNIEnv.GetMethodID (class_ref, "attach", "(Lcom/google/android/gms/car/CarActivityHost;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_attach_Lcom_google_android_gms_car_CarActivityHost_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "attach", "(Lcom/google/android/gms/car/CarActivityHost;)V"), __args);
			} finally {
			}
		}

		static IntPtr id_b;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='b' and count(parameter)=0]"
		[Register ("b", "()Z", "")]
		public unsafe bool B ()
		{
			if (id_b == IntPtr.Zero)
				id_b = JNIEnv.GetMethodID (class_ref, "b", "()Z");
			try {
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_b);
			} finally {
			}
		}

		static IntPtr id_c;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='c' and count(parameter)=0]"
		[Register ("c", "()Landroid/view/Window;", "")]
		public unsafe global::Android.Views.Window InvokeC ()
		{
			if (id_c == IntPtr.Zero)
				id_c = JNIEnv.GetMethodID (class_ref, "c", "()Landroid/view/Window;");
			try {
				return global::Java.Lang.Object.GetObject<global::Android.Views.Window> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_c), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_d;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='d' and count(parameter)=0]"
		[Register ("d", "()Ljava/lang/Object;", "")]
		public unsafe global::Java.Lang.Object D ()
		{
			if (id_d == IntPtr.Zero)
				id_d = JNIEnv.GetMethodID (class_ref, "d", "()Ljava/lang/Object;");
			try {
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_d), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler ()
		{
			if (cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == null)
				cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_);
			return cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		}

		static void n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, IntPtr native_p2)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.FileDescriptor p0 = global::Java.Lang.Object.GetObject<global::Java.IO.FileDescriptor> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.PrintWriter p1 = global::Java.Lang.Object.GetObject<global::Java.IO.PrintWriter> (native_p1, JniHandleOwnership.DoNotTransfer);
			string[] p2 = (string[]) JNIEnv.GetArray (native_p2, JniHandleOwnership.DoNotTransfer, typeof (string));
			__this.Dump (p0, p1, p2);
			if (p2 != null)
				JNIEnv.CopyArray (p2, native_p2);
		}
#pragma warning restore 0169

		static IntPtr id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='dump' and count(parameter)=3 and parameter[1][@type='java.io.FileDescriptor'] and parameter[2][@type='java.io.PrintWriter'] and parameter[3][@type='java.lang.String[]']]"
		[Register ("dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V", "GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler")]
		public virtual unsafe void Dump (global::Java.IO.FileDescriptor p0, global::Java.IO.PrintWriter p1, string[] p2)
		{
			if (id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == IntPtr.Zero)
				id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V");
			IntPtr native_p2 = JNIEnv.NewArray (p2);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (native_p2);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V"), __args);
			} finally {
				if (p2 != null) {
					JNIEnv.CopyArray (native_p2, p2);
					JNIEnv.DeleteLocalRef (native_p2);
				}
			}
		}

		static Delegate cb_findViewById_I;
#pragma warning disable 0169
		static Delegate GetFindViewById_IHandler ()
		{
			if (cb_findViewById_I == null)
				cb_findViewById_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_FindViewById_I);
			return cb_findViewById_I;
		}

		static IntPtr n_FindViewById_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FindViewById (p0));
		}
#pragma warning restore 0169

		static IntPtr id_findViewById_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='findViewById' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("findViewById", "(I)Landroid/view/View;", "GetFindViewById_IHandler")]
		public virtual unsafe global::Android.Views.View FindViewById (int p0)
		{
			if (id_findViewById_I == IntPtr.Zero)
				id_findViewById_I = JNIEnv.GetMethodID (class_ref, "findViewById", "(I)Landroid/view/View;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_findViewById_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "findViewById", "(I)Landroid/view/View;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_onAccessibilityScanRequested_Landroid_os_IBinder_;
#pragma warning disable 0169
		static Delegate GetOnAccessibilityScanRequested_Landroid_os_IBinder_Handler ()
		{
			if (cb_onAccessibilityScanRequested_Landroid_os_IBinder_ == null)
				cb_onAccessibilityScanRequested_Landroid_os_IBinder_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnAccessibilityScanRequested_Landroid_os_IBinder_);
			return cb_onAccessibilityScanRequested_Landroid_os_IBinder_;
		}

		static void n_OnAccessibilityScanRequested_Landroid_os_IBinder_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.IBinder p0 = (global::Android.OS.IBinder)global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnAccessibilityScanRequested (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onAccessibilityScanRequested_Landroid_os_IBinder_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onAccessibilityScanRequested' and count(parameter)=1 and parameter[1][@type='android.os.IBinder']]"
		[Register ("onAccessibilityScanRequested", "(Landroid/os/IBinder;)V", "GetOnAccessibilityScanRequested_Landroid_os_IBinder_Handler")]
		public virtual unsafe void OnAccessibilityScanRequested (global::Android.OS.IBinder p0)
		{
			if (id_onAccessibilityScanRequested_Landroid_os_IBinder_ == IntPtr.Zero)
				id_onAccessibilityScanRequested_Landroid_os_IBinder_ = JNIEnv.GetMethodID (class_ref, "onAccessibilityScanRequested", "(Landroid/os/IBinder;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAccessibilityScanRequested_Landroid_os_IBinder_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onAccessibilityScanRequested", "(Landroid/os/IBinder;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onBackPressed;
#pragma warning disable 0169
		static Delegate GetOnBackPressedHandler ()
		{
			if (cb_onBackPressed == null)
				cb_onBackPressed = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnBackPressed);
			return cb_onBackPressed;
		}

		static void n_OnBackPressed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnBackPressed ();
		}
#pragma warning restore 0169

		static IntPtr id_onBackPressed;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onBackPressed' and count(parameter)=0]"
		[Register ("onBackPressed", "()V", "GetOnBackPressedHandler")]
		public virtual unsafe void OnBackPressed ()
		{
			if (id_onBackPressed == IntPtr.Zero)
				id_onBackPressed = JNIEnv.GetMethodID (class_ref, "onBackPressed", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onBackPressed);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onBackPressed", "()V"));
			} finally {
			}
		}

		static Delegate cb_onConfigurationChanged_Landroid_content_res_Configuration_;
#pragma warning disable 0169
		static Delegate GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler ()
		{
			if (cb_onConfigurationChanged_Landroid_content_res_Configuration_ == null)
				cb_onConfigurationChanged_Landroid_content_res_Configuration_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnConfigurationChanged_Landroid_content_res_Configuration_);
			return cb_onConfigurationChanged_Landroid_content_res_Configuration_;
		}

		static void n_OnConfigurationChanged_Landroid_content_res_Configuration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Res.Configuration p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Res.Configuration> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnConfigurationChanged (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onConfigurationChanged_Landroid_content_res_Configuration_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onConfigurationChanged' and count(parameter)=1 and parameter[1][@type='android.content.res.Configuration']]"
		[Register ("onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler")]
		public virtual unsafe void OnConfigurationChanged (global::Android.Content.Res.Configuration p0)
		{
			if (id_onConfigurationChanged_Landroid_content_res_Configuration_ == IntPtr.Zero)
				id_onConfigurationChanged_Landroid_content_res_Configuration_ = JNIEnv.GetMethodID (class_ref, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConfigurationChanged_Landroid_content_res_Configuration_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onCreate_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnCreate_Landroid_os_Bundle_Handler ()
		{
			if (cb_onCreate_Landroid_os_Bundle_ == null)
				cb_onCreate_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnCreate_Landroid_os_Bundle_);
			return cb_onCreate_Landroid_os_Bundle_;
		}

		static void n_OnCreate_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnCreate (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onCreate_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onCreate' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onCreate", "(Landroid/os/Bundle;)V", "GetOnCreate_Landroid_os_Bundle_Handler")]
		public virtual unsafe void OnCreate (global::Android.OS.Bundle p0)
		{
			if (id_onCreate_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onCreate_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onCreate", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCreate_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCreate", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_;
#pragma warning disable 0169
		static Delegate GetOnCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_Handler ()
		{
			if (cb_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_ == null)
				cb_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_OnCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_);
			return cb_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_;
		}

		static IntPtr n_OnCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, IntPtr native_p2)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Context p1 = global::Java.Lang.Object.GetObject<global::Android.Content.Context> (native_p1, JniHandleOwnership.DoNotTransfer);
			global::Android.Util.IAttributeSet p2 = (global::Android.Util.IAttributeSet)global::Java.Lang.Object.GetObject<global::Android.Util.IAttributeSet> (native_p2, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.OnCreateView (p0, p1, p2));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onCreateView' and count(parameter)=3 and parameter[1][@type='java.lang.String'] and parameter[2][@type='android.content.Context'] and parameter[3][@type='android.util.AttributeSet']]"
		[Register ("onCreateView", "(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;", "GetOnCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_Handler")]
		public virtual unsafe global::Android.Views.View OnCreateView (string p0, global::Android.Content.Context p1, global::Android.Util.IAttributeSet p2)
		{
			if (id_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
				id_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "onCreateView", "(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				global::Android.Views.View __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onCreateView_Ljava_lang_String_Landroid_content_Context_Landroid_util_AttributeSet_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCreateView", "(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_onDestroy;
#pragma warning disable 0169
		static Delegate GetOnDestroyHandler ()
		{
			if (cb_onDestroy == null)
				cb_onDestroy = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDestroy);
			return cb_onDestroy;
		}

		static void n_OnDestroy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDestroy ();
		}
#pragma warning restore 0169

		static IntPtr id_onDestroy;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onDestroy' and count(parameter)=0]"
		[Register ("onDestroy", "()V", "GetOnDestroyHandler")]
		public virtual unsafe void OnDestroy ()
		{
			if (id_onDestroy == IntPtr.Zero)
				id_onDestroy = JNIEnv.GetMethodID (class_ref, "onDestroy", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDestroy);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDestroy", "()V"));
			} finally {
			}
		}

		static Delegate cb_onFrameRateChange_I;
#pragma warning disable 0169
		static Delegate GetOnFrameRateChange_IHandler ()
		{
			if (cb_onFrameRateChange_I == null)
				cb_onFrameRateChange_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnFrameRateChange_I);
			return cb_onFrameRateChange_I;
		}

		static void n_OnFrameRateChange_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnFrameRateChange (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onFrameRateChange_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onFrameRateChange' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onFrameRateChange", "(I)V", "GetOnFrameRateChange_IHandler")]
		public virtual unsafe void OnFrameRateChange (int p0)
		{
			if (id_onFrameRateChange_I == IntPtr.Zero)
				id_onFrameRateChange_I = JNIEnv.GetMethodID (class_ref, "onFrameRateChange", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onFrameRateChange_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onFrameRateChange", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onKeyDown_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyDown_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyDown_ILandroid_view_KeyEvent_ == null)
				cb_onKeyDown_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyDown_ILandroid_view_KeyEvent_);
			return cb_onKeyDown_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyDown_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyDown (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onKeyDown_ILandroid_view_KeyEvent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onKeyDown' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyDown", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyDown_ILandroid_view_KeyEvent_Handler")]
		public virtual unsafe bool OnKeyDown (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyDown_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyDown_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyDown", "(ILandroid/view/KeyEvent;)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyDown_ILandroid_view_KeyEvent_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onKeyDown", "(ILandroid/view/KeyEvent;)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_onKeyLongPress_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyLongPress_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyLongPress_ILandroid_view_KeyEvent_ == null)
				cb_onKeyLongPress_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyLongPress_ILandroid_view_KeyEvent_);
			return cb_onKeyLongPress_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyLongPress_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyLongPress (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onKeyLongPress_ILandroid_view_KeyEvent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onKeyLongPress' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyLongPress", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyLongPress_ILandroid_view_KeyEvent_Handler")]
		public virtual unsafe bool OnKeyLongPress (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyLongPress_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyLongPress_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyLongPress", "(ILandroid/view/KeyEvent;)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyLongPress_ILandroid_view_KeyEvent_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onKeyLongPress", "(ILandroid/view/KeyEvent;)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_onKeyUp_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyUp_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyUp_ILandroid_view_KeyEvent_ == null)
				cb_onKeyUp_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyUp_ILandroid_view_KeyEvent_);
			return cb_onKeyUp_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyUp_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyUp (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onKeyUp_ILandroid_view_KeyEvent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onKeyUp' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyUp", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyUp_ILandroid_view_KeyEvent_Handler")]
		public virtual unsafe bool OnKeyUp (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyUp_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyUp_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyUp", "(ILandroid/view/KeyEvent;)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyUp_ILandroid_view_KeyEvent_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onKeyUp", "(ILandroid/view/KeyEvent;)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_onLowMemory;
#pragma warning disable 0169
		static Delegate GetOnLowMemoryHandler ()
		{
			if (cb_onLowMemory == null)
				cb_onLowMemory = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnLowMemory);
			return cb_onLowMemory;
		}

		static void n_OnLowMemory (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnLowMemory ();
		}
#pragma warning restore 0169

		static IntPtr id_onLowMemory;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onLowMemory' and count(parameter)=0]"
		[Register ("onLowMemory", "()V", "GetOnLowMemoryHandler")]
		public virtual unsafe void OnLowMemory ()
		{
			if (id_onLowMemory == IntPtr.Zero)
				id_onLowMemory = JNIEnv.GetMethodID (class_ref, "onLowMemory", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onLowMemory);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onLowMemory", "()V"));
			} finally {
			}
		}

		static Delegate cb_onNewIntent_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnNewIntent_Landroid_content_Intent_Handler ()
		{
			if (cb_onNewIntent_Landroid_content_Intent_ == null)
				cb_onNewIntent_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnNewIntent_Landroid_content_Intent_);
			return cb_onNewIntent_Landroid_content_Intent_;
		}

		static void n_OnNewIntent_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnNewIntent (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onNewIntent_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onNewIntent' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onNewIntent", "(Landroid/content/Intent;)V", "GetOnNewIntent_Landroid_content_Intent_Handler")]
		public virtual unsafe void OnNewIntent (global::Android.Content.Intent p0)
		{
			if (id_onNewIntent_Landroid_content_Intent_ == IntPtr.Zero)
				id_onNewIntent_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onNewIntent", "(Landroid/content/Intent;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onNewIntent_Landroid_content_Intent_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onNewIntent", "(Landroid/content/Intent;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onPause;
#pragma warning disable 0169
		static Delegate GetOnPauseHandler ()
		{
			if (cb_onPause == null)
				cb_onPause = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPause);
			return cb_onPause;
		}

		static void n_OnPause (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPause ();
		}
#pragma warning restore 0169

		static IntPtr id_onPause;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onPause' and count(parameter)=0]"
		[Register ("onPause", "()V", "GetOnPauseHandler")]
		public virtual unsafe void OnPause ()
		{
			if (id_onPause == IntPtr.Zero)
				id_onPause = JNIEnv.GetMethodID (class_ref, "onPause", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPause);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPause", "()V"));
			} finally {
			}
		}

		static Delegate cb_onPostResume;
#pragma warning disable 0169
		static Delegate GetOnPostResumeHandler ()
		{
			if (cb_onPostResume == null)
				cb_onPostResume = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPostResume);
			return cb_onPostResume;
		}

		static void n_OnPostResume (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPostResume ();
		}
#pragma warning restore 0169

		static IntPtr id_onPostResume;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onPostResume' and count(parameter)=0]"
		[Register ("onPostResume", "()V", "GetOnPostResumeHandler")]
		public virtual unsafe void OnPostResume ()
		{
			if (id_onPostResume == IntPtr.Zero)
				id_onPostResume = JNIEnv.GetMethodID (class_ref, "onPostResume", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPostResume);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPostResume", "()V"));
			} finally {
			}
		}

		static Delegate cb_onPowerStateChange_I;
#pragma warning disable 0169
		static Delegate GetOnPowerStateChange_IHandler ()
		{
			if (cb_onPowerStateChange_I == null)
				cb_onPowerStateChange_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnPowerStateChange_I);
			return cb_onPowerStateChange_I;
		}

		static void n_OnPowerStateChange_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPowerStateChange (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onPowerStateChange_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onPowerStateChange' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onPowerStateChange", "(I)V", "GetOnPowerStateChange_IHandler")]
		public virtual unsafe void OnPowerStateChange (int p0)
		{
			if (id_onPowerStateChange_I == IntPtr.Zero)
				id_onPowerStateChange_I = JNIEnv.GetMethodID (class_ref, "onPowerStateChange", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPowerStateChange_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onPowerStateChange", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onRestoreInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnRestoreInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onRestoreInstanceState_Landroid_os_Bundle_ == null)
				cb_onRestoreInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnRestoreInstanceState_Landroid_os_Bundle_);
			return cb_onRestoreInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnRestoreInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnRestoreInstanceState (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onRestoreInstanceState_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onRestoreInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onRestoreInstanceState", "(Landroid/os/Bundle;)V", "GetOnRestoreInstanceState_Landroid_os_Bundle_Handler")]
		public virtual unsafe void OnRestoreInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onRestoreInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onRestoreInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onRestoreInstanceState", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onRestoreInstanceState_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onRestoreInstanceState", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onResume;
#pragma warning disable 0169
		static Delegate GetOnResumeHandler ()
		{
			if (cb_onResume == null)
				cb_onResume = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnResume);
			return cb_onResume;
		}

		static void n_OnResume (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnResume ();
		}
#pragma warning restore 0169

		static IntPtr id_onResume;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onResume' and count(parameter)=0]"
		[Register ("onResume", "()V", "GetOnResumeHandler")]
		public virtual unsafe void OnResume ()
		{
			if (id_onResume == IntPtr.Zero)
				id_onResume = JNIEnv.GetMethodID (class_ref, "onResume", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onResume);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onResume", "()V"));
			} finally {
			}
		}

		static Delegate cb_onRetainNonConfigurationInstance;
#pragma warning disable 0169
		static Delegate GetOnRetainNonConfigurationInstanceHandler ()
		{
			if (cb_onRetainNonConfigurationInstance == null)
				cb_onRetainNonConfigurationInstance = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_OnRetainNonConfigurationInstance);
			return cb_onRetainNonConfigurationInstance;
		}

		static IntPtr n_OnRetainNonConfigurationInstance (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OnRetainNonConfigurationInstance ());
		}
#pragma warning restore 0169

		static IntPtr id_onRetainNonConfigurationInstance;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onRetainNonConfigurationInstance' and count(parameter)=0]"
		[Register ("onRetainNonConfigurationInstance", "()Ljava/lang/Object;", "GetOnRetainNonConfigurationInstanceHandler")]
		public virtual unsafe global::Java.Lang.Object OnRetainNonConfigurationInstance ()
		{
			if (id_onRetainNonConfigurationInstance == IntPtr.Zero)
				id_onRetainNonConfigurationInstance = JNIEnv.GetMethodID (class_ref, "onRetainNonConfigurationInstance", "()Ljava/lang/Object;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onRetainNonConfigurationInstance), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onRetainNonConfigurationInstance", "()Ljava/lang/Object;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_onSaveInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnSaveInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onSaveInstanceState_Landroid_os_Bundle_ == null)
				cb_onSaveInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnSaveInstanceState_Landroid_os_Bundle_);
			return cb_onSaveInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnSaveInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnSaveInstanceState (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onSaveInstanceState_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onSaveInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onSaveInstanceState", "(Landroid/os/Bundle;)V", "GetOnSaveInstanceState_Landroid_os_Bundle_Handler")]
		public virtual unsafe void OnSaveInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onSaveInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onSaveInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onSaveInstanceState", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSaveInstanceState_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onSaveInstanceState", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onStart;
#pragma warning disable 0169
		static Delegate GetOnStartHandler ()
		{
			if (cb_onStart == null)
				cb_onStart = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStart);
			return cb_onStart;
		}

		static void n_OnStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStart ();
		}
#pragma warning restore 0169

		static IntPtr id_onStart;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onStart' and count(parameter)=0]"
		[Register ("onStart", "()V", "GetOnStartHandler")]
		public virtual unsafe void OnStart ()
		{
			if (id_onStart == IntPtr.Zero)
				id_onStart = JNIEnv.GetMethodID (class_ref, "onStart", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStart);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onStart", "()V"));
			} finally {
			}
		}

		static Delegate cb_onStop;
#pragma warning disable 0169
		static Delegate GetOnStopHandler ()
		{
			if (cb_onStop == null)
				cb_onStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStop);
			return cb_onStop;
		}

		static void n_OnStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStop ();
		}
#pragma warning restore 0169

		static IntPtr id_onStop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onStop' and count(parameter)=0]"
		[Register ("onStop", "()V", "GetOnStopHandler")]
		public virtual unsafe void OnStop ()
		{
			if (id_onStop == IntPtr.Zero)
				id_onStop = JNIEnv.GetMethodID (class_ref, "onStop", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStop);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onStop", "()V"));
			} finally {
			}
		}

		static Delegate cb_onWindowFocusChanged_ZZ;
#pragma warning disable 0169
		static Delegate GetOnWindowFocusChanged_ZZHandler ()
		{
			if (cb_onWindowFocusChanged_ZZ == null)
				cb_onWindowFocusChanged_ZZ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool, bool>) n_OnWindowFocusChanged_ZZ);
			return cb_onWindowFocusChanged_ZZ;
		}

		static void n_OnWindowFocusChanged_ZZ (IntPtr jnienv, IntPtr native__this, bool p0, bool p1)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnWindowFocusChanged (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_onWindowFocusChanged_ZZ;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='onWindowFocusChanged' and count(parameter)=2 and parameter[1][@type='boolean'] and parameter[2][@type='boolean']]"
		[Register ("onWindowFocusChanged", "(ZZ)V", "GetOnWindowFocusChanged_ZZHandler")]
		public virtual unsafe void OnWindowFocusChanged (bool p0, bool p1)
		{
			if (id_onWindowFocusChanged_ZZ == IntPtr.Zero)
				id_onWindowFocusChanged_ZZ = JNIEnv.GetMethodID (class_ref, "onWindowFocusChanged", "(ZZ)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onWindowFocusChanged_ZZ, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onWindowFocusChanged", "(ZZ)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setContentView_Landroid_view_View_;
#pragma warning disable 0169
		static Delegate GetSetContentView_Landroid_view_View_Handler ()
		{
			if (cb_setContentView_Landroid_view_View_ == null)
				cb_setContentView_Landroid_view_View_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentView_Landroid_view_View_);
			return cb_setContentView_Landroid_view_View_;
		}

		static void n_SetContentView_Landroid_view_View_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetContentView (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setContentView_Landroid_view_View_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='setContentView' and count(parameter)=1 and parameter[1][@type='android.view.View']]"
		[Register ("setContentView", "(Landroid/view/View;)V", "GetSetContentView_Landroid_view_View_Handler")]
		public virtual unsafe void SetContentView (global::Android.Views.View p0)
		{
			if (id_setContentView_Landroid_view_View_ == IntPtr.Zero)
				id_setContentView_Landroid_view_View_ = JNIEnv.GetMethodID (class_ref, "setContentView", "(Landroid/view/View;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContentView_Landroid_view_View_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentView", "(Landroid/view/View;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setContentView_I;
#pragma warning disable 0169
		static Delegate GetSetContentView_IHandler ()
		{
			if (cb_setContentView_I == null)
				cb_setContentView_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetContentView_I);
			return cb_setContentView_I;
		}

		static void n_SetContentView_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetContentView (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setContentView_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='setContentView' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setContentView", "(I)V", "GetSetContentView_IHandler")]
		public virtual unsafe void SetContentView (int p0)
		{
			if (id_setContentView_I == IntPtr.Zero)
				id_setContentView_I = JNIEnv.GetMethodID (class_ref, "setContentView", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContentView_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentView", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setContext_Landroid_content_Context_;
#pragma warning disable 0169
		static Delegate GetSetContext_Landroid_content_Context_Handler ()
		{
			if (cb_setContext_Landroid_content_Context_ == null)
				cb_setContext_Landroid_content_Context_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContext_Landroid_content_Context_);
			return cb_setContext_Landroid_content_Context_;
		}

		static void n_SetContext_Landroid_content_Context_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Context p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Context> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetContext (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setContext_Landroid_content_Context_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='setContext' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("setContext", "(Landroid/content/Context;)V", "GetSetContext_Landroid_content_Context_Handler")]
		public virtual unsafe void SetContext (global::Android.Content.Context p0)
		{
			if (id_setContext_Landroid_content_Context_ == IntPtr.Zero)
				id_setContext_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "setContext", "(Landroid/content/Context;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContext_Landroid_content_Context_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContext", "(Landroid/content/Context;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setIgnoreConfigChanges_I;
#pragma warning disable 0169
		static Delegate GetSetIgnoreConfigChanges_IHandler ()
		{
			if (cb_setIgnoreConfigChanges_I == null)
				cb_setIgnoreConfigChanges_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetIgnoreConfigChanges_I);
			return cb_setIgnoreConfigChanges_I;
		}

		static void n_SetIgnoreConfigChanges_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.C __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.C> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetIgnoreConfigChanges (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setIgnoreConfigChanges_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='c']/method[@name='setIgnoreConfigChanges' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setIgnoreConfigChanges", "(I)V", "GetSetIgnoreConfigChanges_IHandler")]
		public virtual unsafe void SetIgnoreConfigChanges (int p0)
		{
			if (id_setIgnoreConfigChanges_I == IntPtr.Zero)
				id_setIgnoreConfigChanges_I = JNIEnv.GetMethodID (class_ref, "setIgnoreConfigChanges", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setIgnoreConfigChanges_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIgnoreConfigChanges", "(I)V"), __args);
			} finally {
			}
		}

	}
}
