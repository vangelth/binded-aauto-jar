using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/SearchController", DoNotGenerateAcw=true)]
	public partial class SearchController : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/SearchController", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (SearchController); }
		}

		protected SearchController (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_hideSearchBox;
#pragma warning disable 0169
		static Delegate GetHideSearchBoxHandler ()
		{
			if (cb_hideSearchBox == null)
				cb_hideSearchBox = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideSearchBox);
			return cb_hideSearchBox;
		}

		static void n_HideSearchBox (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideSearchBox ();
		}
#pragma warning restore 0169

		static IntPtr id_hideSearchBox;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='hideSearchBox' and count(parameter)=0]"
		[Register ("hideSearchBox", "()V", "GetHideSearchBoxHandler")]
		public virtual unsafe void HideSearchBox ()
		{
			if (id_hideSearchBox == IntPtr.Zero)
				id_hideSearchBox = JNIEnv.GetMethodID (class_ref, "hideSearchBox", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideSearchBox);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideSearchBox", "()V"));
			} finally {
			}
		}

		static Delegate cb_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_;
#pragma warning disable 0169
		static Delegate GetSetSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_Handler ()
		{
			if (cb_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_ == null)
				cb_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_);
			return cb_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_;
		}

		static void n_SetSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetSearchCallback (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='setSearchCallback' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.SearchCallback']]"
		[Register ("setSearchCallback", "(Lcom/google/android/apps/auto/sdk/SearchCallback;)V", "GetSetSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_Handler")]
		public virtual unsafe void SetSearchCallback (global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback p0)
		{
			if (id_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_ == IntPtr.Zero)
				id_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_ = JNIEnv.GetMethodID (class_ref, "setSearchCallback", "(Lcom/google/android/apps/auto/sdk/SearchCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSearchCallback_Lcom_google_android_apps_auto_sdk_SearchCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSearchCallback", "(Lcom/google/android/apps/auto/sdk/SearchCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setSearchHint_Ljava_lang_CharSequence_;
#pragma warning disable 0169
		static Delegate GetSetSearchHint_Ljava_lang_CharSequence_Handler ()
		{
			if (cb_setSearchHint_Ljava_lang_CharSequence_ == null)
				cb_setSearchHint_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSearchHint_Ljava_lang_CharSequence_);
			return cb_setSearchHint_Ljava_lang_CharSequence_;
		}

		static void n_SetSearchHint_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetSearchHint (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setSearchHint_Ljava_lang_CharSequence_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='setSearchHint' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
		[Register ("setSearchHint", "(Ljava/lang/CharSequence;)V", "GetSetSearchHint_Ljava_lang_CharSequence_Handler")]
		public virtual unsafe void SetSearchHint (global::Java.Lang.ICharSequence p0)
		{
			if (id_setSearchHint_Ljava_lang_CharSequence_ == IntPtr.Zero)
				id_setSearchHint_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setSearchHint", "(Ljava/lang/CharSequence;)V");
			IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSearchHint_Ljava_lang_CharSequence_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSearchHint", "(Ljava/lang/CharSequence;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		public void SetSearchHint (string p0)
		{
			global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
			SetSearchHint (jls_p0);
			jls_p0?.Dispose ();
		}

		static Delegate cb_setSearchItems_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetSearchItems_Ljava_util_List_Handler ()
		{
			if (cb_setSearchItems_Ljava_util_List_ == null)
				cb_setSearchItems_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSearchItems_Ljava_util_List_);
			return cb_setSearchItems_Ljava_util_List_;
		}

		static void n_SetSearchItems_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Google.Android.Apps.Auto.Sdk.SearchItem>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetSearchItems (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setSearchItems_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='setSearchItems' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.google.android.apps.auto.sdk.SearchItem&gt;']]"
		[Register ("setSearchItems", "(Ljava/util/List;)V", "GetSetSearchItems_Ljava_util_List_Handler")]
		public virtual unsafe void SetSearchItems (global::System.Collections.Generic.IList<global::Com.Google.Android.Apps.Auto.Sdk.SearchItem> p0)
		{
			if (id_setSearchItems_Ljava_util_List_ == IntPtr.Zero)
				id_setSearchItems_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setSearchItems", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Com.Google.Android.Apps.Auto.Sdk.SearchItem>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSearchItems_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSearchItems", "(Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_showSearchBox;
#pragma warning disable 0169
		static Delegate GetShowSearchBoxHandler ()
		{
			if (cb_showSearchBox == null)
				cb_showSearchBox = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowSearchBox);
			return cb_showSearchBox;
		}

		static void n_ShowSearchBox (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowSearchBox ();
		}
#pragma warning restore 0169

		static IntPtr id_showSearchBox;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='showSearchBox' and count(parameter)=0]"
		[Register ("showSearchBox", "()V", "GetShowSearchBoxHandler")]
		public virtual unsafe void ShowSearchBox ()
		{
			if (id_showSearchBox == IntPtr.Zero)
				id_showSearchBox = JNIEnv.GetMethodID (class_ref, "showSearchBox", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showSearchBox);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showSearchBox", "()V"));
			} finally {
			}
		}

		static Delegate cb_startSearch_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetStartSearch_Ljava_lang_String_Handler ()
		{
			if (cb_startSearch_Ljava_lang_String_ == null)
				cb_startSearch_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartSearch_Ljava_lang_String_);
			return cb_startSearch_Ljava_lang_String_;
		}

		static void n_StartSearch_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartSearch (p0);
		}
#pragma warning restore 0169

		static IntPtr id_startSearch_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='startSearch' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("startSearch", "(Ljava/lang/String;)V", "GetStartSearch_Ljava_lang_String_Handler")]
		public virtual unsafe void StartSearch (string p0)
		{
			if (id_startSearch_Ljava_lang_String_ == IntPtr.Zero)
				id_startSearch_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "startSearch", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startSearch_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startSearch", "(Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_stopSearch;
#pragma warning disable 0169
		static Delegate GetStopSearchHandler ()
		{
			if (cb_stopSearch == null)
				cb_stopSearch = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopSearch);
			return cb_stopSearch;
		}

		static void n_StopSearch (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopSearch ();
		}
#pragma warning restore 0169

		static IntPtr id_stopSearch;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchController']/method[@name='stopSearch' and count(parameter)=0]"
		[Register ("stopSearch", "()V", "GetStopSearchHandler")]
		public virtual unsafe void StopSearch ()
		{
			if (id_stopSearch == IntPtr.Zero)
				id_stopSearch = JNIEnv.GetMethodID (class_ref, "stopSearch", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stopSearch);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stopSearch", "()V"));
			} finally {
			}
		}

	}
}
