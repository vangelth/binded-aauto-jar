using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/AlphaJumpKeyItem", DoNotGenerateAcw=true)]
	public partial class AlphaJumpKeyItem : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder", DoNotGenerateAcw=true)]
		public partial class Builder : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Builder); }
			}

			protected Builder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']/constructor[@name='AlphaJumpKeyItem.Builder' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe Builder ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_build;
#pragma warning disable 0169
			static Delegate GetBuildHandler ()
			{
				if (cb_build == null)
					cb_build = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Build);
				return cb_build;
			}

			static IntPtr n_Build (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.Build ());
			}
#pragma warning restore 0169

			static IntPtr id_build;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']/method[@name='build' and count(parameter)=0]"
			[Register ("build", "()Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem;", "GetBuildHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem Build ()
			{
				if (id_build == IntPtr.Zero)
					id_build = JNIEnv.GetMethodID (class_ref, "build", "()Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_build), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "build", "()Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setCharacter_C;
#pragma warning disable 0169
			static Delegate GetSetCharacter_CHandler ()
			{
				if (cb_setCharacter_C == null)
					cb_setCharacter_C = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, char, IntPtr>) n_SetCharacter_C);
				return cb_setCharacter_C;
			}

			static IntPtr n_SetCharacter_C (IntPtr jnienv, IntPtr native__this, char p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetCharacter (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setCharacter_C;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']/method[@name='setCharacter' and count(parameter)=1 and parameter[1][@type='char']]"
			[Register ("setCharacter", "(C)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;", "GetSetCharacter_CHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder SetCharacter (char p0)
			{
				if (id_setCharacter_C == IntPtr.Zero)
					id_setCharacter_C = JNIEnv.GetMethodID (class_ref, "setCharacter", "(C)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setCharacter_C, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCharacter", "(C)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setEnabled_Z;
#pragma warning disable 0169
			static Delegate GetSetEnabled_ZHandler ()
			{
				if (cb_setEnabled_Z == null)
					cb_setEnabled_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, IntPtr>) n_SetEnabled_Z);
				return cb_setEnabled_Z;
			}

			static IntPtr n_SetEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetEnabled (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setEnabled_Z;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']/method[@name='setEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setEnabled", "(Z)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;", "GetSetEnabled_ZHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder SetEnabled (bool p0)
			{
				if (id_setEnabled_Z == IntPtr.Zero)
					id_setEnabled_Z = JNIEnv.GetMethodID (class_ref, "setEnabled", "(Z)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setEnabled_Z, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEnabled", "(Z)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setJumpPosition_I;
#pragma warning disable 0169
			static Delegate GetSetJumpPosition_IHandler ()
			{
				if (cb_setJumpPosition_I == null)
					cb_setJumpPosition_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetJumpPosition_I);
				return cb_setJumpPosition_I;
			}

			static IntPtr n_SetJumpPosition_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetJumpPosition (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setJumpPosition_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem.Builder']/method[@name='setJumpPosition' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setJumpPosition", "(I)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;", "GetSetJumpPosition_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder SetJumpPosition (int p0)
			{
				if (id_setJumpPosition_I == IntPtr.Zero)
					id_setJumpPosition_I = JNIEnv.GetMethodID (class_ref, "setJumpPosition", "(I)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setJumpPosition_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setJumpPosition", "(I)Lcom/google/android/apps/auto/sdk/AlphaJumpKeyItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/AlphaJumpKeyItem", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AlphaJumpKeyItem); }
		}

		protected AlphaJumpKeyItem (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getCharacter;
#pragma warning disable 0169
		static Delegate GetGetCharacterHandler ()
		{
			if (cb_getCharacter == null)
				cb_getCharacter = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, char>) n_GetCharacter);
			return cb_getCharacter;
		}

		static char n_GetCharacter (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Character;
		}
#pragma warning restore 0169

		static IntPtr id_getCharacter;
		public virtual unsafe char Character {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/method[@name='getCharacter' and count(parameter)=0]"
			[Register ("getCharacter", "()C", "GetGetCharacterHandler")]
			get {
				if (id_getCharacter == IntPtr.Zero)
					id_getCharacter = JNIEnv.GetMethodID (class_ref, "getCharacter", "()C");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallCharMethod (((global::Java.Lang.Object) this).Handle, id_getCharacter);
					else
						return JNIEnv.CallNonvirtualCharMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCharacter", "()C"));
				} finally {
				}
			}
		}

		static Delegate cb_isEnabled;
#pragma warning disable 0169
		static Delegate GetIsEnabledHandler ()
		{
			if (cb_isEnabled == null)
				cb_isEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsEnabled);
			return cb_isEnabled;
		}

		static bool n_IsEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isEnabled;
		public virtual unsafe bool IsEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/method[@name='isEnabled' and count(parameter)=0]"
			[Register ("isEnabled", "()Z", "GetIsEnabledHandler")]
			get {
				if (id_isEnabled == IntPtr.Zero)
					id_isEnabled = JNIEnv.GetMethodID (class_ref, "isEnabled", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isEnabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getJumpPosition;
#pragma warning disable 0169
		static Delegate GetGetJumpPositionHandler ()
		{
			if (cb_getJumpPosition == null)
				cb_getJumpPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetJumpPosition);
			return cb_getJumpPosition;
		}

		static int n_GetJumpPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.JumpPosition;
		}
#pragma warning restore 0169

		static IntPtr id_getJumpPosition;
		public virtual unsafe int JumpPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/method[@name='getJumpPosition' and count(parameter)=0]"
			[Register ("getJumpPosition", "()I", "GetGetJumpPositionHandler")]
			get {
				if (id_getJumpPosition == IntPtr.Zero)
					id_getJumpPosition = JNIEnv.GetMethodID (class_ref, "getJumpPosition", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getJumpPosition);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getJumpPosition", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='AlphaJumpKeyItem']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
