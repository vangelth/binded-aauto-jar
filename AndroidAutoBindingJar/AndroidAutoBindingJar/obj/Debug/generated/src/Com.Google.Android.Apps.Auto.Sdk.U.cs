using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/u", DoNotGenerateAcw=true)]
	public sealed partial class U : global::Com.Google.Android.A.Agizmo {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/u", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (U); }
		}

		internal U (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()Z", "")]
		public unsafe bool A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()Z");
			try {
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_a);
			} finally {
			}
		}

		static IntPtr id_a_F;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='a' and count(parameter)=1 and parameter[1][@type='float']]"
		[Register ("a", "(F)V", "")]
		public unsafe void A (float p0)
		{
			if (id_a_F == IntPtr.Zero)
				id_a_F = JNIEnv.GetMethodID (class_ref, "a", "(F)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_F, __args);
			} finally {
			}
		}

		static IntPtr id_a_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='a' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("a", "(I)V", "")]
		public unsafe void A (int p0)
		{
			if (id_a_I == IntPtr.Zero)
				id_a_I = JNIEnv.GetMethodID (class_ref, "a", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_I, __args);
			} finally {
			}
		}

		static IntPtr id_a_Ljava_lang_CharSequence_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='a' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
		[Register ("a", "(Ljava/lang/CharSequence;)V", "")]
		public unsafe void A (global::Java.Lang.ICharSequence p0)
		{
			if (id_a_Ljava_lang_CharSequence_ == IntPtr.Zero)
				id_a_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "a", "(Ljava/lang/CharSequence;)V");
			IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Ljava_lang_CharSequence_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		public void A (string p0)
		{
			global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
			A (jls_p0);
			jls_p0?.Dispose ();
		}

		static IntPtr id_b;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='b' and count(parameter)=0]"
		[Register ("b", "()V", "")]
		public unsafe void B ()
		{
			if (id_b == IntPtr.Zero)
				id_b = JNIEnv.GetMethodID (class_ref, "b", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_b);
			} finally {
			}
		}

		static IntPtr id_b_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='b' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("b", "(I)V", "")]
		public unsafe void B (int p0)
		{
			if (id_b_I == IntPtr.Zero)
				id_b_I = JNIEnv.GetMethodID (class_ref, "b", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_b_I, __args);
			} finally {
			}
		}

		static IntPtr id_c;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='c' and count(parameter)=0]"
		[Register ("c", "()V", "")]
		public unsafe void C ()
		{
			if (id_c == IntPtr.Zero)
				id_c = JNIEnv.GetMethodID (class_ref, "c", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_c);
			} finally {
			}
		}

		static IntPtr id_d;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='d' and count(parameter)=0]"
		[Register ("d", "()V", "")]
		public unsafe void D ()
		{
			if (id_d == IntPtr.Zero)
				id_d = JNIEnv.GetMethodID (class_ref, "d", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_d);
			} finally {
			}
		}

		static IntPtr id_e;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='e' and count(parameter)=0]"
		[Register ("e", "()V", "")]
		public unsafe void E ()
		{
			if (id_e == IntPtr.Zero)
				id_e = JNIEnv.GetMethodID (class_ref, "e", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_e);
			} finally {
			}
		}

		static IntPtr id_f;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='f' and count(parameter)=0]"
		[Register ("f", "()V", "")]
		public unsafe void F ()
		{
			if (id_f == IntPtr.Zero)
				id_f = JNIEnv.GetMethodID (class_ref, "f", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_f);
			} finally {
			}
		}

		static IntPtr id_g;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='g' and count(parameter)=0]"
		[Register ("g", "()V", "")]
		public unsafe void G ()
		{
			if (id_g == IntPtr.Zero)
				id_g = JNIEnv.GetMethodID (class_ref, "g", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_g);
			} finally {
			}
		}

		static IntPtr id_h;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='h' and count(parameter)=0]"
		[Register ("h", "()V", "")]
		public unsafe void H ()
		{
			if (id_h == IntPtr.Zero)
				id_h = JNIEnv.GetMethodID (class_ref, "h", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_h);
			} finally {
			}
		}

		static IntPtr id_i;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='i' and count(parameter)=0]"
		[Register ("i", "()V", "")]
		public unsafe void I ()
		{
			if (id_i == IntPtr.Zero)
				id_i = JNIEnv.GetMethodID (class_ref, "i", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_i);
			} finally {
			}
		}

		static IntPtr id_j;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='j' and count(parameter)=0]"
		[Register ("j", "()V", "")]
		public unsafe void J ()
		{
			if (id_j == IntPtr.Zero)
				id_j = JNIEnv.GetMethodID (class_ref, "j", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_j);
			} finally {
			}
		}

		static IntPtr id_k;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='u']/method[@name='k' and count(parameter)=0]"
		[Register ("k", "()V", "")]
		public unsafe void K ()
		{
			if (id_k == IntPtr.Zero)
				id_k = JNIEnv.GetMethodID (class_ref, "k", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_k);
			} finally {
			}
		}

	}
}
