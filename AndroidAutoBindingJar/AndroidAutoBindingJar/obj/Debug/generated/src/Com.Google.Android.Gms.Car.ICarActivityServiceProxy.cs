using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy.ServiceCallbacks']"
	[Register ("com/google/android/gms/car/CarActivityServiceProxy$ServiceCallbacks", "", "Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacksInvoker")]
	public partial interface ICarActivityServiceProxyServiceCallbacks : IJavaObject {

		global::Java.Lang.Class CarActivity {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy.ServiceCallbacks']/method[@name='getCarActivity' and count(parameter)=0]"
			[Register ("getCarActivity", "()Ljava/lang/Class;", "GetGetCarActivityHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacksInvoker, AndroidAutoBindingJar")] get;
		}

		int HandledConfigChanges {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy.ServiceCallbacks']/method[@name='getHandledConfigChanges' and count(parameter)=0]"
			[Register ("getHandledConfigChanges", "()I", "GetGetHandledConfigChangesHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacksInvoker, AndroidAutoBindingJar")] get;
		}

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarActivityServiceProxy$ServiceCallbacks", DoNotGenerateAcw=true)]
	internal class ICarActivityServiceProxyServiceCallbacksInvoker : global::Java.Lang.Object, ICarActivityServiceProxyServiceCallbacks {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarActivityServiceProxy$ServiceCallbacks");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarActivityServiceProxyServiceCallbacksInvoker); }
		}

		IntPtr class_ref;

		public static ICarActivityServiceProxyServiceCallbacks GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarActivityServiceProxyServiceCallbacks> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarActivityServiceProxy.ServiceCallbacks"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarActivityServiceProxyServiceCallbacksInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getCarActivity;
#pragma warning disable 0169
		static Delegate GetGetCarActivityHandler ()
		{
			if (cb_getCarActivity == null)
				cb_getCarActivity = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarActivity);
			return cb_getCarActivity;
		}

		static IntPtr n_GetCarActivity (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CarActivity);
		}
#pragma warning restore 0169

		IntPtr id_getCarActivity;
		public unsafe global::Java.Lang.Class CarActivity {
			get {
				if (id_getCarActivity == IntPtr.Zero)
					id_getCarActivity = JNIEnv.GetMethodID (class_ref, "getCarActivity", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarActivity), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getHandledConfigChanges;
#pragma warning disable 0169
		static Delegate GetGetHandledConfigChangesHandler ()
		{
			if (cb_getHandledConfigChanges == null)
				cb_getHandledConfigChanges = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHandledConfigChanges);
			return cb_getHandledConfigChanges;
		}

		static int n_GetHandledConfigChanges (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HandledConfigChanges;
		}
#pragma warning restore 0169

		IntPtr id_getHandledConfigChanges;
		public unsafe int HandledConfigChanges {
			get {
				if (id_getHandledConfigChanges == IntPtr.Zero)
					id_getHandledConfigChanges = JNIEnv.GetMethodID (class_ref, "getHandledConfigChanges", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getHandledConfigChanges);
			}
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']"
	[Register ("com/google/android/gms/car/CarActivityServiceProxy", "", "Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker")]
	public partial interface ICarActivityServiceProxy : IJavaObject {

		global::Java.Lang.Object ActivityInstance {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='getActivityInstance' and count(parameter)=0]"
			[Register ("getActivityInstance", "()Ljava/lang/Object;", "GetGetActivityInstanceHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='dump' and count(parameter)=3 and parameter[1][@type='java.io.FileDescriptor'] and parameter[2][@type='java.io.PrintWriter'] and parameter[3][@type='java.lang.String[]']]"
		[Register ("dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V", "GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void Dump (global::Java.IO.FileDescriptor p0, global::Java.IO.PrintWriter p1, string[] p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onBind' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;", "GetOnBind_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		global::Android.OS.IBinder OnBind (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onConfigurationChanged' and count(parameter)=1 and parameter[1][@type='android.content.res.Configuration']]"
		[Register ("onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void OnConfigurationChanged (global::Android.Content.Res.Configuration p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onCreate' and count(parameter)=2 and parameter[1][@type='android.app.Service'] and parameter[2][@type='com.google.android.gms.car.CarActivityServiceProxy.ServiceCallbacks']]"
		[Register ("onCreate", "(Landroid/app/Service;Lcom/google/android/gms/car/CarActivityServiceProxy$ServiceCallbacks;)V", "GetOnCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_Handler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void OnCreate (global::Android.App.Service p0, global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onDestroy' and count(parameter)=0]"
		[Register ("onDestroy", "()V", "GetOnDestroyHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void OnDestroy ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onLowMemory' and count(parameter)=0]"
		[Register ("onLowMemory", "()V", "GetOnLowMemoryHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void OnLowMemory ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='onUnbind' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onUnbind", "(Landroid/content/Intent;)Z", "GetOnUnbind_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		bool OnUnbind (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='relinquishVideoFocus' and count(parameter)=0]"
		[Register ("relinquishVideoFocus", "()V", "GetRelinquishVideoFocusHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void RelinquishVideoFocus ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='requestVideoFocus' and count(parameter)=0]"
		[Register ("requestVideoFocus", "()V", "GetRequestVideoFocusHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void RequestVideoFocus ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityServiceProxy']/method[@name='setCrashReportingEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("setCrashReportingEnabled", "(Z)V", "GetSetCrashReportingEnabled_ZHandler:Com.Google.Android.Gms.Car.ICarActivityServiceProxyInvoker, AndroidAutoBindingJar")]
		void SetCrashReportingEnabled (bool p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarActivityServiceProxy", DoNotGenerateAcw=true)]
	internal class ICarActivityServiceProxyInvoker : global::Java.Lang.Object, ICarActivityServiceProxy {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarActivityServiceProxy");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarActivityServiceProxyInvoker); }
		}

		IntPtr class_ref;

		public static ICarActivityServiceProxy GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarActivityServiceProxy> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarActivityServiceProxy"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarActivityServiceProxyInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getActivityInstance;
#pragma warning disable 0169
		static Delegate GetGetActivityInstanceHandler ()
		{
			if (cb_getActivityInstance == null)
				cb_getActivityInstance = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetActivityInstance);
			return cb_getActivityInstance;
		}

		static IntPtr n_GetActivityInstance (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ActivityInstance);
		}
#pragma warning restore 0169

		IntPtr id_getActivityInstance;
		public unsafe global::Java.Lang.Object ActivityInstance {
			get {
				if (id_getActivityInstance == IntPtr.Zero)
					id_getActivityInstance = JNIEnv.GetMethodID (class_ref, "getActivityInstance", "()Ljava/lang/Object;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getActivityInstance), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler ()
		{
			if (cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == null)
				cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_);
			return cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		}

		static void n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, IntPtr native_p2)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.FileDescriptor p0 = global::Java.Lang.Object.GetObject<global::Java.IO.FileDescriptor> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.PrintWriter p1 = global::Java.Lang.Object.GetObject<global::Java.IO.PrintWriter> (native_p1, JniHandleOwnership.DoNotTransfer);
			string[] p2 = (string[]) JNIEnv.GetArray (native_p2, JniHandleOwnership.DoNotTransfer, typeof (string));
			__this.Dump (p0, p1, p2);
			if (p2 != null)
				JNIEnv.CopyArray (p2, native_p2);
		}
#pragma warning restore 0169

		IntPtr id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		public unsafe void Dump (global::Java.IO.FileDescriptor p0, global::Java.IO.PrintWriter p1, string[] p2)
		{
			if (id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == IntPtr.Zero)
				id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V");
			IntPtr native_p2 = JNIEnv.NewArray (p2);
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (native_p2);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_, __args);
			if (p2 != null) {
				JNIEnv.CopyArray (native_p2, p2);
				JNIEnv.DeleteLocalRef (native_p2);
			}
		}

		static Delegate cb_onBind_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnBind_Landroid_content_Intent_Handler ()
		{
			if (cb_onBind_Landroid_content_Intent_ == null)
				cb_onBind_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnBind_Landroid_content_Intent_);
			return cb_onBind_Landroid_content_Intent_;
		}

		static IntPtr n_OnBind_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.OnBind (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onBind_Landroid_content_Intent_;
		public unsafe global::Android.OS.IBinder OnBind (global::Android.Content.Intent p0)
		{
			if (id_onBind_Landroid_content_Intent_ == IntPtr.Zero)
				id_onBind_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			global::Android.OS.IBinder __ret = global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onBind_Landroid_content_Intent_, __args), JniHandleOwnership.TransferLocalRef);
			return __ret;
		}

		static Delegate cb_onConfigurationChanged_Landroid_content_res_Configuration_;
#pragma warning disable 0169
		static Delegate GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler ()
		{
			if (cb_onConfigurationChanged_Landroid_content_res_Configuration_ == null)
				cb_onConfigurationChanged_Landroid_content_res_Configuration_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnConfigurationChanged_Landroid_content_res_Configuration_);
			return cb_onConfigurationChanged_Landroid_content_res_Configuration_;
		}

		static void n_OnConfigurationChanged_Landroid_content_res_Configuration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Res.Configuration p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Res.Configuration> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnConfigurationChanged (p0);
		}
#pragma warning restore 0169

		IntPtr id_onConfigurationChanged_Landroid_content_res_Configuration_;
		public unsafe void OnConfigurationChanged (global::Android.Content.Res.Configuration p0)
		{
			if (id_onConfigurationChanged_Landroid_content_res_Configuration_ == IntPtr.Zero)
				id_onConfigurationChanged_Landroid_content_res_Configuration_ = JNIEnv.GetMethodID (class_ref, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConfigurationChanged_Landroid_content_res_Configuration_, __args);
		}

		static Delegate cb_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_;
#pragma warning disable 0169
		static Delegate GetOnCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_Handler ()
		{
			if (cb_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_ == null)
				cb_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_);
			return cb_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_;
		}

		static void n_OnCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.App.Service p0 = global::Java.Lang.Object.GetObject<global::Android.App.Service> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks p1 = (global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.OnCreate (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_;
		public unsafe void OnCreate (global::Android.App.Service p0, global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks p1)
		{
			if (id_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_ == IntPtr.Zero)
				id_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_ = JNIEnv.GetMethodID (class_ref, "onCreate", "(Landroid/app/Service;Lcom/google/android/gms/car/CarActivityServiceProxy$ServiceCallbacks;)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCreate_Landroid_app_Service_Lcom_google_android_gms_car_CarActivityServiceProxy_ServiceCallbacks_, __args);
		}

		static Delegate cb_onDestroy;
#pragma warning disable 0169
		static Delegate GetOnDestroyHandler ()
		{
			if (cb_onDestroy == null)
				cb_onDestroy = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDestroy);
			return cb_onDestroy;
		}

		static void n_OnDestroy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDestroy ();
		}
#pragma warning restore 0169

		IntPtr id_onDestroy;
		public unsafe void OnDestroy ()
		{
			if (id_onDestroy == IntPtr.Zero)
				id_onDestroy = JNIEnv.GetMethodID (class_ref, "onDestroy", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDestroy);
		}

		static Delegate cb_onLowMemory;
#pragma warning disable 0169
		static Delegate GetOnLowMemoryHandler ()
		{
			if (cb_onLowMemory == null)
				cb_onLowMemory = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnLowMemory);
			return cb_onLowMemory;
		}

		static void n_OnLowMemory (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnLowMemory ();
		}
#pragma warning restore 0169

		IntPtr id_onLowMemory;
		public unsafe void OnLowMemory ()
		{
			if (id_onLowMemory == IntPtr.Zero)
				id_onLowMemory = JNIEnv.GetMethodID (class_ref, "onLowMemory", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onLowMemory);
		}

		static Delegate cb_onUnbind_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnUnbind_Landroid_content_Intent_Handler ()
		{
			if (cb_onUnbind_Landroid_content_Intent_ == null)
				cb_onUnbind_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_OnUnbind_Landroid_content_Intent_);
			return cb_onUnbind_Landroid_content_Intent_;
		}

		static bool n_OnUnbind_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnUnbind (p0);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onUnbind_Landroid_content_Intent_;
		public unsafe bool OnUnbind (global::Android.Content.Intent p0)
		{
			if (id_onUnbind_Landroid_content_Intent_ == IntPtr.Zero)
				id_onUnbind_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onUnbind", "(Landroid/content/Intent;)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onUnbind_Landroid_content_Intent_, __args);
			return __ret;
		}

		static Delegate cb_relinquishVideoFocus;
#pragma warning disable 0169
		static Delegate GetRelinquishVideoFocusHandler ()
		{
			if (cb_relinquishVideoFocus == null)
				cb_relinquishVideoFocus = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_RelinquishVideoFocus);
			return cb_relinquishVideoFocus;
		}

		static void n_RelinquishVideoFocus (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RelinquishVideoFocus ();
		}
#pragma warning restore 0169

		IntPtr id_relinquishVideoFocus;
		public unsafe void RelinquishVideoFocus ()
		{
			if (id_relinquishVideoFocus == IntPtr.Zero)
				id_relinquishVideoFocus = JNIEnv.GetMethodID (class_ref, "relinquishVideoFocus", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_relinquishVideoFocus);
		}

		static Delegate cb_requestVideoFocus;
#pragma warning disable 0169
		static Delegate GetRequestVideoFocusHandler ()
		{
			if (cb_requestVideoFocus == null)
				cb_requestVideoFocus = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_RequestVideoFocus);
			return cb_requestVideoFocus;
		}

		static void n_RequestVideoFocus (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RequestVideoFocus ();
		}
#pragma warning restore 0169

		IntPtr id_requestVideoFocus;
		public unsafe void RequestVideoFocus ()
		{
			if (id_requestVideoFocus == IntPtr.Zero)
				id_requestVideoFocus = JNIEnv.GetMethodID (class_ref, "requestVideoFocus", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_requestVideoFocus);
		}

		static Delegate cb_setCrashReportingEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetCrashReportingEnabled_ZHandler ()
		{
			if (cb_setCrashReportingEnabled_Z == null)
				cb_setCrashReportingEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetCrashReportingEnabled_Z);
			return cb_setCrashReportingEnabled_Z;
		}

		static void n_SetCrashReportingEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetCrashReportingEnabled (p0);
		}
#pragma warning restore 0169

		IntPtr id_setCrashReportingEnabled_Z;
		public unsafe void SetCrashReportingEnabled (bool p0)
		{
			if (id_setCrashReportingEnabled_Z == IntPtr.Zero)
				id_setCrashReportingEnabled_Z = JNIEnv.GetMethodID (class_ref, "setCrashReportingEnabled", "(Z)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setCrashReportingEnabled_Z, __args);
		}

	}

}
