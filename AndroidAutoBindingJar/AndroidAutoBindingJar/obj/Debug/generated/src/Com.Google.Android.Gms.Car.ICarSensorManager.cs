using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager.CarSensorEventListener']"
	[Register ("com/google/android/gms/car/CarSensorManager$CarSensorEventListener", "", "Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListenerInvoker")]
	public partial interface ICarSensorManagerCarSensorEventListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager.CarSensorEventListener']/method[@name='onSensorChanged' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='long'] and parameter[3][@type='float[]'] and parameter[4][@type='byte[]']]"
		[Register ("onSensorChanged", "(IJ[F[B)V", "GetOnSensorChanged_IJarrayFarrayBHandler:Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListenerInvoker, AndroidAutoBindingJar")]
		void OnSensorChanged (int p0, long p1, float[] p2, byte[] p3);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarSensorManager$CarSensorEventListener", DoNotGenerateAcw=true)]
	internal class ICarSensorManagerCarSensorEventListenerInvoker : global::Java.Lang.Object, ICarSensorManagerCarSensorEventListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarSensorManager$CarSensorEventListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarSensorManagerCarSensorEventListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarSensorManagerCarSensorEventListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarSensorManagerCarSensorEventListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarSensorManager.CarSensorEventListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarSensorManagerCarSensorEventListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onSensorChanged_IJarrayFarrayB;
#pragma warning disable 0169
		static Delegate GetOnSensorChanged_IJarrayFarrayBHandler ()
		{
			if (cb_onSensorChanged_IJarrayFarrayB == null)
				cb_onSensorChanged_IJarrayFarrayB = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, long, IntPtr, IntPtr>) n_OnSensorChanged_IJarrayFarrayB);
			return cb_onSensorChanged_IJarrayFarrayB;
		}

		static void n_OnSensorChanged_IJarrayFarrayB (IntPtr jnienv, IntPtr native__this, int p0, long p1, IntPtr native_p2, IntPtr native_p3)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			float[] p2 = (float[]) JNIEnv.GetArray (native_p2, JniHandleOwnership.DoNotTransfer, typeof (float));
			byte[] p3 = (byte[]) JNIEnv.GetArray (native_p3, JniHandleOwnership.DoNotTransfer, typeof (byte));
			__this.OnSensorChanged (p0, p1, p2, p3);
			if (p2 != null)
				JNIEnv.CopyArray (p2, native_p2);
			if (p3 != null)
				JNIEnv.CopyArray (p3, native_p3);
		}
#pragma warning restore 0169

		IntPtr id_onSensorChanged_IJarrayFarrayB;
		public unsafe void OnSensorChanged (int p0, long p1, float[] p2, byte[] p3)
		{
			if (id_onSensorChanged_IJarrayFarrayB == IntPtr.Zero)
				id_onSensorChanged_IJarrayFarrayB = JNIEnv.GetMethodID (class_ref, "onSensorChanged", "(IJ[F[B)V");
			IntPtr native_p2 = JNIEnv.NewArray (p2);
			IntPtr native_p3 = JNIEnv.NewArray (p3);
			JValue* __args = stackalloc JValue [4];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (native_p2);
			__args [3] = new JValue (native_p3);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSensorChanged_IJarrayFarrayB, __args);
			if (p2 != null) {
				JNIEnv.CopyArray (native_p2, p2);
				JNIEnv.DeleteLocalRef (native_p2);
			}
			if (p3 != null) {
				JNIEnv.CopyArray (native_p3, p3);
				JNIEnv.DeleteLocalRef (native_p3);
			}
		}

	}

	public partial class CarSensorManagerCarSensorEventEventArgs : global::System.EventArgs {

		public CarSensorManagerCarSensorEventEventArgs (int p0, long p1, float[] p2, byte[] p3)
		{
			this.p0 = p0;
			this.p1 = p1;
			this.p2 = p2;
			this.p3 = p3;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}

		long p1;
		public long P1 {
			get { return p1; }
		}

		float[] p2;
		public float[] P2 {
			get { return p2; }
		}

		byte[] p3;
		public byte[] P3 {
			get { return p3; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarSensorManager_CarSensorEventListenerImplementor")]
	internal sealed partial class ICarSensorManagerCarSensorEventListenerImplementor : global::Java.Lang.Object, ICarSensorManagerCarSensorEventListener {

		object sender;

		public ICarSensorManagerCarSensorEventListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarSensorManager_CarSensorEventListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<CarSensorManagerCarSensorEventEventArgs> Handler;
#pragma warning restore 0649

		public void OnSensorChanged (int p0, long p1, float[] p2, byte[] p3)
		{
			var __h = Handler;
			if (__h != null)
				__h (sender, new CarSensorManagerCarSensorEventEventArgs (p0, p1, p2, p3));
		}

		internal static bool __IsEmpty (ICarSensorManagerCarSensorEventListenerImplementor value)
		{
			return value.Handler == null;
		}
	}


	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/CarSensorManager$RawEventData", DoNotGenerateAcw=true)]
	public partial class CarSensorManagerRawEventData : global::Java.Lang.Object {


		static IntPtr byteValues_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']/field[@name='byteValues']"
		[Register ("byteValues")]
		public IList<byte> ByteValues {
			get {
				if (byteValues_jfieldId == IntPtr.Zero)
					byteValues_jfieldId = JNIEnv.GetFieldID (class_ref, "byteValues", "[B");
				return global::Android.Runtime.JavaArray<byte>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, byteValues_jfieldId), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (byteValues_jfieldId == IntPtr.Zero)
					byteValues_jfieldId = JNIEnv.GetFieldID (class_ref, "byteValues", "[B");
				IntPtr native_value = global::Android.Runtime.JavaArray<byte>.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, byteValues_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr floatValues_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']/field[@name='floatValues']"
		[Register ("floatValues")]
		public IList<float> FloatValues {
			get {
				if (floatValues_jfieldId == IntPtr.Zero)
					floatValues_jfieldId = JNIEnv.GetFieldID (class_ref, "floatValues", "[F");
				return global::Android.Runtime.JavaArray<float>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, floatValues_jfieldId), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (floatValues_jfieldId == IntPtr.Zero)
					floatValues_jfieldId = JNIEnv.GetFieldID (class_ref, "floatValues", "[F");
				IntPtr native_value = global::Android.Runtime.JavaArray<float>.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, floatValues_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr sensorType_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']/field[@name='sensorType']"
		[Register ("sensorType")]
		public int SensorType {
			get {
				if (sensorType_jfieldId == IntPtr.Zero)
					sensorType_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorType", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, sensorType_jfieldId);
			}
			set {
				if (sensorType_jfieldId == IntPtr.Zero)
					sensorType_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorType", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, sensorType_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr timeStamp_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']/field[@name='timeStamp']"
		[Register ("timeStamp")]
		public long TimeStamp {
			get {
				if (timeStamp_jfieldId == IntPtr.Zero)
					timeStamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timeStamp", "J");
				return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timeStamp_jfieldId);
			}
			set {
				if (timeStamp_jfieldId == IntPtr.Zero)
					timeStamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timeStamp", "J");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timeStamp_jfieldId, value);
				} finally {
				}
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/CarSensorManager$RawEventData", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorManagerRawEventData); }
		}

		protected CarSensorManagerRawEventData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_IJarrayFarrayB;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarSensorManager.RawEventData']/constructor[@name='CarSensorManager.RawEventData' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='long'] and parameter[3][@type='float[]'] and parameter[4][@type='byte[]']]"
		[Register (".ctor", "(IJ[F[B)V", "")]
		public unsafe CarSensorManagerRawEventData (int p0, long p1, float[] p2, byte[] p3)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			IntPtr native_p2 = JNIEnv.NewArray (p2);
			IntPtr native_p3 = JNIEnv.NewArray (p3);
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (native_p2);
				__args [3] = new JValue (native_p3);
				if (((object) this).GetType () != typeof (CarSensorManagerRawEventData)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(IJ[F[B)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(IJ[F[B)V", __args);
					return;
				}

				if (id_ctor_IJarrayFarrayB == IntPtr.Zero)
					id_ctor_IJarrayFarrayB = JNIEnv.GetMethodID (class_ref, "<init>", "(IJ[F[B)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_IJarrayFarrayB, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_IJarrayFarrayB, __args);
			} finally {
				if (p2 != null) {
					JNIEnv.CopyArray (native_p2, p2);
					JNIEnv.DeleteLocalRef (native_p2);
				}
				if (p3 != null) {
					JNIEnv.CopyArray (native_p3, p3);
					JNIEnv.DeleteLocalRef (native_p3);
				}
			}
		}

	}

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']"
	[Register ("com/google/android/gms/car/CarSensorManager", "", "Com.Google.Android.Gms.Car.ICarSensorManagerInvoker")]
	public partial interface ICarSensorManager : IJavaObject {

		int LocationCharacterization {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='getLocationCharacterization' and count(parameter)=0]"
			[Register ("getLocationCharacterization", "()I", "GetGetLocationCharacterizationHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='getEvConnectorTypes' and count(parameter)=0]"
		[Register ("getEvConnectorTypes", "()[I", "GetGetEvConnectorTypesHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		int[] GetEvConnectorTypes ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='getFuelTypes' and count(parameter)=0]"
		[Register ("getFuelTypes", "()[I", "GetGetFuelTypesHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		int[] GetFuelTypes ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='getLatestSensorEvent' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLatestSensorEvent", "(I)Lcom/google/android/gms/car/CarSensorManager$RawEventData;", "GetGetLatestSensorEvent_IHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.CarSensorManagerRawEventData GetLatestSensorEvent (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='getSupportedSensors' and count(parameter)=0]"
		[Register ("getSupportedSensors", "()[I", "GetGetSupportedSensorsHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		int[] GetSupportedSensors ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='isSensorSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isSensorSupported", "(I)Z", "GetIsSensorSupported_IHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		bool IsSensorSupported (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='registerListener' and count(parameter)=3 and parameter[1][@type='com.google.android.gms.car.CarSensorManager.CarSensorEventListener'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("registerListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;II)Z", "GetRegisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_IIHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		bool RegisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='unregisterListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarSensorManager.CarSensorEventListener']]"
		[Register ("unregisterListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;)V", "GetUnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_Handler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarSensorManager']/method[@name='unregisterListener' and count(parameter)=2 and parameter[1][@type='com.google.android.gms.car.CarSensorManager.CarSensorEventListener'] and parameter[2][@type='int']]"
		[Register ("unregisterListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;I)V", "GetUnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_IHandler:Com.Google.Android.Gms.Car.ICarSensorManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0, int p1);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarSensorManager", DoNotGenerateAcw=true)]
	internal class ICarSensorManagerInvoker : global::Java.Lang.Object, ICarSensorManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarSensorManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarSensorManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarSensorManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarSensorManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarSensorManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarSensorManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getLocationCharacterization;
#pragma warning disable 0169
		static Delegate GetGetLocationCharacterizationHandler ()
		{
			if (cb_getLocationCharacterization == null)
				cb_getLocationCharacterization = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetLocationCharacterization);
			return cb_getLocationCharacterization;
		}

		static int n_GetLocationCharacterization (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.LocationCharacterization;
		}
#pragma warning restore 0169

		IntPtr id_getLocationCharacterization;
		public unsafe int LocationCharacterization {
			get {
				if (id_getLocationCharacterization == IntPtr.Zero)
					id_getLocationCharacterization = JNIEnv.GetMethodID (class_ref, "getLocationCharacterization", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getLocationCharacterization);
			}
		}

		static Delegate cb_getEvConnectorTypes;
#pragma warning disable 0169
		static Delegate GetGetEvConnectorTypesHandler ()
		{
			if (cb_getEvConnectorTypes == null)
				cb_getEvConnectorTypes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEvConnectorTypes);
			return cb_getEvConnectorTypes;
		}

		static IntPtr n_GetEvConnectorTypes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetEvConnectorTypes ());
		}
#pragma warning restore 0169

		IntPtr id_getEvConnectorTypes;
		public unsafe int[] GetEvConnectorTypes ()
		{
			if (id_getEvConnectorTypes == IntPtr.Zero)
				id_getEvConnectorTypes = JNIEnv.GetMethodID (class_ref, "getEvConnectorTypes", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getEvConnectorTypes), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

		static Delegate cb_getFuelTypes;
#pragma warning disable 0169
		static Delegate GetGetFuelTypesHandler ()
		{
			if (cb_getFuelTypes == null)
				cb_getFuelTypes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFuelTypes);
			return cb_getFuelTypes;
		}

		static IntPtr n_GetFuelTypes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetFuelTypes ());
		}
#pragma warning restore 0169

		IntPtr id_getFuelTypes;
		public unsafe int[] GetFuelTypes ()
		{
			if (id_getFuelTypes == IntPtr.Zero)
				id_getFuelTypes = JNIEnv.GetMethodID (class_ref, "getFuelTypes", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getFuelTypes), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

		static Delegate cb_getLatestSensorEvent_I;
#pragma warning disable 0169
		static Delegate GetGetLatestSensorEvent_IHandler ()
		{
			if (cb_getLatestSensorEvent_I == null)
				cb_getLatestSensorEvent_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetLatestSensorEvent_I);
			return cb_getLatestSensorEvent_I;
		}

		static IntPtr n_GetLatestSensorEvent_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLatestSensorEvent (p0));
		}
#pragma warning restore 0169

		IntPtr id_getLatestSensorEvent_I;
		public unsafe global::Com.Google.Android.Gms.Car.CarSensorManagerRawEventData GetLatestSensorEvent (int p0)
		{
			if (id_getLatestSensorEvent_I == IntPtr.Zero)
				id_getLatestSensorEvent_I = JNIEnv.GetMethodID (class_ref, "getLatestSensorEvent", "(I)Lcom/google/android/gms/car/CarSensorManager$RawEventData;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarSensorManagerRawEventData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLatestSensorEvent_I, __args), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_getSupportedSensors;
#pragma warning disable 0169
		static Delegate GetGetSupportedSensorsHandler ()
		{
			if (cb_getSupportedSensors == null)
				cb_getSupportedSensors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSupportedSensors);
			return cb_getSupportedSensors;
		}

		static IntPtr n_GetSupportedSensors (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetSupportedSensors ());
		}
#pragma warning restore 0169

		IntPtr id_getSupportedSensors;
		public unsafe int[] GetSupportedSensors ()
		{
			if (id_getSupportedSensors == IntPtr.Zero)
				id_getSupportedSensors = JNIEnv.GetMethodID (class_ref, "getSupportedSensors", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSupportedSensors), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

		static Delegate cb_isSensorSupported_I;
#pragma warning disable 0169
		static Delegate GetIsSensorSupported_IHandler ()
		{
			if (cb_isSensorSupported_I == null)
				cb_isSensorSupported_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_IsSensorSupported_I);
			return cb_isSensorSupported_I;
		}

		static bool n_IsSensorSupported_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsSensorSupported (p0);
		}
#pragma warning restore 0169

		IntPtr id_isSensorSupported_I;
		public unsafe bool IsSensorSupported (int p0)
		{
			if (id_isSensorSupported_I == IntPtr.Zero)
				id_isSensorSupported_I = JNIEnv.GetMethodID (class_ref, "isSensorSupported", "(I)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isSensorSupported_I, __args);
		}

		static Delegate cb_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II;
#pragma warning disable 0169
		static Delegate GetRegisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_IIHandler ()
		{
			if (cb_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II == null)
				cb_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, bool>) n_RegisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II);
			return cb_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II;
		}

		static bool n_RegisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0 = (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.RegisterListener (p0, p1, p2);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II;
		public unsafe bool RegisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0, int p1, int p2)
		{
			if (id_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II == IntPtr.Zero)
				id_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II = JNIEnv.GetMethodID (class_ref, "registerListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;II)Z");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_registerListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_II, __args);
			return __ret;
		}

		static Delegate cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_;
#pragma warning disable 0169
		static Delegate GetUnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_Handler ()
		{
			if (cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_ == null)
				cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_UnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_);
			return cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_;
		}

		static void n_UnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0 = (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_;
		public unsafe void UnregisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0)
		{
			if (id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_ == IntPtr.Zero)
				id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_ = JNIEnv.GetMethodID (class_ref, "unregisterListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_, __args);
		}

		static Delegate cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I;
#pragma warning disable 0169
		static Delegate GetUnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_IHandler ()
		{
			if (cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I == null)
				cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_UnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I);
			return cb_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I;
		}

		static void n_UnregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Com.Google.Android.Gms.Car.ICarSensorManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0 = (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterListener (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I;
		public unsafe void UnregisterListener (global::Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListener p0, int p1)
		{
			if (id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I == IntPtr.Zero)
				id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I = JNIEnv.GetMethodID (class_ref, "unregisterListener", "(Lcom/google/android/gms/car/CarSensorManager$CarSensorEventListener;I)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterListener_Lcom_google_android_gms_car_CarSensorManager_CarSensorEventListener_I, __args);
		}

	}

}
