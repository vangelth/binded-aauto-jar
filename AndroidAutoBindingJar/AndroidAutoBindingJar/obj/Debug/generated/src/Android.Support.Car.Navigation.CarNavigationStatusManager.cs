using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Navigation {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']"
	[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager", DoNotGenerateAcw=true)]
	public abstract partial class CarNavigationStatusManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='DISTANCE_FEET']"
		[Register ("DISTANCE_FEET")]
		public const int DistanceFeet = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='DISTANCE_KILOMETERS']"
		[Register ("DISTANCE_KILOMETERS")]
		public const int DistanceKilometers = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='DISTANCE_METERS']"
		[Register ("DISTANCE_METERS")]
		public const int DistanceMeters = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='DISTANCE_MILES']"
		[Register ("DISTANCE_MILES")]
		public const int DistanceMiles = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='DISTANCE_YARDS']"
		[Register ("DISTANCE_YARDS")]
		public const int DistanceYards = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='EVENT_TYPE_NEXT_MANEUVER_COUNTDOWN']"
		[Register ("EVENT_TYPE_NEXT_MANEUVER_COUNTDOWN")]
		public const int EventTypeNextManeuverCountdown = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='EVENT_TYPE_NEXT_MANEUVER_INFO']"
		[Register ("EVENT_TYPE_NEXT_MANEUVER_INFO")]
		public const int EventTypeNextManeuverInfo = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='EVENT_TYPE_VENDOR_FIRST']"
		[Register ("EVENT_TYPE_VENDOR_FIRST")]
		public const int EventTypeVendorFirst = (int) 1024;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='STATUS_ACTIVE']"
		[Register ("STATUS_ACTIVE")]
		public const int StatusActive = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='STATUS_INACTIVE']"
		[Register ("STATUS_INACTIVE")]
		public const int StatusInactive = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='STATUS_UNAVAILABLE']"
		[Register ("STATUS_UNAVAILABLE")]
		public const int StatusUnavailable = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_DEPART']"
		[Register ("TURN_DEPART")]
		public const int TurnDepart = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_DESTINATION']"
		[Register ("TURN_DESTINATION")]
		public const int TurnDestination = (int) 19;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_FERRY_BOAT']"
		[Register ("TURN_FERRY_BOAT")]
		public const int TurnFerryBoat = (int) 16;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_FERRY_TRAIN']"
		[Register ("TURN_FERRY_TRAIN")]
		public const int TurnFerryTrain = (int) 17;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_FORK']"
		[Register ("TURN_FORK")]
		public const int TurnFork = (int) 9;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_MERGE']"
		[Register ("TURN_MERGE")]
		public const int TurnMerge = (int) 10;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_NAME_CHANGE']"
		[Register ("TURN_NAME_CHANGE")]
		public const int TurnNameChange = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_OFF_RAMP']"
		[Register ("TURN_OFF_RAMP")]
		public const int TurnOffRamp = (int) 8;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_ON_RAMP']"
		[Register ("TURN_ON_RAMP")]
		public const int TurnOnRamp = (int) 7;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_ROUNDABOUT_ENTER']"
		[Register ("TURN_ROUNDABOUT_ENTER")]
		public const int TurnRoundaboutEnter = (int) 11;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_ROUNDABOUT_ENTER_AND_EXIT']"
		[Register ("TURN_ROUNDABOUT_ENTER_AND_EXIT")]
		public const int TurnRoundaboutEnterAndExit = (int) 13;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_ROUNDABOUT_EXIT']"
		[Register ("TURN_ROUNDABOUT_EXIT")]
		public const int TurnRoundaboutExit = (int) 12;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_SHARP_TURN']"
		[Register ("TURN_SHARP_TURN")]
		public const int TurnSharpTurn = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_SIDE_LEFT']"
		[Register ("TURN_SIDE_LEFT")]
		public const int TurnSideLeft = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_SIDE_RIGHT']"
		[Register ("TURN_SIDE_RIGHT")]
		public const int TurnSideRight = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_SIDE_UNSPECIFIED']"
		[Register ("TURN_SIDE_UNSPECIFIED")]
		public const int TurnSideUnspecified = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_SLIGHT_TURN']"
		[Register ("TURN_SLIGHT_TURN")]
		public const int TurnSlightTurn = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_STRAIGHT']"
		[Register ("TURN_STRAIGHT")]
		public const int TurnStraight = (int) 14;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_TURN']"
		[Register ("TURN_TURN")]
		public const int TurnTurn = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_UNKNOWN']"
		[Register ("TURN_UNKNOWN")]
		public const int TurnUnknown = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/field[@name='TURN_U_TURN']"
		[Register ("TURN_U_TURN")]
		public const int TurnUTurn = (int) 6;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.CarNavigationCallback']"
		[Register ("android/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback", "", "Android.Support.Car.Navigation.CarNavigationStatusManager/ICarNavigationCallbackInvoker")]
		public partial interface ICarNavigationCallback : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.CarNavigationCallback']/method[@name='onInstrumentClusterStarted' and count(parameter)=2 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager'] and parameter[2][@type='android.support.car.navigation.CarNavigationInstrumentCluster']]"
			[Register ("onInstrumentClusterStarted", "(Landroid/support/car/navigation/CarNavigationStatusManager;Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V", "GetOnInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_Handler:Android.Support.Car.Navigation.CarNavigationStatusManager/ICarNavigationCallbackInvoker, AndroidAutoBindingJar")]
			void OnInstrumentClusterStarted (global::Android.Support.Car.Navigation.CarNavigationStatusManager p0, global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster p1);

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.CarNavigationCallback']/method[@name='onInstrumentClusterStopped' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager']]"
			[Register ("onInstrumentClusterStopped", "(Landroid/support/car/navigation/CarNavigationStatusManager;)V", "GetOnInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_Handler:Android.Support.Car.Navigation.CarNavigationStatusManager/ICarNavigationCallbackInvoker, AndroidAutoBindingJar")]
			void OnInstrumentClusterStopped (global::Android.Support.Car.Navigation.CarNavigationStatusManager p0);

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback", DoNotGenerateAcw=true)]
		internal class ICarNavigationCallbackInvoker : global::Java.Lang.Object, ICarNavigationCallback {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ICarNavigationCallbackInvoker); }
			}

			IntPtr class_ref;

			public static ICarNavigationCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ICarNavigationCallback> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationStatusManager.CarNavigationCallback"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ICarNavigationCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_;
#pragma warning disable 0169
			static Delegate GetOnInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_Handler ()
			{
				if (cb_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ == null)
					cb_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_);
				return cb_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_;
			}

			static void n_OnInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.Navigation.CarNavigationStatusManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster p1 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (native_p1, JniHandleOwnership.DoNotTransfer);
				__this.OnInstrumentClusterStarted (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_;
			public unsafe void OnInstrumentClusterStarted (global::Android.Support.Car.Navigation.CarNavigationStatusManager p0, global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster p1)
			{
				if (id_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ == IntPtr.Zero)
					id_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ = JNIEnv.GetMethodID (class_ref, "onInstrumentClusterStarted", "(Landroid/support/car/navigation/CarNavigationStatusManager;Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onInstrumentClusterStarted_Landroid_support_car_navigation_CarNavigationStatusManager_Landroid_support_car_navigation_CarNavigationInstrumentCluster_, __args);
			}

			static Delegate cb_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_;
#pragma warning disable 0169
			static Delegate GetOnInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_Handler ()
			{
				if (cb_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_ == null)
					cb_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_);
				return cb_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_;
			}

			static void n_OnInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.Navigation.CarNavigationStatusManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnInstrumentClusterStopped (p0);
			}
#pragma warning restore 0169

			IntPtr id_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_;
			public unsafe void OnInstrumentClusterStopped (global::Android.Support.Car.Navigation.CarNavigationStatusManager p0)
			{
				if (id_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_ == IntPtr.Zero)
					id_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_ = JNIEnv.GetMethodID (class_ref, "onInstrumentClusterStopped", "(Landroid/support/car/navigation/CarNavigationStatusManager;)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onInstrumentClusterStopped_Landroid_support_car_navigation_CarNavigationStatusManager_, __args);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.DistanceUnit']"
		[Register ("android/support/car/navigation/CarNavigationStatusManager$DistanceUnit", "", "Android.Support.Car.Navigation.CarNavigationStatusManager/IDistanceUnitInvoker")]
		public partial interface IDistanceUnit : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager$DistanceUnit", DoNotGenerateAcw=true)]
		internal class IDistanceUnitInvoker : global::Java.Lang.Object, IDistanceUnit {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager$DistanceUnit");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IDistanceUnitInvoker); }
			}

			IntPtr class_ref;

			public static IDistanceUnit GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IDistanceUnit> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationStatusManager.DistanceUnit"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IDistanceUnitInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IDistanceUnit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.Status']"
		[Register ("android/support/car/navigation/CarNavigationStatusManager$Status", "", "Android.Support.Car.Navigation.CarNavigationStatusManager/IStatusInvoker")]
		public partial interface IStatus : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager$Status", DoNotGenerateAcw=true)]
		internal class IStatusInvoker : global::Java.Lang.Object, IStatus {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager$Status");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IStatusInvoker); }
			}

			IntPtr class_ref;

			public static IStatus GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IStatus> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationStatusManager.Status"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IStatusInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.IStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.TurnEvent']"
		[Register ("android/support/car/navigation/CarNavigationStatusManager$TurnEvent", "", "Android.Support.Car.Navigation.CarNavigationStatusManager/ITurnEventInvoker")]
		public partial interface ITurnEvent : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager$TurnEvent", DoNotGenerateAcw=true)]
		internal class ITurnEventInvoker : global::Java.Lang.Object, ITurnEvent {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager$TurnEvent");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITurnEventInvoker); }
			}

			IntPtr class_ref;

			public static ITurnEvent GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ITurnEvent> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationStatusManager.TurnEvent"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITurnEventInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationStatusManager.TurnSide']"
		[Register ("android/support/car/navigation/CarNavigationStatusManager$TurnSide", "", "Android.Support.Car.Navigation.CarNavigationStatusManager/ITurnSideInvoker")]
		public partial interface ITurnSide : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager$TurnSide", DoNotGenerateAcw=true)]
		internal class ITurnSideInvoker : global::Java.Lang.Object, ITurnSide {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager$TurnSide");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITurnSideInvoker); }
			}

			IntPtr class_ref;

			public static ITurnSide GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ITurnSide> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationStatusManager.TurnSide"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITurnSideInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ITurnSide> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarNavigationStatusManager); }
		}

		protected CarNavigationStatusManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/constructor[@name='CarNavigationStatusManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarNavigationStatusManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarNavigationStatusManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
#pragma warning disable 0169
		static Delegate GetAddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_Handler ()
		{
			if (cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ == null)
				cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_);
			return cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
		}

		static void n_AddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0 = (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddListener (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager.CarNavigationCallback']]"
		[Register ("addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V", "GetAddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_Handler")]
		public abstract void AddListener (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0);

		static Delegate cb_removeListener;
#pragma warning disable 0169
		static Delegate GetRemoveListenerHandler ()
		{
			if (cb_removeListener == null)
				cb_removeListener = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_RemoveListener);
			return cb_removeListener;
		}

		static void n_RemoveListener (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='removeListener' and count(parameter)=0]"
		[Register ("removeListener", "()V", "GetRemoveListenerHandler")]
		public abstract void RemoveListener ();

		static Delegate cb_sendEvent_ILandroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetSendEvent_ILandroid_os_Bundle_Handler ()
		{
			if (cb_sendEvent_ILandroid_os_Bundle_ == null)
				cb_sendEvent_ILandroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_SendEvent_ILandroid_os_Bundle_);
			return cb_sendEvent_ILandroid_os_Bundle_;
		}

		static void n_SendEvent_ILandroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p1 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SendEvent (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendEvent' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Bundle']]"
		[Register ("sendEvent", "(ILandroid/os/Bundle;)V", "GetSendEvent_ILandroid_os_Bundle_Handler")]
		public abstract void SendEvent (int p0, global::Android.OS.Bundle p1);

		static Delegate cb_sendNavigationStatus_I;
#pragma warning disable 0169
		static Delegate GetSendNavigationStatus_IHandler ()
		{
			if (cb_sendNavigationStatus_I == null)
				cb_sendNavigationStatus_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SendNavigationStatus_I);
			return cb_sendNavigationStatus_I;
		}

		static void n_SendNavigationStatus_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationStatus (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("sendNavigationStatus", "(I)V", "GetSendNavigationStatus_IHandler")]
		public abstract void SendNavigationStatus (int p0);

		static Delegate cb_sendNavigationTurnDistanceEvent_IIII;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnDistanceEvent_IIIIHandler ()
		{
			if (cb_sendNavigationTurnDistanceEvent_IIII == null)
				cb_sendNavigationTurnDistanceEvent_IIII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int, int>) n_SendNavigationTurnDistanceEvent_IIII);
			return cb_sendNavigationTurnDistanceEvent_IIII;
		}

		static void n_SendNavigationTurnDistanceEvent_IIII (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, int p3)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnDistanceEvent (p0, p1, p2, p3);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnDistanceEvent' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("sendNavigationTurnDistanceEvent", "(IIII)V", "GetSendNavigationTurnDistanceEvent_IIIIHandler")]
		public abstract void SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3);

		static Delegate cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_IHandler ()
		{
			if (cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I == null)
				cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr, int, int, IntPtr, int>) n_SendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I);
			return cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
		}

		static void n_SendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, int p2, int p3, IntPtr native_p4, int p5)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p1 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p1, JniHandleOwnership.DoNotTransfer);
			global::Android.Graphics.Bitmap p4 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p4, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnEvent (p0, p1, p2, p3, p4, p5);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnEvent' and count(parameter)=6 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='android.graphics.Bitmap'] and parameter[6][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_IHandler")]
		public abstract void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, global::Android.Graphics.Bitmap p4, int p5);

		public void SendNavigationTurnEvent (int p0, string p1, int p2, int p3, global::Android.Graphics.Bitmap p4, int p5)
		{
			global::Java.Lang.String jls_p1 = p1 == null ? null : new global::Java.Lang.String (p1);
			SendNavigationTurnEvent (p0, jls_p1, p2, p3, p4, p5);
			jls_p1?.Dispose ();
		}

		static Delegate cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IIIHandler ()
		{
			if (cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III == null)
				cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr, int, int, int>) n_SendNavigationTurnEvent_ILjava_lang_CharSequence_III);
			return cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
		}

		static void n_SendNavigationTurnEvent_ILjava_lang_CharSequence_III (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, int p2, int p3, int p4)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p1 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnEvent (p0, p1, p2, p3, p4);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IIIHandler")]
		public abstract void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, int p4);

		public void SendNavigationTurnEvent (int p0, string p1, int p2, int p3, int p4)
		{
			global::Java.Lang.String jls_p1 = p1 == null ? null : new global::Java.Lang.String (p1);
			SendNavigationTurnEvent (p0, jls_p1, p2, p3, p4);
			jls_p1?.Dispose ();
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManager", DoNotGenerateAcw=true)]
	internal partial class CarNavigationStatusManagerInvoker : CarNavigationStatusManager {

		public CarNavigationStatusManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarNavigationStatusManagerInvoker); }
		}

		static IntPtr id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager.CarNavigationCallback']]"
		[Register ("addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V", "GetAddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_Handler")]
		public override unsafe void AddListener (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0)
		{
			if (id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ == IntPtr.Zero)
				id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_, __args);
			} finally {
			}
		}

		static IntPtr id_removeListener;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='removeListener' and count(parameter)=0]"
		[Register ("removeListener", "()V", "GetRemoveListenerHandler")]
		public override unsafe void RemoveListener ()
		{
			if (id_removeListener == IntPtr.Zero)
				id_removeListener = JNIEnv.GetMethodID (class_ref, "removeListener", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener);
			} finally {
			}
		}

		static IntPtr id_sendEvent_ILandroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendEvent' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Bundle']]"
		[Register ("sendEvent", "(ILandroid/os/Bundle;)V", "GetSendEvent_ILandroid_os_Bundle_Handler")]
		public override unsafe void SendEvent (int p0, global::Android.OS.Bundle p1)
		{
			if (id_sendEvent_ILandroid_os_Bundle_ == IntPtr.Zero)
				id_sendEvent_ILandroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "sendEvent", "(ILandroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendEvent_ILandroid_os_Bundle_, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationStatus_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("sendNavigationStatus", "(I)V", "GetSendNavigationStatus_IHandler")]
		public override unsafe void SendNavigationStatus (int p0)
		{
			if (id_sendNavigationStatus_I == IntPtr.Zero)
				id_sendNavigationStatus_I = JNIEnv.GetMethodID (class_ref, "sendNavigationStatus", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationStatus_I, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationTurnDistanceEvent_IIII;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnDistanceEvent' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("sendNavigationTurnDistanceEvent", "(IIII)V", "GetSendNavigationTurnDistanceEvent_IIIIHandler")]
		public override unsafe void SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3)
		{
			if (id_sendNavigationTurnDistanceEvent_IIII == IntPtr.Zero)
				id_sendNavigationTurnDistanceEvent_IIII = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnDistanceEvent", "(IIII)V");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnDistanceEvent_IIII, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnEvent' and count(parameter)=6 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='android.graphics.Bitmap'] and parameter[6][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_IHandler")]
		public override unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, global::Android.Graphics.Bitmap p4, int p5)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [6];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				__args [5] = new JValue (p5);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IIIHandler")]
		public override unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, int p4)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
