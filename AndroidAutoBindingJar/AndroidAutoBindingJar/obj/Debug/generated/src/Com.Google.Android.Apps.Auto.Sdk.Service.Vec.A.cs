using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.Vec {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/vec/a", DoNotGenerateAcw=true)]
	public sealed partial class A : global::Java.Lang.Object, global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManager {


		public static class InterfaceConsts {

			// The following are fields from: com.google.android.apps.auto.sdk.service.vec.CarVendorExtensionManager

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/interface[@name='CarVendorExtensionManager']/field[@name='PERMISSION_VENDOR_EXTENSION']"
			[Register ("PERMISSION_VENDOR_EXTENSION")]
			public const string PermissionVendorExtension = (string) "com.google.android.gms.permission.CAR_VENDOR_EXTENSION";
		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/vec/a", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (A); }
		}

		internal A (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarVendorExtensionManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/constructor[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarVendorExtensionManager']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarVendorExtensionManager;)V", "")]
		public unsafe A (global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (A)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarVendorExtensionManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarVendorExtensionManager;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarVendorExtensionManager_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarVendorExtensionManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarVendorExtensionManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarVendorExtensionManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarVendorExtensionManager_, __args);
			} finally {
			}
		}

		static IntPtr id_getServiceName;
		public unsafe string ServiceName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='getServiceName' and count(parameter)=0]"
			[Register ("getServiceName", "()Ljava/lang/String;", "GetGetServiceNameHandler")]
			get {
				if (id_getServiceName == IntPtr.Zero)
					id_getServiceName = JNIEnv.GetMethodID (class_ref, "getServiceName", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getServiceName), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getServiceData;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='getServiceData' and count(parameter)=0]"
		[Register ("getServiceData", "()[B", "")]
		public unsafe byte[] GetServiceData ()
		{
			if (id_getServiceData == IntPtr.Zero)
				id_getServiceData = JNIEnv.GetMethodID (class_ref, "getServiceData", "()[B");
			try {
				return (byte[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getServiceData), JniHandleOwnership.TransferLocalRef, typeof (byte));
			} finally {
			}
		}

		static IntPtr id_registerListener_Lcom_google_android_apps_auto_sdk_service_vec_CarVendorExtensionManager_CarVendorExtensionListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='registerListener' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.service.vec.CarVendorExtensionManager.CarVendorExtensionListener']]"
		[Register ("registerListener", "(Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager$CarVendorExtensionListener;)V", "")]
		public unsafe void RegisterListener (global::Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManagerCarVendorExtensionListener p0)
		{
			if (id_registerListener_Lcom_google_android_apps_auto_sdk_service_vec_CarVendorExtensionManager_CarVendorExtensionListener_ == IntPtr.Zero)
				id_registerListener_Lcom_google_android_apps_auto_sdk_service_vec_CarVendorExtensionManager_CarVendorExtensionListener_ = JNIEnv.GetMethodID (class_ref, "registerListener", "(Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager$CarVendorExtensionListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerListener_Lcom_google_android_apps_auto_sdk_service_vec_CarVendorExtensionManager_CarVendorExtensionListener_, __args);
			} finally {
			}
		}

		static IntPtr id_release;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "")]
		public unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
			} finally {
			}
		}

		static IntPtr id_sendData_arrayB;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='sendData' and count(parameter)=1 and parameter[1][@type='byte[]']]"
		[Register ("sendData", "([B)V", "")]
		public unsafe void SendData (byte[] p0)
		{
			if (id_sendData_arrayB == IntPtr.Zero)
				id_sendData_arrayB = JNIEnv.GetMethodID (class_ref, "sendData", "([B)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendData_arrayB, __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static IntPtr id_sendData_arrayBII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='sendData' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("sendData", "([BII)V", "")]
		public unsafe void SendData (byte[] p0, int p1, int p2)
		{
			if (id_sendData_arrayBII == IntPtr.Zero)
				id_sendData_arrayBII = JNIEnv.GetMethodID (class_ref, "sendData", "([BII)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendData_arrayBII, __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static IntPtr id_unregisterListener;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.vec']/class[@name='a']/method[@name='unregisterListener' and count(parameter)=0]"
		[Register ("unregisterListener", "()V", "")]
		public unsafe void UnregisterListener ()
		{
			if (id_unregisterListener == IntPtr.Zero)
				id_unregisterListener = JNIEnv.GetMethodID (class_ref, "unregisterListener", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterListener);
			} finally {
			}
		}

	}
}
