using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car.Input {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']"
	[Register ("com/google/android/gms/car/input/InputManager", "", "Com.Google.Android.Gms.Car.Input.IInputManagerInvoker")]
	public partial interface IInputManager : IJavaObject {

		bool IsInputActive {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']/method[@name='isInputActive' and count(parameter)=0]"
			[Register ("isInputActive", "()Z", "GetIsInputActiveHandler:Com.Google.Android.Gms.Car.Input.IInputManagerInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsValid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']/method[@name='isValid' and count(parameter)=0]"
			[Register ("isValid", "()Z", "GetIsValidHandler:Com.Google.Android.Gms.Car.Input.IInputManagerInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']/method[@name='isCurrentCarEditable' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.input.CarEditable']]"
		[Register ("isCurrentCarEditable", "(Lcom/google/android/gms/car/input/CarEditable;)Z", "GetIsCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_Handler:Com.Google.Android.Gms.Car.Input.IInputManagerInvoker, AndroidAutoBindingJar")]
		bool IsCurrentCarEditable (global::Com.Google.Android.Gms.Car.Input.ICarEditable p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']/method[@name='startInput' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.input.CarEditable']]"
		[Register ("startInput", "(Lcom/google/android/gms/car/input/CarEditable;)V", "GetStartInput_Lcom_google_android_gms_car_input_CarEditable_Handler:Com.Google.Android.Gms.Car.Input.IInputManagerInvoker, AndroidAutoBindingJar")]
		void StartInput (global::Com.Google.Android.Gms.Car.Input.ICarEditable p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car.input']/interface[@name='InputManager']/method[@name='stopInput' and count(parameter)=0]"
		[Register ("stopInput", "()V", "GetStopInputHandler:Com.Google.Android.Gms.Car.Input.IInputManagerInvoker, AndroidAutoBindingJar")]
		void StopInput ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/input/InputManager", DoNotGenerateAcw=true)]
	internal class IInputManagerInvoker : global::Java.Lang.Object, IInputManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/input/InputManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IInputManagerInvoker); }
		}

		IntPtr class_ref;

		public static IInputManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IInputManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.input.InputManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IInputManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_isInputActive;
#pragma warning disable 0169
		static Delegate GetIsInputActiveHandler ()
		{
			if (cb_isInputActive == null)
				cb_isInputActive = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsInputActive);
			return cb_isInputActive;
		}

		static bool n_IsInputActive (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.Input.IInputManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsInputActive;
		}
#pragma warning restore 0169

		IntPtr id_isInputActive;
		public unsafe bool IsInputActive {
			get {
				if (id_isInputActive == IntPtr.Zero)
					id_isInputActive = JNIEnv.GetMethodID (class_ref, "isInputActive", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isInputActive);
			}
		}

		static Delegate cb_isValid;
#pragma warning disable 0169
		static Delegate GetIsValidHandler ()
		{
			if (cb_isValid == null)
				cb_isValid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsValid);
			return cb_isValid;
		}

		static bool n_IsValid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.Input.IInputManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsValid;
		}
#pragma warning restore 0169

		IntPtr id_isValid;
		public unsafe bool IsValid {
			get {
				if (id_isValid == IntPtr.Zero)
					id_isValid = JNIEnv.GetMethodID (class_ref, "isValid", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isValid);
			}
		}

		static Delegate cb_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_;
#pragma warning disable 0169
		static Delegate GetIsCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_Handler ()
		{
			if (cb_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_ == null)
				cb_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_IsCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_);
			return cb_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_;
		}

		static bool n_IsCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.Input.IInputManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.Input.ICarEditable p0 = (global::Com.Google.Android.Gms.Car.Input.ICarEditable)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditable> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsCurrentCarEditable (p0);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_;
		public unsafe bool IsCurrentCarEditable (global::Com.Google.Android.Gms.Car.Input.ICarEditable p0)
		{
			if (id_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_ == IntPtr.Zero)
				id_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_ = JNIEnv.GetMethodID (class_ref, "isCurrentCarEditable", "(Lcom/google/android/gms/car/input/CarEditable;)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isCurrentCarEditable_Lcom_google_android_gms_car_input_CarEditable_, __args);
			return __ret;
		}

		static Delegate cb_startInput_Lcom_google_android_gms_car_input_CarEditable_;
#pragma warning disable 0169
		static Delegate GetStartInput_Lcom_google_android_gms_car_input_CarEditable_Handler ()
		{
			if (cb_startInput_Lcom_google_android_gms_car_input_CarEditable_ == null)
				cb_startInput_Lcom_google_android_gms_car_input_CarEditable_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartInput_Lcom_google_android_gms_car_input_CarEditable_);
			return cb_startInput_Lcom_google_android_gms_car_input_CarEditable_;
		}

		static void n_StartInput_Lcom_google_android_gms_car_input_CarEditable_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.Input.IInputManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.Input.ICarEditable p0 = (global::Com.Google.Android.Gms.Car.Input.ICarEditable)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.ICarEditable> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartInput (p0);
		}
#pragma warning restore 0169

		IntPtr id_startInput_Lcom_google_android_gms_car_input_CarEditable_;
		public unsafe void StartInput (global::Com.Google.Android.Gms.Car.Input.ICarEditable p0)
		{
			if (id_startInput_Lcom_google_android_gms_car_input_CarEditable_ == IntPtr.Zero)
				id_startInput_Lcom_google_android_gms_car_input_CarEditable_ = JNIEnv.GetMethodID (class_ref, "startInput", "(Lcom/google/android/gms/car/input/CarEditable;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startInput_Lcom_google_android_gms_car_input_CarEditable_, __args);
		}

		static Delegate cb_stopInput;
#pragma warning disable 0169
		static Delegate GetStopInputHandler ()
		{
			if (cb_stopInput == null)
				cb_stopInput = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopInput);
			return cb_stopInput;
		}

		static void n_StopInput (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.Input.IInputManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopInput ();
		}
#pragma warning restore 0169

		IntPtr id_stopInput;
		public unsafe void StopInput ()
		{
			if (id_stopInput == IntPtr.Zero)
				id_stopInput = JNIEnv.GetMethodID (class_ref, "stopInput", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stopInput);
		}

	}

}
