using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/e", DoNotGenerateAcw=true)]
	public sealed partial class E : global::Android.Support.Car.CarInfoManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/e", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (E); }
		}

		internal E (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarInfoManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/constructor[@name='e' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarInfoManager']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarInfoManager;)V", "")]
		public unsafe E (global::Com.Google.Android.Gms.Car.ICarInfoManager p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (E)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarInfoManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarInfoManager;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarInfoManager_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarInfoManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarInfoManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarInfoManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarInfoManager_, __args);
			} finally {
			}
		}

		static IntPtr id_getDriverPosition;
		public override unsafe int DriverPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getDriverPosition' and count(parameter)=0]"
			[Register ("getDriverPosition", "()I", "GetGetDriverPositionHandler")]
			get {
				if (id_getDriverPosition == IntPtr.Zero)
					id_getDriverPosition = JNIEnv.GetMethodID (class_ref, "getDriverPosition", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getDriverPosition);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitManufacturer;
		public override unsafe string HeadunitManufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getHeadunitManufacturer' and count(parameter)=0]"
			[Register ("getHeadunitManufacturer", "()Ljava/lang/String;", "GetGetHeadunitManufacturerHandler")]
			get {
				if (id_getHeadunitManufacturer == IntPtr.Zero)
					id_getHeadunitManufacturer = JNIEnv.GetMethodID (class_ref, "getHeadunitManufacturer", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitManufacturer), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitModel;
		public override unsafe string HeadunitModel {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getHeadunitModel' and count(parameter)=0]"
			[Register ("getHeadunitModel", "()Ljava/lang/String;", "GetGetHeadunitModelHandler")]
			get {
				if (id_getHeadunitModel == IntPtr.Zero)
					id_getHeadunitModel = JNIEnv.GetMethodID (class_ref, "getHeadunitModel", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitModel), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitSoftwareBuild;
		public override unsafe string HeadunitSoftwareBuild {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getHeadunitSoftwareBuild' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareBuild", "()Ljava/lang/String;", "GetGetHeadunitSoftwareBuildHandler")]
			get {
				if (id_getHeadunitSoftwareBuild == IntPtr.Zero)
					id_getHeadunitSoftwareBuild = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareBuild", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareBuild), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getHeadunitSoftwareVersion;
		public override unsafe string HeadunitSoftwareVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getHeadunitSoftwareVersion' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareVersion", "()Ljava/lang/String;", "GetGetHeadunitSoftwareVersionHandler")]
			get {
				if (id_getHeadunitSoftwareVersion == IntPtr.Zero)
					id_getHeadunitSoftwareVersion = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareVersion", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareVersion), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getManufacturer;
		public override unsafe string Manufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getManufacturer' and count(parameter)=0]"
			[Register ("getManufacturer", "()Ljava/lang/String;", "GetGetManufacturerHandler")]
			get {
				if (id_getManufacturer == IntPtr.Zero)
					id_getManufacturer = JNIEnv.GetMethodID (class_ref, "getManufacturer", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getManufacturer), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getModel;
		public override unsafe string Model {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getModel' and count(parameter)=0]"
			[Register ("getModel", "()Ljava/lang/String;", "GetGetModelHandler")]
			get {
				if (id_getModel == IntPtr.Zero)
					id_getModel = JNIEnv.GetMethodID (class_ref, "getModel", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModel), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getModelYear;
		public override unsafe string ModelYear {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getModelYear' and count(parameter)=0]"
			[Register ("getModelYear", "()Ljava/lang/String;", "GetGetModelYearHandler")]
			get {
				if (id_getModelYear == IntPtr.Zero)
					id_getModelYear = JNIEnv.GetMethodID (class_ref, "getModelYear", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModelYear), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getVehicleId;
		public override unsafe string VehicleId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='getVehicleId' and count(parameter)=0]"
			[Register ("getVehicleId", "()Ljava/lang/String;", "GetGetVehicleIdHandler")]
			get {
				if (id_getVehicleId == IntPtr.Zero)
					id_getVehicleId = JNIEnv.GetMethodID (class_ref, "getVehicleId", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getVehicleId), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='e']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "")]
		public override sealed unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}
}
