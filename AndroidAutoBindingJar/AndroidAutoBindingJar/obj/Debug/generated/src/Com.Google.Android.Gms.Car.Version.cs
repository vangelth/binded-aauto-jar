using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='Version']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/Version", DoNotGenerateAcw=true)]
	public partial class Version : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/Version", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Version); }
		}

		protected Version (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getVersion;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='Version']/method[@name='getVersion' and count(parameter)=0]"
		[Register ("getVersion", "()I", "")]
		public static unsafe int GetVersion ()
		{
			if (id_getVersion == IntPtr.Zero)
				id_getVersion = JNIEnv.GetStaticMethodID (class_ref, "getVersion", "()I");
			try {
				return JNIEnv.CallStaticIntMethod  (class_ref, id_getVersion);
			} finally {
			}
		}

	}
}
