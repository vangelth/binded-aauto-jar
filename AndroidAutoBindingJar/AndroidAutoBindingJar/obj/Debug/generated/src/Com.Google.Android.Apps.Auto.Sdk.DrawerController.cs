using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/DrawerController", DoNotGenerateAcw=true)]
	public partial class DrawerController : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/DrawerController", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (DrawerController); }
		}

		protected DrawerController (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_isDrawerOpen;
#pragma warning disable 0169
		static Delegate GetIsDrawerOpenHandler ()
		{
			if (cb_isDrawerOpen == null)
				cb_isDrawerOpen = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsDrawerOpen);
			return cb_isDrawerOpen;
		}

		static bool n_IsDrawerOpen (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsDrawerOpen;
		}
#pragma warning restore 0169

		static IntPtr id_isDrawerOpen;
		public virtual unsafe bool IsDrawerOpen {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='isDrawerOpen' and count(parameter)=0]"
			[Register ("isDrawerOpen", "()Z", "GetIsDrawerOpenHandler")]
			get {
				if (id_isDrawerOpen == IntPtr.Zero)
					id_isDrawerOpen = JNIEnv.GetMethodID (class_ref, "isDrawerOpen", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isDrawerOpen);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isDrawerOpen", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isDrawerVisible;
#pragma warning disable 0169
		static Delegate GetIsDrawerVisibleHandler ()
		{
			if (cb_isDrawerVisible == null)
				cb_isDrawerVisible = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsDrawerVisible);
			return cb_isDrawerVisible;
		}

		static bool n_IsDrawerVisible (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsDrawerVisible;
		}
#pragma warning restore 0169

		static IntPtr id_isDrawerVisible;
		public virtual unsafe bool IsDrawerVisible {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='isDrawerVisible' and count(parameter)=0]"
			[Register ("isDrawerVisible", "()Z", "GetIsDrawerVisibleHandler")]
			get {
				if (id_isDrawerVisible == IntPtr.Zero)
					id_isDrawerVisible = JNIEnv.GetMethodID (class_ref, "isDrawerVisible", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isDrawerVisible);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isDrawerVisible", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_closeDrawer;
#pragma warning disable 0169
		static Delegate GetCloseDrawerHandler ()
		{
			if (cb_closeDrawer == null)
				cb_closeDrawer = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_CloseDrawer);
			return cb_closeDrawer;
		}

		static void n_CloseDrawer (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CloseDrawer ();
		}
#pragma warning restore 0169

		static IntPtr id_closeDrawer;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='closeDrawer' and count(parameter)=0]"
		[Register ("closeDrawer", "()V", "GetCloseDrawerHandler")]
		public virtual unsafe void CloseDrawer ()
		{
			if (id_closeDrawer == IntPtr.Zero)
				id_closeDrawer = JNIEnv.GetMethodID (class_ref, "closeDrawer", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_closeDrawer);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "closeDrawer", "()V"));
			} finally {
			}
		}

		static Delegate cb_openDrawer;
#pragma warning disable 0169
		static Delegate GetOpenDrawerHandler ()
		{
			if (cb_openDrawer == null)
				cb_openDrawer = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OpenDrawer);
			return cb_openDrawer;
		}

		static void n_OpenDrawer (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OpenDrawer ();
		}
#pragma warning restore 0169

		static IntPtr id_openDrawer;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='openDrawer' and count(parameter)=0]"
		[Register ("openDrawer", "()V", "GetOpenDrawerHandler")]
		public virtual unsafe void OpenDrawer ()
		{
			if (id_openDrawer == IntPtr.Zero)
				id_openDrawer = JNIEnv.GetMethodID (class_ref, "openDrawer", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_openDrawer);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "openDrawer", "()V"));
			} finally {
			}
		}

		static Delegate cb_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_;
#pragma warning disable 0169
		static Delegate GetSetDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_Handler ()
		{
			if (cb_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_ == null)
				cb_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_);
			return cb_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_;
		}

		static void n_SetDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetDrawerCallback (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='setDrawerCallback' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.DrawerCallback']]"
		[Register ("setDrawerCallback", "(Lcom/google/android/apps/auto/sdk/DrawerCallback;)V", "GetSetDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_Handler")]
		public virtual unsafe void SetDrawerCallback (global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback p0)
		{
			if (id_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_ == IntPtr.Zero)
				id_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_ = JNIEnv.GetMethodID (class_ref, "setDrawerCallback", "(Lcom/google/android/apps/auto/sdk/DrawerCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setDrawerCallback_Lcom_google_android_apps_auto_sdk_DrawerCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDrawerCallback", "(Lcom/google/android/apps/auto/sdk/DrawerCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setScrimColor_I;
#pragma warning disable 0169
		static Delegate GetSetScrimColor_IHandler ()
		{
			if (cb_setScrimColor_I == null)
				cb_setScrimColor_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetScrimColor_I);
			return cb_setScrimColor_I;
		}

		static void n_SetScrimColor_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetScrimColor (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setScrimColor_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerController']/method[@name='setScrimColor' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setScrimColor", "(I)V", "GetSetScrimColor_IHandler")]
		public virtual unsafe void SetScrimColor (int p0)
		{
			if (id_setScrimColor_I == IntPtr.Zero)
				id_setScrimColor_I = JNIEnv.GetMethodID (class_ref, "setScrimColor", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setScrimColor_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setScrimColor", "(I)V"), __args);
			} finally {
			}
		}

	}
}
