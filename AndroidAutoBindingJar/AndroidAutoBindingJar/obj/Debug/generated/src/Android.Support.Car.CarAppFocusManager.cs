using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']"
	[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager", DoNotGenerateAcw=true)]
	public abstract partial class CarAppFocusManager : global::Java.Lang.Object, global::Android.Support.Car.ICarManagerBase {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/field[@name='APP_FOCUS_REQUEST_FAILED']"
		[Register ("APP_FOCUS_REQUEST_FAILED")]
		public const int AppFocusRequestFailed = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/field[@name='APP_FOCUS_REQUEST_SUCCEEDED']"
		[Register ("APP_FOCUS_REQUEST_SUCCEEDED")]
		public const int AppFocusRequestSucceeded = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/field[@name='APP_FOCUS_TYPE_MAX']"
		[Register ("APP_FOCUS_TYPE_MAX")]
		public const int AppFocusTypeMax = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/field[@name='APP_FOCUS_TYPE_NAVIGATION']"
		[Register ("APP_FOCUS_TYPE_NAVIGATION")]
		public const int AppFocusTypeNavigation = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/field[@name='APP_FOCUS_TYPE_VOICE_COMMAND']"
		[Register ("APP_FOCUS_TYPE_VOICE_COMMAND")]
		public const int AppFocusTypeVoiceCommand = (int) 2;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.AppFocusRequestResult']"
		[Register ("android/support/car/CarAppFocusManager$AppFocusRequestResult", "", "Android.Support.Car.CarAppFocusManager/IAppFocusRequestResultInvoker")]
		public partial interface IAppFocusRequestResult : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager$AppFocusRequestResult", DoNotGenerateAcw=true)]
		internal class IAppFocusRequestResultInvoker : global::Java.Lang.Object, IAppFocusRequestResult {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarAppFocusManager$AppFocusRequestResult");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IAppFocusRequestResultInvoker); }
			}

			IntPtr class_ref;

			public static IAppFocusRequestResult GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IAppFocusRequestResult> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarAppFocusManager.AppFocusRequestResult"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IAppFocusRequestResultInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusRequestResult> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.AppFocusType']"
		[Register ("android/support/car/CarAppFocusManager$AppFocusType", "", "Android.Support.Car.CarAppFocusManager/IAppFocusTypeInvoker")]
		public partial interface IAppFocusType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager$AppFocusType", DoNotGenerateAcw=true)]
		internal class IAppFocusTypeInvoker : global::Java.Lang.Object, IAppFocusType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarAppFocusManager$AppFocusType");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IAppFocusTypeInvoker); }
			}

			IntPtr class_ref;

			public static IAppFocusType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IAppFocusType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarAppFocusManager.AppFocusType"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IAppFocusTypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.CarAppFocusManager.IAppFocusType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IAppFocusType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.OnAppFocusChangedListener']"
		[Register ("android/support/car/CarAppFocusManager$OnAppFocusChangedListener", "", "Android.Support.Car.CarAppFocusManager/IOnAppFocusChangedListenerInvoker")]
		public partial interface IOnAppFocusChangedListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.OnAppFocusChangedListener']/method[@name='onAppFocusChanged' and count(parameter)=3 and parameter[1][@type='android.support.car.CarAppFocusManager'] and parameter[2][@type='int'] and parameter[3][@type='boolean']]"
			[Register ("onAppFocusChanged", "(Landroid/support/car/CarAppFocusManager;IZ)V", "GetOnAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZHandler:Android.Support.Car.CarAppFocusManager/IOnAppFocusChangedListenerInvoker, AndroidAutoBindingJar")]
			void OnAppFocusChanged (global::Android.Support.Car.CarAppFocusManager p0, int p1, bool p2);

		}

		[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager$OnAppFocusChangedListener", DoNotGenerateAcw=true)]
		internal class IOnAppFocusChangedListenerInvoker : global::Java.Lang.Object, IOnAppFocusChangedListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarAppFocusManager$OnAppFocusChangedListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnAppFocusChangedListenerInvoker); }
			}

			IntPtr class_ref;

			public static IOnAppFocusChangedListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnAppFocusChangedListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarAppFocusManager.OnAppFocusChangedListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnAppFocusChangedListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ;
#pragma warning disable 0169
			static Delegate GetOnAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZHandler ()
			{
				if (cb_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ == null)
					cb_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int, bool>) n_OnAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ);
				return cb_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ;
			}

			static void n_OnAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, bool p2)
			{
				global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.CarAppFocusManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnAppFocusChanged (p0, p1, p2);
			}
#pragma warning restore 0169

			IntPtr id_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ;
			public unsafe void OnAppFocusChanged (global::Android.Support.Car.CarAppFocusManager p0, int p1, bool p2)
			{
				if (id_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ == IntPtr.Zero)
					id_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ = JNIEnv.GetMethodID (class_ref, "onAppFocusChanged", "(Landroid/support/car/CarAppFocusManager;IZ)V");
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZ, __args);
			}

		}

		public partial class AppFocusChangedEventArgs : global::System.EventArgs {

			public AppFocusChangedEventArgs (global::Android.Support.Car.CarAppFocusManager p0, int p1, bool p2)
			{
				this.p0 = p0;
				this.p1 = p1;
				this.p2 = p2;
			}

			global::Android.Support.Car.CarAppFocusManager p0;
			public global::Android.Support.Car.CarAppFocusManager P0 {
				get { return p0; }
			}

			int p1;
			public int P1 {
				get { return p1; }
			}

			bool p2;
			public bool P2 {
				get { return p2; }
			}
		}

		[global::Android.Runtime.Register ("mono/android/support/car/CarAppFocusManager_OnAppFocusChangedListenerImplementor")]
		internal sealed partial class IOnAppFocusChangedListenerImplementor : global::Java.Lang.Object, IOnAppFocusChangedListener {

			object sender;

			public IOnAppFocusChangedListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/android/support/car/CarAppFocusManager_OnAppFocusChangedListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<AppFocusChangedEventArgs> Handler;
#pragma warning restore 0649

			public void OnAppFocusChanged (global::Android.Support.Car.CarAppFocusManager p0, int p1, bool p2)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new AppFocusChangedEventArgs (p0, p1, p2));
			}

			internal static bool __IsEmpty (IOnAppFocusChangedListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.OnAppFocusOwnershipCallback']"
		[Register ("android/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback", "", "Android.Support.Car.CarAppFocusManager/IOnAppFocusOwnershipCallbackInvoker")]
		public partial interface IOnAppFocusOwnershipCallback : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.OnAppFocusOwnershipCallback']/method[@name='onAppFocusOwnershipGranted' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager'] and parameter[2][@type='int']]"
			[Register ("onAppFocusOwnershipGranted", "(Landroid/support/car/CarAppFocusManager;I)V", "GetOnAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_IHandler:Android.Support.Car.CarAppFocusManager/IOnAppFocusOwnershipCallbackInvoker, AndroidAutoBindingJar")]
			void OnAppFocusOwnershipGranted (global::Android.Support.Car.CarAppFocusManager p0, int p1);

			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarAppFocusManager.OnAppFocusOwnershipCallback']/method[@name='onAppFocusOwnershipLost' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager'] and parameter[2][@type='int']]"
			[Register ("onAppFocusOwnershipLost", "(Landroid/support/car/CarAppFocusManager;I)V", "GetOnAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_IHandler:Android.Support.Car.CarAppFocusManager/IOnAppFocusOwnershipCallbackInvoker, AndroidAutoBindingJar")]
			void OnAppFocusOwnershipLost (global::Android.Support.Car.CarAppFocusManager p0, int p1);

		}

		[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback", DoNotGenerateAcw=true)]
		internal class IOnAppFocusOwnershipCallbackInvoker : global::Java.Lang.Object, IOnAppFocusOwnershipCallback {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnAppFocusOwnershipCallbackInvoker); }
			}

			IntPtr class_ref;

			public static IOnAppFocusOwnershipCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnAppFocusOwnershipCallback> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnAppFocusOwnershipCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I;
#pragma warning disable 0169
			static Delegate GetOnAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_IHandler ()
			{
				if (cb_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I == null)
					cb_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_OnAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I);
				return cb_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I;
			}

			static void n_OnAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
			{
				global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.CarAppFocusManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnAppFocusOwnershipGranted (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I;
			public unsafe void OnAppFocusOwnershipGranted (global::Android.Support.Car.CarAppFocusManager p0, int p1)
			{
				if (id_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I == IntPtr.Zero)
					id_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I = JNIEnv.GetMethodID (class_ref, "onAppFocusOwnershipGranted", "(Landroid/support/car/CarAppFocusManager;I)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAppFocusOwnershipGranted_Landroid_support_car_CarAppFocusManager_I, __args);
			}

			static Delegate cb_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I;
#pragma warning disable 0169
			static Delegate GetOnAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_IHandler ()
			{
				if (cb_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I == null)
					cb_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_OnAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I);
				return cb_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I;
			}

			static void n_OnAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
			{
				global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Support.Car.CarAppFocusManager p0 = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnAppFocusOwnershipLost (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I;
			public unsafe void OnAppFocusOwnershipLost (global::Android.Support.Car.CarAppFocusManager p0, int p1)
			{
				if (id_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I == IntPtr.Zero)
					id_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I = JNIEnv.GetMethodID (class_ref, "onAppFocusOwnershipLost", "(Landroid/support/car/CarAppFocusManager;I)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAppFocusOwnershipLost_Landroid_support_car_CarAppFocusManager_I, __args);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarAppFocusManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAppFocusManager); }
		}

		protected CarAppFocusManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/constructor[@name='CarAppFocusManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarAppFocusManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarAppFocusManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static void n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAppFocus (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='abandonAppFocus' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public abstract void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0);

		static Delegate cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
#pragma warning disable 0169
		static Delegate GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_IHandler ()
		{
			if (cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I == null)
				cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I);
			return cb_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
		}

		static void n_AbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AbandonAppFocus (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='abandonAppFocus' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback'] and parameter[2][@type='int']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_IHandler")]
		public abstract void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0, int p1);

		static Delegate cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
#pragma warning disable 0169
		static Delegate GetAddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler ()
		{
			if (cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == null)
				cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_AddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I);
			return cb_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		}

		static void n_AddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddFocusListener (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='addFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetAddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public abstract void AddFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1);

		static Delegate cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetIsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_IsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static bool n_IsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsOwningFocus (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='isOwningFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z", "GetIsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public abstract bool IsOwningFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1);

		static Delegate cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
#pragma warning disable 0169
		static Delegate GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_Handler ()
		{
			if (cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ == null)
				cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_);
			return cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
		}

		static void n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveFocusListener (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='removeFocusListener' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_Handler")]
		public abstract void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0);

		static Delegate cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
#pragma warning disable 0169
		static Delegate GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler ()
		{
			if (cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == null)
				cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I);
			return cb_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		}

		static void n_RemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveFocusListener (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='removeFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public abstract void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1);

		static Delegate cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
#pragma warning disable 0169
		static Delegate GetRequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler ()
		{
			if (cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == null)
				cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, int>) n_RequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_);
			return cb_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		}

		static int n_RequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1 = (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback> (native_p1, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.RequestAppFocus (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='requestAppFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I", "GetRequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public abstract int RequestAppFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1);

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarAppFocusManager __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarAppFocusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public abstract void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/CarAppFocusManager", DoNotGenerateAcw=true)]
	internal partial class CarAppFocusManagerInvoker : CarAppFocusManager {

		public CarAppFocusManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAppFocusManagerInvoker); }
		}

		static IntPtr id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='abandonAppFocus' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0)
		{
			if (id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
			} finally {
			}
		}

		static IntPtr id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='abandonAppFocus' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback'] and parameter[2][@type='int']]"
		[Register ("abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V", "GetAbandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_IHandler")]
		public override unsafe void AbandonAppFocus (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p0, int p1)
		{
			if (id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I == IntPtr.Zero)
				id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I = JNIEnv.GetMethodID (class_ref, "abandonAppFocus", "(Landroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_abandonAppFocus_Landroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_I, __args);
			} finally {
			}
		}

		static IntPtr id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='addFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetAddFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public override unsafe void AddFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1)
		{
			if (id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == IntPtr.Zero)
				id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNIEnv.GetMethodID (class_ref, "addFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_addFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I, __args);
			} finally {
			}
		}

		static IntPtr id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='isOwningFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z", "GetIsOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe bool IsOwningFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1)
		{
			if (id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "isOwningFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isOwningFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='removeFocusListener' and count(parameter)=1 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_Handler")]
		public override unsafe void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0)
		{
			if (id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ == IntPtr.Zero)
				id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_ = JNIEnv.GetMethodID (class_ref, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_, __args);
			} finally {
			}
		}

		static IntPtr id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='removeFocusListener' and count(parameter)=2 and parameter[1][@type='android.support.car.CarAppFocusManager.OnAppFocusChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V", "GetRemoveFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_IHandler")]
		public override unsafe void RemoveFocusListener (global::Android.Support.Car.CarAppFocusManager.IOnAppFocusChangedListener p0, int p1)
		{
			if (id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I == IntPtr.Zero)
				id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I = JNIEnv.GetMethodID (class_ref, "removeFocusListener", "(Landroid/support/car/CarAppFocusManager$OnAppFocusChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeFocusListener_Landroid_support_car_CarAppFocusManager_OnAppFocusChangedListener_I, __args);
			} finally {
			}
		}

		static IntPtr id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarAppFocusManager']/method[@name='requestAppFocus' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.support.car.CarAppFocusManager.OnAppFocusOwnershipCallback']]"
		[Register ("requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I", "GetRequestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_Handler")]
		public override unsafe int RequestAppFocus (int p0, global::Android.Support.Car.CarAppFocusManager.IOnAppFocusOwnershipCallback p1)
		{
			if (id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ == IntPtr.Zero)
				id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_ = JNIEnv.GetMethodID (class_ref, "requestAppFocus", "(ILandroid/support/car/CarAppFocusManager$OnAppFocusOwnershipCallback;)I");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_requestAppFocus_ILandroid_support_car_CarAppFocusManager_OnAppFocusOwnershipCallback_, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

	}

}
