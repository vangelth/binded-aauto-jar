using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/CarServiceLoaderGms", DoNotGenerateAcw=true)]
	public partial class CarServiceLoaderGms : global::Android.Support.Car.CarServiceLoader {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/CarServiceLoaderGms", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarServiceLoaderGms); }
		}

		protected CarServiceLoaderGms (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/constructor[@name='CarServiceLoaderGms' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.support.car.CarServiceLoader.CarConnectionCallbackProxy'] and parameter[3][@type='android.os.Handler']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", "")]
		public unsafe CarServiceLoaderGms (global::Android.Content.Context p0, global::Android.Support.Car.CarServiceLoader.CarConnectionCallbackProxy p1, global::Android.OS.Handler p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (CarServiceLoaderGms)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/support/car/CarServiceLoader$CarConnectionCallbackProxy;Landroid/os/Handler;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_support_car_CarServiceLoader_CarConnectionCallbackProxy_Landroid_os_Handler_, __args);
			} finally {
			}
		}

		static Delegate cb_getCarConnectionType;
#pragma warning disable 0169
		static Delegate GetGetCarConnectionTypeHandler ()
		{
			if (cb_getCarConnectionType == null)
				cb_getCarConnectionType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetCarConnectionType);
			return cb_getCarConnectionType;
		}

		static int n_GetCarConnectionType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CarConnectionType;
		}
#pragma warning restore 0169

		static IntPtr id_getCarConnectionType;
		public override unsafe int CarConnectionType {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/method[@name='getCarConnectionType' and count(parameter)=0]"
			[Register ("getCarConnectionType", "()I", "GetGetCarConnectionTypeHandler")]
			get {
				if (id_getCarConnectionType == IntPtr.Zero)
					id_getCarConnectionType = JNIEnv.GetMethodID (class_ref, "getCarConnectionType", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getCarConnectionType);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarConnectionType", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_isConnected;
#pragma warning disable 0169
		static Delegate GetIsConnectedHandler ()
		{
			if (cb_isConnected == null)
				cb_isConnected = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConnected);
			return cb_isConnected;
		}

		static bool n_IsConnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsConnected;
		}
#pragma warning restore 0169

		static IntPtr id_isConnected;
		public override unsafe bool IsConnected {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/method[@name='isConnected' and count(parameter)=0]"
			[Register ("isConnected", "()Z", "GetIsConnectedHandler")]
			get {
				if (id_isConnected == IntPtr.Zero)
					id_isConnected = JNIEnv.GetMethodID (class_ref, "isConnected", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConnected);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isConnected", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_connect;
#pragma warning disable 0169
		static Delegate GetConnectHandler ()
		{
			if (cb_connect == null)
				cb_connect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Connect);
			return cb_connect;
		}

		static void n_Connect (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Connect ();
		}
#pragma warning restore 0169

		static IntPtr id_connect;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/method[@name='connect' and count(parameter)=0]"
		[Register ("connect", "()V", "GetConnectHandler")]
		public override unsafe void Connect ()
		{
			if (id_connect == IntPtr.Zero)
				id_connect = JNIEnv.GetMethodID (class_ref, "connect", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_connect);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "connect", "()V"));
			} finally {
			}
		}

		static Delegate cb_disconnect;
#pragma warning disable 0169
		static Delegate GetDisconnectHandler ()
		{
			if (cb_disconnect == null)
				cb_disconnect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Disconnect);
			return cb_disconnect;
		}

		static void n_Disconnect (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Disconnect ();
		}
#pragma warning restore 0169

		static IntPtr id_disconnect;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/method[@name='disconnect' and count(parameter)=0]"
		[Register ("disconnect", "()V", "GetDisconnectHandler")]
		public override unsafe void Disconnect ()
		{
			if (id_disconnect == IntPtr.Zero)
				id_disconnect = JNIEnv.GetMethodID (class_ref, "disconnect", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_disconnect);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "disconnect", "()V"));
			} finally {
			}
		}

		static Delegate cb_getCarManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_String_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_String_ == null)
				cb_getCarManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_String_);
			return cb_getCarManager_Ljava_lang_String_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Service.CarServiceLoaderGms> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getCarManager_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service']/class[@name='CarServiceLoaderGms']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler")]
		public override unsafe global::Java.Lang.Object GetCarManager (string p0)
		{
			if (id_getCarManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				global::Java.Lang.Object __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
