using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Java.Interop {

	partial class __TypeRegistrations {

		public static void RegisterPackages ()
		{
#if MONODROID_TIMING
			var start = DateTime.Now;
			Android.Util.Log.Info ("MonoDroid-Timing", "RegisterPackages start: " + (start - new DateTime (1970, 1, 1)).TotalMilliseconds);
#endif // def MONODROID_TIMING
			Java.Interop.TypeManager.RegisterPackages (
					new string[]{
						"com/google/android/apps/auto/sdk",
						"com/google/android/gms/car",
					},
					new Converter<string, Type>[]{
						lookup_com_google_android_apps_auto_sdk_package,
						lookup_com_google_android_gms_car_package,
					});
#if MONODROID_TIMING
			var end = DateTime.Now;
			Android.Util.Log.Info ("MonoDroid-Timing", "RegisterPackages time: " + (end - new DateTime (1970, 1, 1)).TotalMilliseconds + " [elapsed: " + (end - start).TotalMilliseconds + " ms]");
#endif // def MONODROID_TIMING
		}

		static Type Lookup (string[] mappings, string javaType)
		{
			string managedType = Java.Interop.TypeManager.LookupTypeMapping (mappings, javaType);
			if (managedType == null)
				return null;
			return Type.GetType (managedType);
		}

		static string[] package_com_google_android_apps_auto_sdk_mappings;
		static Type lookup_com_google_android_apps_auto_sdk_package (string klass)
		{
			if (package_com_google_android_apps_auto_sdk_mappings == null) {
				package_com_google_android_apps_auto_sdk_mappings = new string[]{
					"com/google/android/apps/auto/sdk/ad:Com.Google.Android.Apps.Auto.Sdk.Ad",
				};
			}

			return Lookup (package_com_google_android_apps_auto_sdk_mappings, klass);
		}

		static string[] package_com_google_android_gms_car_mappings;
		static Type lookup_com_google_android_gms_car_package (string klass)
		{
			if (package_com_google_android_gms_car_mappings == null) {
				package_com_google_android_gms_car_mappings = new string[]{
					"com/google/android/gms/car/CarSensorManager$RawEventData:Com.Google.Android.Gms.Car.CarSensorManagerRawEventData",
				};
			}

			return Lookup (package_com_google_android_gms_car_mappings, klass);
		}
	}
}
