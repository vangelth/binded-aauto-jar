using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Input {

	// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarEditableListener']"
	[Register ("android/support/car/input/CarEditableListener", "", "Android.Support.Car.Input.ICarEditableListenerInvoker")]
	public partial interface ICarEditableListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.input']/interface[@name='CarEditableListener']/method[@name='onUpdateSelection' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("onUpdateSelection", "(IIII)V", "GetOnUpdateSelection_IIIIHandler:Android.Support.Car.Input.ICarEditableListenerInvoker, AndroidAutoBindingJar")]
		void OnUpdateSelection (int p0, int p1, int p2, int p3);

	}

	[global::Android.Runtime.Register ("android/support/car/input/CarEditableListener", DoNotGenerateAcw=true)]
	internal class ICarEditableListenerInvoker : global::Java.Lang.Object, ICarEditableListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/input/CarEditableListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarEditableListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarEditableListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarEditableListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "android.support.car.input.CarEditableListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarEditableListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onUpdateSelection_IIII;
#pragma warning disable 0169
		static Delegate GetOnUpdateSelection_IIIIHandler ()
		{
			if (cb_onUpdateSelection_IIII == null)
				cb_onUpdateSelection_IIII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int, int>) n_OnUpdateSelection_IIII);
			return cb_onUpdateSelection_IIII;
		}

		static void n_OnUpdateSelection_IIII (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, int p3)
		{
			global::Android.Support.Car.Input.ICarEditableListener __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Input.ICarEditableListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnUpdateSelection (p0, p1, p2, p3);
		}
#pragma warning restore 0169

		IntPtr id_onUpdateSelection_IIII;
		public unsafe void OnUpdateSelection (int p0, int p1, int p2, int p3)
		{
			if (id_onUpdateSelection_IIII == IntPtr.Zero)
				id_onUpdateSelection_IIII = JNIEnv.GetMethodID (class_ref, "onUpdateSelection", "(IIII)V");
			JValue* __args = stackalloc JValue [4];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			__args [3] = new JValue (p3);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onUpdateSelection_IIII, __args);
		}

	}

	public partial class CarEditableEventArgs : global::System.EventArgs {

		public CarEditableEventArgs (int p0, int p1, int p2, int p3)
		{
			this.p0 = p0;
			this.p1 = p1;
			this.p2 = p2;
			this.p3 = p3;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}

		int p1;
		public int P1 {
			get { return p1; }
		}

		int p2;
		public int P2 {
			get { return p2; }
		}

		int p3;
		public int P3 {
			get { return p3; }
		}
	}

	[global::Android.Runtime.Register ("mono/android/support/car/input/CarEditableListenerImplementor")]
	internal sealed partial class ICarEditableListenerImplementor : global::Java.Lang.Object, ICarEditableListener {

		object sender;

		public ICarEditableListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/android/support/car/input/CarEditableListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<CarEditableEventArgs> Handler;
#pragma warning restore 0649

		public void OnUpdateSelection (int p0, int p1, int p2, int p3)
		{
			var __h = Handler;
			if (__h != null)
				__h (sender, new CarEditableEventArgs (p0, p1, p2, p3));
		}

		internal static bool __IsEmpty (ICarEditableListenerImplementor value)
		{
			return value.Handler == null;
		}
	}

}
