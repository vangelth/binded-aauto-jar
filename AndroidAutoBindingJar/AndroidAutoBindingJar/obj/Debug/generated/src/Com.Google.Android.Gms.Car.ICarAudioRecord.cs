using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']"
	[Register ("com/google/android/gms/car/CarAudioRecord", "", "Com.Google.Android.Gms.Car.ICarAudioRecordInvoker")]
	public partial interface ICarAudioRecord : IJavaObject {

		int BufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='getBufferSize' and count(parameter)=0]"
			[Register ("getBufferSize", "()I", "GetGetBufferSizeHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")] get;
		}

		int ConfigurationIndex {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='getConfigurationIndex' and count(parameter)=0]"
			[Register ("getConfigurationIndex", "()I", "GetGetConfigurationIndexHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")] get;
		}

		int RecordingState {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='getRecordingState' and count(parameter)=0]"
			[Register ("getRecordingState", "()I", "GetGetRecordingStateHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")] get;
		}

		int State {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='getState' and count(parameter)=0]"
			[Register ("getState", "()I", "GetGetStateHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")] get;
		}

		int StreamType {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='getStreamType' and count(parameter)=0]"
			[Register ("getStreamType", "()I", "GetGetStreamTypeHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='read' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("read", "([BII)I", "GetRead_arrayBIIHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")]
		int Read (byte[] p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "GetReleaseHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")]
		void Release ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='startRecording' and count(parameter)=0]"
		[Register ("startRecording", "()V", "GetStartRecordingHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")]
		void StartRecording ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioRecord']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "GetStopHandler:Com.Google.Android.Gms.Car.ICarAudioRecordInvoker, AndroidAutoBindingJar")]
		void Stop ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioRecord", DoNotGenerateAcw=true)]
	internal class ICarAudioRecordInvoker : global::Java.Lang.Object, ICarAudioRecord {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarAudioRecord");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarAudioRecordInvoker); }
		}

		IntPtr class_ref;

		public static ICarAudioRecord GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarAudioRecord> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarAudioRecord"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarAudioRecordInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getBufferSize;
#pragma warning disable 0169
		static Delegate GetGetBufferSizeHandler ()
		{
			if (cb_getBufferSize == null)
				cb_getBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetBufferSize);
			return cb_getBufferSize;
		}

		static int n_GetBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.BufferSize;
		}
#pragma warning restore 0169

		IntPtr id_getBufferSize;
		public unsafe int BufferSize {
			get {
				if (id_getBufferSize == IntPtr.Zero)
					id_getBufferSize = JNIEnv.GetMethodID (class_ref, "getBufferSize", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getBufferSize);
			}
		}

		static Delegate cb_getConfigurationIndex;
#pragma warning disable 0169
		static Delegate GetGetConfigurationIndexHandler ()
		{
			if (cb_getConfigurationIndex == null)
				cb_getConfigurationIndex = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetConfigurationIndex);
			return cb_getConfigurationIndex;
		}

		static int n_GetConfigurationIndex (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ConfigurationIndex;
		}
#pragma warning restore 0169

		IntPtr id_getConfigurationIndex;
		public unsafe int ConfigurationIndex {
			get {
				if (id_getConfigurationIndex == IntPtr.Zero)
					id_getConfigurationIndex = JNIEnv.GetMethodID (class_ref, "getConfigurationIndex", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getConfigurationIndex);
			}
		}

		static Delegate cb_getRecordingState;
#pragma warning disable 0169
		static Delegate GetGetRecordingStateHandler ()
		{
			if (cb_getRecordingState == null)
				cb_getRecordingState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetRecordingState);
			return cb_getRecordingState;
		}

		static int n_GetRecordingState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.RecordingState;
		}
#pragma warning restore 0169

		IntPtr id_getRecordingState;
		public unsafe int RecordingState {
			get {
				if (id_getRecordingState == IntPtr.Zero)
					id_getRecordingState = JNIEnv.GetMethodID (class_ref, "getRecordingState", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getRecordingState);
			}
		}

		static Delegate cb_getState;
#pragma warning disable 0169
		static Delegate GetGetStateHandler ()
		{
			if (cb_getState == null)
				cb_getState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetState);
			return cb_getState;
		}

		static int n_GetState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.State;
		}
#pragma warning restore 0169

		IntPtr id_getState;
		public unsafe int State {
			get {
				if (id_getState == IntPtr.Zero)
					id_getState = JNIEnv.GetMethodID (class_ref, "getState", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getState);
			}
		}

		static Delegate cb_getStreamType;
#pragma warning disable 0169
		static Delegate GetGetStreamTypeHandler ()
		{
			if (cb_getStreamType == null)
				cb_getStreamType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetStreamType);
			return cb_getStreamType;
		}

		static int n_GetStreamType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.StreamType;
		}
#pragma warning restore 0169

		IntPtr id_getStreamType;
		public unsafe int StreamType {
			get {
				if (id_getStreamType == IntPtr.Zero)
					id_getStreamType = JNIEnv.GetMethodID (class_ref, "getStreamType", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getStreamType);
			}
		}

		static Delegate cb_read_arrayBII;
#pragma warning disable 0169
		static Delegate GetRead_arrayBIIHandler ()
		{
			if (cb_read_arrayBII == null)
				cb_read_arrayBII = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, int>) n_Read_arrayBII);
			return cb_read_arrayBII;
		}

		static int n_Read_arrayBII (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			int __ret = __this.Read (p0, p1, p2);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_read_arrayBII;
		public unsafe int Read (byte[] p0, int p1, int p2)
		{
			if (id_read_arrayBII == IntPtr.Zero)
				id_read_arrayBII = JNIEnv.GetMethodID (class_ref, "read", "([BII)I");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_read_arrayBII, __args);
			if (p0 != null) {
				JNIEnv.CopyArray (native_p0, p0);
				JNIEnv.DeleteLocalRef (native_p0);
			}
			return __ret;
		}

		static Delegate cb_release;
#pragma warning disable 0169
		static Delegate GetReleaseHandler ()
		{
			if (cb_release == null)
				cb_release = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Release);
			return cb_release;
		}

		static void n_Release (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Release ();
		}
#pragma warning restore 0169

		IntPtr id_release;
		public unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
		}

		static Delegate cb_startRecording;
#pragma warning disable 0169
		static Delegate GetStartRecordingHandler ()
		{
			if (cb_startRecording == null)
				cb_startRecording = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartRecording);
			return cb_startRecording;
		}

		static void n_StartRecording (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartRecording ();
		}
#pragma warning restore 0169

		IntPtr id_startRecording;
		public unsafe void StartRecording ()
		{
			if (id_startRecording == IntPtr.Zero)
				id_startRecording = JNIEnv.GetMethodID (class_ref, "startRecording", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startRecording);
		}

		static Delegate cb_stop;
#pragma warning disable 0169
		static Delegate GetStopHandler ()
		{
			if (cb_stop == null)
				cb_stop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Stop);
			return cb_stop;
		}

		static void n_Stop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioRecord __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Stop ();
		}
#pragma warning restore 0169

		IntPtr id_stop;
		public unsafe void Stop ()
		{
			if (id_stop == IntPtr.Zero)
				id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stop);
		}

	}

}
