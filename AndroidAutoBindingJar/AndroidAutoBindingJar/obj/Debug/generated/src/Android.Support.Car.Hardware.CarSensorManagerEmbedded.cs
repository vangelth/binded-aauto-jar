using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Hardware {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorManagerEmbedded", DoNotGenerateAcw=true)]
	public partial class CarSensorManagerEmbedded : global::Android.Support.Car.Hardware.CarSensorManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/hardware/CarSensorManagerEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorManagerEmbedded); }
		}

		protected CarSensorManagerEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Ljava_lang_Object_Landroid_content_Context_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/constructor[@name='CarSensorManagerEmbedded' and count(parameter)=2 and parameter[1][@type='java.lang.Object'] and parameter[2][@type='android.content.Context']]"
		[Register (".ctor", "(Ljava/lang/Object;Landroid/content/Context;)V", "")]
		public unsafe CarSensorManagerEmbedded (global::Java.Lang.Object p0, global::Android.Content.Context p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (CarSensorManagerEmbedded)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Ljava/lang/Object;Landroid/content/Context;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Ljava/lang/Object;Landroid/content/Context;)V", __args);
					return;
				}

				if (id_ctor_Ljava_lang_Object_Landroid_content_Context_ == IntPtr.Zero)
					id_ctor_Ljava_lang_Object_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/lang/Object;Landroid/content/Context;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_lang_Object_Landroid_content_Context_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Ljava_lang_Object_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static Delegate cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
#pragma warning disable 0169
		static Delegate GetAddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IIHandler ()
		{
			if (cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II == null)
				cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, bool>) n_AddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II);
			return cb_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
		}

		static bool n_AddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.AddListener (p0, p1, p2);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='addListener' and count(parameter)=3 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z", "GetAddListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IIHandler")]
		public override unsafe bool AddListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1, int p2)
		{
			if (id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II == IntPtr.Zero)
				id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				bool __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_getLatestSensorEvent_I;
#pragma warning disable 0169
		static Delegate GetGetLatestSensorEvent_IHandler ()
		{
			if (cb_getLatestSensorEvent_I == null)
				cb_getLatestSensorEvent_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetLatestSensorEvent_I);
			return cb_getLatestSensorEvent_I;
		}

		static IntPtr n_GetLatestSensorEvent_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLatestSensorEvent (p0));
		}
#pragma warning restore 0169

		static IntPtr id_getLatestSensorEvent_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='getLatestSensorEvent' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;", "GetGetLatestSensorEvent_IHandler")]
		public override unsafe global::Android.Support.Car.Hardware.CarSensorEvent GetLatestSensorEvent (int p0)
		{
			if (id_getLatestSensorEvent_I == IntPtr.Zero)
				id_getLatestSensorEvent_I = JNIEnv.GetMethodID (class_ref, "getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLatestSensorEvent_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getSensorConfig_I;
#pragma warning disable 0169
		static Delegate GetGetSensorConfig_IHandler ()
		{
			if (cb_getSensorConfig_I == null)
				cb_getSensorConfig_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetSensorConfig_I);
			return cb_getSensorConfig_I;
		}

		static IntPtr n_GetSensorConfig_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetSensorConfig (p0));
		}
#pragma warning restore 0169

		static IntPtr id_getSensorConfig_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='getSensorConfig' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;", "GetGetSensorConfig_IHandler")]
		public override unsafe global::Android.Support.Car.Hardware.CarSensorConfig GetSensorConfig (int p0)
		{
			if (id_getSensorConfig_I == IntPtr.Zero)
				id_getSensorConfig_I = JNIEnv.GetMethodID (class_ref, "getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSensorConfig_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getSupportedSensors;
#pragma warning disable 0169
		static Delegate GetGetSupportedSensorsHandler ()
		{
			if (cb_getSupportedSensors == null)
				cb_getSupportedSensors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSupportedSensors);
			return cb_getSupportedSensors;
		}

		static IntPtr n_GetSupportedSensors (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetSupportedSensors ());
		}
#pragma warning restore 0169

		static IntPtr id_getSupportedSensors;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='getSupportedSensors' and count(parameter)=0]"
		[Register ("getSupportedSensors", "()[I", "GetGetSupportedSensorsHandler")]
		public override unsafe int[] GetSupportedSensors ()
		{
			if (id_getSupportedSensors == IntPtr.Zero)
				id_getSupportedSensors = JNIEnv.GetMethodID (class_ref, "getSupportedSensors", "()[I");
			try {

				if (((object) this).GetType () == ThresholdType)
					return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSupportedSensors), JniHandleOwnership.TransferLocalRef, typeof (int));
				else
					return (int[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSupportedSensors", "()[I")), JniHandleOwnership.TransferLocalRef, typeof (int));
			} finally {
			}
		}

		static Delegate cb_isSensorSupported_I;
#pragma warning disable 0169
		static Delegate GetIsSensorSupported_IHandler ()
		{
			if (cb_isSensorSupported_I == null)
				cb_isSensorSupported_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_IsSensorSupported_I);
			return cb_isSensorSupported_I;
		}

		static bool n_IsSensorSupported_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsSensorSupported (p0);
		}
#pragma warning restore 0169

		static IntPtr id_isSensorSupported_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='isSensorSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isSensorSupported", "(I)Z", "GetIsSensorSupported_IHandler")]
		public override unsafe bool IsSensorSupported (int p0)
		{
			if (id_isSensorSupported_I == IntPtr.Zero)
				id_isSensorSupported_I = JNIEnv.GetMethodID (class_ref, "isSensorSupported", "(I)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isSensorSupported_I, __args);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isSensorSupported", "(I)Z"), __args);
			} finally {
			}
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCarDisconnected", "()V"));
			} finally {
			}
		}

		static Delegate cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_Handler ()
		{
			if (cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ == null)
				cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_);
			return cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
		}

		static void n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_Handler")]
		public override unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IHandler ()
		{
			if (cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I == null)
				cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I);
			return cb_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
		}

		static void n_RemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Android.Support.Car.Hardware.CarSensorManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0 = (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorManagerEmbedded']/method[@name='removeListener' and count(parameter)=2 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V", "GetRemoveListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_IHandler")]
		public override unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V"), __args);
			} finally {
			}
		}

	}
}
