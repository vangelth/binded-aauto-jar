using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A.B {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/b/a", DoNotGenerateAcw=true)]
	public sealed partial class A : global::Android.Support.Car.Navigation.CarNavigationStatusManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/b/a", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (A); }
		}

		internal A (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarNavigationStatusManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/constructor[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarNavigationStatusManager']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarNavigationStatusManager;)V", "")]
		public unsafe A (global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (A)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarNavigationStatusManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarNavigationStatusManager;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarNavigationStatusManager_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarNavigationStatusManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarNavigationStatusManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarNavigationStatusManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarNavigationStatusManager_, __args);
			} finally {
			}
		}

		static IntPtr id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager.CarNavigationCallback']]"
		[Register ("addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V", "")]
		public override sealed unsafe void AddListener (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0)
		{
			if (id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ == IntPtr.Zero)
				id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_, __args);
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "")]
		public override sealed unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

		static IntPtr id_removeListener;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='removeListener' and count(parameter)=0]"
		[Register ("removeListener", "()V", "")]
		public override sealed unsafe void RemoveListener ()
		{
			if (id_removeListener == IntPtr.Zero)
				id_removeListener = JNIEnv.GetMethodID (class_ref, "removeListener", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener);
			} finally {
			}
		}

		static IntPtr id_sendEvent_ILandroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='sendEvent' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Bundle']]"
		[Register ("sendEvent", "(ILandroid/os/Bundle;)V", "")]
		public override sealed unsafe void SendEvent (int p0, global::Android.OS.Bundle p1)
		{
			if (id_sendEvent_ILandroid_os_Bundle_ == IntPtr.Zero)
				id_sendEvent_ILandroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "sendEvent", "(ILandroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendEvent_ILandroid_os_Bundle_, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationStatus_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='sendNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("sendNavigationStatus", "(I)V", "")]
		public override sealed unsafe void SendNavigationStatus (int p0)
		{
			if (id_sendNavigationStatus_I == IntPtr.Zero)
				id_sendNavigationStatus_I = JNIEnv.GetMethodID (class_ref, "sendNavigationStatus", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationStatus_I, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationTurnDistanceEvent_IIII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='sendNavigationTurnDistanceEvent' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("sendNavigationTurnDistanceEvent", "(IIII)V", "")]
		public override sealed unsafe void SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3)
		{
			if (id_sendNavigationTurnDistanceEvent_IIII == IntPtr.Zero)
				id_sendNavigationTurnDistanceEvent_IIII = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnDistanceEvent", "(IIII)V");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnDistanceEvent_IIII, __args);
			} finally {
			}
		}

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='sendNavigationTurnEvent' and count(parameter)=6 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='android.graphics.Bitmap'] and parameter[6][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V", "")]
		public override sealed unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, global::Android.Graphics.Bitmap p4, int p5)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [6];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				__args [5] = new JValue (p5);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.b']/class[@name='a']/method[@name='sendNavigationTurnEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V", "")]
		public override sealed unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, int p4)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

	}
}
