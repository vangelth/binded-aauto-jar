using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager.CarVendorExtensionListener']"
	[Register ("com/google/android/gms/car/CarVendorExtensionManager$CarVendorExtensionListener", "", "Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListenerInvoker")]
	public partial interface ICarVendorExtensionManagerCarVendorExtensionListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager.CarVendorExtensionListener']/method[@name='onData' and count(parameter)=1 and parameter[1][@type='byte[]']]"
		[Register ("onData", "([B)V", "GetOnData_arrayBHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListenerInvoker, AndroidAutoBindingJar")]
		void OnData (byte[] p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarVendorExtensionManager$CarVendorExtensionListener", DoNotGenerateAcw=true)]
	internal class ICarVendorExtensionManagerCarVendorExtensionListenerInvoker : global::Java.Lang.Object, ICarVendorExtensionManagerCarVendorExtensionListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarVendorExtensionManager$CarVendorExtensionListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarVendorExtensionManagerCarVendorExtensionListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarVendorExtensionManagerCarVendorExtensionListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarVendorExtensionManagerCarVendorExtensionListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarVendorExtensionManager.CarVendorExtensionListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarVendorExtensionManagerCarVendorExtensionListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onData_arrayB;
#pragma warning disable 0169
		static Delegate GetOnData_arrayBHandler ()
		{
			if (cb_onData_arrayB == null)
				cb_onData_arrayB = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnData_arrayB);
			return cb_onData_arrayB;
		}

		static void n_OnData_arrayB (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			__this.OnData (p0);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
		}
#pragma warning restore 0169

		IntPtr id_onData_arrayB;
		public unsafe void OnData (byte[] p0)
		{
			if (id_onData_arrayB == IntPtr.Zero)
				id_onData_arrayB = JNIEnv.GetMethodID (class_ref, "onData", "([B)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onData_arrayB, __args);
			if (p0 != null) {
				JNIEnv.CopyArray (native_p0, p0);
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}

	public partial class CarVendorExtensionManagerCarVendorExtensionEventArgs : global::System.EventArgs {

		public CarVendorExtensionManagerCarVendorExtensionEventArgs (byte[] p0)
		{
			this.p0 = p0;
		}

		byte[] p0;
		public byte[] P0 {
			get { return p0; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarVendorExtensionManager_CarVendorExtensionListenerImplementor")]
	internal sealed partial class ICarVendorExtensionManagerCarVendorExtensionListenerImplementor : global::Java.Lang.Object, ICarVendorExtensionManagerCarVendorExtensionListener {

		object sender;

		public ICarVendorExtensionManagerCarVendorExtensionListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarVendorExtensionManager_CarVendorExtensionListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<CarVendorExtensionManagerCarVendorExtensionEventArgs> Handler;
#pragma warning restore 0649

		public void OnData (byte[] p0)
		{
			var __h = Handler;
			if (__h != null)
				__h (sender, new CarVendorExtensionManagerCarVendorExtensionEventArgs (p0));
		}

		internal static bool __IsEmpty (ICarVendorExtensionManagerCarVendorExtensionListenerImplementor value)
		{
			return value.Handler == null;
		}
	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']"
	[Register ("com/google/android/gms/car/CarVendorExtensionManager", "", "Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker")]
	public partial interface ICarVendorExtensionManager : IJavaObject {

		string ServiceName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='getServiceName' and count(parameter)=0]"
			[Register ("getServiceName", "()Ljava/lang/String;", "GetGetServiceNameHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='getServiceData' and count(parameter)=0]"
		[Register ("getServiceData", "()[B", "GetGetServiceDataHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		byte[] GetServiceData ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='registerListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarVendorExtensionManager.CarVendorExtensionListener']]"
		[Register ("registerListener", "(Lcom/google/android/gms/car/CarVendorExtensionManager$CarVendorExtensionListener;)V", "GetRegisterListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_Handler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		void RegisterListener (global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "GetReleaseHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		void Release ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='sendData' and count(parameter)=1 and parameter[1][@type='byte[]']]"
		[Register ("sendData", "([B)V", "GetSendData_arrayBHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		void SendData (byte[] p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='sendData' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("sendData", "([BII)V", "GetSendData_arrayBIIHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		void SendData (byte[] p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarVendorExtensionManager']/method[@name='unregisterListener' and count(parameter)=0]"
		[Register ("unregisterListener", "()V", "GetUnregisterListenerHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterListener ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarVendorExtensionManager", DoNotGenerateAcw=true)]
	internal class ICarVendorExtensionManagerInvoker : global::Java.Lang.Object, ICarVendorExtensionManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarVendorExtensionManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarVendorExtensionManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarVendorExtensionManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarVendorExtensionManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarVendorExtensionManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarVendorExtensionManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getServiceName;
#pragma warning disable 0169
		static Delegate GetGetServiceNameHandler ()
		{
			if (cb_getServiceName == null)
				cb_getServiceName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetServiceName);
			return cb_getServiceName;
		}

		static IntPtr n_GetServiceName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ServiceName);
		}
#pragma warning restore 0169

		IntPtr id_getServiceName;
		public unsafe string ServiceName {
			get {
				if (id_getServiceName == IntPtr.Zero)
					id_getServiceName = JNIEnv.GetMethodID (class_ref, "getServiceName", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getServiceName), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getServiceData;
#pragma warning disable 0169
		static Delegate GetGetServiceDataHandler ()
		{
			if (cb_getServiceData == null)
				cb_getServiceData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetServiceData);
			return cb_getServiceData;
		}

		static IntPtr n_GetServiceData (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetServiceData ());
		}
#pragma warning restore 0169

		IntPtr id_getServiceData;
		public unsafe byte[] GetServiceData ()
		{
			if (id_getServiceData == IntPtr.Zero)
				id_getServiceData = JNIEnv.GetMethodID (class_ref, "getServiceData", "()[B");
			return (byte[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getServiceData), JniHandleOwnership.TransferLocalRef, typeof (byte));
		}

		static Delegate cb_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_;
#pragma warning disable 0169
		static Delegate GetRegisterListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_Handler ()
		{
			if (cb_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_ == null)
				cb_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RegisterListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_);
			return cb_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_;
		}

		static void n_RegisterListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener p0 = (global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegisterListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_;
		public unsafe void RegisterListener (global::Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListener p0)
		{
			if (id_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_ == IntPtr.Zero)
				id_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_ = JNIEnv.GetMethodID (class_ref, "registerListener", "(Lcom/google/android/gms/car/CarVendorExtensionManager$CarVendorExtensionListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerListener_Lcom_google_android_gms_car_CarVendorExtensionManager_CarVendorExtensionListener_, __args);
		}

		static Delegate cb_release;
#pragma warning disable 0169
		static Delegate GetReleaseHandler ()
		{
			if (cb_release == null)
				cb_release = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Release);
			return cb_release;
		}

		static void n_Release (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Release ();
		}
#pragma warning restore 0169

		IntPtr id_release;
		public unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
		}

		static Delegate cb_sendData_arrayB;
#pragma warning disable 0169
		static Delegate GetSendData_arrayBHandler ()
		{
			if (cb_sendData_arrayB == null)
				cb_sendData_arrayB = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SendData_arrayB);
			return cb_sendData_arrayB;
		}

		static void n_SendData_arrayB (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			__this.SendData (p0);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
		}
#pragma warning restore 0169

		IntPtr id_sendData_arrayB;
		public unsafe void SendData (byte[] p0)
		{
			if (id_sendData_arrayB == IntPtr.Zero)
				id_sendData_arrayB = JNIEnv.GetMethodID (class_ref, "sendData", "([B)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendData_arrayB, __args);
			if (p0 != null) {
				JNIEnv.CopyArray (native_p0, p0);
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_sendData_arrayBII;
#pragma warning disable 0169
		static Delegate GetSendData_arrayBIIHandler ()
		{
			if (cb_sendData_arrayBII == null)
				cb_sendData_arrayBII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int, int>) n_SendData_arrayBII);
			return cb_sendData_arrayBII;
		}

		static void n_SendData_arrayBII (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			__this.SendData (p0, p1, p2);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
		}
#pragma warning restore 0169

		IntPtr id_sendData_arrayBII;
		public unsafe void SendData (byte[] p0, int p1, int p2)
		{
			if (id_sendData_arrayBII == IntPtr.Zero)
				id_sendData_arrayBII = JNIEnv.GetMethodID (class_ref, "sendData", "([BII)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendData_arrayBII, __args);
			if (p0 != null) {
				JNIEnv.CopyArray (native_p0, p0);
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_unregisterListener;
#pragma warning disable 0169
		static Delegate GetUnregisterListenerHandler ()
		{
			if (cb_unregisterListener == null)
				cb_unregisterListener = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_UnregisterListener);
			return cb_unregisterListener;
		}

		static void n_UnregisterListener (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterListener ();
		}
#pragma warning restore 0169

		IntPtr id_unregisterListener;
		public unsafe void UnregisterListener ()
		{
			if (id_unregisterListener == IntPtr.Zero)
				id_unregisterListener = JNIEnv.GetMethodID (class_ref, "unregisterListener", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterListener);
		}

	}

}
