using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']"
	[Register ("android/support/car/CarManagerBase", "", "Android.Support.Car.ICarManagerBaseInvoker")]
	public partial interface ICarManagerBase : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/interface[@name='CarManagerBase']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler:Android.Support.Car.ICarManagerBaseInvoker, AndroidAutoBindingJar")]
		void OnCarDisconnected ();

	}

	[global::Android.Runtime.Register ("android/support/car/CarManagerBase", DoNotGenerateAcw=true)]
	internal class ICarManagerBaseInvoker : global::Java.Lang.Object, ICarManagerBase {

		static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/CarManagerBase");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarManagerBaseInvoker); }
		}

		IntPtr class_ref;

		public static ICarManagerBase GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarManagerBase> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "android.support.car.CarManagerBase"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarManagerBaseInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.ICarManagerBase __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.ICarManagerBase> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		IntPtr id_onCarDisconnected;
		public unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
		}

	}

}
