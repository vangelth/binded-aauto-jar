using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/f", DoNotGenerateAcw=true)]
	public sealed partial class F : global::Android.Support.Car.Hardware.CarSensorManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/f", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (F); }
		}

		internal F (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarSensorManager_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/constructor[@name='f' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarSensorManager']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarSensorManager;)V", "")]
		public unsafe F (global::Com.Google.Android.Gms.Car.ICarSensorManager p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (F)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarSensorManager;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarSensorManager;)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarSensorManager_ == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarSensorManager_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarSensorManager;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarSensorManager_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarSensorManager_, __args);
			} finally {
			}
		}

		static IntPtr id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='addListener' and count(parameter)=3 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z", "")]
		public override sealed unsafe bool AddListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1, int p2)
		{
			if (id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II == IntPtr.Zero)
				id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;II)Z");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_II, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_getLatestSensorEvent_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='getLatestSensorEvent' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;", "")]
		public override sealed unsafe global::Android.Support.Car.Hardware.CarSensorEvent GetLatestSensorEvent (int p0)
		{
			if (id_getLatestSensorEvent_I == IntPtr.Zero)
				id_getLatestSensorEvent_I = JNIEnv.GetMethodID (class_ref, "getLatestSensorEvent", "(I)Landroid/support/car/hardware/CarSensorEvent;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLatestSensorEvent_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getSensorConfig_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='getSensorConfig' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;", "")]
		public override sealed unsafe global::Android.Support.Car.Hardware.CarSensorConfig GetSensorConfig (int p0)
		{
			if (id_getSensorConfig_I == IntPtr.Zero)
				id_getSensorConfig_I = JNIEnv.GetMethodID (class_ref, "getSensorConfig", "(I)Landroid/support/car/hardware/CarSensorConfig;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSensorConfig_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_getSupportedSensors;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='getSupportedSensors' and count(parameter)=0]"
		[Register ("getSupportedSensors", "()[I", "")]
		public override sealed unsafe int[] GetSupportedSensors ()
		{
			if (id_getSupportedSensors == IntPtr.Zero)
				id_getSupportedSensors = JNIEnv.GetMethodID (class_ref, "getSupportedSensors", "()[I");
			try {
				return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSupportedSensors), JniHandleOwnership.TransferLocalRef, typeof (int));
			} finally {
			}
		}

		static IntPtr id_isSensorSupported_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='isSensorSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isSensorSupported", "(I)Z", "")]
		public override sealed unsafe bool IsSensorSupported (int p0)
		{
			if (id_isSensorSupported_I == IntPtr.Zero)
				id_isSensorSupported_I = JNIEnv.GetMethodID (class_ref, "isSensorSupported", "(I)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isSensorSupported_I, __args);
			} finally {
			}
		}

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "")]
		public override sealed unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
			} finally {
			}
		}

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V", "")]
		public override sealed unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_, __args);
			} finally {
			}
		}

		static IntPtr id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a']/class[@name='f']/method[@name='removeListener' and count(parameter)=2 and parameter[1][@type='android.support.car.hardware.CarSensorManager.OnSensorChangedListener'] and parameter[2][@type='int']]"
		[Register ("removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V", "")]
		public override sealed unsafe void RemoveListener (global::Android.Support.Car.Hardware.CarSensorManager.IOnSensorChangedListener p0, int p1)
		{
			if (id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I == IntPtr.Zero)
				id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I = JNIEnv.GetMethodID (class_ref, "removeListener", "(Landroid/support/car/hardware/CarSensorManager$OnSensorChangedListener;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener_Landroid_support_car_hardware_CarSensorManager_OnSensorChangedListener_I, __args);
			} finally {
			}
		}

	}
}
