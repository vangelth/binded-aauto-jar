using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.State {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary", DoNotGenerateAcw=true)]
	public partial class NavigationSummary : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder", DoNotGenerateAcw=true)]
		public partial class Builder : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Builder); }
			}

			protected Builder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']/constructor[@name='NavigationSummary.Builder' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe Builder ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_build;
#pragma warning disable 0169
			static Delegate GetBuildHandler ()
			{
				if (cb_build == null)
					cb_build = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Build);
				return cb_build;
			}

			static IntPtr n_Build (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.Build ());
			}
#pragma warning restore 0169

			static IntPtr id_build;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']/method[@name='build' and count(parameter)=0]"
			[Register ("build", "()Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary;", "GetBuildHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary Build ()
			{
				if (id_build == IntPtr.Zero)
					id_build = JNIEnv.GetMethodID (class_ref, "build", "()Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_build), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "build", "()Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setFormattedDestinationEta_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetFormattedDestinationEta_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setFormattedDestinationEta_Ljava_lang_CharSequence_ == null)
					cb_setFormattedDestinationEta_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetFormattedDestinationEta_Ljava_lang_CharSequence_);
				return cb_setFormattedDestinationEta_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetFormattedDestinationEta_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetFormattedDestinationEta (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setFormattedDestinationEta_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']/method[@name='setFormattedDestinationEta' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setFormattedDestinationEta", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;", "GetSetFormattedDestinationEta_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder SetFormattedDestinationEta (global::Java.Lang.ICharSequence p0)
			{
				if (id_setFormattedDestinationEta_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setFormattedDestinationEta_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setFormattedDestinationEta", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setFormattedDestinationEta_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFormattedDestinationEta", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder SetFormattedDestinationEta (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __result = SetFormattedDestinationEta (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setNavigationStatus_I;
#pragma warning disable 0169
			static Delegate GetSetNavigationStatus_IHandler ()
			{
				if (cb_setNavigationStatus_I == null)
					cb_setNavigationStatus_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetNavigationStatus_I);
				return cb_setNavigationStatus_I;
			}

			static IntPtr n_SetNavigationStatus_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetNavigationStatus (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setNavigationStatus_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']/method[@name='setNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setNavigationStatus", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;", "GetSetNavigationStatus_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder SetNavigationStatus (int p0)
			{
				if (id_setNavigationStatus_I == IntPtr.Zero)
					id_setNavigationStatus_I = JNIEnv.GetMethodID (class_ref, "setNavigationStatus", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setNavigationStatus_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setNavigationStatus", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setSecondsToDestination_I;
#pragma warning disable 0169
			static Delegate GetSetSecondsToDestination_IHandler ()
			{
				if (cb_setSecondsToDestination_I == null)
					cb_setSecondsToDestination_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetSecondsToDestination_I);
				return cb_setSecondsToDestination_I;
			}

			static IntPtr n_SetSecondsToDestination_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetSecondsToDestination (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setSecondsToDestination_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary.Builder']/method[@name='setSecondsToDestination' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;", "GetSetSecondsToDestination_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder SetSecondsToDestination (int p0)
			{
				if (id_setSecondsToDestination_I == IntPtr.Zero)
					id_setSecondsToDestination_I = JNIEnv.GetMethodID (class_ref, "setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setSecondsToDestination_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/state/NavigationSummary$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$NavigationStatus", DoNotGenerateAcw=true)]
		public abstract class NavigationStatus : Java.Lang.Object {

			internal NavigationStatus ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']/field[@name='NAVIGATING']"
			[Register ("NAVIGATING")]
			public const int Navigating = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']/field[@name='PAUSING']"
			[Register ("PAUSING")]
			public const int Pausing = (int) 4;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']/field[@name='RESUMING']"
			[Register ("RESUMING")]
			public const int Resuming = (int) 3;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']/field[@name='STOPPED']"
			[Register ("STOPPED")]
			public const int Stopped = (int) 2;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']/field[@name='UNKNOWN']"
			[Register ("UNKNOWN")]
			public const int Unknown = (int) 0;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$NavigationStatus", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'NavigationStatus' type. This type will be removed in a future release.")]
		public abstract class NavigationStatusConsts : NavigationStatus {

			private NavigationStatusConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/interface[@name='NavigationSummary.NavigationStatus']"
		[Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$NavigationStatus", "", "Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary/INavigationStatusInvoker")]
		public partial interface INavigationStatus : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$NavigationStatus", DoNotGenerateAcw=true)]
		internal class INavigationStatusInvoker : global::Java.Lang.Object, INavigationStatus {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary$NavigationStatus");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (INavigationStatusInvoker); }
			}

			IntPtr class_ref;

			public static INavigationStatus GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<INavigationStatus> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.nav.state.NavigationSummary.NavigationStatus"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public INavigationStatusInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary.INavigationStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/state/NavigationSummary", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (NavigationSummary); }
		}

		protected NavigationSummary (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/constructor[@name='NavigationSummary' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe NavigationSummary ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (NavigationSummary)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getFormattedDestinationEta;
#pragma warning disable 0169
		static Delegate GetGetFormattedDestinationEtaHandler ()
		{
			if (cb_getFormattedDestinationEta == null)
				cb_getFormattedDestinationEta = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFormattedDestinationEta);
			return cb_getFormattedDestinationEta;
		}

		static IntPtr n_GetFormattedDestinationEta (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.FormattedDestinationEtaFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getFormattedDestinationEta;
		public virtual unsafe global::Java.Lang.ICharSequence FormattedDestinationEtaFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/method[@name='getFormattedDestinationEta' and count(parameter)=0]"
			[Register ("getFormattedDestinationEta", "()Ljava/lang/CharSequence;", "GetGetFormattedDestinationEtaHandler")]
			get {
				if (id_getFormattedDestinationEta == IntPtr.Zero)
					id_getFormattedDestinationEta = JNIEnv.GetMethodID (class_ref, "getFormattedDestinationEta", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getFormattedDestinationEta), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFormattedDestinationEta", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string FormattedDestinationEta {
			get { return FormattedDestinationEtaFormatted == null ? null : FormattedDestinationEtaFormatted.ToString (); }
		}

		static Delegate cb_hasSecondsToDestination;
#pragma warning disable 0169
		static Delegate GetHasSecondsToDestinationHandler ()
		{
			if (cb_hasSecondsToDestination == null)
				cb_hasSecondsToDestination = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasSecondsToDestination);
			return cb_hasSecondsToDestination;
		}

		static bool n_HasSecondsToDestination (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasSecondsToDestination;
		}
#pragma warning restore 0169

		static IntPtr id_hasSecondsToDestination;
		public virtual unsafe bool HasSecondsToDestination {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/method[@name='hasSecondsToDestination' and count(parameter)=0]"
			[Register ("hasSecondsToDestination", "()Z", "GetHasSecondsToDestinationHandler")]
			get {
				if (id_hasSecondsToDestination == IntPtr.Zero)
					id_hasSecondsToDestination = JNIEnv.GetMethodID (class_ref, "hasSecondsToDestination", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasSecondsToDestination);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasSecondsToDestination", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getSecondsToDestination;
#pragma warning disable 0169
		static Delegate GetGetSecondsToDestinationHandler ()
		{
			if (cb_getSecondsToDestination == null)
				cb_getSecondsToDestination = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetSecondsToDestination);
			return cb_getSecondsToDestination;
		}

		static int n_GetSecondsToDestination (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SecondsToDestination;
		}
#pragma warning restore 0169

		static IntPtr id_getSecondsToDestination;
		public virtual unsafe int SecondsToDestination {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/method[@name='getSecondsToDestination' and count(parameter)=0]"
			[Register ("getSecondsToDestination", "()I", "GetGetSecondsToDestinationHandler")]
			get {
				if (id_getSecondsToDestination == IntPtr.Zero)
					id_getSecondsToDestination = JNIEnv.GetMethodID (class_ref, "getSecondsToDestination", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getSecondsToDestination);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSecondsToDestination", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.State.NavigationSummary> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.state']/class[@name='NavigationSummary']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
