using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivity']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/CarActivity", DoNotGenerateAcw=true)]
	public partial class CarActivity : global::Com.Google.Android.Gms.Car.E {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/CarActivity", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarActivity); }
		}

		protected CarActivity (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivity']/constructor[@name='CarActivity' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarActivity ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarActivity)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_startCarActivity_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetStartCarActivity_Landroid_content_Intent_Handler ()
		{
			if (cb_startCarActivity_Landroid_content_Intent_ == null)
				cb_startCarActivity_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartCarActivity_Landroid_content_Intent_);
			return cb_startCarActivity_Landroid_content_Intent_;
		}

		static void n_StartCarActivity_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.CarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.CarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartCarActivity (p0);
		}
#pragma warning restore 0169

		static IntPtr id_startCarActivity_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivity']/method[@name='startCarActivity' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("startCarActivity", "(Landroid/content/Intent;)V", "GetStartCarActivity_Landroid_content_Intent_Handler")]
		public virtual unsafe void StartCarActivity (global::Android.Content.Intent p0)
		{
			if (id_startCarActivity_Landroid_content_Intent_ == IntPtr.Zero)
				id_startCarActivity_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "startCarActivity", "(Landroid/content/Intent;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startCarActivity_Landroid_content_Intent_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startCarActivity", "(Landroid/content/Intent;)V"), __args);
			} finally {
			}
		}

	}
}
