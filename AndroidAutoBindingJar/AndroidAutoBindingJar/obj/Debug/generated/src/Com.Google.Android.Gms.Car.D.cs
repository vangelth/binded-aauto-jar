using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/d", DoNotGenerateAcw=true)]
	public abstract partial class D : global::Android.App.Service, global::Com.Google.Android.Gms.Car.ICarActivityServiceProxyServiceCallbacks {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/d", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (D); }
		}

		protected D (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']/constructor[@name='d' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe D ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (D)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCarActivity;
#pragma warning disable 0169
		static Delegate GetGetCarActivityHandler ()
		{
			if (cb_getCarActivity == null)
				cb_getCarActivity = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarActivity);
			return cb_getCarActivity;
		}

		static IntPtr n_GetCarActivity (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CarActivity);
		}
#pragma warning restore 0169

		public abstract global::Java.Lang.Class CarActivity {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']/method[@name='getCarActivity' and count(parameter)=0]"
			[Register ("getCarActivity", "()Ljava/lang/Class;", "GetGetCarActivityHandler")] get;
		}

		static Delegate cb_getHandledConfigChanges;
#pragma warning disable 0169
		static Delegate GetGetHandledConfigChangesHandler ()
		{
			if (cb_getHandledConfigChanges == null)
				cb_getHandledConfigChanges = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHandledConfigChanges);
			return cb_getHandledConfigChanges;
		}

		static int n_GetHandledConfigChanges (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HandledConfigChanges;
		}
#pragma warning restore 0169

		static IntPtr id_getHandledConfigChanges;
		public virtual unsafe int HandledConfigChanges {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']/method[@name='getHandledConfigChanges' and count(parameter)=0]"
			[Register ("getHandledConfigChanges", "()I", "GetGetHandledConfigChangesHandler")]
			get {
				if (id_getHandledConfigChanges == IntPtr.Zero)
					id_getHandledConfigChanges = JNIEnv.GetMethodID (class_ref, "getHandledConfigChanges", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getHandledConfigChanges);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHandledConfigChanges", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_onBind_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnBind_Landroid_content_Intent_Handler ()
		{
			if (cb_onBind_Landroid_content_Intent_ == null)
				cb_onBind_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnBind_Landroid_content_Intent_);
			return cb_onBind_Landroid_content_Intent_;
		}

		static IntPtr n_OnBind_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.D __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.D> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.OnBind (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onBind_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']/method[@name='onBind' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;", "GetOnBind_Landroid_content_Intent_Handler")]
		public override unsafe global::Android.OS.IBinder OnBind (global::Android.Content.Intent p0)
		{
			if (id_onBind_Landroid_content_Intent_ == IntPtr.Zero)
				id_onBind_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Android.OS.IBinder __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onBind_Landroid_content_Intent_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/d", DoNotGenerateAcw=true)]
	internal partial class DInvoker : D {

		public DInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (DInvoker); }
		}

		static IntPtr id_getCarActivity;
		public override unsafe global::Java.Lang.Class CarActivity {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='d']/method[@name='getCarActivity' and count(parameter)=0]"
			[Register ("getCarActivity", "()Ljava/lang/Class;", "GetGetCarActivityHandler")]
			get {
				if (id_getCarActivity == IntPtr.Zero)
					id_getCarActivity = JNIEnv.GetMethodID (class_ref, "getCarActivity", "()Ljava/lang/Class;");
				try {
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarActivity), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}

}
