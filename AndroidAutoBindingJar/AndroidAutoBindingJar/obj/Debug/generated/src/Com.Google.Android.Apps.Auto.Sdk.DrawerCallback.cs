using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/DrawerCallback", DoNotGenerateAcw=true)]
	public partial class DrawerCallback : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/DrawerCallback", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (DrawerCallback); }
		}

		protected DrawerCallback (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']/constructor[@name='DrawerCallback' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe DrawerCallback ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (DrawerCallback)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onDrawerClosed;
#pragma warning disable 0169
		static Delegate GetOnDrawerClosedHandler ()
		{
			if (cb_onDrawerClosed == null)
				cb_onDrawerClosed = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDrawerClosed);
			return cb_onDrawerClosed;
		}

		static void n_OnDrawerClosed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDrawerClosed ();
		}
#pragma warning restore 0169

		static IntPtr id_onDrawerClosed;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']/method[@name='onDrawerClosed' and count(parameter)=0]"
		[Register ("onDrawerClosed", "()V", "GetOnDrawerClosedHandler")]
		public virtual unsafe void OnDrawerClosed ()
		{
			if (id_onDrawerClosed == IntPtr.Zero)
				id_onDrawerClosed = JNIEnv.GetMethodID (class_ref, "onDrawerClosed", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDrawerClosed);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDrawerClosed", "()V"));
			} finally {
			}
		}

		static Delegate cb_onDrawerClosing;
#pragma warning disable 0169
		static Delegate GetOnDrawerClosingHandler ()
		{
			if (cb_onDrawerClosing == null)
				cb_onDrawerClosing = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDrawerClosing);
			return cb_onDrawerClosing;
		}

		static void n_OnDrawerClosing (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDrawerClosing ();
		}
#pragma warning restore 0169

		static IntPtr id_onDrawerClosing;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']/method[@name='onDrawerClosing' and count(parameter)=0]"
		[Register ("onDrawerClosing", "()V", "GetOnDrawerClosingHandler")]
		public virtual unsafe void OnDrawerClosing ()
		{
			if (id_onDrawerClosing == IntPtr.Zero)
				id_onDrawerClosing = JNIEnv.GetMethodID (class_ref, "onDrawerClosing", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDrawerClosing);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDrawerClosing", "()V"));
			} finally {
			}
		}

		static Delegate cb_onDrawerOpened;
#pragma warning disable 0169
		static Delegate GetOnDrawerOpenedHandler ()
		{
			if (cb_onDrawerOpened == null)
				cb_onDrawerOpened = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDrawerOpened);
			return cb_onDrawerOpened;
		}

		static void n_OnDrawerOpened (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDrawerOpened ();
		}
#pragma warning restore 0169

		static IntPtr id_onDrawerOpened;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']/method[@name='onDrawerOpened' and count(parameter)=0]"
		[Register ("onDrawerOpened", "()V", "GetOnDrawerOpenedHandler")]
		public virtual unsafe void OnDrawerOpened ()
		{
			if (id_onDrawerOpened == IntPtr.Zero)
				id_onDrawerOpened = JNIEnv.GetMethodID (class_ref, "onDrawerOpened", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDrawerOpened);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDrawerOpened", "()V"));
			} finally {
			}
		}

		static Delegate cb_onDrawerOpening;
#pragma warning disable 0169
		static Delegate GetOnDrawerOpeningHandler ()
		{
			if (cb_onDrawerOpening == null)
				cb_onDrawerOpening = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDrawerOpening);
			return cb_onDrawerOpening;
		}

		static void n_OnDrawerOpening (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.DrawerCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDrawerOpening ();
		}
#pragma warning restore 0169

		static IntPtr id_onDrawerOpening;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='DrawerCallback']/method[@name='onDrawerOpening' and count(parameter)=0]"
		[Register ("onDrawerOpening", "()V", "GetOnDrawerOpeningHandler")]
		public virtual unsafe void OnDrawerOpening ()
		{
			if (id_onDrawerOpening == IntPtr.Zero)
				id_onDrawerOpening = JNIEnv.GetMethodID (class_ref, "onDrawerOpening", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDrawerOpening);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDrawerOpening", "()V"));
			} finally {
			}
		}

	}
}
