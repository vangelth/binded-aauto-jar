using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioTrack", DoNotGenerateAcw=true)]
	public abstract partial class CarAudioTrack : global::Java.Lang.Object {


		static IntPtr mAudioManager_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/field[@name='mAudioManager']"
		[Register ("mAudioManager")]
		protected global::Com.Google.Android.Gms.Car.ICarAudioManager MAudioManager {
			get {
				if (mAudioManager_jfieldId == IntPtr.Zero)
					mAudioManager_jfieldId = JNIEnv.GetFieldID (class_ref, "mAudioManager", "Lcom/google/android/gms/car/CarAudioManager;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mAudioManager_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mAudioManager_jfieldId == IntPtr.Zero)
					mAudioManager_jfieldId = JNIEnv.GetFieldID (class_ref, "mAudioManager", "Lcom/google/android/gms/car/CarAudioManager;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mAudioManager_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr mBufferSize_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/field[@name='mBufferSize']"
		[Register ("mBufferSize")]
		protected int MBufferSize {
			get {
				if (mBufferSize_jfieldId == IntPtr.Zero)
					mBufferSize_jfieldId = JNIEnv.GetFieldID (class_ref, "mBufferSize", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, mBufferSize_jfieldId);
			}
			set {
				if (mBufferSize_jfieldId == IntPtr.Zero)
					mBufferSize_jfieldId = JNIEnv.GetFieldID (class_ref, "mBufferSize", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mBufferSize_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr mConfigurationIndex_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/field[@name='mConfigurationIndex']"
		[Register ("mConfigurationIndex")]
		protected int MConfigurationIndex {
			get {
				if (mConfigurationIndex_jfieldId == IntPtr.Zero)
					mConfigurationIndex_jfieldId = JNIEnv.GetFieldID (class_ref, "mConfigurationIndex", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, mConfigurationIndex_jfieldId);
			}
			set {
				if (mConfigurationIndex_jfieldId == IntPtr.Zero)
					mConfigurationIndex_jfieldId = JNIEnv.GetFieldID (class_ref, "mConfigurationIndex", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mConfigurationIndex_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr mMinBufferSize_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/field[@name='mMinBufferSize']"
		[Register ("mMinBufferSize")]
		protected int MMinBufferSize {
			get {
				if (mMinBufferSize_jfieldId == IntPtr.Zero)
					mMinBufferSize_jfieldId = JNIEnv.GetFieldID (class_ref, "mMinBufferSize", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, mMinBufferSize_jfieldId);
			}
			set {
				if (mMinBufferSize_jfieldId == IntPtr.Zero)
					mMinBufferSize_jfieldId = JNIEnv.GetFieldID (class_ref, "mMinBufferSize", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mMinBufferSize_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr mStreamType_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/field[@name='mStreamType']"
		[Register ("mStreamType")]
		protected int MStreamType {
			get {
				if (mStreamType_jfieldId == IntPtr.Zero)
					mStreamType_jfieldId = JNIEnv.GetFieldID (class_ref, "mStreamType", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, mStreamType_jfieldId);
			}
			set {
				if (mStreamType_jfieldId == IntPtr.Zero)
					mStreamType_jfieldId = JNIEnv.GetFieldID (class_ref, "mStreamType", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mStreamType_jfieldId, value);
				} finally {
				}
			}
		}
		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioTrack.PlaybackNotificationListener']"
		[Register ("com/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener", "", "Com.Google.Android.Gms.Car.CarAudioTrack/IPlaybackNotificationListenerInvoker")]
		public partial interface IPlaybackNotificationListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioTrack.PlaybackNotificationListener']/method[@name='onPeriodicNotification' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarAudioTrack']]"
			[Register ("onPeriodicNotification", "(Lcom/google/android/gms/car/CarAudioTrack;)V", "GetOnPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_Handler:Com.Google.Android.Gms.Car.CarAudioTrack/IPlaybackNotificationListenerInvoker, AndroidAutoBindingJar")]
			void OnPeriodicNotification (global::Com.Google.Android.Gms.Car.CarAudioTrack p0);

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioTrack.PlaybackNotificationListener']/method[@name='onPlayError' and count(parameter)=2 and parameter[1][@type='com.google.android.gms.car.CarAudioTrack'] and parameter[2][@type='int']]"
			[Register ("onPlayError", "(Lcom/google/android/gms/car/CarAudioTrack;I)V", "GetOnPlayError_Lcom_google_android_gms_car_CarAudioTrack_IHandler:Com.Google.Android.Gms.Car.CarAudioTrack/IPlaybackNotificationListenerInvoker, AndroidAutoBindingJar")]
			void OnPlayError (global::Com.Google.Android.Gms.Car.CarAudioTrack p0, int p1);

		}

		[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener", DoNotGenerateAcw=true)]
		internal class IPlaybackNotificationListenerInvoker : global::Java.Lang.Object, IPlaybackNotificationListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IPlaybackNotificationListenerInvoker); }
			}

			IntPtr class_ref;

			public static IPlaybackNotificationListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IPlaybackNotificationListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarAudioTrack.PlaybackNotificationListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IPlaybackNotificationListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_;
#pragma warning disable 0169
			static Delegate GetOnPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_Handler ()
			{
				if (cb_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_ == null)
					cb_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_);
				return cb_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_;
			}

			static void n_OnPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Google.Android.Gms.Car.CarAudioTrack p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnPeriodicNotification (p0);
			}
#pragma warning restore 0169

			IntPtr id_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_;
			public unsafe void OnPeriodicNotification (global::Com.Google.Android.Gms.Car.CarAudioTrack p0)
			{
				if (id_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_ == IntPtr.Zero)
					id_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_ = JNIEnv.GetMethodID (class_ref, "onPeriodicNotification", "(Lcom/google/android/gms/car/CarAudioTrack;)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_, __args);
			}

			static Delegate cb_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I;
#pragma warning disable 0169
			static Delegate GetOnPlayError_Lcom_google_android_gms_car_CarAudioTrack_IHandler ()
			{
				if (cb_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I == null)
					cb_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_OnPlayError_Lcom_google_android_gms_car_CarAudioTrack_I);
				return cb_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I;
			}

			static void n_OnPlayError_Lcom_google_android_gms_car_CarAudioTrack_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
			{
				global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Google.Android.Gms.Car.CarAudioTrack p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnPlayError (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I;
			public unsafe void OnPlayError (global::Com.Google.Android.Gms.Car.CarAudioTrack p0, int p1)
			{
				if (id_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I == IntPtr.Zero)
					id_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I = JNIEnv.GetMethodID (class_ref, "onPlayError", "(Lcom/google/android/gms/car/CarAudioTrack;I)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPlayError_Lcom_google_android_gms_car_CarAudioTrack_I, __args);
			}

		}

		public partial class PeriodicNotificationEventArgs : global::System.EventArgs {

			public PeriodicNotificationEventArgs (global::Com.Google.Android.Gms.Car.CarAudioTrack p0)
			{
				this.p0 = p0;
			}

			global::Com.Google.Android.Gms.Car.CarAudioTrack p0;
			public global::Com.Google.Android.Gms.Car.CarAudioTrack P0 {
				get { return p0; }
			}
		}

		public partial class PlayErrorEventArgs : global::System.EventArgs {

			public PlayErrorEventArgs (global::Com.Google.Android.Gms.Car.CarAudioTrack p0, int p1)
			{
				this.p0 = p0;
				this.p1 = p1;
			}

			global::Com.Google.Android.Gms.Car.CarAudioTrack p0;
			public global::Com.Google.Android.Gms.Car.CarAudioTrack P0 {
				get { return p0; }
			}

			int p1;
			public int P1 {
				get { return p1; }
			}
		}

		[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarAudioTrack_PlaybackNotificationListenerImplementor")]
		internal sealed partial class IPlaybackNotificationListenerImplementor : global::Java.Lang.Object, IPlaybackNotificationListener {

			object sender;

			public IPlaybackNotificationListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarAudioTrack_PlaybackNotificationListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<PeriodicNotificationEventArgs> OnPeriodicNotificationHandler;
#pragma warning restore 0649

			public void OnPeriodicNotification (global::Com.Google.Android.Gms.Car.CarAudioTrack p0)
			{
				var __h = OnPeriodicNotificationHandler;
				if (__h != null)
					__h (sender, new PeriodicNotificationEventArgs (p0));
			}
#pragma warning disable 0649
			public EventHandler<PlayErrorEventArgs> OnPlayErrorHandler;
#pragma warning restore 0649

			public void OnPlayError (global::Com.Google.Android.Gms.Car.CarAudioTrack p0, int p1)
			{
				var __h = OnPlayErrorHandler;
				if (__h != null)
					__h (sender, new PlayErrorEventArgs (p0, p1));
			}

			internal static bool __IsEmpty (IPlaybackNotificationListenerImplementor value)
			{
				return value.OnPeriodicNotificationHandler == null && value.OnPlayErrorHandler == null;
			}
		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/CarAudioTrack", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioTrack); }
		}

		protected CarAudioTrack (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_google_android_gms_car_CarAudioManager_IIII;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/constructor[@name='CarAudioTrack' and count(parameter)=5 and parameter[1][@type='com.google.android.gms.car.CarAudioManager'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register (".ctor", "(Lcom/google/android/gms/car/CarAudioManager;IIII)V", "")]
		protected unsafe CarAudioTrack (global::Com.Google.Android.Gms.Car.ICarAudioManager p0, int p1, int p2, int p3, int p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				if (((object) this).GetType () != typeof (CarAudioTrack)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/gms/car/CarAudioManager;IIII)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/gms/car/CarAudioManager;IIII)V", __args);
					return;
				}

				if (id_ctor_Lcom_google_android_gms_car_CarAudioManager_IIII == IntPtr.Zero)
					id_ctor_Lcom_google_android_gms_car_CarAudioManager_IIII = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/gms/car/CarAudioManager;IIII)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_gms_car_CarAudioManager_IIII, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_gms_car_CarAudioManager_IIII, __args);
			} finally {
			}
		}

		static Delegate cb_getBufferSize;
#pragma warning disable 0169
		static Delegate GetGetBufferSizeHandler ()
		{
			if (cb_getBufferSize == null)
				cb_getBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetBufferSize);
			return cb_getBufferSize;
		}

		static int n_GetBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.BufferSize;
		}
#pragma warning restore 0169

		static IntPtr id_getBufferSize;
		public virtual unsafe int BufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='getBufferSize' and count(parameter)=0]"
			[Register ("getBufferSize", "()I", "GetGetBufferSizeHandler")]
			get {
				if (id_getBufferSize == IntPtr.Zero)
					id_getBufferSize = JNIEnv.GetMethodID (class_ref, "getBufferSize", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getBufferSize);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBufferSize", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getConfigurationIndex;
#pragma warning disable 0169
		static Delegate GetGetConfigurationIndexHandler ()
		{
			if (cb_getConfigurationIndex == null)
				cb_getConfigurationIndex = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetConfigurationIndex);
			return cb_getConfigurationIndex;
		}

		static int n_GetConfigurationIndex (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ConfigurationIndex;
		}
#pragma warning restore 0169

		static IntPtr id_getConfigurationIndex;
		public virtual unsafe int ConfigurationIndex {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='getConfigurationIndex' and count(parameter)=0]"
			[Register ("getConfigurationIndex", "()I", "GetGetConfigurationIndexHandler")]
			get {
				if (id_getConfigurationIndex == IntPtr.Zero)
					id_getConfigurationIndex = JNIEnv.GetMethodID (class_ref, "getConfigurationIndex", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getConfigurationIndex);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getConfigurationIndex", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getPlayState;
#pragma warning disable 0169
		static Delegate GetGetPlayStateHandler ()
		{
			if (cb_getPlayState == null)
				cb_getPlayState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetPlayState);
			return cb_getPlayState;
		}

		static int n_GetPlayState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.PlayState;
		}
#pragma warning restore 0169

		public abstract int PlayState {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='getPlayState' and count(parameter)=0]"
			[Register ("getPlayState", "()I", "GetGetPlayStateHandler")] get;
		}

		static Delegate cb_getStreamType;
#pragma warning disable 0169
		static Delegate GetGetStreamTypeHandler ()
		{
			if (cb_getStreamType == null)
				cb_getStreamType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetStreamType);
			return cb_getStreamType;
		}

		static int n_GetStreamType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.StreamType;
		}
#pragma warning restore 0169

		static IntPtr id_getStreamType;
		public virtual unsafe int StreamType {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='getStreamType' and count(parameter)=0]"
			[Register ("getStreamType", "()I", "GetGetStreamTypeHandler")]
			get {
				if (id_getStreamType == IntPtr.Zero)
					id_getStreamType = JNIEnv.GetMethodID (class_ref, "getStreamType", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getStreamType);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStreamType", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_flush;
#pragma warning disable 0169
		static Delegate GetFlushHandler ()
		{
			if (cb_flush == null)
				cb_flush = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Flush);
			return cb_flush;
		}

		static void n_Flush (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Flush ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='flush' and count(parameter)=0]"
		[Register ("flush", "()V", "GetFlushHandler")]
		public abstract void Flush ();

		static Delegate cb_pause;
#pragma warning disable 0169
		static Delegate GetPauseHandler ()
		{
			if (cb_pause == null)
				cb_pause = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Pause);
			return cb_pause;
		}

		static void n_Pause (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Pause ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='pause' and count(parameter)=0]"
		[Register ("pause", "()V", "GetPauseHandler")]
		public abstract void Pause ();

		static Delegate cb_play;
#pragma warning disable 0169
		static Delegate GetPlayHandler ()
		{
			if (cb_play == null)
				cb_play = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Play);
			return cb_play;
		}

		static void n_Play (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Play ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='play' and count(parameter)=0]"
		[Register ("play", "()V", "GetPlayHandler")]
		public abstract void Play ();

		static Delegate cb_release;
#pragma warning disable 0169
		static Delegate GetReleaseHandler ()
		{
			if (cb_release == null)
				cb_release = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Release);
			return cb_release;
		}

		static void n_Release (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Release ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "GetReleaseHandler")]
		public abstract void Release ();

		static Delegate cb_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_;
#pragma warning disable 0169
		static Delegate GetSetPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_Handler ()
		{
			if (cb_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_ == null)
				cb_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_);
			return cb_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_;
		}

		static void n_SetPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener p0 = (global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetPlaybackNotificationListener (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='setPlaybackNotificationListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarAudioTrack.PlaybackNotificationListener']]"
		[Register ("setPlaybackNotificationListener", "(Lcom/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener;)V", "GetSetPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_Handler")]
		public abstract void SetPlaybackNotificationListener (global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener p0);

		static Delegate cb_setPositionNotificationPeriod_I;
#pragma warning disable 0169
		static Delegate GetSetPositionNotificationPeriod_IHandler ()
		{
			if (cb_setPositionNotificationPeriod_I == null)
				cb_setPositionNotificationPeriod_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetPositionNotificationPeriod_I);
			return cb_setPositionNotificationPeriod_I;
		}

		static void n_SetPositionNotificationPeriod_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetPositionNotificationPeriod (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='setPositionNotificationPeriod' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setPositionNotificationPeriod", "(I)V", "GetSetPositionNotificationPeriod_IHandler")]
		public abstract void SetPositionNotificationPeriod (int p0);

		static Delegate cb_stop;
#pragma warning disable 0169
		static Delegate GetStopHandler ()
		{
			if (cb_stop == null)
				cb_stop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Stop);
			return cb_stop;
		}

		static void n_Stop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Stop ();
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "GetStopHandler")]
		public abstract void Stop ();

		static Delegate cb_write_arrayBII;
#pragma warning disable 0169
		static Delegate GetWrite_arrayBIIHandler ()
		{
			if (cb_write_arrayBII == null)
				cb_write_arrayBII = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, int>) n_Write_arrayBII);
			return cb_write_arrayBII;
		}

		static int n_Write_arrayBII (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.CarAudioTrack __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			int __ret = __this.Write (p0, p1, p2);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='write' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("write", "([BII)I", "GetWrite_arrayBIIHandler")]
		public abstract int Write (byte[] p0, int p1, int p2);

#region "Event implementation for Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener"
		public event EventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.PeriodicNotificationEventArgs> PeriodicNotification {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener, global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor>(
						ref weak_implementor_SetPlaybackNotificationListener,
						__CreateIPlaybackNotificationListenerImplementor,
						SetPlaybackNotificationListener,
						__h => __h.OnPeriodicNotificationHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener, global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor>(
						ref weak_implementor_SetPlaybackNotificationListener,
						global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor.__IsEmpty,
						__v => SetPlaybackNotificationListener (null),
						__h => __h.OnPeriodicNotificationHandler -= value);
			}
		}

		public event EventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.PlayErrorEventArgs> PlayError {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener, global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor>(
						ref weak_implementor_SetPlaybackNotificationListener,
						__CreateIPlaybackNotificationListenerImplementor,
						SetPlaybackNotificationListener,
						__h => __h.OnPlayErrorHandler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener, global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor>(
						ref weak_implementor_SetPlaybackNotificationListener,
						global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor.__IsEmpty,
						__v => SetPlaybackNotificationListener (null),
						__h => __h.OnPlayErrorHandler -= value);
			}
		}

		WeakReference weak_implementor_SetPlaybackNotificationListener;

		global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor __CreateIPlaybackNotificationListenerImplementor ()
		{
			return new global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListenerImplementor (this);
		}
#endregion
	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioTrack", DoNotGenerateAcw=true)]
	internal partial class CarAudioTrackInvoker : CarAudioTrack {

		public CarAudioTrackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioTrackInvoker); }
		}

		static IntPtr id_getPlayState;
		public override unsafe int PlayState {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='getPlayState' and count(parameter)=0]"
			[Register ("getPlayState", "()I", "GetGetPlayStateHandler")]
			get {
				if (id_getPlayState == IntPtr.Zero)
					id_getPlayState = JNIEnv.GetMethodID (class_ref, "getPlayState", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getPlayState);
				} finally {
				}
			}
		}

		static IntPtr id_flush;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='flush' and count(parameter)=0]"
		[Register ("flush", "()V", "GetFlushHandler")]
		public override unsafe void Flush ()
		{
			if (id_flush == IntPtr.Zero)
				id_flush = JNIEnv.GetMethodID (class_ref, "flush", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_flush);
			} finally {
			}
		}

		static IntPtr id_pause;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='pause' and count(parameter)=0]"
		[Register ("pause", "()V", "GetPauseHandler")]
		public override unsafe void Pause ()
		{
			if (id_pause == IntPtr.Zero)
				id_pause = JNIEnv.GetMethodID (class_ref, "pause", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_pause);
			} finally {
			}
		}

		static IntPtr id_play;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='play' and count(parameter)=0]"
		[Register ("play", "()V", "GetPlayHandler")]
		public override unsafe void Play ()
		{
			if (id_play == IntPtr.Zero)
				id_play = JNIEnv.GetMethodID (class_ref, "play", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_play);
			} finally {
			}
		}

		static IntPtr id_release;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "GetReleaseHandler")]
		public override unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
			} finally {
			}
		}

		static IntPtr id_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='setPlaybackNotificationListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarAudioTrack.PlaybackNotificationListener']]"
		[Register ("setPlaybackNotificationListener", "(Lcom/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener;)V", "GetSetPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_Handler")]
		public override unsafe void SetPlaybackNotificationListener (global::Com.Google.Android.Gms.Car.CarAudioTrack.IPlaybackNotificationListener p0)
		{
			if (id_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_ == IntPtr.Zero)
				id_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_ = JNIEnv.GetMethodID (class_ref, "setPlaybackNotificationListener", "(Lcom/google/android/gms/car/CarAudioTrack$PlaybackNotificationListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setPlaybackNotificationListener_Lcom_google_android_gms_car_CarAudioTrack_PlaybackNotificationListener_, __args);
			} finally {
			}
		}

		static IntPtr id_setPositionNotificationPeriod_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='setPositionNotificationPeriod' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setPositionNotificationPeriod", "(I)V", "GetSetPositionNotificationPeriod_IHandler")]
		public override unsafe void SetPositionNotificationPeriod (int p0)
		{
			if (id_setPositionNotificationPeriod_I == IntPtr.Zero)
				id_setPositionNotificationPeriod_I = JNIEnv.GetMethodID (class_ref, "setPositionNotificationPeriod", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setPositionNotificationPeriod_I, __args);
			} finally {
			}
		}

		static IntPtr id_stop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "GetStopHandler")]
		public override unsafe void Stop ()
		{
			if (id_stop == IntPtr.Zero)
				id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stop);
			} finally {
			}
		}

		static IntPtr id_write_arrayBII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioTrack']/method[@name='write' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("write", "([BII)I", "GetWrite_arrayBIIHandler")]
		public override unsafe int Write (byte[] p0, int p1, int p2)
		{
			if (id_write_arrayBII == IntPtr.Zero)
				id_write_arrayBII = JNIEnv.GetMethodID (class_ref, "write", "([BII)I");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_write_arrayBII, __args);
				return __ret;
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

	}

}
