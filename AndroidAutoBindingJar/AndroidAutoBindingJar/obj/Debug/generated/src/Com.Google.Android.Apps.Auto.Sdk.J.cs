using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='j']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/j", DoNotGenerateAcw=true)]
	public sealed partial class J : global::Com.Google.Android.A.Agizmo {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/j", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (J); }
		}

		internal J (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

	}
}
