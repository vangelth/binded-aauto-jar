using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarLibLog']"
	[global::Android.Runtime.Register ("android/support/car/CarLibLog", DoNotGenerateAcw=true)]
	public partial class CarLibLog : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarLibLog']/field[@name='TAG_CAR']"
		[Register ("TAG_CAR")]
		public const string TagCar = (string) "CAR.L";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarLibLog']/field[@name='TAG_NAV']"
		[Register ("TAG_NAV")]
		public const string TagNav = (string) "CAR.L.NAV";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car']/class[@name='CarLibLog']/field[@name='TAG_SENSOR']"
		[Register ("TAG_SENSOR")]
		public const string TagSensor = (string) "CAR.L.SENSOR";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarLibLog", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarLibLog); }
		}

		protected CarLibLog (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car']/class[@name='CarLibLog']/constructor[@name='CarLibLog' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarLibLog ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarLibLog)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
