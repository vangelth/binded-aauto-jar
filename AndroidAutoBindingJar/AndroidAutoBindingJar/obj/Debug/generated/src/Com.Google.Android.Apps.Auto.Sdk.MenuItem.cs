using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuItem", DoNotGenerateAcw=true)]
	public partial class MenuItem : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuItem$Builder", DoNotGenerateAcw=true)]
		public partial class Builder : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuItem$Builder", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Builder); }
			}

			protected Builder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/constructor[@name='MenuItem.Builder' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe Builder ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static IntPtr id_ctor_Lcom_google_android_apps_auto_sdk_MenuItem_;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/constructor[@name='MenuItem.Builder' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuItem']]"
			[Register (".ctor", "(Lcom/google/android/apps/auto/sdk/MenuItem;)V", "")]
			public unsafe Builder (global::Com.Google.Android.Apps.Auto.Sdk.MenuItem p0)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Lcom/google/android/apps/auto/sdk/MenuItem;)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Lcom/google/android/apps/auto/sdk/MenuItem;)V", __args);
						return;
					}

					if (id_ctor_Lcom_google_android_apps_auto_sdk_MenuItem_ == IntPtr.Zero)
						id_ctor_Lcom_google_android_apps_auto_sdk_MenuItem_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/google/android/apps/auto/sdk/MenuItem;)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_google_android_apps_auto_sdk_MenuItem_, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Lcom_google_android_apps_auto_sdk_MenuItem_, __args);
				} finally {
				}
			}

			static Delegate cb_build;
#pragma warning disable 0169
			static Delegate GetBuildHandler ()
			{
				if (cb_build == null)
					cb_build = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Build);
				return cb_build;
			}

			static IntPtr n_Build (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.Build ());
			}
#pragma warning restore 0169

			static IntPtr id_build;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='build' and count(parameter)=0]"
			[Register ("build", "()Lcom/google/android/apps/auto/sdk/MenuItem;", "GetBuildHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem Build ()
			{
				if (id_build == IntPtr.Zero)
					id_build = JNIEnv.GetMethodID (class_ref, "build", "()Lcom/google/android/apps/auto/sdk/MenuItem;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_build), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "build", "()Lcom/google/android/apps/auto/sdk/MenuItem;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setChecked_Z;
#pragma warning disable 0169
			static Delegate GetSetChecked_ZHandler ()
			{
				if (cb_setChecked_Z == null)
					cb_setChecked_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, IntPtr>) n_SetChecked_Z);
				return cb_setChecked_Z;
			}

			static IntPtr n_SetChecked_Z (IntPtr jnienv, IntPtr native__this, bool p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetChecked (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setChecked_Z;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setChecked' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setChecked", "(Z)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetChecked_ZHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetChecked (bool p0)
			{
				if (id_setChecked_Z == IntPtr.Zero)
					id_setChecked_Z = JNIEnv.GetMethodID (class_ref, "setChecked", "(Z)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setChecked_Z, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setChecked", "(Z)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setExtras_Landroid_os_Bundle_;
#pragma warning disable 0169
			static Delegate GetSetExtras_Landroid_os_Bundle_Handler ()
			{
				if (cb_setExtras_Landroid_os_Bundle_ == null)
					cb_setExtras_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetExtras_Landroid_os_Bundle_);
				return cb_setExtras_Landroid_os_Bundle_;
			}

			static IntPtr n_SetExtras_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetExtras (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setExtras_Landroid_os_Bundle_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setExtras' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
			[Register ("setExtras", "(Landroid/os/Bundle;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetExtras_Landroid_os_Bundle_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetExtras (global::Android.OS.Bundle p0)
			{
				if (id_setExtras_Landroid_os_Bundle_ == IntPtr.Zero)
					id_setExtras_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "setExtras", "(Landroid/os/Bundle;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setExtras_Landroid_os_Bundle_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExtras", "(Landroid/os/Bundle;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
				}
			}

			static Delegate cb_setIconBitmap_Landroid_graphics_Bitmap_;
#pragma warning disable 0169
			static Delegate GetSetIconBitmap_Landroid_graphics_Bitmap_Handler ()
			{
				if (cb_setIconBitmap_Landroid_graphics_Bitmap_ == null)
					cb_setIconBitmap_Landroid_graphics_Bitmap_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetIconBitmap_Landroid_graphics_Bitmap_);
				return cb_setIconBitmap_Landroid_graphics_Bitmap_;
			}

			static IntPtr n_SetIconBitmap_Landroid_graphics_Bitmap_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Graphics.Bitmap p0 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetIconBitmap (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setIconBitmap_Landroid_graphics_Bitmap_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setIconBitmap' and count(parameter)=1 and parameter[1][@type='android.graphics.Bitmap']]"
			[Register ("setIconBitmap", "(Landroid/graphics/Bitmap;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetIconBitmap_Landroid_graphics_Bitmap_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetIconBitmap (global::Android.Graphics.Bitmap p0)
			{
				if (id_setIconBitmap_Landroid_graphics_Bitmap_ == IntPtr.Zero)
					id_setIconBitmap_Landroid_graphics_Bitmap_ = JNIEnv.GetMethodID (class_ref, "setIconBitmap", "(Landroid/graphics/Bitmap;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setIconBitmap_Landroid_graphics_Bitmap_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIconBitmap", "(Landroid/graphics/Bitmap;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
				}
			}

			static Delegate cb_setIconResId_I;
#pragma warning disable 0169
			static Delegate GetSetIconResId_IHandler ()
			{
				if (cb_setIconResId_I == null)
					cb_setIconResId_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetIconResId_I);
				return cb_setIconResId_I;
			}

			static IntPtr n_SetIconResId_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetIconResId (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setIconResId_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setIconResId' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setIconResId", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetIconResId_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetIconResId (int p0)
			{
				if (id_setIconResId_I == IntPtr.Zero)
					id_setIconResId_I = JNIEnv.GetMethodID (class_ref, "setIconResId", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setIconResId_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIconResId", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setSubtitle_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetSubtitle_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setSubtitle_Ljava_lang_CharSequence_ == null)
					cb_setSubtitle_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetSubtitle_Ljava_lang_CharSequence_);
				return cb_setSubtitle_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetSubtitle_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetSubtitle (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setSubtitle_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setSubtitle' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setSubtitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetSubtitle_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetSubtitle (global::Java.Lang.ICharSequence p0)
			{
				if (id_setSubtitle_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setSubtitle_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setSubtitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setSubtitle_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSubtitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetSubtitle (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __result = SetSubtitle (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setTitle_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetTitle_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setTitle_Ljava_lang_CharSequence_ == null)
					cb_setTitle_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_CharSequence_);
				return cb_setTitle_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetTitle_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetTitle (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setTitle_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setTitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetTitle_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetTitle (global::Java.Lang.ICharSequence p0)
			{
				if (id_setTitle_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setTitle_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTitle_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetTitle (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __result = SetTitle (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setType_I;
#pragma warning disable 0169
			static Delegate GetSetType_IHandler ()
			{
				if (cb_setType_I == null)
					cb_setType_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetType_I);
				return cb_setType_I;
			}

			static IntPtr n_SetType_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetType (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setType_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem.Builder']/method[@name='setType' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setType", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;", "GetSetType_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder SetType (int p0)
			{
				if (id_setType_I == IntPtr.Zero)
					id_setType_I = JNIEnv.GetMethodID (class_ref, "setType", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setType_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setType", "(I)Lcom/google/android/apps/auto/sdk/MenuItem$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

		}

		[Register ("com/google/android/apps/auto/sdk/MenuItem$Type", DoNotGenerateAcw=true)]
		public abstract class Type : Java.Lang.Object {

			internal Type ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuItem.Type']/field[@name='CHECKBOX']"
			[Register ("CHECKBOX")]
			public const int Checkbox = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuItem.Type']/field[@name='ITEM']"
			[Register ("ITEM")]
			public const int Item = (int) 0;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuItem.Type']/field[@name='SUBMENU']"
			[Register ("SUBMENU")]
			public const int Submenu = (int) 2;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/MenuItem$Type", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'Type' type. This type will be removed in a future release.")]
		public abstract class TypeConsts : Type {

			private TypeConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuItem.Type']"
		[Register ("com/google/android/apps/auto/sdk/MenuItem$Type", "", "Com.Google.Android.Apps.Auto.Sdk.MenuItem/ITypeInvoker")]
		public partial interface IType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuItem$Type", DoNotGenerateAcw=true)]
		internal class ITypeInvoker : global::Java.Lang.Object, IType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuItem$Type");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITypeInvoker); }
			}

			IntPtr class_ref;

			public static IType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.MenuItem.Type"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem.IType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuItem", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (MenuItem); }
		}

		protected MenuItem (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getExtras;
#pragma warning disable 0169
		static Delegate GetGetExtrasHandler ()
		{
			if (cb_getExtras == null)
				cb_getExtras = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetExtras);
			return cb_getExtras;
		}

		static IntPtr n_GetExtras (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Extras);
		}
#pragma warning restore 0169

		static IntPtr id_getExtras;
		public virtual unsafe global::Android.OS.Bundle Extras {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='getExtras' and count(parameter)=0]"
			[Register ("getExtras", "()Landroid/os/Bundle;", "GetGetExtrasHandler")]
			get {
				if (id_getExtras == IntPtr.Zero)
					id_getExtras = JNIEnv.GetMethodID (class_ref, "getExtras", "()Landroid/os/Bundle;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getExtras), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExtras", "()Landroid/os/Bundle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getIconBitmap;
#pragma warning disable 0169
		static Delegate GetGetIconBitmapHandler ()
		{
			if (cb_getIconBitmap == null)
				cb_getIconBitmap = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIconBitmap);
			return cb_getIconBitmap;
		}

		static IntPtr n_GetIconBitmap (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IconBitmap);
		}
#pragma warning restore 0169

		static IntPtr id_getIconBitmap;
		public virtual unsafe global::Android.Graphics.Bitmap IconBitmap {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='getIconBitmap' and count(parameter)=0]"
			[Register ("getIconBitmap", "()Landroid/graphics/Bitmap;", "GetGetIconBitmapHandler")]
			get {
				if (id_getIconBitmap == IntPtr.Zero)
					id_getIconBitmap = JNIEnv.GetMethodID (class_ref, "getIconBitmap", "()Landroid/graphics/Bitmap;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getIconBitmap), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIconBitmap", "()Landroid/graphics/Bitmap;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getIconResId;
#pragma warning disable 0169
		static Delegate GetGetIconResIdHandler ()
		{
			if (cb_getIconResId == null)
				cb_getIconResId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetIconResId);
			return cb_getIconResId;
		}

		static int n_GetIconResId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IconResId;
		}
#pragma warning restore 0169

		static IntPtr id_getIconResId;
		public virtual unsafe int IconResId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='getIconResId' and count(parameter)=0]"
			[Register ("getIconResId", "()I", "GetGetIconResIdHandler")]
			get {
				if (id_getIconResId == IntPtr.Zero)
					id_getIconResId = JNIEnv.GetMethodID (class_ref, "getIconResId", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getIconResId);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIconResId", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_isChecked;
#pragma warning disable 0169
		static Delegate GetIsCheckedHandler ()
		{
			if (cb_isChecked == null)
				cb_isChecked = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsChecked);
			return cb_isChecked;
		}

		static bool n_IsChecked (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsChecked;
		}
#pragma warning restore 0169

		static IntPtr id_isChecked;
		public virtual unsafe bool IsChecked {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='isChecked' and count(parameter)=0]"
			[Register ("isChecked", "()Z", "GetIsCheckedHandler")]
			get {
				if (id_isChecked == IntPtr.Zero)
					id_isChecked = JNIEnv.GetMethodID (class_ref, "isChecked", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isChecked);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isChecked", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getSubtitle;
#pragma warning disable 0169
		static Delegate GetGetSubtitleHandler ()
		{
			if (cb_getSubtitle == null)
				cb_getSubtitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSubtitle);
			return cb_getSubtitle;
		}

		static IntPtr n_GetSubtitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.SubtitleFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getSubtitle;
		public virtual unsafe global::Java.Lang.ICharSequence SubtitleFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='getSubtitle' and count(parameter)=0]"
			[Register ("getSubtitle", "()Ljava/lang/CharSequence;", "GetGetSubtitleHandler")]
			get {
				if (id_getSubtitle == IntPtr.Zero)
					id_getSubtitle = JNIEnv.GetMethodID (class_ref, "getSubtitle", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getSubtitle), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSubtitle", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string Subtitle {
			get { return SubtitleFormatted == null ? null : SubtitleFormatted.ToString (); }
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.TitleFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		public virtual unsafe global::Java.Lang.ICharSequence TitleFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/CharSequence;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string Title {
			get { return TitleFormatted == null ? null : TitleFormatted.ToString (); }
		}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()I", "")]
		public unsafe int A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()I");
			try {
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_a);
			} finally {
			}
		}

		static IntPtr id_b;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='b' and count(parameter)=0]"
		[Register ("b", "()Landroid/net/Uri;", "")]
		public unsafe global::Android.Net.Uri B ()
		{
			if (id_b == IntPtr.Zero)
				id_b = JNIEnv.GetMethodID (class_ref, "b", "()Landroid/net/Uri;");
			try {
				return global::Java.Lang.Object.GetObject<global::Android.Net.Uri> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_b), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuItem __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuItem']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
