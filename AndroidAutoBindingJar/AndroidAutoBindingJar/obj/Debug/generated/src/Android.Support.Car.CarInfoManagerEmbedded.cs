using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/CarInfoManagerEmbedded", DoNotGenerateAcw=true)]
	public partial class CarInfoManagerEmbedded : global::Android.Support.Car.CarInfoManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/CarInfoManagerEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarInfoManagerEmbedded); }
		}

		protected CarInfoManagerEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getDriverPosition;
#pragma warning disable 0169
		static Delegate GetGetDriverPositionHandler ()
		{
			if (cb_getDriverPosition == null)
				cb_getDriverPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetDriverPosition);
			return cb_getDriverPosition;
		}

		static int n_GetDriverPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DriverPosition;
		}
#pragma warning restore 0169

		static IntPtr id_getDriverPosition;
		public override unsafe int DriverPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getDriverPosition' and count(parameter)=0]"
			[Register ("getDriverPosition", "()I", "GetGetDriverPositionHandler")]
			get {
				if (id_getDriverPosition == IntPtr.Zero)
					id_getDriverPosition = JNIEnv.GetMethodID (class_ref, "getDriverPosition", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getDriverPosition);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDriverPosition", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getHeadunitManufacturer;
#pragma warning disable 0169
		static Delegate GetGetHeadunitManufacturerHandler ()
		{
			if (cb_getHeadunitManufacturer == null)
				cb_getHeadunitManufacturer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitManufacturer);
			return cb_getHeadunitManufacturer;
		}

		static IntPtr n_GetHeadunitManufacturer (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitManufacturer);
		}
#pragma warning restore 0169

		static IntPtr id_getHeadunitManufacturer;
		public override unsafe string HeadunitManufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getHeadunitManufacturer' and count(parameter)=0]"
			[Register ("getHeadunitManufacturer", "()Ljava/lang/String;", "GetGetHeadunitManufacturerHandler")]
			get {
				if (id_getHeadunitManufacturer == IntPtr.Zero)
					id_getHeadunitManufacturer = JNIEnv.GetMethodID (class_ref, "getHeadunitManufacturer", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitManufacturer), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHeadunitManufacturer", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getHeadunitModel;
#pragma warning disable 0169
		static Delegate GetGetHeadunitModelHandler ()
		{
			if (cb_getHeadunitModel == null)
				cb_getHeadunitModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitModel);
			return cb_getHeadunitModel;
		}

		static IntPtr n_GetHeadunitModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitModel);
		}
#pragma warning restore 0169

		static IntPtr id_getHeadunitModel;
		public override unsafe string HeadunitModel {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getHeadunitModel' and count(parameter)=0]"
			[Register ("getHeadunitModel", "()Ljava/lang/String;", "GetGetHeadunitModelHandler")]
			get {
				if (id_getHeadunitModel == IntPtr.Zero)
					id_getHeadunitModel = JNIEnv.GetMethodID (class_ref, "getHeadunitModel", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitModel), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHeadunitModel", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getHeadunitSoftwareBuild;
#pragma warning disable 0169
		static Delegate GetGetHeadunitSoftwareBuildHandler ()
		{
			if (cb_getHeadunitSoftwareBuild == null)
				cb_getHeadunitSoftwareBuild = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitSoftwareBuild);
			return cb_getHeadunitSoftwareBuild;
		}

		static IntPtr n_GetHeadunitSoftwareBuild (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitSoftwareBuild);
		}
#pragma warning restore 0169

		static IntPtr id_getHeadunitSoftwareBuild;
		public override unsafe string HeadunitSoftwareBuild {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getHeadunitSoftwareBuild' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareBuild", "()Ljava/lang/String;", "GetGetHeadunitSoftwareBuildHandler")]
			get {
				if (id_getHeadunitSoftwareBuild == IntPtr.Zero)
					id_getHeadunitSoftwareBuild = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareBuild", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareBuild), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHeadunitSoftwareBuild", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getHeadunitSoftwareVersion;
#pragma warning disable 0169
		static Delegate GetGetHeadunitSoftwareVersionHandler ()
		{
			if (cb_getHeadunitSoftwareVersion == null)
				cb_getHeadunitSoftwareVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadunitSoftwareVersion);
			return cb_getHeadunitSoftwareVersion;
		}

		static IntPtr n_GetHeadunitSoftwareVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadunitSoftwareVersion);
		}
#pragma warning restore 0169

		static IntPtr id_getHeadunitSoftwareVersion;
		public override unsafe string HeadunitSoftwareVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getHeadunitSoftwareVersion' and count(parameter)=0]"
			[Register ("getHeadunitSoftwareVersion", "()Ljava/lang/String;", "GetGetHeadunitSoftwareVersionHandler")]
			get {
				if (id_getHeadunitSoftwareVersion == IntPtr.Zero)
					id_getHeadunitSoftwareVersion = JNIEnv.GetMethodID (class_ref, "getHeadunitSoftwareVersion", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadunitSoftwareVersion), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHeadunitSoftwareVersion", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getManufacturer;
#pragma warning disable 0169
		static Delegate GetGetManufacturerHandler ()
		{
			if (cb_getManufacturer == null)
				cb_getManufacturer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetManufacturer);
			return cb_getManufacturer;
		}

		static IntPtr n_GetManufacturer (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Manufacturer);
		}
#pragma warning restore 0169

		static IntPtr id_getManufacturer;
		public override unsafe string Manufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getManufacturer' and count(parameter)=0]"
			[Register ("getManufacturer", "()Ljava/lang/String;", "GetGetManufacturerHandler")]
			get {
				if (id_getManufacturer == IntPtr.Zero)
					id_getManufacturer = JNIEnv.GetMethodID (class_ref, "getManufacturer", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getManufacturer), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getManufacturer", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getModel;
#pragma warning disable 0169
		static Delegate GetGetModelHandler ()
		{
			if (cb_getModel == null)
				cb_getModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModel);
			return cb_getModel;
		}

		static IntPtr n_GetModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Model);
		}
#pragma warning restore 0169

		static IntPtr id_getModel;
		public override unsafe string Model {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getModel' and count(parameter)=0]"
			[Register ("getModel", "()Ljava/lang/String;", "GetGetModelHandler")]
			get {
				if (id_getModel == IntPtr.Zero)
					id_getModel = JNIEnv.GetMethodID (class_ref, "getModel", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModel), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getModel", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getModelYear;
#pragma warning disable 0169
		static Delegate GetGetModelYearHandler ()
		{
			if (cb_getModelYear == null)
				cb_getModelYear = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModelYear);
			return cb_getModelYear;
		}

		static IntPtr n_GetModelYear (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ModelYear);
		}
#pragma warning restore 0169

		static IntPtr id_getModelYear;
		public override unsafe string ModelYear {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getModelYear' and count(parameter)=0]"
			[Register ("getModelYear", "()Ljava/lang/String;", "GetGetModelYearHandler")]
			get {
				if (id_getModelYear == IntPtr.Zero)
					id_getModelYear = JNIEnv.GetMethodID (class_ref, "getModelYear", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModelYear), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getModelYear", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getVehicleId;
#pragma warning disable 0169
		static Delegate GetGetVehicleIdHandler ()
		{
			if (cb_getVehicleId == null)
				cb_getVehicleId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVehicleId);
			return cb_getVehicleId;
		}

		static IntPtr n_GetVehicleId (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.VehicleId);
		}
#pragma warning restore 0169

		static IntPtr id_getVehicleId;
		public override unsafe string VehicleId {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='getVehicleId' and count(parameter)=0]"
			[Register ("getVehicleId", "()Ljava/lang/String;", "GetGetVehicleIdHandler")]
			get {
				if (id_getVehicleId == IntPtr.Zero)
					id_getVehicleId = JNIEnv.GetMethodID (class_ref, "getVehicleId", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getVehicleId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getVehicleId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.CarInfoManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.CarInfoManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car']/class[@name='CarInfoManagerEmbedded']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCarDisconnected", "()V"));
			} finally {
			}
		}

	}
}
