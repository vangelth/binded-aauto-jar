using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager.CarActivityStartListener']"
	[Register ("com/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener", "", "Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerInvoker")]
	public partial interface ICarFirstPartyManagerCarActivityStartListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager.CarActivityStartListener']/method[@name='onActivityStarted' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onActivityStarted", "(Landroid/content/Intent;)V", "GetOnActivityStarted_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerInvoker, AndroidAutoBindingJar")]
		void OnActivityStarted (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager.CarActivityStartListener']/method[@name='onNewActivityRequest' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onNewActivityRequest", "(Landroid/content/Intent;)V", "GetOnNewActivityRequest_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerInvoker, AndroidAutoBindingJar")]
		void OnNewActivityRequest (global::Android.Content.Intent p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener", DoNotGenerateAcw=true)]
	internal class ICarFirstPartyManagerCarActivityStartListenerInvoker : global::Java.Lang.Object, ICarFirstPartyManagerCarActivityStartListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarFirstPartyManagerCarActivityStartListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarFirstPartyManagerCarActivityStartListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarFirstPartyManagerCarActivityStartListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarFirstPartyManager.CarActivityStartListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarFirstPartyManagerCarActivityStartListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onActivityStarted_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnActivityStarted_Landroid_content_Intent_Handler ()
		{
			if (cb_onActivityStarted_Landroid_content_Intent_ == null)
				cb_onActivityStarted_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnActivityStarted_Landroid_content_Intent_);
			return cb_onActivityStarted_Landroid_content_Intent_;
		}

		static void n_OnActivityStarted_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnActivityStarted (p0);
		}
#pragma warning restore 0169

		IntPtr id_onActivityStarted_Landroid_content_Intent_;
		public unsafe void OnActivityStarted (global::Android.Content.Intent p0)
		{
			if (id_onActivityStarted_Landroid_content_Intent_ == IntPtr.Zero)
				id_onActivityStarted_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onActivityStarted", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onActivityStarted_Landroid_content_Intent_, __args);
		}

		static Delegate cb_onNewActivityRequest_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnNewActivityRequest_Landroid_content_Intent_Handler ()
		{
			if (cb_onNewActivityRequest_Landroid_content_Intent_ == null)
				cb_onNewActivityRequest_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnNewActivityRequest_Landroid_content_Intent_);
			return cb_onNewActivityRequest_Landroid_content_Intent_;
		}

		static void n_OnNewActivityRequest_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnNewActivityRequest (p0);
		}
#pragma warning restore 0169

		IntPtr id_onNewActivityRequest_Landroid_content_Intent_;
		public unsafe void OnNewActivityRequest (global::Android.Content.Intent p0)
		{
			if (id_onNewActivityRequest_Landroid_content_Intent_ == IntPtr.Zero)
				id_onNewActivityRequest_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onNewActivityRequest", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onNewActivityRequest_Landroid_content_Intent_, __args);
		}

	}

	public partial class ActivityStartedEventArgs : global::System.EventArgs {

		public ActivityStartedEventArgs (global::Android.Content.Intent p0)
		{
			this.p0 = p0;
		}

		global::Android.Content.Intent p0;
		public global::Android.Content.Intent P0 {
			get { return p0; }
		}
	}

	public partial class NewActivityRequestEventArgs : global::System.EventArgs {

		public NewActivityRequestEventArgs (global::Android.Content.Intent p0)
		{
			this.p0 = p0;
		}

		global::Android.Content.Intent p0;
		public global::Android.Content.Intent P0 {
			get { return p0; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarFirstPartyManager_CarActivityStartListenerImplementor")]
	internal sealed partial class ICarFirstPartyManagerCarActivityStartListenerImplementor : global::Java.Lang.Object, ICarFirstPartyManagerCarActivityStartListener {

		object sender;

		public ICarFirstPartyManagerCarActivityStartListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarFirstPartyManager_CarActivityStartListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<ActivityStartedEventArgs> OnActivityStartedHandler;
#pragma warning restore 0649

		public void OnActivityStarted (global::Android.Content.Intent p0)
		{
			var __h = OnActivityStartedHandler;
			if (__h != null)
				__h (sender, new ActivityStartedEventArgs (p0));
		}
#pragma warning disable 0649
		public EventHandler<NewActivityRequestEventArgs> OnNewActivityRequestHandler;
#pragma warning restore 0649

		public void OnNewActivityRequest (global::Android.Content.Intent p0)
		{
			var __h = OnNewActivityRequestHandler;
			if (__h != null)
				__h (sender, new NewActivityRequestEventArgs (p0));
		}

		internal static bool __IsEmpty (ICarFirstPartyManagerCarActivityStartListenerImplementor value)
		{
			return value.OnActivityStartedHandler == null && value.OnNewActivityRequestHandler == null;
		}
	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager.ScreenshotResultCallback']"
	[Register ("com/google/android/gms/car/CarFirstPartyManager$ScreenshotResultCallback", "", "Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallbackInvoker")]
	public partial interface ICarFirstPartyManagerScreenshotResultCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager.ScreenshotResultCallback']/method[@name='onScreeshotResult' and count(parameter)=1 and parameter[1][@type='android.graphics.Bitmap']]"
		[Register ("onScreeshotResult", "(Landroid/graphics/Bitmap;)V", "GetOnScreeshotResult_Landroid_graphics_Bitmap_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallbackInvoker, AndroidAutoBindingJar")]
		void OnScreeshotResult (global::Android.Graphics.Bitmap p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarFirstPartyManager$ScreenshotResultCallback", DoNotGenerateAcw=true)]
	internal class ICarFirstPartyManagerScreenshotResultCallbackInvoker : global::Java.Lang.Object, ICarFirstPartyManagerScreenshotResultCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarFirstPartyManager$ScreenshotResultCallback");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarFirstPartyManagerScreenshotResultCallbackInvoker); }
		}

		IntPtr class_ref;

		public static ICarFirstPartyManagerScreenshotResultCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarFirstPartyManagerScreenshotResultCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarFirstPartyManager.ScreenshotResultCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarFirstPartyManagerScreenshotResultCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onScreeshotResult_Landroid_graphics_Bitmap_;
#pragma warning disable 0169
		static Delegate GetOnScreeshotResult_Landroid_graphics_Bitmap_Handler ()
		{
			if (cb_onScreeshotResult_Landroid_graphics_Bitmap_ == null)
				cb_onScreeshotResult_Landroid_graphics_Bitmap_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnScreeshotResult_Landroid_graphics_Bitmap_);
			return cb_onScreeshotResult_Landroid_graphics_Bitmap_;
		}

		static void n_OnScreeshotResult_Landroid_graphics_Bitmap_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Graphics.Bitmap p0 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnScreeshotResult (p0);
		}
#pragma warning restore 0169

		IntPtr id_onScreeshotResult_Landroid_graphics_Bitmap_;
		public unsafe void OnScreeshotResult (global::Android.Graphics.Bitmap p0)
		{
			if (id_onScreeshotResult_Landroid_graphics_Bitmap_ == IntPtr.Zero)
				id_onScreeshotResult_Landroid_graphics_Bitmap_ = JNIEnv.GetMethodID (class_ref, "onScreeshotResult", "(Landroid/graphics/Bitmap;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onScreeshotResult_Landroid_graphics_Bitmap_, __args);
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']"
	[Register ("com/google/android/gms/car/CarFirstPartyManager", "", "Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker")]
	public partial interface ICarFirstPartyManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='captureScreenshot' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarFirstPartyManager.ScreenshotResultCallback']]"
		[Register ("captureScreenshot", "(Lcom/google/android/gms/car/CarFirstPartyManager$ScreenshotResultCallback;)V", "GetCaptureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void CaptureScreenshot (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='getBooleanCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='boolean']]"
		[Register ("getBooleanCarServiceSetting", "(Ljava/lang/String;Z)Z", "GetGetBooleanCarServiceSetting_Ljava_lang_String_ZHandler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		bool GetBooleanCarServiceSetting (string p0, bool p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='getStringCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("getStringCarServiceSetting", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "GetGetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		string GetStringCarServiceSetting (string p0, string p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='getStringSetCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.util.Set&lt;java.lang.String&gt;']]"
		[Register ("getStringSetCarServiceSetting", "(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;", "GetGetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		global::System.Collections.Generic.ICollection<string> GetStringSetCarServiceSetting (string p0, global::System.Collections.Generic.ICollection<string> p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='logFacetChange' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("logFacetChange", "(ILjava/lang/String;)V", "GetLogFacetChange_ILjava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void LogFacetChange (int p0, string p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='queryAllowedProjectionServices' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("queryAllowedProjectionServices", "(Landroid/content/Intent;)Ljava/util/List;", "GetQueryAllowedProjectionServices_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		global::System.Collections.Generic.IList<global::Android.Content.PM.ResolveInfo> QueryAllowedProjectionServices (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='registerCarActivityStartListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarFirstPartyManager.CarActivityStartListener']]"
		[Register ("registerCarActivityStartListener", "(Lcom/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener;)V", "GetRegisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void RegisterCarActivityStartListener (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='setBooleanCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='boolean']]"
		[Register ("setBooleanCarServiceSetting", "(Ljava/lang/String;Z)V", "GetSetBooleanCarServiceSetting_Ljava_lang_String_ZHandler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void SetBooleanCarServiceSetting (string p0, bool p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='setStringCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("setStringCarServiceSetting", "(Ljava/lang/String;Ljava/lang/String;)V", "GetSetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void SetStringCarServiceSetting (string p0, string p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='setStringSetCarServiceSetting' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.util.Set&lt;java.lang.String&gt;']]"
		[Register ("setStringSetCarServiceSetting", "(Ljava/lang/String;Ljava/util/Set;)V", "GetSetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void SetStringSetCarServiceSetting (string p0, global::System.Collections.Generic.ICollection<string> p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='startCarActivity' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("startCarActivity", "(Landroid/content/Intent;)V", "GetStartCarActivity_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void StartCarActivity (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarFirstPartyManager']/method[@name='unregisterCarActivityStartListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarFirstPartyManager.CarActivityStartListener']]"
		[Register ("unregisterCarActivityStartListener", "(Lcom/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener;)V", "GetUnregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterCarActivityStartListener (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarFirstPartyManager", DoNotGenerateAcw=true)]
	internal class ICarFirstPartyManagerInvoker : global::Java.Lang.Object, ICarFirstPartyManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarFirstPartyManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarFirstPartyManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarFirstPartyManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarFirstPartyManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarFirstPartyManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarFirstPartyManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_;
#pragma warning disable 0169
		static Delegate GetCaptureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_Handler ()
		{
			if (cb_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_ == null)
				cb_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_CaptureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_);
			return cb_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_;
		}

		static void n_CaptureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback p0 = (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CaptureScreenshot (p0);
		}
#pragma warning restore 0169

		IntPtr id_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_;
		public unsafe void CaptureScreenshot (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerScreenshotResultCallback p0)
		{
			if (id_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_ == IntPtr.Zero)
				id_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_ = JNIEnv.GetMethodID (class_ref, "captureScreenshot", "(Lcom/google/android/gms/car/CarFirstPartyManager$ScreenshotResultCallback;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_captureScreenshot_Lcom_google_android_gms_car_CarFirstPartyManager_ScreenshotResultCallback_, __args);
		}

		static Delegate cb_getBooleanCarServiceSetting_Ljava_lang_String_Z;
#pragma warning disable 0169
		static Delegate GetGetBooleanCarServiceSetting_Ljava_lang_String_ZHandler ()
		{
			if (cb_getBooleanCarServiceSetting_Ljava_lang_String_Z == null)
				cb_getBooleanCarServiceSetting_Ljava_lang_String_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool, bool>) n_GetBooleanCarServiceSetting_Ljava_lang_String_Z);
			return cb_getBooleanCarServiceSetting_Ljava_lang_String_Z;
		}

		static bool n_GetBooleanCarServiceSetting_Ljava_lang_String_Z (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, bool p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.GetBooleanCarServiceSetting (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getBooleanCarServiceSetting_Ljava_lang_String_Z;
		public unsafe bool GetBooleanCarServiceSetting (string p0, bool p1)
		{
			if (id_getBooleanCarServiceSetting_Ljava_lang_String_Z == IntPtr.Zero)
				id_getBooleanCarServiceSetting_Ljava_lang_String_Z = JNIEnv.GetMethodID (class_ref, "getBooleanCarServiceSetting", "(Ljava/lang/String;Z)Z");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (p1);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_getBooleanCarServiceSetting_Ljava_lang_String_Z, __args);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

		static Delegate cb_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_Handler ()
		{
			if (cb_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ == null)
				cb_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_GetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_);
			return cb_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
		}

		static IntPtr n_GetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.GetStringCarServiceSetting (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
		public unsafe string GetStringCarServiceSetting (string p0, string p1)
		{
			if (id_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getStringCarServiceSetting", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (native_p1);
			string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			JNIEnv.DeleteLocalRef (native_p1);
			return __ret;
		}

		static Delegate cb_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetGetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_Handler ()
		{
			if (cb_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ == null)
				cb_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_GetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_);
			return cb_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
		}

		static IntPtr n_GetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			var p1 = global::Android.Runtime.JavaSet<string>.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (__this.GetStringSetCarServiceSetting (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
		public unsafe global::System.Collections.Generic.ICollection<string> GetStringSetCarServiceSetting (string p0, global::System.Collections.Generic.ICollection<string> p1)
		{
			if (id_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ == IntPtr.Zero)
				id_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "getStringSetCarServiceSetting", "(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (native_p1);
			global::System.Collections.Generic.ICollection<string> __ret = global::Android.Runtime.JavaSet<string>.FromJniHandle (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			JNIEnv.DeleteLocalRef (native_p1);
			return __ret;
		}

		static Delegate cb_logFacetChange_ILjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetLogFacetChange_ILjava_lang_String_Handler ()
		{
			if (cb_logFacetChange_ILjava_lang_String_ == null)
				cb_logFacetChange_ILjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_LogFacetChange_ILjava_lang_String_);
			return cb_logFacetChange_ILjava_lang_String_;
		}

		static void n_LogFacetChange_ILjava_lang_String_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.LogFacetChange (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_logFacetChange_ILjava_lang_String_;
		public unsafe void LogFacetChange (int p0, string p1)
		{
			if (id_logFacetChange_ILjava_lang_String_ == IntPtr.Zero)
				id_logFacetChange_ILjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "logFacetChange", "(ILjava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (native_p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_logFacetChange_ILjava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p1);
		}

		static Delegate cb_queryAllowedProjectionServices_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetQueryAllowedProjectionServices_Landroid_content_Intent_Handler ()
		{
			if (cb_queryAllowedProjectionServices_Landroid_content_Intent_ == null)
				cb_queryAllowedProjectionServices_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_QueryAllowedProjectionServices_Landroid_content_Intent_);
			return cb_queryAllowedProjectionServices_Landroid_content_Intent_;
		}

		static IntPtr n_QueryAllowedProjectionServices_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = global::Android.Runtime.JavaList<global::Android.Content.PM.ResolveInfo>.ToLocalJniHandle (__this.QueryAllowedProjectionServices (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_queryAllowedProjectionServices_Landroid_content_Intent_;
		public unsafe global::System.Collections.Generic.IList<global::Android.Content.PM.ResolveInfo> QueryAllowedProjectionServices (global::Android.Content.Intent p0)
		{
			if (id_queryAllowedProjectionServices_Landroid_content_Intent_ == IntPtr.Zero)
				id_queryAllowedProjectionServices_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "queryAllowedProjectionServices", "(Landroid/content/Intent;)Ljava/util/List;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			global::System.Collections.Generic.IList<global::Android.Content.PM.ResolveInfo> __ret = global::Android.Runtime.JavaList<global::Android.Content.PM.ResolveInfo>.FromJniHandle (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_queryAllowedProjectionServices_Landroid_content_Intent_, __args), JniHandleOwnership.TransferLocalRef);
			return __ret;
		}

		static Delegate cb_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
#pragma warning disable 0169
		static Delegate GetRegisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_Handler ()
		{
			if (cb_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ == null)
				cb_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RegisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_);
			return cb_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
		}

		static void n_RegisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0 = (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegisterCarActivityStartListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
		public unsafe void RegisterCarActivityStartListener (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0)
		{
			if (id_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ == IntPtr.Zero)
				id_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ = JNIEnv.GetMethodID (class_ref, "registerCarActivityStartListener", "(Lcom/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_, __args);
		}

		static Delegate cb_setBooleanCarServiceSetting_Ljava_lang_String_Z;
#pragma warning disable 0169
		static Delegate GetSetBooleanCarServiceSetting_Ljava_lang_String_ZHandler ()
		{
			if (cb_setBooleanCarServiceSetting_Ljava_lang_String_Z == null)
				cb_setBooleanCarServiceSetting_Ljava_lang_String_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, bool>) n_SetBooleanCarServiceSetting_Ljava_lang_String_Z);
			return cb_setBooleanCarServiceSetting_Ljava_lang_String_Z;
		}

		static void n_SetBooleanCarServiceSetting_Ljava_lang_String_Z (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, bool p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetBooleanCarServiceSetting (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_setBooleanCarServiceSetting_Ljava_lang_String_Z;
		public unsafe void SetBooleanCarServiceSetting (string p0, bool p1)
		{
			if (id_setBooleanCarServiceSetting_Ljava_lang_String_Z == IntPtr.Zero)
				id_setBooleanCarServiceSetting_Ljava_lang_String_Z = JNIEnv.GetMethodID (class_ref, "setBooleanCarServiceSetting", "(Ljava/lang/String;Z)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setBooleanCarServiceSetting_Ljava_lang_String_Z, __args);
			JNIEnv.DeleteLocalRef (native_p0);
		}

		static Delegate cb_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_Handler ()
		{
			if (cb_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ == null)
				cb_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_);
			return cb_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
		}

		static void n_SetStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SetStringCarServiceSetting (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_;
		public unsafe void SetStringCarServiceSetting (string p0, string p1)
		{
			if (id_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setStringCarServiceSetting", "(Ljava/lang/String;Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (native_p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setStringCarServiceSetting_Ljava_lang_String_Ljava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
			JNIEnv.DeleteLocalRef (native_p1);
		}

		static Delegate cb_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetSetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_Handler ()
		{
			if (cb_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ == null)
				cb_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_);
			return cb_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
		}

		static void n_SetStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			var p1 = global::Android.Runtime.JavaSet<string>.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SetStringSetCarServiceSetting (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_;
		public unsafe void SetStringSetCarServiceSetting (string p0, global::System.Collections.Generic.ICollection<string> p1)
		{
			if (id_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ == IntPtr.Zero)
				id_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "setStringSetCarServiceSetting", "(Ljava/lang/String;Ljava/util/Set;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (native_p0);
			__args [1] = new JValue (native_p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setStringSetCarServiceSetting_Ljava_lang_String_Ljava_util_Set_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
			JNIEnv.DeleteLocalRef (native_p1);
		}

		static Delegate cb_startCarActivity_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetStartCarActivity_Landroid_content_Intent_Handler ()
		{
			if (cb_startCarActivity_Landroid_content_Intent_ == null)
				cb_startCarActivity_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartCarActivity_Landroid_content_Intent_);
			return cb_startCarActivity_Landroid_content_Intent_;
		}

		static void n_StartCarActivity_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartCarActivity (p0);
		}
#pragma warning restore 0169

		IntPtr id_startCarActivity_Landroid_content_Intent_;
		public unsafe void StartCarActivity (global::Android.Content.Intent p0)
		{
			if (id_startCarActivity_Landroid_content_Intent_ == IntPtr.Zero)
				id_startCarActivity_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "startCarActivity", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startCarActivity_Landroid_content_Intent_, __args);
		}

		static Delegate cb_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
#pragma warning disable 0169
		static Delegate GetUnregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_Handler ()
		{
			if (cb_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ == null)
				cb_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_UnregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_);
			return cb_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
		}

		static void n_UnregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0 = (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterCarActivityStartListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_;
		public unsafe void UnregisterCarActivityStartListener (global::Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListener p0)
		{
			if (id_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ == IntPtr.Zero)
				id_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_ = JNIEnv.GetMethodID (class_ref, "unregisterCarActivityStartListener", "(Lcom/google/android/gms/car/CarFirstPartyManager$CarActivityStartListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterCarActivityStartListener_Lcom_google_android_gms_car_CarFirstPartyManager_CarActivityStartListener_, __args);
		}

	}

}
