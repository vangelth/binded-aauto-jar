using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi.CarConnectionCallback']"
	[Register ("com/google/android/gms/car/CarApi$CarConnectionCallback", "", "Com.Google.Android.Gms.Car.ICarApiCarConnectionCallbackInvoker")]
	public partial interface ICarApiCarConnectionCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi.CarConnectionCallback']/method[@name='onConnected' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onConnected", "(I)V", "GetOnConnected_IHandler:Com.Google.Android.Gms.Car.ICarApiCarConnectionCallbackInvoker, AndroidAutoBindingJar")]
		void OnConnected (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi.CarConnectionCallback']/method[@name='onDisconnected' and count(parameter)=0]"
		[Register ("onDisconnected", "()V", "GetOnDisconnectedHandler:Com.Google.Android.Gms.Car.ICarApiCarConnectionCallbackInvoker, AndroidAutoBindingJar")]
		void OnDisconnected ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarApi$CarConnectionCallback", DoNotGenerateAcw=true)]
	internal class ICarApiCarConnectionCallbackInvoker : global::Java.Lang.Object, ICarApiCarConnectionCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarApi$CarConnectionCallback");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarApiCarConnectionCallbackInvoker); }
		}

		IntPtr class_ref;

		public static ICarApiCarConnectionCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarApiCarConnectionCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarApi.CarConnectionCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarApiCarConnectionCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onConnected_I;
#pragma warning disable 0169
		static Delegate GetOnConnected_IHandler ()
		{
			if (cb_onConnected_I == null)
				cb_onConnected_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnConnected_I);
			return cb_onConnected_I;
		}

		static void n_OnConnected_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnConnected (p0);
		}
#pragma warning restore 0169

		IntPtr id_onConnected_I;
		public unsafe void OnConnected (int p0)
		{
			if (id_onConnected_I == IntPtr.Zero)
				id_onConnected_I = JNIEnv.GetMethodID (class_ref, "onConnected", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnected_I, __args);
		}

		static Delegate cb_onDisconnected;
#pragma warning disable 0169
		static Delegate GetOnDisconnectedHandler ()
		{
			if (cb_onDisconnected == null)
				cb_onDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDisconnected);
			return cb_onDisconnected;
		}

		static void n_OnDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDisconnected ();
		}
#pragma warning restore 0169

		IntPtr id_onDisconnected;
		public unsafe void OnDisconnected ()
		{
			if (id_onDisconnected == IntPtr.Zero)
				id_onDisconnected = JNIEnv.GetMethodID (class_ref, "onDisconnected", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDisconnected);
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']"
	[Register ("com/google/android/gms/car/CarApi", "", "Com.Google.Android.Gms.Car.ICarApiInvoker")]
	public partial interface ICarApi : IJavaObject {

		int CarConnectionType {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='getCarConnectionType' and count(parameter)=0]"
			[Register ("getCarConnectionType", "()I", "GetGetCarConnectionTypeHandler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsConnectedToCar {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='isConnectedToCar' and count(parameter)=0]"
			[Register ("isConnectedToCar", "()Z", "GetIsConnectedToCarHandler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")]
		global::Java.Lang.Object GetCarManager (string p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='getVendorExtensionManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getVendorExtensionManager", "(Ljava/lang/String;)Lcom/google/android/gms/car/CarVendorExtensionManager;", "GetGetVendorExtensionManager_Ljava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager GetVendorExtensionManager (string p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='registerCarConnectionListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarApi.CarConnectionCallback']]"
		[Register ("registerCarConnectionListener", "(Lcom/google/android/gms/car/CarApi$CarConnectionCallback;)V", "GetRegisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_Handler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")]
		void RegisterCarConnectionListener (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApi']/method[@name='unregisterCarConnectionListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarApi.CarConnectionCallback']]"
		[Register ("unregisterCarConnectionListener", "(Lcom/google/android/gms/car/CarApi$CarConnectionCallback;)V", "GetUnregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_Handler:Com.Google.Android.Gms.Car.ICarApiInvoker, AndroidAutoBindingJar")]
		void UnregisterCarConnectionListener (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarApi", DoNotGenerateAcw=true)]
	internal class ICarApiInvoker : global::Java.Lang.Object, ICarApi {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarApi");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarApiInvoker); }
		}

		IntPtr class_ref;

		public static ICarApi GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarApi> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarApi"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarApiInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getCarConnectionType;
#pragma warning disable 0169
		static Delegate GetGetCarConnectionTypeHandler ()
		{
			if (cb_getCarConnectionType == null)
				cb_getCarConnectionType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetCarConnectionType);
			return cb_getCarConnectionType;
		}

		static int n_GetCarConnectionType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CarConnectionType;
		}
#pragma warning restore 0169

		IntPtr id_getCarConnectionType;
		public unsafe int CarConnectionType {
			get {
				if (id_getCarConnectionType == IntPtr.Zero)
					id_getCarConnectionType = JNIEnv.GetMethodID (class_ref, "getCarConnectionType", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getCarConnectionType);
			}
		}

		static Delegate cb_isConnectedToCar;
#pragma warning disable 0169
		static Delegate GetIsConnectedToCarHandler ()
		{
			if (cb_isConnectedToCar == null)
				cb_isConnectedToCar = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConnectedToCar);
			return cb_isConnectedToCar;
		}

		static bool n_IsConnectedToCar (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsConnectedToCar;
		}
#pragma warning restore 0169

		IntPtr id_isConnectedToCar;
		public unsafe bool IsConnectedToCar {
			get {
				if (id_isConnectedToCar == IntPtr.Zero)
					id_isConnectedToCar = JNIEnv.GetMethodID (class_ref, "isConnectedToCar", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConnectedToCar);
			}
		}

		static Delegate cb_getCarManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_String_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_String_ == null)
				cb_getCarManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_String_);
			return cb_getCarManager_Ljava_lang_String_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getCarManager_Ljava_lang_String_;
		public unsafe global::Java.Lang.Object GetCarManager (string p0)
		{
			if (id_getCarManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			global::Java.Lang.Object __ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

		static Delegate cb_getVendorExtensionManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetVendorExtensionManager_Ljava_lang_String_Handler ()
		{
			if (cb_getVendorExtensionManager_Ljava_lang_String_ == null)
				cb_getVendorExtensionManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetVendorExtensionManager_Ljava_lang_String_);
			return cb_getVendorExtensionManager_Ljava_lang_String_;
		}

		static IntPtr n_GetVendorExtensionManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetVendorExtensionManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getVendorExtensionManager_Ljava_lang_String_;
		public unsafe global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager GetVendorExtensionManager (string p0)
		{
			if (id_getVendorExtensionManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getVendorExtensionManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getVendorExtensionManager", "(Ljava/lang/String;)Lcom/google/android/gms/car/CarVendorExtensionManager;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager __ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarVendorExtensionManager> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getVendorExtensionManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

		static Delegate cb_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
#pragma warning disable 0169
		static Delegate GetRegisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_Handler ()
		{
			if (cb_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ == null)
				cb_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RegisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_);
			return cb_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
		}

		static void n_RegisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0 = (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegisterCarConnectionListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
		public unsafe void RegisterCarConnectionListener (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0)
		{
			if (id_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ == IntPtr.Zero)
				id_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ = JNIEnv.GetMethodID (class_ref, "registerCarConnectionListener", "(Lcom/google/android/gms/car/CarApi$CarConnectionCallback;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_, __args);
		}

		static Delegate cb_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
#pragma warning disable 0169
		static Delegate GetUnregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_Handler ()
		{
			if (cb_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ == null)
				cb_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_UnregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_);
			return cb_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
		}

		static void n_UnregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarApi __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0 = (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterCarConnectionListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_;
		public unsafe void UnregisterCarConnectionListener (global::Com.Google.Android.Gms.Car.ICarApiCarConnectionCallback p0)
		{
			if (id_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ == IntPtr.Zero)
				id_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_ = JNIEnv.GetMethodID (class_ref, "unregisterCarConnectionListener", "(Lcom/google/android/gms/car/CarApi$CarConnectionCallback;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterCarConnectionListener_Lcom_google_android_gms_car_CarApi_CarConnectionCallback_, __args);
		}

	}

}
