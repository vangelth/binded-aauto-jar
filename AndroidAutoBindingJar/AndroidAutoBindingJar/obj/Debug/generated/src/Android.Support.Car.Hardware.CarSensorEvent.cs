using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Hardware {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']"
	[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent", DoNotGenerateAcw=true)]
	public partial class CarSensorEvent : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_FULLY_RESTRICTED']"
		[Register ("DRIVE_STATUS_FULLY_RESTRICTED")]
		public const int DriveStatusFullyRestricted = (int) 31;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_LIMIT_MESSAGE_LEN']"
		[Register ("DRIVE_STATUS_LIMIT_MESSAGE_LEN")]
		public const int DriveStatusLimitMessageLen = (int) 16;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_NO_CONFIG']"
		[Register ("DRIVE_STATUS_NO_CONFIG")]
		public const int DriveStatusNoConfig = (int) 8;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_NO_KEYBOARD_INPUT']"
		[Register ("DRIVE_STATUS_NO_KEYBOARD_INPUT")]
		public const int DriveStatusNoKeyboardInput = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_NO_VIDEO']"
		[Register ("DRIVE_STATUS_NO_VIDEO")]
		public const int DriveStatusNoVideo = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_NO_VOICE_INPUT']"
		[Register ("DRIVE_STATUS_NO_VOICE_INPUT")]
		public const int DriveStatusNoVoiceInput = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='DRIVE_STATUS_UNRESTRICTED']"
		[Register ("DRIVE_STATUS_UNRESTRICTED")]
		public const int DriveStatusUnrestricted = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_DRIVE']"
		[Register ("GEAR_DRIVE")]
		public const int GearDrive = (int) 100;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_EIGHTH']"
		[Register ("GEAR_EIGHTH")]
		public const int GearEighth = (int) 8;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_FIFTH']"
		[Register ("GEAR_FIFTH")]
		public const int GearFifth = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_FIRST']"
		[Register ("GEAR_FIRST")]
		public const int GearFirst = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_FOURTH']"
		[Register ("GEAR_FOURTH")]
		public const int GearFourth = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_NEUTRAL']"
		[Register ("GEAR_NEUTRAL")]
		public const int GearNeutral = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_NINTH']"
		[Register ("GEAR_NINTH")]
		public const int GearNinth = (int) 9;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_PARK']"
		[Register ("GEAR_PARK")]
		public const int GearPark = (int) 101;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_REVERSE']"
		[Register ("GEAR_REVERSE")]
		public const int GearReverse = (int) 102;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_SECOND']"
		[Register ("GEAR_SECOND")]
		public const int GearSecond = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_SEVENTH']"
		[Register ("GEAR_SEVENTH")]
		public const int GearSeventh = (int) 7;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_SIXTH']"
		[Register ("GEAR_SIXTH")]
		public const int GearSixth = (int) 6;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_TENTH']"
		[Register ("GEAR_TENTH")]
		public const int GearTenth = (int) 10;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='GEAR_THIRD']"
		[Register ("GEAR_THIRD")]
		public const int GearThird = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_ACCELEROMETER_X']"
		[Register ("INDEX_ACCELEROMETER_X")]
		public const int IndexAccelerometerX = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_ACCELEROMETER_Y']"
		[Register ("INDEX_ACCELEROMETER_Y")]
		public const int IndexAccelerometerY = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_ACCELEROMETER_Z']"
		[Register ("INDEX_ACCELEROMETER_Z")]
		public const int IndexAccelerometerZ = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_COMPASS_BEARING']"
		[Register ("INDEX_COMPASS_BEARING")]
		public const int IndexCompassBearing = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_COMPASS_PITCH']"
		[Register ("INDEX_COMPASS_PITCH")]
		public const int IndexCompassPitch = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_COMPASS_ROLL']"
		[Register ("INDEX_COMPASS_ROLL")]
		public const int IndexCompassRoll = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_ENVIRONMENT_PRESSURE']"
		[Register ("INDEX_ENVIRONMENT_PRESSURE")]
		public const int IndexEnvironmentPressure = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_ENVIRONMENT_TEMPERATURE']"
		[Register ("INDEX_ENVIRONMENT_TEMPERATURE")]
		public const int IndexEnvironmentTemperature = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_FUEL_LEVEL_IN_DISTANCE']"
		[Register ("INDEX_FUEL_LEVEL_IN_DISTANCE")]
		public const int IndexFuelLevelInDistance = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_FUEL_LEVEL_IN_PERCENTILE']"
		[Register ("INDEX_FUEL_LEVEL_IN_PERCENTILE")]
		public const int IndexFuelLevelInPercentile = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_FUEL_LOW_WARNING']"
		[Register ("INDEX_FUEL_LOW_WARNING")]
		public const int IndexFuelLowWarning = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_ARRAY_FLOAT_INTERVAL']"
		[Register ("INDEX_GPS_SATELLITE_ARRAY_FLOAT_INTERVAL")]
		public const int IndexGpsSatelliteArrayFloatInterval = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_ARRAY_FLOAT_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_ARRAY_FLOAT_OFFSET")]
		public const int IndexGpsSatelliteArrayFloatOffset = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_ARRAY_INT_INTERVAL']"
		[Register ("INDEX_GPS_SATELLITE_ARRAY_INT_INTERVAL")]
		public const int IndexGpsSatelliteArrayIntInterval = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_ARRAY_INT_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_ARRAY_INT_OFFSET")]
		public const int IndexGpsSatelliteArrayIntOffset = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_AZIMUTH_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_AZIMUTH_OFFSET")]
		public const int IndexGpsSatelliteAzimuthOffset = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_ELEVATION_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_ELEVATION_OFFSET")]
		public const int IndexGpsSatelliteElevationOffset = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_NUMBER_IN_USE']"
		[Register ("INDEX_GPS_SATELLITE_NUMBER_IN_USE")]
		public const int IndexGpsSatelliteNumberInUse = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_NUMBER_IN_VIEW']"
		[Register ("INDEX_GPS_SATELLITE_NUMBER_IN_VIEW")]
		public const int IndexGpsSatelliteNumberInView = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_PRN_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_PRN_OFFSET")]
		public const int IndexGpsSatellitePrnOffset = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GPS_SATELLITE_SNR_OFFSET']"
		[Register ("INDEX_GPS_SATELLITE_SNR_OFFSET")]
		public const int IndexGpsSatelliteSnrOffset = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GYROSCOPE_X']"
		[Register ("INDEX_GYROSCOPE_X")]
		public const int IndexGyroscopeX = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GYROSCOPE_Y']"
		[Register ("INDEX_GYROSCOPE_Y")]
		public const int IndexGyroscopeY = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_GYROSCOPE_Z']"
		[Register ("INDEX_GYROSCOPE_Z")]
		public const int IndexGyroscopeZ = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_ACCURACY']"
		[Register ("INDEX_LOCATION_ACCURACY")]
		public const int IndexLocationAccuracy = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_ALTITUDE']"
		[Register ("INDEX_LOCATION_ALTITUDE")]
		public const int IndexLocationAltitude = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_BEARING']"
		[Register ("INDEX_LOCATION_BEARING")]
		public const int IndexLocationBearing = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_LATITUDE']"
		[Register ("INDEX_LOCATION_LATITUDE")]
		public const int IndexLocationLatitude = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_LATITUDE_INTS']"
		[Register ("INDEX_LOCATION_LATITUDE_INTS")]
		public const int IndexLocationLatitudeInts = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_LONGITUDE']"
		[Register ("INDEX_LOCATION_LONGITUDE")]
		public const int IndexLocationLongitude = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_LONGITUDE_INTS']"
		[Register ("INDEX_LOCATION_LONGITUDE_INTS")]
		public const int IndexLocationLongitudeInts = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_MAX']"
		[Register ("INDEX_LOCATION_MAX")]
		public const int IndexLocationMax = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_LOCATION_SPEED']"
		[Register ("INDEX_LOCATION_SPEED")]
		public const int IndexLocationSpeed = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_WHEEL_DISTANCE_FRONT_LEFT']"
		[Register ("INDEX_WHEEL_DISTANCE_FRONT_LEFT")]
		public const int IndexWheelDistanceFrontLeft = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_WHEEL_DISTANCE_FRONT_RIGHT']"
		[Register ("INDEX_WHEEL_DISTANCE_FRONT_RIGHT")]
		public const int IndexWheelDistanceFrontRight = (int) 2;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_WHEEL_DISTANCE_REAR_LEFT']"
		[Register ("INDEX_WHEEL_DISTANCE_REAR_LEFT")]
		public const int IndexWheelDistanceRearLeft = (int) 4;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_WHEEL_DISTANCE_REAR_RIGHT']"
		[Register ("INDEX_WHEEL_DISTANCE_REAR_RIGHT")]
		public const int IndexWheelDistanceRearRight = (int) 3;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='INDEX_WHEEL_DISTANCE_RESET_COUNT']"
		[Register ("INDEX_WHEEL_DISTANCE_RESET_COUNT")]
		public const int IndexWheelDistanceResetCount = (int) 0;

		static IntPtr floatValues_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='floatValues']"
		[Register ("floatValues")]
		public IList<float> FloatValues {
			get {
				if (floatValues_jfieldId == IntPtr.Zero)
					floatValues_jfieldId = JNIEnv.GetFieldID (class_ref, "floatValues", "[F");
				return global::Android.Runtime.JavaArray<float>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, floatValues_jfieldId), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (floatValues_jfieldId == IntPtr.Zero)
					floatValues_jfieldId = JNIEnv.GetFieldID (class_ref, "floatValues", "[F");
				IntPtr native_value = global::Android.Runtime.JavaArray<float>.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, floatValues_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr intValues_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='intValues']"
		[Register ("intValues")]
		public IList<int> IntValues {
			get {
				if (intValues_jfieldId == IntPtr.Zero)
					intValues_jfieldId = JNIEnv.GetFieldID (class_ref, "intValues", "[I");
				return global::Android.Runtime.JavaArray<int>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, intValues_jfieldId), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (intValues_jfieldId == IntPtr.Zero)
					intValues_jfieldId = JNIEnv.GetFieldID (class_ref, "intValues", "[I");
				IntPtr native_value = global::Android.Runtime.JavaArray<int>.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, intValues_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr longValues_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='longValues']"
		[Register ("longValues")]
		public IList<long> LongValues {
			get {
				if (longValues_jfieldId == IntPtr.Zero)
					longValues_jfieldId = JNIEnv.GetFieldID (class_ref, "longValues", "[J");
				return global::Android.Runtime.JavaArray<long>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, longValues_jfieldId), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (longValues_jfieldId == IntPtr.Zero)
					longValues_jfieldId = JNIEnv.GetFieldID (class_ref, "longValues", "[J");
				IntPtr native_value = global::Android.Runtime.JavaArray<long>.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, longValues_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr sensorType_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='sensorType']"
		[Register ("sensorType")]
		public int SensorType {
			get {
				if (sensorType_jfieldId == IntPtr.Zero)
					sensorType_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorType", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, sensorType_jfieldId);
			}
			set {
				if (sensorType_jfieldId == IntPtr.Zero)
					sensorType_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorType", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, sensorType_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr timestamp_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/field[@name='timestamp']"
		[Register ("timestamp")]
		public long Timestamp {
			get {
				if (timestamp_jfieldId == IntPtr.Zero)
					timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
				return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
			}
			set {
				if (timestamp_jfieldId == IntPtr.Zero)
					timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
				} finally {
				}
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$AccelerometerData", DoNotGenerateAcw=true)]
		public partial class AccelerometerData : global::Java.Lang.Object {


			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr x_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']/field[@name='x']"
			[Register ("x")]
			public float X {
				get {
					if (x_jfieldId == IntPtr.Zero)
						x_jfieldId = JNIEnv.GetFieldID (class_ref, "x", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, x_jfieldId);
				}
				set {
					if (x_jfieldId == IntPtr.Zero)
						x_jfieldId = JNIEnv.GetFieldID (class_ref, "x", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, x_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr y_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']/field[@name='y']"
			[Register ("y")]
			public float Y {
				get {
					if (y_jfieldId == IntPtr.Zero)
						y_jfieldId = JNIEnv.GetFieldID (class_ref, "y", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, y_jfieldId);
				}
				set {
					if (y_jfieldId == IntPtr.Zero)
						y_jfieldId = JNIEnv.GetFieldID (class_ref, "y", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, y_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr z_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']/field[@name='z']"
			[Register ("z")]
			public float Z {
				get {
					if (z_jfieldId == IntPtr.Zero)
						z_jfieldId = JNIEnv.GetFieldID (class_ref, "z", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, z_jfieldId);
				}
				set {
					if (z_jfieldId == IntPtr.Zero)
						z_jfieldId = JNIEnv.GetFieldID (class_ref, "z", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, z_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$AccelerometerData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (AccelerometerData); }
			}

			protected AccelerometerData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JFFF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.AccelerometerData']/constructor[@name='CarSensorEvent.AccelerometerData' and count(parameter)=4 and parameter[1][@type='long'] and parameter[2][@type='float'] and parameter[3][@type='float'] and parameter[4][@type='float']]"
			[Register (".ctor", "(JFFF)V", "")]
			public unsafe AccelerometerData (long p0, float p1, float p2, float p3)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [4];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (p3);
					if (((object) this).GetType () != typeof (AccelerometerData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JFFF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JFFF)V", __args);
						return;
					}

					if (id_ctor_JFFF == IntPtr.Zero)
						id_ctor_JFFF = JNIEnv.GetMethodID (class_ref, "<init>", "(JFFF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JFFF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JFFF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarAbsActiveData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$CarAbsActiveData", DoNotGenerateAcw=true)]
		public partial class CarAbsActiveData : global::Java.Lang.Object {


			static IntPtr absIsActive_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarAbsActiveData']/field[@name='absIsActive']"
			[Register ("absIsActive")]
			public bool AbsIsActive {
				get {
					if (absIsActive_jfieldId == IntPtr.Zero)
						absIsActive_jfieldId = JNIEnv.GetFieldID (class_ref, "absIsActive", "Z");
					return JNIEnv.GetBooleanField (((global::Java.Lang.Object) this).Handle, absIsActive_jfieldId);
				}
				set {
					if (absIsActive_jfieldId == IntPtr.Zero)
						absIsActive_jfieldId = JNIEnv.GetFieldID (class_ref, "absIsActive", "Z");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, absIsActive_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarAbsActiveData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$CarAbsActiveData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarAbsActiveData); }
			}

			protected CarAbsActiveData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JZ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarAbsActiveData']/constructor[@name='CarSensorEvent.CarAbsActiveData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='boolean']]"
			[Register (".ctor", "(JZ)V", "")]
			public unsafe CarAbsActiveData (long p0, bool p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (CarAbsActiveData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JZ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JZ)V", __args);
						return;
					}

					if (id_ctor_JZ == IntPtr.Zero)
						id_ctor_JZ = JNIEnv.GetMethodID (class_ref, "<init>", "(JZ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JZ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JZ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarSpeedData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$CarSpeedData", DoNotGenerateAcw=true)]
		public partial class CarSpeedData : global::Java.Lang.Object {


			static IntPtr carSpeed_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarSpeedData']/field[@name='carSpeed']"
			[Register ("carSpeed")]
			public float CarSpeed {
				get {
					if (carSpeed_jfieldId == IntPtr.Zero)
						carSpeed_jfieldId = JNIEnv.GetFieldID (class_ref, "carSpeed", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, carSpeed_jfieldId);
				}
				set {
					if (carSpeed_jfieldId == IntPtr.Zero)
						carSpeed_jfieldId = JNIEnv.GetFieldID (class_ref, "carSpeed", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, carSpeed_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarSpeedData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$CarSpeedData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarSpeedData); }
			}

			protected CarSpeedData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarSpeedData']/constructor[@name='CarSensorEvent.CarSpeedData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='float']]"
			[Register (".ctor", "(JF)V", "")]
			public unsafe CarSpeedData (long p0, float p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (CarSpeedData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JF)V", __args);
						return;
					}

					if (id_ctor_JF == IntPtr.Zero)
						id_ctor_JF = JNIEnv.GetMethodID (class_ref, "<init>", "(JF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarTractionControlActiveData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$CarTractionControlActiveData", DoNotGenerateAcw=true)]
		public partial class CarTractionControlActiveData : global::Java.Lang.Object {


			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarTractionControlActiveData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr tractionControlIsActive_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarTractionControlActiveData']/field[@name='tractionControlIsActive']"
			[Register ("tractionControlIsActive")]
			public bool TractionControlIsActive {
				get {
					if (tractionControlIsActive_jfieldId == IntPtr.Zero)
						tractionControlIsActive_jfieldId = JNIEnv.GetFieldID (class_ref, "tractionControlIsActive", "Z");
					return JNIEnv.GetBooleanField (((global::Java.Lang.Object) this).Handle, tractionControlIsActive_jfieldId);
				}
				set {
					if (tractionControlIsActive_jfieldId == IntPtr.Zero)
						tractionControlIsActive_jfieldId = JNIEnv.GetFieldID (class_ref, "tractionControlIsActive", "Z");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, tractionControlIsActive_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$CarTractionControlActiveData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarTractionControlActiveData); }
			}

			protected CarTractionControlActiveData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JZ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarTractionControlActiveData']/constructor[@name='CarSensorEvent.CarTractionControlActiveData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='boolean']]"
			[Register (".ctor", "(JZ)V", "")]
			public unsafe CarTractionControlActiveData (long p0, bool p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (CarTractionControlActiveData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JZ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JZ)V", __args);
						return;
					}

					if (id_ctor_JZ == IntPtr.Zero)
						id_ctor_JZ = JNIEnv.GetMethodID (class_ref, "<init>", "(JZ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JZ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JZ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$CarWheelTickDistanceData", DoNotGenerateAcw=true)]
		public partial class CarWheelTickDistanceData : global::Java.Lang.Object {


			static IntPtr frontLeftWheelDistanceMm_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='frontLeftWheelDistanceMm']"
			[Register ("frontLeftWheelDistanceMm")]
			public long FrontLeftWheelDistanceMm {
				get {
					if (frontLeftWheelDistanceMm_jfieldId == IntPtr.Zero)
						frontLeftWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "frontLeftWheelDistanceMm", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, frontLeftWheelDistanceMm_jfieldId);
				}
				set {
					if (frontLeftWheelDistanceMm_jfieldId == IntPtr.Zero)
						frontLeftWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "frontLeftWheelDistanceMm", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, frontLeftWheelDistanceMm_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr frontRightWheelDistanceMm_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='frontRightWheelDistanceMm']"
			[Register ("frontRightWheelDistanceMm")]
			public long FrontRightWheelDistanceMm {
				get {
					if (frontRightWheelDistanceMm_jfieldId == IntPtr.Zero)
						frontRightWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "frontRightWheelDistanceMm", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, frontRightWheelDistanceMm_jfieldId);
				}
				set {
					if (frontRightWheelDistanceMm_jfieldId == IntPtr.Zero)
						frontRightWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "frontRightWheelDistanceMm", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, frontRightWheelDistanceMm_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr rearLeftWheelDistanceMm_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='rearLeftWheelDistanceMm']"
			[Register ("rearLeftWheelDistanceMm")]
			public long RearLeftWheelDistanceMm {
				get {
					if (rearLeftWheelDistanceMm_jfieldId == IntPtr.Zero)
						rearLeftWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "rearLeftWheelDistanceMm", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, rearLeftWheelDistanceMm_jfieldId);
				}
				set {
					if (rearLeftWheelDistanceMm_jfieldId == IntPtr.Zero)
						rearLeftWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "rearLeftWheelDistanceMm", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, rearLeftWheelDistanceMm_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr rearRightWheelDistanceMm_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='rearRightWheelDistanceMm']"
			[Register ("rearRightWheelDistanceMm")]
			public long RearRightWheelDistanceMm {
				get {
					if (rearRightWheelDistanceMm_jfieldId == IntPtr.Zero)
						rearRightWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "rearRightWheelDistanceMm", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, rearRightWheelDistanceMm_jfieldId);
				}
				set {
					if (rearRightWheelDistanceMm_jfieldId == IntPtr.Zero)
						rearRightWheelDistanceMm_jfieldId = JNIEnv.GetFieldID (class_ref, "rearRightWheelDistanceMm", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, rearRightWheelDistanceMm_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr sensorResetCount_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='sensorResetCount']"
			[Register ("sensorResetCount")]
			public long SensorResetCount {
				get {
					if (sensorResetCount_jfieldId == IntPtr.Zero)
						sensorResetCount_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorResetCount", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, sensorResetCount_jfieldId);
				}
				set {
					if (sensorResetCount_jfieldId == IntPtr.Zero)
						sensorResetCount_jfieldId = JNIEnv.GetFieldID (class_ref, "sensorResetCount", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, sensorResetCount_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$CarWheelTickDistanceData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CarWheelTickDistanceData); }
			}

			protected CarWheelTickDistanceData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JJJJJJ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CarWheelTickDistanceData']/constructor[@name='CarSensorEvent.CarWheelTickDistanceData' and count(parameter)=6 and parameter[1][@type='long'] and parameter[2][@type='long'] and parameter[3][@type='long'] and parameter[4][@type='long'] and parameter[5][@type='long'] and parameter[6][@type='long']]"
			[Register (".ctor", "(JJJJJJ)V", "")]
			public unsafe CarWheelTickDistanceData (long p0, long p1, long p2, long p3, long p4, long p5)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [6];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (p3);
					__args [4] = new JValue (p4);
					__args [5] = new JValue (p5);
					if (((object) this).GetType () != typeof (CarWheelTickDistanceData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JJJJJJ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JJJJJJ)V", __args);
						return;
					}

					if (id_ctor_JJJJJJ == IntPtr.Zero)
						id_ctor_JJJJJJ = JNIEnv.GetMethodID (class_ref, "<init>", "(JJJJJJ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JJJJJJ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JJJJJJ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$CompassData", DoNotGenerateAcw=true)]
		public partial class CompassData : global::Java.Lang.Object {


			static IntPtr bearing_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']/field[@name='bearing']"
			[Register ("bearing")]
			public float Bearing {
				get {
					if (bearing_jfieldId == IntPtr.Zero)
						bearing_jfieldId = JNIEnv.GetFieldID (class_ref, "bearing", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, bearing_jfieldId);
				}
				set {
					if (bearing_jfieldId == IntPtr.Zero)
						bearing_jfieldId = JNIEnv.GetFieldID (class_ref, "bearing", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, bearing_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr pitch_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']/field[@name='pitch']"
			[Register ("pitch")]
			public float Pitch {
				get {
					if (pitch_jfieldId == IntPtr.Zero)
						pitch_jfieldId = JNIEnv.GetFieldID (class_ref, "pitch", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, pitch_jfieldId);
				}
				set {
					if (pitch_jfieldId == IntPtr.Zero)
						pitch_jfieldId = JNIEnv.GetFieldID (class_ref, "pitch", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, pitch_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr roll_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']/field[@name='roll']"
			[Register ("roll")]
			public float Roll {
				get {
					if (roll_jfieldId == IntPtr.Zero)
						roll_jfieldId = JNIEnv.GetFieldID (class_ref, "roll", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, roll_jfieldId);
				}
				set {
					if (roll_jfieldId == IntPtr.Zero)
						roll_jfieldId = JNIEnv.GetFieldID (class_ref, "roll", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, roll_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$CompassData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CompassData); }
			}

			protected CompassData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JFFF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.CompassData']/constructor[@name='CarSensorEvent.CompassData' and count(parameter)=4 and parameter[1][@type='long'] and parameter[2][@type='float'] and parameter[3][@type='float'] and parameter[4][@type='float']]"
			[Register (".ctor", "(JFFF)V", "")]
			public unsafe CompassData (long p0, float p1, float p2, float p3)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [4];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (p3);
					if (((object) this).GetType () != typeof (CompassData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JFFF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JFFF)V", __args);
						return;
					}

					if (id_ctor_JFFF == IntPtr.Zero)
						id_ctor_JFFF = JNIEnv.GetMethodID (class_ref, "<init>", "(JFFF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JFFF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JFFF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$DrivingStatusData", DoNotGenerateAcw=true)]
		public partial class DrivingStatusData : global::Java.Lang.Object {


			static IntPtr status_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/field[@name='status']"
			[Register ("status")]
			public int Status {
				get {
					if (status_jfieldId == IntPtr.Zero)
						status_jfieldId = JNIEnv.GetFieldID (class_ref, "status", "I");
					return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, status_jfieldId);
				}
				set {
					if (status_jfieldId == IntPtr.Zero)
						status_jfieldId = JNIEnv.GetFieldID (class_ref, "status", "I");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, status_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$DrivingStatusData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (DrivingStatusData); }
			}

			protected DrivingStatusData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JI;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/constructor[@name='CarSensorEvent.DrivingStatusData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='int']]"
			[Register (".ctor", "(JI)V", "")]
			public unsafe DrivingStatusData (long p0, int p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (DrivingStatusData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JI)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JI)V", __args);
						return;
					}

					if (id_ctor_JI == IntPtr.Zero)
						id_ctor_JI = JNIEnv.GetMethodID (class_ref, "<init>", "(JI)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JI, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JI, __args);
				} finally {
				}
			}

			static Delegate cb_isConfigurationRestricted;
#pragma warning disable 0169
			static Delegate GetIsConfigurationRestrictedHandler ()
			{
				if (cb_isConfigurationRestricted == null)
					cb_isConfigurationRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsConfigurationRestricted);
				return cb_isConfigurationRestricted;
			}

			static bool n_IsConfigurationRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsConfigurationRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isConfigurationRestricted;
			public virtual unsafe bool IsConfigurationRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isConfigurationRestricted' and count(parameter)=0]"
				[Register ("isConfigurationRestricted", "()Z", "GetIsConfigurationRestrictedHandler")]
				get {
					if (id_isConfigurationRestricted == IntPtr.Zero)
						id_isConfigurationRestricted = JNIEnv.GetMethodID (class_ref, "isConfigurationRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isConfigurationRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isConfigurationRestricted", "()Z"));
					} finally {
					}
				}
			}

			static Delegate cb_isFullyRestricted;
#pragma warning disable 0169
			static Delegate GetIsFullyRestrictedHandler ()
			{
				if (cb_isFullyRestricted == null)
					cb_isFullyRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsFullyRestricted);
				return cb_isFullyRestricted;
			}

			static bool n_IsFullyRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsFullyRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isFullyRestricted;
			public virtual unsafe bool IsFullyRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isFullyRestricted' and count(parameter)=0]"
				[Register ("isFullyRestricted", "()Z", "GetIsFullyRestrictedHandler")]
				get {
					if (id_isFullyRestricted == IntPtr.Zero)
						id_isFullyRestricted = JNIEnv.GetMethodID (class_ref, "isFullyRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isFullyRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isFullyRestricted", "()Z"));
					} finally {
					}
				}
			}

			static Delegate cb_isKeyboardRestricted;
#pragma warning disable 0169
			static Delegate GetIsKeyboardRestrictedHandler ()
			{
				if (cb_isKeyboardRestricted == null)
					cb_isKeyboardRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsKeyboardRestricted);
				return cb_isKeyboardRestricted;
			}

			static bool n_IsKeyboardRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsKeyboardRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isKeyboardRestricted;
			public virtual unsafe bool IsKeyboardRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isKeyboardRestricted' and count(parameter)=0]"
				[Register ("isKeyboardRestricted", "()Z", "GetIsKeyboardRestrictedHandler")]
				get {
					if (id_isKeyboardRestricted == IntPtr.Zero)
						id_isKeyboardRestricted = JNIEnv.GetMethodID (class_ref, "isKeyboardRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isKeyboardRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isKeyboardRestricted", "()Z"));
					} finally {
					}
				}
			}

			static Delegate cb_isMessageLengthRestricted;
#pragma warning disable 0169
			static Delegate GetIsMessageLengthRestrictedHandler ()
			{
				if (cb_isMessageLengthRestricted == null)
					cb_isMessageLengthRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsMessageLengthRestricted);
				return cb_isMessageLengthRestricted;
			}

			static bool n_IsMessageLengthRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsMessageLengthRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isMessageLengthRestricted;
			public virtual unsafe bool IsMessageLengthRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isMessageLengthRestricted' and count(parameter)=0]"
				[Register ("isMessageLengthRestricted", "()Z", "GetIsMessageLengthRestrictedHandler")]
				get {
					if (id_isMessageLengthRestricted == IntPtr.Zero)
						id_isMessageLengthRestricted = JNIEnv.GetMethodID (class_ref, "isMessageLengthRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isMessageLengthRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isMessageLengthRestricted", "()Z"));
					} finally {
					}
				}
			}

			static Delegate cb_isVideoRestricted;
#pragma warning disable 0169
			static Delegate GetIsVideoRestrictedHandler ()
			{
				if (cb_isVideoRestricted == null)
					cb_isVideoRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsVideoRestricted);
				return cb_isVideoRestricted;
			}

			static bool n_IsVideoRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsVideoRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isVideoRestricted;
			public virtual unsafe bool IsVideoRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isVideoRestricted' and count(parameter)=0]"
				[Register ("isVideoRestricted", "()Z", "GetIsVideoRestrictedHandler")]
				get {
					if (id_isVideoRestricted == IntPtr.Zero)
						id_isVideoRestricted = JNIEnv.GetMethodID (class_ref, "isVideoRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isVideoRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isVideoRestricted", "()Z"));
					} finally {
					}
				}
			}

			static Delegate cb_isVoiceRestricted;
#pragma warning disable 0169
			static Delegate GetIsVoiceRestrictedHandler ()
			{
				if (cb_isVoiceRestricted == null)
					cb_isVoiceRestricted = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsVoiceRestricted);
				return cb_isVoiceRestricted;
			}

			static bool n_IsVoiceRestricted (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.IsVoiceRestricted;
			}
#pragma warning restore 0169

			static IntPtr id_isVoiceRestricted;
			public virtual unsafe bool IsVoiceRestricted {
				// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.DrivingStatusData']/method[@name='isVoiceRestricted' and count(parameter)=0]"
				[Register ("isVoiceRestricted", "()Z", "GetIsVoiceRestrictedHandler")]
				get {
					if (id_isVoiceRestricted == IntPtr.Zero)
						id_isVoiceRestricted = JNIEnv.GetMethodID (class_ref, "isVoiceRestricted", "()Z");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isVoiceRestricted);
						else
							return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isVoiceRestricted", "()Z"));
					} finally {
					}
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.EnvironmentData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$EnvironmentData", DoNotGenerateAcw=true)]
		public partial class EnvironmentData : global::Java.Lang.Object {


			static IntPtr pressure_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.EnvironmentData']/field[@name='pressure']"
			[Register ("pressure")]
			public float Pressure {
				get {
					if (pressure_jfieldId == IntPtr.Zero)
						pressure_jfieldId = JNIEnv.GetFieldID (class_ref, "pressure", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, pressure_jfieldId);
				}
				set {
					if (pressure_jfieldId == IntPtr.Zero)
						pressure_jfieldId = JNIEnv.GetFieldID (class_ref, "pressure", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, pressure_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr temperature_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.EnvironmentData']/field[@name='temperature']"
			[Register ("temperature")]
			public float Temperature {
				get {
					if (temperature_jfieldId == IntPtr.Zero)
						temperature_jfieldId = JNIEnv.GetFieldID (class_ref, "temperature", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, temperature_jfieldId);
				}
				set {
					if (temperature_jfieldId == IntPtr.Zero)
						temperature_jfieldId = JNIEnv.GetFieldID (class_ref, "temperature", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, temperature_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.EnvironmentData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$EnvironmentData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (EnvironmentData); }
			}

			protected EnvironmentData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JFF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.EnvironmentData']/constructor[@name='CarSensorEvent.EnvironmentData' and count(parameter)=3 and parameter[1][@type='long'] and parameter[2][@type='float'] and parameter[3][@type='float']]"
			[Register (".ctor", "(JFF)V", "")]
			public unsafe EnvironmentData (long p0, float p1, float p2)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [3];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					if (((object) this).GetType () != typeof (EnvironmentData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JFF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JFF)V", __args);
						return;
					}

					if (id_ctor_JFF == IntPtr.Zero)
						id_ctor_JFF = JNIEnv.GetMethodID (class_ref, "<init>", "(JFF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JFF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JFF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$FuelLevelData", DoNotGenerateAcw=true)]
		public partial class FuelLevelData : global::Java.Lang.Object {


			static IntPtr level_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']/field[@name='level']"
			[Register ("level")]
			public int Level {
				get {
					if (level_jfieldId == IntPtr.Zero)
						level_jfieldId = JNIEnv.GetFieldID (class_ref, "level", "I");
					return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, level_jfieldId);
				}
				set {
					if (level_jfieldId == IntPtr.Zero)
						level_jfieldId = JNIEnv.GetFieldID (class_ref, "level", "I");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, level_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr lowFuelWarning_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']/field[@name='lowFuelWarning']"
			[Register ("lowFuelWarning")]
			public bool LowFuelWarning {
				get {
					if (lowFuelWarning_jfieldId == IntPtr.Zero)
						lowFuelWarning_jfieldId = JNIEnv.GetFieldID (class_ref, "lowFuelWarning", "Z");
					return JNIEnv.GetBooleanField (((global::Java.Lang.Object) this).Handle, lowFuelWarning_jfieldId);
				}
				set {
					if (lowFuelWarning_jfieldId == IntPtr.Zero)
						lowFuelWarning_jfieldId = JNIEnv.GetFieldID (class_ref, "lowFuelWarning", "Z");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, lowFuelWarning_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr range_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']/field[@name='range']"
			[Register ("range")]
			public float Range {
				get {
					if (range_jfieldId == IntPtr.Zero)
						range_jfieldId = JNIEnv.GetFieldID (class_ref, "range", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, range_jfieldId);
				}
				set {
					if (range_jfieldId == IntPtr.Zero)
						range_jfieldId = JNIEnv.GetFieldID (class_ref, "range", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, range_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$FuelLevelData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (FuelLevelData); }
			}

			protected FuelLevelData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JIFZ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.FuelLevelData']/constructor[@name='CarSensorEvent.FuelLevelData' and count(parameter)=4 and parameter[1][@type='long'] and parameter[2][@type='int'] and parameter[3][@type='float'] and parameter[4][@type='boolean']]"
			[Register (".ctor", "(JIFZ)V", "")]
			public unsafe FuelLevelData (long p0, int p1, float p2, bool p3)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [4];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (p3);
					if (((object) this).GetType () != typeof (FuelLevelData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JIFZ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JIFZ)V", __args);
						return;
					}

					if (id_ctor_JIFZ == IntPtr.Zero)
						id_ctor_JIFZ = JNIEnv.GetMethodID (class_ref, "<init>", "(JIFZ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JIFZ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JIFZ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GearData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$GearData", DoNotGenerateAcw=true)]
		public partial class GearData : global::Java.Lang.Object {


			static IntPtr gear_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GearData']/field[@name='gear']"
			[Register ("gear")]
			public int Gear {
				get {
					if (gear_jfieldId == IntPtr.Zero)
						gear_jfieldId = JNIEnv.GetFieldID (class_ref, "gear", "I");
					return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, gear_jfieldId);
				}
				set {
					if (gear_jfieldId == IntPtr.Zero)
						gear_jfieldId = JNIEnv.GetFieldID (class_ref, "gear", "I");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, gear_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GearData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$GearData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (GearData); }
			}

			protected GearData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JI;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GearData']/constructor[@name='CarSensorEvent.GearData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='int']]"
			[Register (".ctor", "(JI)V", "")]
			public unsafe GearData (long p0, int p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (GearData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JI)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JI)V", __args);
						return;
					}

					if (id_ctor_JI == IntPtr.Zero)
						id_ctor_JI = JNIEnv.GetMethodID (class_ref, "<init>", "(JI)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JI, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JI, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$GpsSatelliteData", DoNotGenerateAcw=true)]
		public partial class GpsSatelliteData : global::Java.Lang.Object {


			static IntPtr azimuth_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='azimuth']"
			[Register ("azimuth")]
			public IList<float> Azimuth {
				get {
					if (azimuth_jfieldId == IntPtr.Zero)
						azimuth_jfieldId = JNIEnv.GetFieldID (class_ref, "azimuth", "[F");
					return global::Android.Runtime.JavaArray<float>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, azimuth_jfieldId), JniHandleOwnership.TransferLocalRef);
				}
				set {
					if (azimuth_jfieldId == IntPtr.Zero)
						azimuth_jfieldId = JNIEnv.GetFieldID (class_ref, "azimuth", "[F");
					IntPtr native_value = global::Android.Runtime.JavaArray<float>.ToLocalJniHandle (value);
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, azimuth_jfieldId, native_value);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static IntPtr elevation_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='elevation']"
			[Register ("elevation")]
			public IList<float> Elevation {
				get {
					if (elevation_jfieldId == IntPtr.Zero)
						elevation_jfieldId = JNIEnv.GetFieldID (class_ref, "elevation", "[F");
					return global::Android.Runtime.JavaArray<float>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, elevation_jfieldId), JniHandleOwnership.TransferLocalRef);
				}
				set {
					if (elevation_jfieldId == IntPtr.Zero)
						elevation_jfieldId = JNIEnv.GetFieldID (class_ref, "elevation", "[F");
					IntPtr native_value = global::Android.Runtime.JavaArray<float>.ToLocalJniHandle (value);
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, elevation_jfieldId, native_value);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static IntPtr numberInUse_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='numberInUse']"
			[Register ("numberInUse")]
			public int NumberInUse {
				get {
					if (numberInUse_jfieldId == IntPtr.Zero)
						numberInUse_jfieldId = JNIEnv.GetFieldID (class_ref, "numberInUse", "I");
					return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, numberInUse_jfieldId);
				}
				set {
					if (numberInUse_jfieldId == IntPtr.Zero)
						numberInUse_jfieldId = JNIEnv.GetFieldID (class_ref, "numberInUse", "I");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, numberInUse_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr numberInView_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='numberInView']"
			[Register ("numberInView")]
			public int NumberInView {
				get {
					if (numberInView_jfieldId == IntPtr.Zero)
						numberInView_jfieldId = JNIEnv.GetFieldID (class_ref, "numberInView", "I");
					return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, numberInView_jfieldId);
				}
				set {
					if (numberInView_jfieldId == IntPtr.Zero)
						numberInView_jfieldId = JNIEnv.GetFieldID (class_ref, "numberInView", "I");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, numberInView_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr prn_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='prn']"
			[Register ("prn")]
			public IList<int> Prn {
				get {
					if (prn_jfieldId == IntPtr.Zero)
						prn_jfieldId = JNIEnv.GetFieldID (class_ref, "prn", "[I");
					return global::Android.Runtime.JavaArray<int>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, prn_jfieldId), JniHandleOwnership.TransferLocalRef);
				}
				set {
					if (prn_jfieldId == IntPtr.Zero)
						prn_jfieldId = JNIEnv.GetFieldID (class_ref, "prn", "[I");
					IntPtr native_value = global::Android.Runtime.JavaArray<int>.ToLocalJniHandle (value);
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, prn_jfieldId, native_value);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static IntPtr snr_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='snr']"
			[Register ("snr")]
			public IList<float> Snr {
				get {
					if (snr_jfieldId == IntPtr.Zero)
						snr_jfieldId = JNIEnv.GetFieldID (class_ref, "snr", "[F");
					return global::Android.Runtime.JavaArray<float>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, snr_jfieldId), JniHandleOwnership.TransferLocalRef);
				}
				set {
					if (snr_jfieldId == IntPtr.Zero)
						snr_jfieldId = JNIEnv.GetFieldID (class_ref, "snr", "[F");
					IntPtr native_value = global::Android.Runtime.JavaArray<float>.ToLocalJniHandle (value);
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, snr_jfieldId, native_value);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr usedInFix_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/field[@name='usedInFix']"
			[Register ("usedInFix")]
			public IList<bool> UsedInFix {
				get {
					if (usedInFix_jfieldId == IntPtr.Zero)
						usedInFix_jfieldId = JNIEnv.GetFieldID (class_ref, "usedInFix", "[Z");
					return global::Android.Runtime.JavaArray<bool>.FromJniHandle (JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, usedInFix_jfieldId), JniHandleOwnership.TransferLocalRef);
				}
				set {
					if (usedInFix_jfieldId == IntPtr.Zero)
						usedInFix_jfieldId = JNIEnv.GetFieldID (class_ref, "usedInFix", "[Z");
					IntPtr native_value = global::Android.Runtime.JavaArray<bool>.ToLocalJniHandle (value);
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, usedInFix_jfieldId, native_value);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$GpsSatelliteData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (GpsSatelliteData); }
			}

			protected GpsSatelliteData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JIIarrayZarrayIarrayFarrayFarrayF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GpsSatelliteData']/constructor[@name='CarSensorEvent.GpsSatelliteData' and count(parameter)=8 and parameter[1][@type='long'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='boolean[]'] and parameter[5][@type='int[]'] and parameter[6][@type='float[]'] and parameter[7][@type='float[]'] and parameter[8][@type='float[]']]"
			[Register (".ctor", "(JII[Z[I[F[F[F)V", "")]
			public unsafe GpsSatelliteData (long p0, int p1, int p2, bool[] p3, int[] p4, float[] p5, float[] p6, float[] p7)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				IntPtr native_p3 = JNIEnv.NewArray (p3);
				IntPtr native_p4 = JNIEnv.NewArray (p4);
				IntPtr native_p5 = JNIEnv.NewArray (p5);
				IntPtr native_p6 = JNIEnv.NewArray (p6);
				IntPtr native_p7 = JNIEnv.NewArray (p7);
				try {
					JValue* __args = stackalloc JValue [8];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (native_p3);
					__args [4] = new JValue (native_p4);
					__args [5] = new JValue (native_p5);
					__args [6] = new JValue (native_p6);
					__args [7] = new JValue (native_p7);
					if (((object) this).GetType () != typeof (GpsSatelliteData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JII[Z[I[F[F[F)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JII[Z[I[F[F[F)V", __args);
						return;
					}

					if (id_ctor_JIIarrayZarrayIarrayFarrayFarrayF == IntPtr.Zero)
						id_ctor_JIIarrayZarrayIarrayFarrayFarrayF = JNIEnv.GetMethodID (class_ref, "<init>", "(JII[Z[I[F[F[F)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JIIarrayZarrayIarrayFarrayFarrayF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JIIarrayZarrayIarrayFarrayFarrayF, __args);
				} finally {
					if (p3 != null) {
						JNIEnv.CopyArray (native_p3, p3);
						JNIEnv.DeleteLocalRef (native_p3);
					}
					if (p4 != null) {
						JNIEnv.CopyArray (native_p4, p4);
						JNIEnv.DeleteLocalRef (native_p4);
					}
					if (p5 != null) {
						JNIEnv.CopyArray (native_p5, p5);
						JNIEnv.DeleteLocalRef (native_p5);
					}
					if (p6 != null) {
						JNIEnv.CopyArray (native_p6, p6);
						JNIEnv.DeleteLocalRef (native_p6);
					}
					if (p7 != null) {
						JNIEnv.CopyArray (native_p7, p7);
						JNIEnv.DeleteLocalRef (native_p7);
					}
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$GyroscopeData", DoNotGenerateAcw=true)]
		public partial class GyroscopeData : global::Java.Lang.Object {


			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr x_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']/field[@name='x']"
			[Register ("x")]
			public float X {
				get {
					if (x_jfieldId == IntPtr.Zero)
						x_jfieldId = JNIEnv.GetFieldID (class_ref, "x", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, x_jfieldId);
				}
				set {
					if (x_jfieldId == IntPtr.Zero)
						x_jfieldId = JNIEnv.GetFieldID (class_ref, "x", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, x_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr y_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']/field[@name='y']"
			[Register ("y")]
			public float Y {
				get {
					if (y_jfieldId == IntPtr.Zero)
						y_jfieldId = JNIEnv.GetFieldID (class_ref, "y", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, y_jfieldId);
				}
				set {
					if (y_jfieldId == IntPtr.Zero)
						y_jfieldId = JNIEnv.GetFieldID (class_ref, "y", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, y_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr z_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']/field[@name='z']"
			[Register ("z")]
			public float Z {
				get {
					if (z_jfieldId == IntPtr.Zero)
						z_jfieldId = JNIEnv.GetFieldID (class_ref, "z", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, z_jfieldId);
				}
				set {
					if (z_jfieldId == IntPtr.Zero)
						z_jfieldId = JNIEnv.GetFieldID (class_ref, "z", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, z_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$GyroscopeData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (GyroscopeData); }
			}

			protected GyroscopeData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JFFF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.GyroscopeData']/constructor[@name='CarSensorEvent.GyroscopeData' and count(parameter)=4 and parameter[1][@type='long'] and parameter[2][@type='float'] and parameter[3][@type='float'] and parameter[4][@type='float']]"
			[Register (".ctor", "(JFFF)V", "")]
			public unsafe GyroscopeData (long p0, float p1, float p2, float p3)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [4];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					__args [2] = new JValue (p2);
					__args [3] = new JValue (p3);
					if (((object) this).GetType () != typeof (GyroscopeData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JFFF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JFFF)V", __args);
						return;
					}

					if (id_ctor_JFFF == IntPtr.Zero)
						id_ctor_JFFF = JNIEnv.GetMethodID (class_ref, "<init>", "(JFFF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JFFF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JFFF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.NightData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$NightData", DoNotGenerateAcw=true)]
		public partial class NightData : global::Java.Lang.Object {


			static IntPtr isNightMode_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.NightData']/field[@name='isNightMode']"
			[Register ("isNightMode")]
			public bool IsNightMode {
				get {
					if (isNightMode_jfieldId == IntPtr.Zero)
						isNightMode_jfieldId = JNIEnv.GetFieldID (class_ref, "isNightMode", "Z");
					return JNIEnv.GetBooleanField (((global::Java.Lang.Object) this).Handle, isNightMode_jfieldId);
				}
				set {
					if (isNightMode_jfieldId == IntPtr.Zero)
						isNightMode_jfieldId = JNIEnv.GetFieldID (class_ref, "isNightMode", "Z");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, isNightMode_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.NightData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$NightData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (NightData); }
			}

			protected NightData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JZ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.NightData']/constructor[@name='CarSensorEvent.NightData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='boolean']]"
			[Register (".ctor", "(JZ)V", "")]
			public unsafe NightData (long p0, bool p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (NightData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JZ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JZ)V", __args);
						return;
					}

					if (id_ctor_JZ == IntPtr.Zero)
						id_ctor_JZ = JNIEnv.GetMethodID (class_ref, "<init>", "(JZ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JZ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JZ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.OdometerData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$OdometerData", DoNotGenerateAcw=true)]
		public partial class OdometerData : global::Java.Lang.Object {


			static IntPtr kms_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.OdometerData']/field[@name='kms']"
			[Register ("kms")]
			public float Kms {
				get {
					if (kms_jfieldId == IntPtr.Zero)
						kms_jfieldId = JNIEnv.GetFieldID (class_ref, "kms", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, kms_jfieldId);
				}
				set {
					if (kms_jfieldId == IntPtr.Zero)
						kms_jfieldId = JNIEnv.GetFieldID (class_ref, "kms", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, kms_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.OdometerData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$OdometerData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (OdometerData); }
			}

			protected OdometerData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.OdometerData']/constructor[@name='CarSensorEvent.OdometerData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='float']]"
			[Register (".ctor", "(JF)V", "")]
			public unsafe OdometerData (long p0, float p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (OdometerData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JF)V", __args);
						return;
					}

					if (id_ctor_JF == IntPtr.Zero)
						id_ctor_JF = JNIEnv.GetMethodID (class_ref, "<init>", "(JF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JF, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.ParkingBrakeData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$ParkingBrakeData", DoNotGenerateAcw=true)]
		public partial class ParkingBrakeData : global::Java.Lang.Object {


			static IntPtr isEngaged_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.ParkingBrakeData']/field[@name='isEngaged']"
			[Register ("isEngaged")]
			public bool IsEngaged {
				get {
					if (isEngaged_jfieldId == IntPtr.Zero)
						isEngaged_jfieldId = JNIEnv.GetFieldID (class_ref, "isEngaged", "Z");
					return JNIEnv.GetBooleanField (((global::Java.Lang.Object) this).Handle, isEngaged_jfieldId);
				}
				set {
					if (isEngaged_jfieldId == IntPtr.Zero)
						isEngaged_jfieldId = JNIEnv.GetFieldID (class_ref, "isEngaged", "Z");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, isEngaged_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.ParkingBrakeData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$ParkingBrakeData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ParkingBrakeData); }
			}

			protected ParkingBrakeData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JZ;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.ParkingBrakeData']/constructor[@name='CarSensorEvent.ParkingBrakeData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='boolean']]"
			[Register (".ctor", "(JZ)V", "")]
			public unsafe ParkingBrakeData (long p0, bool p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (ParkingBrakeData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JZ)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JZ)V", __args);
						return;
					}

					if (id_ctor_JZ == IntPtr.Zero)
						id_ctor_JZ = JNIEnv.GetMethodID (class_ref, "<init>", "(JZ)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JZ, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JZ, __args);
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.RpmData']"
		[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorEvent$RpmData", DoNotGenerateAcw=true)]
		public partial class RpmData : global::Java.Lang.Object {


			static IntPtr rpm_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.RpmData']/field[@name='rpm']"
			[Register ("rpm")]
			public float Rpm {
				get {
					if (rpm_jfieldId == IntPtr.Zero)
						rpm_jfieldId = JNIEnv.GetFieldID (class_ref, "rpm", "F");
					return JNIEnv.GetFloatField (((global::Java.Lang.Object) this).Handle, rpm_jfieldId);
				}
				set {
					if (rpm_jfieldId == IntPtr.Zero)
						rpm_jfieldId = JNIEnv.GetFieldID (class_ref, "rpm", "F");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, rpm_jfieldId, value);
					} finally {
					}
				}
			}

			static IntPtr timestamp_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.RpmData']/field[@name='timestamp']"
			[Register ("timestamp")]
			public long Timestamp {
				get {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					return JNIEnv.GetLongField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId);
				}
				set {
					if (timestamp_jfieldId == IntPtr.Zero)
						timestamp_jfieldId = JNIEnv.GetFieldID (class_ref, "timestamp", "J");
					try {
						JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, timestamp_jfieldId, value);
					} finally {
					}
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent$RpmData", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (RpmData); }
			}

			protected RpmData (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_JF;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent.RpmData']/constructor[@name='CarSensorEvent.RpmData' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='float']]"
			[Register (".ctor", "(JF)V", "")]
			public unsafe RpmData (long p0, float p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (RpmData)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(JF)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(JF)V", __args);
						return;
					}

					if (id_ctor_JF == IntPtr.Zero)
						id_ctor_JF = JNIEnv.GetMethodID (class_ref, "<init>", "(JF)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_JF, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_JF, __args);
				} finally {
				}
			}

		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/hardware/CarSensorEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorEvent); }
		}

		protected CarSensorEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_IJarrayFarrayBarrayJ;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/constructor[@name='CarSensorEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='long'] and parameter[3][@type='float[]'] and parameter[4][@type='byte[]'] and parameter[5][@type='long[]']]"
		[Register (".ctor", "(IJ[F[B[J)V", "")]
		public unsafe CarSensorEvent (int p0, long p1, float[] p2, byte[] p3, long[] p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			IntPtr native_p2 = JNIEnv.NewArray (p2);
			IntPtr native_p3 = JNIEnv.NewArray (p3);
			IntPtr native_p4 = JNIEnv.NewArray (p4);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (native_p2);
				__args [3] = new JValue (native_p3);
				__args [4] = new JValue (native_p4);
				if (((object) this).GetType () != typeof (CarSensorEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(IJ[F[B[J)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(IJ[F[B[J)V", __args);
					return;
				}

				if (id_ctor_IJarrayFarrayBarrayJ == IntPtr.Zero)
					id_ctor_IJarrayFarrayBarrayJ = JNIEnv.GetMethodID (class_ref, "<init>", "(IJ[F[B[J)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_IJarrayFarrayBarrayJ, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_IJarrayFarrayBarrayJ, __args);
			} finally {
				if (p2 != null) {
					JNIEnv.CopyArray (native_p2, p2);
					JNIEnv.DeleteLocalRef (native_p2);
				}
				if (p3 != null) {
					JNIEnv.CopyArray (native_p3, p3);
					JNIEnv.DeleteLocalRef (native_p3);
				}
				if (p4 != null) {
					JNIEnv.CopyArray (native_p4, p4);
					JNIEnv.DeleteLocalRef (native_p4);
				}
			}
		}

		static IntPtr id_ctor_IJarrayFarrayIarrayJ;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/constructor[@name='CarSensorEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='long'] and parameter[3][@type='float[]'] and parameter[4][@type='int[]'] and parameter[5][@type='long[]']]"
		[Register (".ctor", "(IJ[F[I[J)V", "")]
		public unsafe CarSensorEvent (int p0, long p1, float[] p2, int[] p3, long[] p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			IntPtr native_p2 = JNIEnv.NewArray (p2);
			IntPtr native_p3 = JNIEnv.NewArray (p3);
			IntPtr native_p4 = JNIEnv.NewArray (p4);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (native_p2);
				__args [3] = new JValue (native_p3);
				__args [4] = new JValue (native_p4);
				if (((object) this).GetType () != typeof (CarSensorEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(IJ[F[I[J)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(IJ[F[I[J)V", __args);
					return;
				}

				if (id_ctor_IJarrayFarrayIarrayJ == IntPtr.Zero)
					id_ctor_IJarrayFarrayIarrayJ = JNIEnv.GetMethodID (class_ref, "<init>", "(IJ[F[I[J)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_IJarrayFarrayIarrayJ, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_IJarrayFarrayIarrayJ, __args);
			} finally {
				if (p2 != null) {
					JNIEnv.CopyArray (native_p2, p2);
					JNIEnv.DeleteLocalRef (native_p2);
				}
				if (p3 != null) {
					JNIEnv.CopyArray (native_p3, p3);
					JNIEnv.DeleteLocalRef (native_p3);
				}
				if (p4 != null) {
					JNIEnv.CopyArray (native_p4, p4);
					JNIEnv.DeleteLocalRef (native_p4);
				}
			}
		}

		static IntPtr id_ctor_IJIII;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/constructor[@name='CarSensorEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='long'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register (".ctor", "(IJIII)V", "")]
		public unsafe CarSensorEvent (int p0, long p1, int p2, int p3, int p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				if (((object) this).GetType () != typeof (CarSensorEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(IJIII)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(IJIII)V", __args);
					return;
				}

				if (id_ctor_IJIII == IntPtr.Zero)
					id_ctor_IJIII = JNIEnv.GetMethodID (class_ref, "<init>", "(IJIII)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_IJIII, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_IJIII, __args);
			} finally {
			}
		}

		static Delegate cb_getAccelerometerData;
#pragma warning disable 0169
		static Delegate GetGetAccelerometerDataHandler ()
		{
			if (cb_getAccelerometerData == null)
				cb_getAccelerometerData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAccelerometerData);
			return cb_getAccelerometerData;
		}

		static IntPtr n_GetAccelerometerData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetAccelerometerData ());
		}
#pragma warning restore 0169

		static IntPtr id_getAccelerometerData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getAccelerometerData' and count(parameter)=0]"
		[Register ("getAccelerometerData", "()Landroid/support/car/hardware/CarSensorEvent$AccelerometerData;", "GetGetAccelerometerDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.AccelerometerData GetAccelerometerData ()
		{
			if (id_getAccelerometerData == IntPtr.Zero)
				id_getAccelerometerData = JNIEnv.GetMethodID (class_ref, "getAccelerometerData", "()Landroid/support/car/hardware/CarSensorEvent$AccelerometerData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.AccelerometerData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAccelerometerData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.AccelerometerData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAccelerometerData", "()Landroid/support/car/hardware/CarSensorEvent$AccelerometerData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getCarAbsActiveData;
#pragma warning disable 0169
		static Delegate GetGetCarAbsActiveDataHandler ()
		{
			if (cb_getCarAbsActiveData == null)
				cb_getCarAbsActiveData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarAbsActiveData);
			return cb_getCarAbsActiveData;
		}

		static IntPtr n_GetCarAbsActiveData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCarAbsActiveData ());
		}
#pragma warning restore 0169

		static IntPtr id_getCarAbsActiveData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getCarAbsActiveData' and count(parameter)=0]"
		[Register ("getCarAbsActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarAbsActiveData;", "GetGetCarAbsActiveDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.CarAbsActiveData GetCarAbsActiveData ()
		{
			if (id_getCarAbsActiveData == IntPtr.Zero)
				id_getCarAbsActiveData = JNIEnv.GetMethodID (class_ref, "getCarAbsActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarAbsActiveData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarAbsActiveData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarAbsActiveData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarAbsActiveData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarAbsActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarAbsActiveData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getCarSpeedData;
#pragma warning disable 0169
		static Delegate GetGetCarSpeedDataHandler ()
		{
			if (cb_getCarSpeedData == null)
				cb_getCarSpeedData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarSpeedData);
			return cb_getCarSpeedData;
		}

		static IntPtr n_GetCarSpeedData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCarSpeedData ());
		}
#pragma warning restore 0169

		static IntPtr id_getCarSpeedData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getCarSpeedData' and count(parameter)=0]"
		[Register ("getCarSpeedData", "()Landroid/support/car/hardware/CarSensorEvent$CarSpeedData;", "GetGetCarSpeedDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.CarSpeedData GetCarSpeedData ()
		{
			if (id_getCarSpeedData == IntPtr.Zero)
				id_getCarSpeedData = JNIEnv.GetMethodID (class_ref, "getCarSpeedData", "()Landroid/support/car/hardware/CarSensorEvent$CarSpeedData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarSpeedData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarSpeedData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarSpeedData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarSpeedData", "()Landroid/support/car/hardware/CarSensorEvent$CarSpeedData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getCarTractionControlActiveData;
#pragma warning disable 0169
		static Delegate GetGetCarTractionControlActiveDataHandler ()
		{
			if (cb_getCarTractionControlActiveData == null)
				cb_getCarTractionControlActiveData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarTractionControlActiveData);
			return cb_getCarTractionControlActiveData;
		}

		static IntPtr n_GetCarTractionControlActiveData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCarTractionControlActiveData ());
		}
#pragma warning restore 0169

		static IntPtr id_getCarTractionControlActiveData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getCarTractionControlActiveData' and count(parameter)=0]"
		[Register ("getCarTractionControlActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarTractionControlActiveData;", "GetGetCarTractionControlActiveDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.CarTractionControlActiveData GetCarTractionControlActiveData ()
		{
			if (id_getCarTractionControlActiveData == IntPtr.Zero)
				id_getCarTractionControlActiveData = JNIEnv.GetMethodID (class_ref, "getCarTractionControlActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarTractionControlActiveData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarTractionControlActiveData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarTractionControlActiveData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarTractionControlActiveData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarTractionControlActiveData", "()Landroid/support/car/hardware/CarSensorEvent$CarTractionControlActiveData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getCarWheelTickDistanceData;
#pragma warning disable 0169
		static Delegate GetGetCarWheelTickDistanceDataHandler ()
		{
			if (cb_getCarWheelTickDistanceData == null)
				cb_getCarWheelTickDistanceData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarWheelTickDistanceData);
			return cb_getCarWheelTickDistanceData;
		}

		static IntPtr n_GetCarWheelTickDistanceData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCarWheelTickDistanceData ());
		}
#pragma warning restore 0169

		static IntPtr id_getCarWheelTickDistanceData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getCarWheelTickDistanceData' and count(parameter)=0]"
		[Register ("getCarWheelTickDistanceData", "()Landroid/support/car/hardware/CarSensorEvent$CarWheelTickDistanceData;", "GetGetCarWheelTickDistanceDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.CarWheelTickDistanceData GetCarWheelTickDistanceData ()
		{
			if (id_getCarWheelTickDistanceData == IntPtr.Zero)
				id_getCarWheelTickDistanceData = JNIEnv.GetMethodID (class_ref, "getCarWheelTickDistanceData", "()Landroid/support/car/hardware/CarSensorEvent$CarWheelTickDistanceData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarWheelTickDistanceData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarWheelTickDistanceData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CarWheelTickDistanceData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCarWheelTickDistanceData", "()Landroid/support/car/hardware/CarSensorEvent$CarWheelTickDistanceData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getCompassData;
#pragma warning disable 0169
		static Delegate GetGetCompassDataHandler ()
		{
			if (cb_getCompassData == null)
				cb_getCompassData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCompassData);
			return cb_getCompassData;
		}

		static IntPtr n_GetCompassData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCompassData ());
		}
#pragma warning restore 0169

		static IntPtr id_getCompassData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getCompassData' and count(parameter)=0]"
		[Register ("getCompassData", "()Landroid/support/car/hardware/CarSensorEvent$CompassData;", "GetGetCompassDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.CompassData GetCompassData ()
		{
			if (id_getCompassData == IntPtr.Zero)
				id_getCompassData = JNIEnv.GetMethodID (class_ref, "getCompassData", "()Landroid/support/car/hardware/CarSensorEvent$CompassData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CompassData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCompassData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.CompassData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCompassData", "()Landroid/support/car/hardware/CarSensorEvent$CompassData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getDrivingStatusData;
#pragma warning disable 0169
		static Delegate GetGetDrivingStatusDataHandler ()
		{
			if (cb_getDrivingStatusData == null)
				cb_getDrivingStatusData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDrivingStatusData);
			return cb_getDrivingStatusData;
		}

		static IntPtr n_GetDrivingStatusData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetDrivingStatusData ());
		}
#pragma warning restore 0169

		static IntPtr id_getDrivingStatusData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getDrivingStatusData' and count(parameter)=0]"
		[Register ("getDrivingStatusData", "()Landroid/support/car/hardware/CarSensorEvent$DrivingStatusData;", "GetGetDrivingStatusDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData GetDrivingStatusData ()
		{
			if (id_getDrivingStatusData == IntPtr.Zero)
				id_getDrivingStatusData = JNIEnv.GetMethodID (class_ref, "getDrivingStatusData", "()Landroid/support/car/hardware/CarSensorEvent$DrivingStatusData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getDrivingStatusData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.DrivingStatusData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDrivingStatusData", "()Landroid/support/car/hardware/CarSensorEvent$DrivingStatusData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getEnvironmentData;
#pragma warning disable 0169
		static Delegate GetGetEnvironmentDataHandler ()
		{
			if (cb_getEnvironmentData == null)
				cb_getEnvironmentData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEnvironmentData);
			return cb_getEnvironmentData;
		}

		static IntPtr n_GetEnvironmentData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetEnvironmentData ());
		}
#pragma warning restore 0169

		static IntPtr id_getEnvironmentData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getEnvironmentData' and count(parameter)=0]"
		[Register ("getEnvironmentData", "()Landroid/support/car/hardware/CarSensorEvent$EnvironmentData;", "GetGetEnvironmentDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.EnvironmentData GetEnvironmentData ()
		{
			if (id_getEnvironmentData == IntPtr.Zero)
				id_getEnvironmentData = JNIEnv.GetMethodID (class_ref, "getEnvironmentData", "()Landroid/support/car/hardware/CarSensorEvent$EnvironmentData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.EnvironmentData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getEnvironmentData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.EnvironmentData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEnvironmentData", "()Landroid/support/car/hardware/CarSensorEvent$EnvironmentData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getFuelLevelData;
#pragma warning disable 0169
		static Delegate GetGetFuelLevelDataHandler ()
		{
			if (cb_getFuelLevelData == null)
				cb_getFuelLevelData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFuelLevelData);
			return cb_getFuelLevelData;
		}

		static IntPtr n_GetFuelLevelData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetFuelLevelData ());
		}
#pragma warning restore 0169

		static IntPtr id_getFuelLevelData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getFuelLevelData' and count(parameter)=0]"
		[Register ("getFuelLevelData", "()Landroid/support/car/hardware/CarSensorEvent$FuelLevelData;", "GetGetFuelLevelDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.FuelLevelData GetFuelLevelData ()
		{
			if (id_getFuelLevelData == IntPtr.Zero)
				id_getFuelLevelData = JNIEnv.GetMethodID (class_ref, "getFuelLevelData", "()Landroid/support/car/hardware/CarSensorEvent$FuelLevelData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.FuelLevelData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getFuelLevelData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.FuelLevelData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFuelLevelData", "()Landroid/support/car/hardware/CarSensorEvent$FuelLevelData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getGearData;
#pragma warning disable 0169
		static Delegate GetGetGearDataHandler ()
		{
			if (cb_getGearData == null)
				cb_getGearData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGearData);
			return cb_getGearData;
		}

		static IntPtr n_GetGearData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetGearData ());
		}
#pragma warning restore 0169

		static IntPtr id_getGearData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getGearData' and count(parameter)=0]"
		[Register ("getGearData", "()Landroid/support/car/hardware/CarSensorEvent$GearData;", "GetGetGearDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.GearData GetGearData ()
		{
			if (id_getGearData == IntPtr.Zero)
				id_getGearData = JNIEnv.GetMethodID (class_ref, "getGearData", "()Landroid/support/car/hardware/CarSensorEvent$GearData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GearData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getGearData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GearData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGearData", "()Landroid/support/car/hardware/CarSensorEvent$GearData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getGpsSatelliteData_Z;
#pragma warning disable 0169
		static Delegate GetGetGpsSatelliteData_ZHandler ()
		{
			if (cb_getGpsSatelliteData_Z == null)
				cb_getGpsSatelliteData_Z = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool, IntPtr>) n_GetGpsSatelliteData_Z);
			return cb_getGpsSatelliteData_Z;
		}

		static IntPtr n_GetGpsSatelliteData_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetGpsSatelliteData (p0));
		}
#pragma warning restore 0169

		static IntPtr id_getGpsSatelliteData_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getGpsSatelliteData' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("getGpsSatelliteData", "(Z)Landroid/support/car/hardware/CarSensorEvent$GpsSatelliteData;", "GetGetGpsSatelliteData_ZHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.GpsSatelliteData GetGpsSatelliteData (bool p0)
		{
			if (id_getGpsSatelliteData_Z == IntPtr.Zero)
				id_getGpsSatelliteData_Z = JNIEnv.GetMethodID (class_ref, "getGpsSatelliteData", "(Z)Landroid/support/car/hardware/CarSensorEvent$GpsSatelliteData;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GpsSatelliteData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getGpsSatelliteData_Z, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GpsSatelliteData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGpsSatelliteData", "(Z)Landroid/support/car/hardware/CarSensorEvent$GpsSatelliteData;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getGyroscopeData;
#pragma warning disable 0169
		static Delegate GetGetGyroscopeDataHandler ()
		{
			if (cb_getGyroscopeData == null)
				cb_getGyroscopeData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGyroscopeData);
			return cb_getGyroscopeData;
		}

		static IntPtr n_GetGyroscopeData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetGyroscopeData ());
		}
#pragma warning restore 0169

		static IntPtr id_getGyroscopeData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getGyroscopeData' and count(parameter)=0]"
		[Register ("getGyroscopeData", "()Landroid/support/car/hardware/CarSensorEvent$GyroscopeData;", "GetGetGyroscopeDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.GyroscopeData GetGyroscopeData ()
		{
			if (id_getGyroscopeData == IntPtr.Zero)
				id_getGyroscopeData = JNIEnv.GetMethodID (class_ref, "getGyroscopeData", "()Landroid/support/car/hardware/CarSensorEvent$GyroscopeData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GyroscopeData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getGyroscopeData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.GyroscopeData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGyroscopeData", "()Landroid/support/car/hardware/CarSensorEvent$GyroscopeData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getLocation_Landroid_location_Location_;
#pragma warning disable 0169
		static Delegate GetGetLocation_Landroid_location_Location_Handler ()
		{
			if (cb_getLocation_Landroid_location_Location_ == null)
				cb_getLocation_Landroid_location_Location_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetLocation_Landroid_location_Location_);
			return cb_getLocation_Landroid_location_Location_;
		}

		static IntPtr n_GetLocation_Landroid_location_Location_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Locations.Location p0 = global::Java.Lang.Object.GetObject<global::Android.Locations.Location> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetLocation (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getLocation_Landroid_location_Location_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getLocation' and count(parameter)=1 and parameter[1][@type='android.location.Location']]"
		[Register ("getLocation", "(Landroid/location/Location;)Landroid/location/Location;", "GetGetLocation_Landroid_location_Location_Handler")]
		public virtual unsafe global::Android.Locations.Location GetLocation (global::Android.Locations.Location p0)
		{
			if (id_getLocation_Landroid_location_Location_ == IntPtr.Zero)
				id_getLocation_Landroid_location_Location_ = JNIEnv.GetMethodID (class_ref, "getLocation", "(Landroid/location/Location;)Landroid/location/Location;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Android.Locations.Location __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.Locations.Location> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLocation_Landroid_location_Location_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.Locations.Location> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocation", "(Landroid/location/Location;)Landroid/location/Location;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_getNightData;
#pragma warning disable 0169
		static Delegate GetGetNightDataHandler ()
		{
			if (cb_getNightData == null)
				cb_getNightData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetNightData);
			return cb_getNightData;
		}

		static IntPtr n_GetNightData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetNightData ());
		}
#pragma warning restore 0169

		static IntPtr id_getNightData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getNightData' and count(parameter)=0]"
		[Register ("getNightData", "()Landroid/support/car/hardware/CarSensorEvent$NightData;", "GetGetNightDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.NightData GetNightData ()
		{
			if (id_getNightData == IntPtr.Zero)
				id_getNightData = JNIEnv.GetMethodID (class_ref, "getNightData", "()Landroid/support/car/hardware/CarSensorEvent$NightData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.NightData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getNightData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.NightData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getNightData", "()Landroid/support/car/hardware/CarSensorEvent$NightData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getOdometerData;
#pragma warning disable 0169
		static Delegate GetGetOdometerDataHandler ()
		{
			if (cb_getOdometerData == null)
				cb_getOdometerData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOdometerData);
			return cb_getOdometerData;
		}

		static IntPtr n_GetOdometerData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetOdometerData ());
		}
#pragma warning restore 0169

		static IntPtr id_getOdometerData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getOdometerData' and count(parameter)=0]"
		[Register ("getOdometerData", "()Landroid/support/car/hardware/CarSensorEvent$OdometerData;", "GetGetOdometerDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.OdometerData GetOdometerData ()
		{
			if (id_getOdometerData == IntPtr.Zero)
				id_getOdometerData = JNIEnv.GetMethodID (class_ref, "getOdometerData", "()Landroid/support/car/hardware/CarSensorEvent$OdometerData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.OdometerData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getOdometerData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.OdometerData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOdometerData", "()Landroid/support/car/hardware/CarSensorEvent$OdometerData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getParkingBrakeData;
#pragma warning disable 0169
		static Delegate GetGetParkingBrakeDataHandler ()
		{
			if (cb_getParkingBrakeData == null)
				cb_getParkingBrakeData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetParkingBrakeData);
			return cb_getParkingBrakeData;
		}

		static IntPtr n_GetParkingBrakeData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetParkingBrakeData ());
		}
#pragma warning restore 0169

		static IntPtr id_getParkingBrakeData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getParkingBrakeData' and count(parameter)=0]"
		[Register ("getParkingBrakeData", "()Landroid/support/car/hardware/CarSensorEvent$ParkingBrakeData;", "GetGetParkingBrakeDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.ParkingBrakeData GetParkingBrakeData ()
		{
			if (id_getParkingBrakeData == IntPtr.Zero)
				id_getParkingBrakeData = JNIEnv.GetMethodID (class_ref, "getParkingBrakeData", "()Landroid/support/car/hardware/CarSensorEvent$ParkingBrakeData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.ParkingBrakeData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getParkingBrakeData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.ParkingBrakeData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getParkingBrakeData", "()Landroid/support/car/hardware/CarSensorEvent$ParkingBrakeData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getRpmData;
#pragma warning disable 0169
		static Delegate GetGetRpmDataHandler ()
		{
			if (cb_getRpmData == null)
				cb_getRpmData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRpmData);
			return cb_getRpmData;
		}

		static IntPtr n_GetRpmData (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorEvent __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetRpmData ());
		}
#pragma warning restore 0169

		static IntPtr id_getRpmData;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorEvent']/method[@name='getRpmData' and count(parameter)=0]"
		[Register ("getRpmData", "()Landroid/support/car/hardware/CarSensorEvent$RpmData;", "GetGetRpmDataHandler")]
		public virtual unsafe global::Android.Support.Car.Hardware.CarSensorEvent.RpmData GetRpmData ()
		{
			if (id_getRpmData == IntPtr.Zero)
				id_getRpmData = JNIEnv.GetMethodID (class_ref, "getRpmData", "()Landroid/support/car/hardware/CarSensorEvent$RpmData;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.RpmData> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getRpmData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorEvent.RpmData> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRpmData", "()Landroid/support/car/hardware/CarSensorEvent$RpmData;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}
}
