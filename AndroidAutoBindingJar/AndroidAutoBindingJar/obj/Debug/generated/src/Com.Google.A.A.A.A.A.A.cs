using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.A.A.A.A.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.a.a.a.a.a']/class[@name='a']"
	[global::Android.Runtime.Register ("com/google/a/a/a/a/a/a", DoNotGenerateAcw=true)]
	public sealed partial class A : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/a/a/a/a/a/a", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (A); }
		}

		internal A (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a_Ljava_lang_Throwable_Ljava_lang_Throwable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.a.a.a.a.a']/class[@name='a']/method[@name='a' and count(parameter)=2 and parameter[1][@type='java.lang.Throwable'] and parameter[2][@type='java.lang.Throwable']]"
		[Register ("a", "(Ljava/lang/Throwable;Ljava/lang/Throwable;)V", "")]
		public static unsafe void InvokeA (global::Java.Lang.Throwable p0, global::Java.Lang.Throwable p1)
		{
			if (id_a_Ljava_lang_Throwable_Ljava_lang_Throwable_ == IntPtr.Zero)
				id_a_Ljava_lang_Throwable_Ljava_lang_Throwable_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Ljava/lang/Throwable;Ljava/lang/Throwable;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_a_Ljava_lang_Throwable_Ljava_lang_Throwable_, __args);
			} finally {
			}
		}

	}
}
