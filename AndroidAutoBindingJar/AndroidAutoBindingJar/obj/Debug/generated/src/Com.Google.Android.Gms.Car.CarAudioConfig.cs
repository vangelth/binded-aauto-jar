using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioConfig']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioConfig", DoNotGenerateAcw=true)]
	public partial class CarAudioConfig : global::Java.Lang.Object {


		static IntPtr audioFormat_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioConfig']/field[@name='audioFormat']"
		[Register ("audioFormat")]
		public int AudioFormat {
			get {
				if (audioFormat_jfieldId == IntPtr.Zero)
					audioFormat_jfieldId = JNIEnv.GetFieldID (class_ref, "audioFormat", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, audioFormat_jfieldId);
			}
			set {
				if (audioFormat_jfieldId == IntPtr.Zero)
					audioFormat_jfieldId = JNIEnv.GetFieldID (class_ref, "audioFormat", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, audioFormat_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr channelConfig_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioConfig']/field[@name='channelConfig']"
		[Register ("channelConfig")]
		public int ChannelConfig {
			get {
				if (channelConfig_jfieldId == IntPtr.Zero)
					channelConfig_jfieldId = JNIEnv.GetFieldID (class_ref, "channelConfig", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, channelConfig_jfieldId);
			}
			set {
				if (channelConfig_jfieldId == IntPtr.Zero)
					channelConfig_jfieldId = JNIEnv.GetFieldID (class_ref, "channelConfig", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, channelConfig_jfieldId, value);
				} finally {
				}
			}
		}

		static IntPtr sampleRate_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioConfig']/field[@name='sampleRate']"
		[Register ("sampleRate")]
		public int SampleRate {
			get {
				if (sampleRate_jfieldId == IntPtr.Zero)
					sampleRate_jfieldId = JNIEnv.GetFieldID (class_ref, "sampleRate", "I");
				return JNIEnv.GetIntField (((global::Java.Lang.Object) this).Handle, sampleRate_jfieldId);
			}
			set {
				if (sampleRate_jfieldId == IntPtr.Zero)
					sampleRate_jfieldId = JNIEnv.GetFieldID (class_ref, "sampleRate", "I");
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, sampleRate_jfieldId, value);
				} finally {
				}
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/CarAudioConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioConfig); }
		}

		protected CarAudioConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_III;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='CarAudioConfig']/constructor[@name='CarAudioConfig' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register (".ctor", "(III)V", "")]
		public unsafe CarAudioConfig (int p0, int p1, int p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (CarAudioConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(III)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(III)V", __args);
					return;
				}

				if (id_ctor_III == IntPtr.Zero)
					id_ctor_III = JNIEnv.GetMethodID (class_ref, "<init>", "(III)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_III, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_III, __args);
			} finally {
			}
		}

	}
}
