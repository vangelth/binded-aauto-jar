[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car", Managed="Android.Support.Car")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.annotation", Managed="Android.Support.Car.Annotation")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.content.pm", Managed="Android.Support.Car.Content.PM")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.hardware", Managed="Android.Support.Car.Hardware")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.input", Managed="Android.Support.Car.Input")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.media", Managed="Android.Support.Car.Media")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.navigation", Managed="Android.Support.Car.Navigation")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "android.support.car.ui", Managed="Android.Support.Car.UI")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.a.a.a.a.a", Managed="Com.Google.A.A.A.A.A")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.a", Managed="Com.Google.Android.A")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk", Managed="Com.Google.Android.Apps.Auto.Sdk")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.nav", Managed="Com.Google.Android.Apps.Auto.Sdk.Nav")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.nav.state", Managed="Com.Google.Android.Apps.Auto.Sdk.Nav.State")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.nav.suggestion", Managed="Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.service", Managed="Com.Google.Android.Apps.Auto.Sdk.Service")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.service.a", Managed="Com.Google.Android.Apps.Auto.Sdk.Service.A")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.service.a.a", Managed="Com.Google.Android.Apps.Auto.Sdk.Service.A.A")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.service.a.b", Managed="Com.Google.Android.Apps.Auto.Sdk.Service.A.B")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.service.vec", Managed="Com.Google.Android.Apps.Auto.Sdk.Service.Vec")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.ui", Managed="Com.Google.Android.Apps.Auto.Sdk.UI")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.apps.auto.sdk.vanagon", Managed="Com.Google.Android.Apps.Auto.Sdk.Vanagon")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.gms.car", Managed="Com.Google.Android.Gms.Car")]
[assembly:global::Android.Runtime.NamespaceMapping (Java = "com.google.android.gms.car.input", Managed="Com.Google.Android.Gms.Car.Input")]
