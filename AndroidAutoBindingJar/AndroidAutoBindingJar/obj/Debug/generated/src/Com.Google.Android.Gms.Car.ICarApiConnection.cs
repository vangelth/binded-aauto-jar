using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection.ApiConnectionCallback']"
	[Register ("com/google/android/gms/car/CarApiConnection$ApiConnectionCallback", "", "Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallbackInvoker")]
	public partial interface ICarApiConnectionApiConnectionCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection.ApiConnectionCallback']/method[@name='onConnected' and count(parameter)=0]"
		[Register ("onConnected", "()V", "GetOnConnectedHandler:Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallbackInvoker, AndroidAutoBindingJar")]
		void OnConnected ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection.ApiConnectionCallback']/method[@name='onConnectionFailed' and count(parameter)=0]"
		[Register ("onConnectionFailed", "()V", "GetOnConnectionFailedHandler:Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallbackInvoker, AndroidAutoBindingJar")]
		void OnConnectionFailed ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection.ApiConnectionCallback']/method[@name='onConnectionSuspended' and count(parameter)=0]"
		[Register ("onConnectionSuspended", "()V", "GetOnConnectionSuspendedHandler:Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallbackInvoker, AndroidAutoBindingJar")]
		void OnConnectionSuspended ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarApiConnection$ApiConnectionCallback", DoNotGenerateAcw=true)]
	internal class ICarApiConnectionApiConnectionCallbackInvoker : global::Java.Lang.Object, ICarApiConnectionApiConnectionCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarApiConnection$ApiConnectionCallback");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarApiConnectionApiConnectionCallbackInvoker); }
		}

		IntPtr class_ref;

		public static ICarApiConnectionApiConnectionCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarApiConnectionApiConnectionCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarApiConnection.ApiConnectionCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarApiConnectionApiConnectionCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onConnected;
#pragma warning disable 0169
		static Delegate GetOnConnectedHandler ()
		{
			if (cb_onConnected == null)
				cb_onConnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnConnected);
			return cb_onConnected;
		}

		static void n_OnConnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnConnected ();
		}
#pragma warning restore 0169

		IntPtr id_onConnected;
		public unsafe void OnConnected ()
		{
			if (id_onConnected == IntPtr.Zero)
				id_onConnected = JNIEnv.GetMethodID (class_ref, "onConnected", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnected);
		}

		static Delegate cb_onConnectionFailed;
#pragma warning disable 0169
		static Delegate GetOnConnectionFailedHandler ()
		{
			if (cb_onConnectionFailed == null)
				cb_onConnectionFailed = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnConnectionFailed);
			return cb_onConnectionFailed;
		}

		static void n_OnConnectionFailed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnConnectionFailed ();
		}
#pragma warning restore 0169

		IntPtr id_onConnectionFailed;
		public unsafe void OnConnectionFailed ()
		{
			if (id_onConnectionFailed == IntPtr.Zero)
				id_onConnectionFailed = JNIEnv.GetMethodID (class_ref, "onConnectionFailed", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnectionFailed);
		}

		static Delegate cb_onConnectionSuspended;
#pragma warning disable 0169
		static Delegate GetOnConnectionSuspendedHandler ()
		{
			if (cb_onConnectionSuspended == null)
				cb_onConnectionSuspended = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnConnectionSuspended);
			return cb_onConnectionSuspended;
		}

		static void n_OnConnectionSuspended (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnConnectionSuspended ();
		}
#pragma warning restore 0169

		IntPtr id_onConnectionSuspended;
		public unsafe void OnConnectionSuspended ()
		{
			if (id_onConnectionSuspended == IntPtr.Zero)
				id_onConnectionSuspended = JNIEnv.GetMethodID (class_ref, "onConnectionSuspended", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConnectionSuspended);
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection']"
	[Register ("com/google/android/gms/car/CarApiConnection", "", "Com.Google.Android.Gms.Car.ICarApiConnectionInvoker")]
	public partial interface ICarApiConnection : IJavaObject {

		global::Com.Google.Android.Gms.Car.ICarApi CarApi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection']/method[@name='getCarApi' and count(parameter)=0]"
			[Register ("getCarApi", "()Lcom/google/android/gms/car/CarApi;", "GetGetCarApiHandler:Com.Google.Android.Gms.Car.ICarApiConnectionInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection']/method[@name='connect' and count(parameter)=0]"
		[Register ("connect", "()V", "GetConnectHandler:Com.Google.Android.Gms.Car.ICarApiConnectionInvoker, AndroidAutoBindingJar")]
		void Connect ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarApiConnection']/method[@name='disconnect' and count(parameter)=0]"
		[Register ("disconnect", "()V", "GetDisconnectHandler:Com.Google.Android.Gms.Car.ICarApiConnectionInvoker, AndroidAutoBindingJar")]
		void Disconnect ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarApiConnection", DoNotGenerateAcw=true)]
	internal class ICarApiConnectionInvoker : global::Java.Lang.Object, ICarApiConnection {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarApiConnection");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarApiConnectionInvoker); }
		}

		IntPtr class_ref;

		public static ICarApiConnection GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarApiConnection> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarApiConnection"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarApiConnectionInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getCarApi;
#pragma warning disable 0169
		static Delegate GetGetCarApiHandler ()
		{
			if (cb_getCarApi == null)
				cb_getCarApi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarApi);
			return cb_getCarApi;
		}

		static IntPtr n_GetCarApi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnection __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnection> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CarApi);
		}
#pragma warning restore 0169

		IntPtr id_getCarApi;
		public unsafe global::Com.Google.Android.Gms.Car.ICarApi CarApi {
			get {
				if (id_getCarApi == IntPtr.Zero)
					id_getCarApi = JNIEnv.GetMethodID (class_ref, "getCarApi", "()Lcom/google/android/gms/car/CarApi;");
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApi> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarApi), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_connect;
#pragma warning disable 0169
		static Delegate GetConnectHandler ()
		{
			if (cb_connect == null)
				cb_connect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Connect);
			return cb_connect;
		}

		static void n_Connect (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnection __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnection> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Connect ();
		}
#pragma warning restore 0169

		IntPtr id_connect;
		public unsafe void Connect ()
		{
			if (id_connect == IntPtr.Zero)
				id_connect = JNIEnv.GetMethodID (class_ref, "connect", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_connect);
		}

		static Delegate cb_disconnect;
#pragma warning disable 0169
		static Delegate GetDisconnectHandler ()
		{
			if (cb_disconnect == null)
				cb_disconnect = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Disconnect);
			return cb_disconnect;
		}

		static void n_Disconnect (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarApiConnection __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnection> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Disconnect ();
		}
#pragma warning restore 0169

		IntPtr id_disconnect;
		public unsafe void Disconnect ()
		{
			if (id_disconnect == IntPtr.Zero)
				id_disconnect = JNIEnv.GetMethodID (class_ref, "disconnect", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_disconnect);
		}

	}

}
