using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion", DoNotGenerateAcw=true)]
	public partial class NavigationSuggestion : global::Com.Google.Android.Apps.Auto.Sdk.A {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder", DoNotGenerateAcw=true)]
		public partial class Builder : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Builder); }
			}

			protected Builder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/constructor[@name='NavigationSuggestion.Builder' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe Builder ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					if (((object) this).GetType () != typeof (Builder)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_build;
#pragma warning disable 0169
			static Delegate GetBuildHandler ()
			{
				if (cb_build == null)
					cb_build = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Build);
				return cb_build;
			}

			static IntPtr n_Build (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.Build ());
			}
#pragma warning restore 0169

			static IntPtr id_build;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='build' and count(parameter)=0]"
			[Register ("build", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion;", "GetBuildHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion Build ()
			{
				if (id_build == IntPtr.Zero)
					id_build = JNIEnv.GetMethodID (class_ref, "build", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_build), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "build", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setAddress_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetAddress_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setAddress_Ljava_lang_CharSequence_ == null)
					cb_setAddress_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetAddress_Ljava_lang_CharSequence_);
				return cb_setAddress_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetAddress_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetAddress (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setAddress_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setAddress' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setAddress", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetAddress_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetAddress (global::Java.Lang.ICharSequence p0)
			{
				if (id_setAddress_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setAddress_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setAddress", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setAddress_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAddress", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetAddress (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __result = SetAddress (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setFormattedTimeToDestination_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetFormattedTimeToDestination_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setFormattedTimeToDestination_Ljava_lang_CharSequence_ == null)
					cb_setFormattedTimeToDestination_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetFormattedTimeToDestination_Ljava_lang_CharSequence_);
				return cb_setFormattedTimeToDestination_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetFormattedTimeToDestination_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetFormattedTimeToDestination (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setFormattedTimeToDestination_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setFormattedTimeToDestination' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setFormattedTimeToDestination", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetFormattedTimeToDestination_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetFormattedTimeToDestination (global::Java.Lang.ICharSequence p0)
			{
				if (id_setFormattedTimeToDestination_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setFormattedTimeToDestination_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setFormattedTimeToDestination", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setFormattedTimeToDestination_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFormattedTimeToDestination", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetFormattedTimeToDestination (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __result = SetFormattedTimeToDestination (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setLatLng_DD;
#pragma warning disable 0169
			static Delegate GetSetLatLng_DDHandler ()
			{
				if (cb_setLatLng_DD == null)
					cb_setLatLng_DD = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double, double, IntPtr>) n_SetLatLng_DD);
				return cb_setLatLng_DD;
			}

			static IntPtr n_SetLatLng_DD (IntPtr jnienv, IntPtr native__this, double p0, double p1)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetLatLng (p0, p1));
			}
#pragma warning restore 0169

			static IntPtr id_setLatLng_DD;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setLatLng' and count(parameter)=2 and parameter[1][@type='double'] and parameter[2][@type='double']]"
			[Register ("setLatLng", "(DD)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetLatLng_DDHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetLatLng (double p0, double p1)
			{
				if (id_setLatLng_DD == IntPtr.Zero)
					id_setLatLng_DD = JNIEnv.GetMethodID (class_ref, "setLatLng", "(DD)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setLatLng_DD, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLatLng", "(DD)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setName_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetName_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setName_Ljava_lang_CharSequence_ == null)
					cb_setName_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_CharSequence_);
				return cb_setName_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetName_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetName (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setName_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetName_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetName (global::Java.Lang.ICharSequence p0)
			{
				if (id_setName_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setName_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setName_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetName (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __result = SetName (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setNavigationIntent_Landroid_content_Intent_;
#pragma warning disable 0169
			static Delegate GetSetNavigationIntent_Landroid_content_Intent_Handler ()
			{
				if (cb_setNavigationIntent_Landroid_content_Intent_ == null)
					cb_setNavigationIntent_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetNavigationIntent_Landroid_content_Intent_);
				return cb_setNavigationIntent_Landroid_content_Intent_;
			}

			static IntPtr n_SetNavigationIntent_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetNavigationIntent (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setNavigationIntent_Landroid_content_Intent_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setNavigationIntent' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
			[Register ("setNavigationIntent", "(Landroid/content/Intent;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetNavigationIntent_Landroid_content_Intent_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetNavigationIntent (global::Android.Content.Intent p0)
			{
				if (id_setNavigationIntent_Landroid_content_Intent_ == IntPtr.Zero)
					id_setNavigationIntent_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "setNavigationIntent", "(Landroid/content/Intent;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setNavigationIntent_Landroid_content_Intent_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setNavigationIntent", "(Landroid/content/Intent;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
				}
			}

			static Delegate cb_setRoute_Ljava_lang_CharSequence_;
#pragma warning disable 0169
			static Delegate GetSetRoute_Ljava_lang_CharSequence_Handler ()
			{
				if (cb_setRoute_Ljava_lang_CharSequence_ == null)
					cb_setRoute_Ljava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetRoute_Ljava_lang_CharSequence_);
				return cb_setRoute_Ljava_lang_CharSequence_;
			}

			static IntPtr n_SetRoute_Ljava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.ICharSequence p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p0, JniHandleOwnership.DoNotTransfer);
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetRoute (p0));
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setRoute_Ljava_lang_CharSequence_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setRoute' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence']]"
			[Register ("setRoute", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetRoute_Ljava_lang_CharSequence_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetRoute (global::Java.Lang.ICharSequence p0)
			{
				if (id_setRoute_Ljava_lang_CharSequence_ == IntPtr.Zero)
					id_setRoute_Ljava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setRoute", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				IntPtr native_p0 = CharSequence.ToLocalJniHandle (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setRoute_Ljava_lang_CharSequence_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRoute", "(Ljava/lang/CharSequence;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			public global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetRoute (string p0)
			{
				global::Java.Lang.String jls_p0 = p0 == null ? null : new global::Java.Lang.String (p0);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __result = SetRoute (jls_p0);
				var __rsval = __result;
				jls_p0?.Dispose ();
				return __rsval;
			}

			static Delegate cb_setSecondsToDestination_I;
#pragma warning disable 0169
			static Delegate GetSetSecondsToDestination_IHandler ()
			{
				if (cb_setSecondsToDestination_I == null)
					cb_setSecondsToDestination_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetSecondsToDestination_I);
				return cb_setSecondsToDestination_I;
			}

			static IntPtr n_SetSecondsToDestination_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetSecondsToDestination (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setSecondsToDestination_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setSecondsToDestination' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetSecondsToDestination_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetSecondsToDestination (int p0)
			{
				if (id_setSecondsToDestination_I == IntPtr.Zero)
					id_setSecondsToDestination_I = JNIEnv.GetMethodID (class_ref, "setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setSecondsToDestination_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSecondsToDestination", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setTraffic_I;
#pragma warning disable 0169
			static Delegate GetSetTraffic_IHandler ()
			{
				if (cb_setTraffic_I == null)
					cb_setTraffic_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_SetTraffic_I);
				return cb_setTraffic_I;
			}

			static IntPtr n_SetTraffic_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.SetTraffic (p0));
			}
#pragma warning restore 0169

			static IntPtr id_setTraffic_I;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setTraffic' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setTraffic", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetTraffic_IHandler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetTraffic (int p0)
			{
				if (id_setTraffic_I == IntPtr.Zero)
					id_setTraffic_I = JNIEnv.GetMethodID (class_ref, "setTraffic", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setTraffic_I, __args), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTraffic", "(I)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}

			static Delegate cb_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_;
#pragma warning disable 0169
			static Delegate GetSetWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_Handler ()
			{
				if (cb_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_ == null)
					cb_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_);
				return cb_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_;
			}

			static IntPtr n_SetWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[] p0 = (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng));
				IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.SetWaypoints (p0));
				if (p0 != null)
					JNIEnv.CopyArray (p0, native_p0);
				return __ret;
			}
#pragma warning restore 0169

			static IntPtr id_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.Builder']/method[@name='setWaypoints' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.suggestion.NavigationSuggestion.LatLng[]']]"
			[Register ("setWaypoints", "([Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;", "GetSetWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_Handler")]
			public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder SetWaypoints (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[] p0)
			{
				if (id_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_ == IntPtr.Zero)
					id_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_ = JNIEnv.GetMethodID (class_ref, "setWaypoints", "([Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;");
				IntPtr native_p0 = JNIEnv.NewArray (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder __ret;
					if (((object) this).GetType () == ThresholdType)
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_setWaypoints_arrayLcom_google_android_apps_auto_sdk_nav_suggestion_NavigationSuggestion_LatLng_, __args), JniHandleOwnership.TransferLocalRef);
					else
						__ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.Builder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setWaypoints", "([Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;)Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					if (p0 != null) {
						JNIEnv.CopyArray (native_p0, p0);
						JNIEnv.DeleteLocalRef (native_p0);
					}
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.LatLng']"
		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng", DoNotGenerateAcw=true)]
		public partial class LatLng : global::Java.Lang.Object {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (LatLng); }
			}

			protected LatLng (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_DD;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.LatLng']/constructor[@name='NavigationSuggestion.LatLng' and count(parameter)=2 and parameter[1][@type='double'] and parameter[2][@type='double']]"
			[Register (".ctor", "(DD)V", "")]
			public unsafe LatLng (double p0, double p1)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [2];
					__args [0] = new JValue (p0);
					__args [1] = new JValue (p1);
					if (((object) this).GetType () != typeof (LatLng)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(DD)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(DD)V", __args);
						return;
					}

					if (id_ctor_DD == IntPtr.Zero)
						id_ctor_DD = JNIEnv.GetMethodID (class_ref, "<init>", "(DD)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_DD, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_DD, __args);
				} finally {
				}
			}

			static Delegate cb_getLat;
#pragma warning disable 0169
			static Delegate GetGetLatHandler ()
			{
				if (cb_getLat == null)
					cb_getLat = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLat);
				return cb_getLat;
			}

			static double n_GetLat (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.Lat;
			}
#pragma warning restore 0169

			static IntPtr id_getLat;
			public virtual unsafe double Lat {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.LatLng']/method[@name='getLat' and count(parameter)=0]"
				[Register ("getLat", "()D", "GetGetLatHandler")]
				get {
					if (id_getLat == IntPtr.Zero)
						id_getLat = JNIEnv.GetMethodID (class_ref, "getLat", "()D");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallDoubleMethod (((global::Java.Lang.Object) this).Handle, id_getLat);
						else
							return JNIEnv.CallNonvirtualDoubleMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLat", "()D"));
					} finally {
					}
				}
			}

			static Delegate cb_getLng;
#pragma warning disable 0169
			static Delegate GetGetLngHandler ()
			{
				if (cb_getLng == null)
					cb_getLng = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLng);
				return cb_getLng;
			}

			static double n_GetLng (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.Lng;
			}
#pragma warning restore 0169

			static IntPtr id_getLng;
			public virtual unsafe double Lng {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion.LatLng']/method[@name='getLng' and count(parameter)=0]"
				[Register ("getLng", "()D", "GetGetLngHandler")]
				get {
					if (id_getLng == IntPtr.Zero)
						id_getLng = JNIEnv.GetMethodID (class_ref, "getLng", "()D");
					try {

						if (((object) this).GetType () == ThresholdType)
							return JNIEnv.CallDoubleMethod (((global::Java.Lang.Object) this).Handle, id_getLng);
						else
							return JNIEnv.CallNonvirtualDoubleMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLng", "()D"));
					} finally {
					}
				}
			}

		}

		[Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Traffic", DoNotGenerateAcw=true)]
		public abstract class Traffic : Java.Lang.Object {

			internal Traffic ()
			{
			}

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/interface[@name='NavigationSuggestion.Traffic']/field[@name='HEAVY']"
			[Register ("HEAVY")]
			public const int Heavy = (int) 3;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/interface[@name='NavigationSuggestion.Traffic']/field[@name='LIGHT']"
			[Register ("LIGHT")]
			public const int Light = (int) 1;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/interface[@name='NavigationSuggestion.Traffic']/field[@name='MEDIUM']"
			[Register ("MEDIUM")]
			public const int Medium = (int) 2;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/interface[@name='NavigationSuggestion.Traffic']/field[@name='UNKNOWN']"
			[Register ("UNKNOWN")]
			public const int Unknown = (int) 0;

			// The following are fields from: java.lang.annotation.Annotation

			// The following are fields from: Android.Runtime.IJavaObject

			// The following are fields from: System.IDisposable
		}

		[Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Traffic", DoNotGenerateAcw=true)]
		[global::System.Obsolete ("Use the 'Traffic' type. This type will be removed in a future release.")]
		public abstract class TrafficConsts : Traffic {

			private TrafficConsts ()
			{
			}
		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/interface[@name='NavigationSuggestion.Traffic']"
		[Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Traffic", "", "Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion/ITrafficInvoker")]
		public partial interface ITraffic : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Traffic", DoNotGenerateAcw=true)]
		internal class ITrafficInvoker : global::Java.Lang.Object, ITraffic {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$Traffic");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ITrafficInvoker); }
			}

			IntPtr class_ref;

			public static ITraffic GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ITraffic> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.nav.suggestion.NavigationSuggestion.Traffic"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ITrafficInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.ITraffic> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (NavigationSuggestion); }
		}

		protected NavigationSuggestion (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/constructor[@name='NavigationSuggestion' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe NavigationSuggestion ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (NavigationSuggestion)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAddress;
#pragma warning disable 0169
		static Delegate GetGetAddressHandler ()
		{
			if (cb_getAddress == null)
				cb_getAddress = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAddress);
			return cb_getAddress;
		}

		static IntPtr n_GetAddress (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.AddressFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getAddress;
		public virtual unsafe global::Java.Lang.ICharSequence AddressFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getAddress' and count(parameter)=0]"
			[Register ("getAddress", "()Ljava/lang/CharSequence;", "GetGetAddressHandler")]
			get {
				if (id_getAddress == IntPtr.Zero)
					id_getAddress = JNIEnv.GetMethodID (class_ref, "getAddress", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAddress), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAddress", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string Address {
			get { return AddressFormatted == null ? null : AddressFormatted.ToString (); }
		}

		static Delegate cb_getFormattedTimeToDestination;
#pragma warning disable 0169
		static Delegate GetGetFormattedTimeToDestinationHandler ()
		{
			if (cb_getFormattedTimeToDestination == null)
				cb_getFormattedTimeToDestination = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFormattedTimeToDestination);
			return cb_getFormattedTimeToDestination;
		}

		static IntPtr n_GetFormattedTimeToDestination (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.FormattedTimeToDestinationFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getFormattedTimeToDestination;
		public virtual unsafe global::Java.Lang.ICharSequence FormattedTimeToDestinationFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getFormattedTimeToDestination' and count(parameter)=0]"
			[Register ("getFormattedTimeToDestination", "()Ljava/lang/CharSequence;", "GetGetFormattedTimeToDestinationHandler")]
			get {
				if (id_getFormattedTimeToDestination == IntPtr.Zero)
					id_getFormattedTimeToDestination = JNIEnv.GetMethodID (class_ref, "getFormattedTimeToDestination", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getFormattedTimeToDestination), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFormattedTimeToDestination", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string FormattedTimeToDestination {
			get { return FormattedTimeToDestinationFormatted == null ? null : FormattedTimeToDestinationFormatted.ToString (); }
		}

		static Delegate cb_hasLatLng;
#pragma warning disable 0169
		static Delegate GetHasLatLngHandler ()
		{
			if (cb_hasLatLng == null)
				cb_hasLatLng = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasLatLng);
			return cb_hasLatLng;
		}

		static bool n_HasLatLng (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasLatLng;
		}
#pragma warning restore 0169

		static IntPtr id_hasLatLng;
		public virtual unsafe bool HasLatLng {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='hasLatLng' and count(parameter)=0]"
			[Register ("hasLatLng", "()Z", "GetHasLatLngHandler")]
			get {
				if (id_hasLatLng == IntPtr.Zero)
					id_hasLatLng = JNIEnv.GetMethodID (class_ref, "hasLatLng", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasLatLng);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasLatLng", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.NameFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		public virtual unsafe global::Java.Lang.ICharSequence NameFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/CharSequence;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string Name {
			get { return NameFormatted == null ? null : NameFormatted.ToString (); }
		}

		static Delegate cb_getNavigationIntent;
#pragma warning disable 0169
		static Delegate GetGetNavigationIntentHandler ()
		{
			if (cb_getNavigationIntent == null)
				cb_getNavigationIntent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetNavigationIntent);
			return cb_getNavigationIntent;
		}

		static IntPtr n_GetNavigationIntent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.NavigationIntent);
		}
#pragma warning restore 0169

		static IntPtr id_getNavigationIntent;
		public virtual unsafe global::Android.Content.Intent NavigationIntent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getNavigationIntent' and count(parameter)=0]"
			[Register ("getNavigationIntent", "()Landroid/content/Intent;", "GetGetNavigationIntentHandler")]
			get {
				if (id_getNavigationIntent == IntPtr.Zero)
					id_getNavigationIntent = JNIEnv.GetMethodID (class_ref, "getNavigationIntent", "()Landroid/content/Intent;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getNavigationIntent), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getNavigationIntent", "()Landroid/content/Intent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getRoute;
#pragma warning disable 0169
		static Delegate GetGetRouteHandler ()
		{
			if (cb_getRoute == null)
				cb_getRoute = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRoute);
			return cb_getRoute;
		}

		static IntPtr n_GetRoute (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return CharSequence.ToLocalJniHandle (__this.RouteFormatted);
		}
#pragma warning restore 0169

		static IntPtr id_getRoute;
		public virtual unsafe global::Java.Lang.ICharSequence RouteFormatted {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getRoute' and count(parameter)=0]"
			[Register ("getRoute", "()Ljava/lang/CharSequence;", "GetGetRouteHandler")]
			get {
				if (id_getRoute == IntPtr.Zero)
					id_getRoute = JNIEnv.GetMethodID (class_ref, "getRoute", "()Ljava/lang/CharSequence;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getRoute), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<Java.Lang.ICharSequence> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRoute", "()Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		public string Route {
			get { return RouteFormatted == null ? null : RouteFormatted.ToString (); }
		}

		static Delegate cb_getSecondsToDestination;
#pragma warning disable 0169
		static Delegate GetGetSecondsToDestinationHandler ()
		{
			if (cb_getSecondsToDestination == null)
				cb_getSecondsToDestination = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetSecondsToDestination);
			return cb_getSecondsToDestination;
		}

		static int n_GetSecondsToDestination (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SecondsToDestination;
		}
#pragma warning restore 0169

		static IntPtr id_getSecondsToDestination;
		public virtual unsafe int SecondsToDestination {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getSecondsToDestination' and count(parameter)=0]"
			[Register ("getSecondsToDestination", "()I", "GetGetSecondsToDestinationHandler")]
			get {
				if (id_getSecondsToDestination == IntPtr.Zero)
					id_getSecondsToDestination = JNIEnv.GetMethodID (class_ref, "getSecondsToDestination", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getSecondsToDestination);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSecondsToDestination", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getLatLng;
#pragma warning disable 0169
		static Delegate GetGetLatLngHandler ()
		{
			if (cb_getLatLng == null)
				cb_getLatLng = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLatLng);
			return cb_getLatLng;
		}

		static IntPtr n_GetLatLng (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLatLng ());
		}
#pragma warning restore 0169

		static IntPtr id_getLatLng;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getLatLng' and count(parameter)=0]"
		[Register ("getLatLng", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;", "GetGetLatLngHandler")]
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng GetLatLng ()
		{
			if (id_getLatLng == IntPtr.Zero)
				id_getLatLng = JNIEnv.GetMethodID (class_ref, "getLatLng", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLatLng), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatLng", "()Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_getWaypoints;
#pragma warning disable 0169
		static Delegate GetGetWaypointsHandler ()
		{
			if (cb_getWaypoints == null)
				cb_getWaypoints = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetWaypoints);
			return cb_getWaypoints;
		}

		static IntPtr n_GetWaypoints (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetWaypoints ());
		}
#pragma warning restore 0169

		static IntPtr id_getWaypoints;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='getWaypoints' and count(parameter)=0]"
		[Register ("getWaypoints", "()[Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;", "GetGetWaypointsHandler")]
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[] GetWaypoints ()
		{
			if (id_getWaypoints == IntPtr.Zero)
				id_getWaypoints = JNIEnv.GetMethodID (class_ref, "getWaypoints", "()[Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getWaypoints), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng));
				else
					return (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getWaypoints", "()[Lcom/google/android/apps/auto/sdk/nav/suggestion/NavigationSuggestion$LatLng;")), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion.LatLng));
			} finally {
			}
		}

		static Delegate cb_readFromBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetReadFromBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_readFromBundle_Landroid_os_Bundle_ == null)
				cb_readFromBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ReadFromBundle_Landroid_os_Bundle_);
			return cb_readFromBundle_Landroid_os_Bundle_;
		}

		static void n_ReadFromBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReadFromBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_readFromBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='readFromBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("readFromBundle", "(Landroid/os/Bundle;)V", "GetReadFromBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void ReadFromBundle (global::Android.OS.Bundle p0)
		{
			if (id_readFromBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_readFromBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "readFromBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_readFromBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readFromBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToBundle_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetWriteToBundle_Landroid_os_Bundle_Handler ()
		{
			if (cb_writeToBundle_Landroid_os_Bundle_ == null)
				cb_writeToBundle_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_WriteToBundle_Landroid_os_Bundle_);
			return cb_writeToBundle_Landroid_os_Bundle_;
		}

		static void n_WriteToBundle_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.Suggestion.NavigationSuggestion> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.WriteToBundle (p0);
		}
#pragma warning restore 0169

		static IntPtr id_writeToBundle_Landroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav.suggestion']/class[@name='NavigationSuggestion']/method[@name='writeToBundle' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("writeToBundle", "(Landroid/os/Bundle;)V", "GetWriteToBundle_Landroid_os_Bundle_Handler")]
		protected override unsafe void WriteToBundle (global::Android.OS.Bundle p0)
		{
			if (id_writeToBundle_Landroid_os_Bundle_ == IntPtr.Zero)
				id_writeToBundle_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "writeToBundle", "(Landroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_writeToBundle_Landroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToBundle", "(Landroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

	}
}
