using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager.CarNavigationStatusListener']"
	[Register ("com/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener", "", "Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerInvoker")]
	public partial interface ICarNavigationStatusManagerCarNavigationStatusListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager.CarNavigationStatusListener']/method[@name='onStart' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("onStart", "(IIIII)V", "GetOnStart_IIIIIHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerInvoker, AndroidAutoBindingJar")]
		void OnStart (int p0, int p1, int p2, int p3, int p4);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager.CarNavigationStatusListener']/method[@name='onStop' and count(parameter)=0]"
		[Register ("onStop", "()V", "GetOnStopHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerInvoker, AndroidAutoBindingJar")]
		void OnStop ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener", DoNotGenerateAcw=true)]
	internal class ICarNavigationStatusManagerCarNavigationStatusListenerInvoker : global::Java.Lang.Object, ICarNavigationStatusManagerCarNavigationStatusListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarNavigationStatusManagerCarNavigationStatusListenerInvoker); }
		}

		IntPtr class_ref;

		public static ICarNavigationStatusManagerCarNavigationStatusListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarNavigationStatusManagerCarNavigationStatusListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarNavigationStatusManager.CarNavigationStatusListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarNavigationStatusManagerCarNavigationStatusListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_onStart_IIIII;
#pragma warning disable 0169
		static Delegate GetOnStart_IIIIIHandler ()
		{
			if (cb_onStart_IIIII == null)
				cb_onStart_IIIII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int, int, int>) n_OnStart_IIIII);
			return cb_onStart_IIIII;
		}

		static void n_OnStart_IIIII (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, int p3, int p4)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStart (p0, p1, p2, p3, p4);
		}
#pragma warning restore 0169

		IntPtr id_onStart_IIIII;
		public unsafe void OnStart (int p0, int p1, int p2, int p3, int p4)
		{
			if (id_onStart_IIIII == IntPtr.Zero)
				id_onStart_IIIII = JNIEnv.GetMethodID (class_ref, "onStart", "(IIIII)V");
			JValue* __args = stackalloc JValue [5];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			__args [3] = new JValue (p3);
			__args [4] = new JValue (p4);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStart_IIIII, __args);
		}

		static Delegate cb_onStop;
#pragma warning disable 0169
		static Delegate GetOnStopHandler ()
		{
			if (cb_onStop == null)
				cb_onStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStop);
			return cb_onStop;
		}

		static void n_OnStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStop ();
		}
#pragma warning restore 0169

		IntPtr id_onStop;
		public unsafe void OnStop ()
		{
			if (id_onStop == IntPtr.Zero)
				id_onStop = JNIEnv.GetMethodID (class_ref, "onStop", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStop);
		}

	}

	public partial class StartEventArgs : global::System.EventArgs {

		public StartEventArgs (int p0, int p1, int p2, int p3, int p4)
		{
			this.p0 = p0;
			this.p1 = p1;
			this.p2 = p2;
			this.p3 = p3;
			this.p4 = p4;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}

		int p1;
		public int P1 {
			get { return p1; }
		}

		int p2;
		public int P2 {
			get { return p2; }
		}

		int p3;
		public int P3 {
			get { return p3; }
		}

		int p4;
		public int P4 {
			get { return p4; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/google/android/gms/car/CarNavigationStatusManager_CarNavigationStatusListenerImplementor")]
	internal sealed partial class ICarNavigationStatusManagerCarNavigationStatusListenerImplementor : global::Java.Lang.Object, ICarNavigationStatusManagerCarNavigationStatusListener {

		object sender;

		public ICarNavigationStatusManagerCarNavigationStatusListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/gms/car/CarNavigationStatusManager_CarNavigationStatusListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<StartEventArgs> OnStartHandler;
#pragma warning restore 0649

		public void OnStart (int p0, int p1, int p2, int p3, int p4)
		{
			var __h = OnStartHandler;
			if (__h != null)
				__h (sender, new StartEventArgs (p0, p1, p2, p3, p4));
		}
#pragma warning disable 0649
		public EventHandler OnStopHandler;
#pragma warning restore 0649

		public void OnStop ()
		{
			var __h = OnStopHandler;
			if (__h != null)
				__h (sender, new EventArgs ());
		}

		internal static bool __IsEmpty (ICarNavigationStatusManagerCarNavigationStatusListenerImplementor value)
		{
			return value.OnStartHandler == null && value.OnStopHandler == null;
		}
	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']"
	[Register ("com/google/android/gms/car/CarNavigationStatusManager", "", "Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker")]
	public partial interface ICarNavigationStatusManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']/method[@name='registerListener' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarNavigationStatusManager.CarNavigationStatusListener']]"
		[Register ("registerListener", "(Lcom/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener;)V", "GetRegisterListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_Handler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker, AndroidAutoBindingJar")]
		void RegisterListener (global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']/method[@name='sendNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("sendNavigationStatus", "(I)Z", "GetSendNavigationStatus_IHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker, AndroidAutoBindingJar")]
		bool SendNavigationStatus (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnDistanceEvent' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("sendNavigationTurnDistanceEvent", "(IIII)Z", "GetSendNavigationTurnDistanceEvent_IIIIHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker, AndroidAutoBindingJar")]
		bool SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']/method[@name='sendNavigationTurnEvent' and count(parameter)=6 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='byte[]'] and parameter[6][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/String;II[BI)Z", "GetSendNavigationTurnEvent_ILjava_lang_String_IIarrayBIHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker, AndroidAutoBindingJar")]
		bool SendNavigationTurnEvent (int p0, string p1, int p2, int p3, byte[] p4, int p5);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarNavigationStatusManager']/method[@name='unregisterListener' and count(parameter)=0]"
		[Register ("unregisterListener", "()V", "GetUnregisterListenerHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerInvoker, AndroidAutoBindingJar")]
		void UnregisterListener ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarNavigationStatusManager", DoNotGenerateAcw=true)]
	internal class ICarNavigationStatusManagerInvoker : global::Java.Lang.Object, ICarNavigationStatusManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarNavigationStatusManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarNavigationStatusManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarNavigationStatusManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarNavigationStatusManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarNavigationStatusManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarNavigationStatusManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_;
#pragma warning disable 0169
		static Delegate GetRegisterListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_Handler ()
		{
			if (cb_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_ == null)
				cb_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RegisterListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_);
			return cb_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_;
		}

		static void n_RegisterListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener p0 = (global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegisterListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_;
		public unsafe void RegisterListener (global::Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListener p0)
		{
			if (id_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_ == IntPtr.Zero)
				id_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_ = JNIEnv.GetMethodID (class_ref, "registerListener", "(Lcom/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_registerListener_Lcom_google_android_gms_car_CarNavigationStatusManager_CarNavigationStatusListener_, __args);
		}

		static Delegate cb_sendNavigationStatus_I;
#pragma warning disable 0169
		static Delegate GetSendNavigationStatus_IHandler ()
		{
			if (cb_sendNavigationStatus_I == null)
				cb_sendNavigationStatus_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_SendNavigationStatus_I);
			return cb_sendNavigationStatus_I;
		}

		static bool n_SendNavigationStatus_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SendNavigationStatus (p0);
		}
#pragma warning restore 0169

		IntPtr id_sendNavigationStatus_I;
		public unsafe bool SendNavigationStatus (int p0)
		{
			if (id_sendNavigationStatus_I == IntPtr.Zero)
				id_sendNavigationStatus_I = JNIEnv.GetMethodID (class_ref, "sendNavigationStatus", "(I)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationStatus_I, __args);
		}

		static Delegate cb_sendNavigationTurnDistanceEvent_IIII;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnDistanceEvent_IIIIHandler ()
		{
			if (cb_sendNavigationTurnDistanceEvent_IIII == null)
				cb_sendNavigationTurnDistanceEvent_IIII = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int, int, bool>) n_SendNavigationTurnDistanceEvent_IIII);
			return cb_sendNavigationTurnDistanceEvent_IIII;
		}

		static bool n_SendNavigationTurnDistanceEvent_IIII (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, int p3)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SendNavigationTurnDistanceEvent (p0, p1, p2, p3);
		}
#pragma warning restore 0169

		IntPtr id_sendNavigationTurnDistanceEvent_IIII;
		public unsafe bool SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3)
		{
			if (id_sendNavigationTurnDistanceEvent_IIII == IntPtr.Zero)
				id_sendNavigationTurnDistanceEvent_IIII = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnDistanceEvent", "(IIII)Z");
			JValue* __args = stackalloc JValue [4];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			__args [3] = new JValue (p3);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnDistanceEvent_IIII, __args);
		}

		static Delegate cb_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnEvent_ILjava_lang_String_IIarrayBIHandler ()
		{
			if (cb_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI == null)
				cb_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, int, int, IntPtr, int, bool>) n_SendNavigationTurnEvent_ILjava_lang_String_IIarrayBI);
			return cb_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI;
		}

		static bool n_SendNavigationTurnEvent_ILjava_lang_String_IIarrayBI (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, int p2, int p3, IntPtr native_p4, int p5)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			byte[] p4 = (byte[]) JNIEnv.GetArray (native_p4, JniHandleOwnership.DoNotTransfer, typeof (byte));
			bool __ret = __this.SendNavigationTurnEvent (p0, p1, p2, p3, p4, p5);
			if (p4 != null)
				JNIEnv.CopyArray (p4, native_p4);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI;
		public unsafe bool SendNavigationTurnEvent (int p0, string p1, int p2, int p3, byte[] p4, int p5)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/String;II[BI)Z");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			IntPtr native_p4 = JNIEnv.NewArray (p4);
			JValue* __args = stackalloc JValue [6];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (native_p1);
			__args [2] = new JValue (p2);
			__args [3] = new JValue (p3);
			__args [4] = new JValue (native_p4);
			__args [5] = new JValue (p5);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_String_IIarrayBI, __args);
			JNIEnv.DeleteLocalRef (native_p1);
			if (p4 != null) {
				JNIEnv.CopyArray (native_p4, p4);
				JNIEnv.DeleteLocalRef (native_p4);
			}
			return __ret;
		}

		static Delegate cb_unregisterListener;
#pragma warning disable 0169
		static Delegate GetUnregisterListenerHandler ()
		{
			if (cb_unregisterListener == null)
				cb_unregisterListener = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_UnregisterListener);
			return cb_unregisterListener;
		}

		static void n_UnregisterListener (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarNavigationStatusManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.UnregisterListener ();
		}
#pragma warning restore 0169

		IntPtr id_unregisterListener;
		public unsafe void UnregisterListener ()
		{
			if (id_unregisterListener == IntPtr.Zero)
				id_unregisterListener = JNIEnv.GetMethodID (class_ref, "unregisterListener", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_unregisterListener);
		}

	}

}
