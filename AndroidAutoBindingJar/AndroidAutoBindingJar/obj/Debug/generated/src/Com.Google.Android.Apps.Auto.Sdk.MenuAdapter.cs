using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuAdapter", DoNotGenerateAcw=true)]
	public abstract partial class MenuAdapter : global::Java.Lang.Object {


		static IntPtr mConfig_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/field[@name='mConfig']"
		[Register ("mConfig")]
		protected global::Android.OS.Bundle MConfig {
			get {
				if (mConfig_jfieldId == IntPtr.Zero)
					mConfig_jfieldId = JNIEnv.GetFieldID (class_ref, "mConfig", "Landroid/os/Bundle;");
				IntPtr __ret = JNIEnv.GetObjectField (((global::Java.Lang.Object) this).Handle, mConfig_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (mConfig_jfieldId == IntPtr.Zero)
					mConfig_jfieldId = JNIEnv.GetFieldID (class_ref, "mConfig", "Landroid/os/Bundle;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetField (((global::Java.Lang.Object) this).Handle, mConfig_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuAdapter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (MenuAdapter); }
		}

		protected MenuAdapter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/constructor[@name='MenuAdapter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe MenuAdapter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (MenuAdapter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getMenuItemCount;
#pragma warning disable 0169
		static Delegate GetGetMenuItemCountHandler ()
		{
			if (cb_getMenuItemCount == null)
				cb_getMenuItemCount = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMenuItemCount);
			return cb_getMenuItemCount;
		}

		static int n_GetMenuItemCount (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MenuItemCount;
		}
#pragma warning restore 0169

		public abstract int MenuItemCount {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItemCount' and count(parameter)=0]"
			[Register ("getMenuItemCount", "()I", "GetGetMenuItemCountHandler")] get;
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()Lcom/google/android/apps/auto/sdk/MenuAdapter;", "")]
		public unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()Lcom/google/android/apps/auto/sdk/MenuAdapter;");
			try {
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_a_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuAdapter']]"
		[Register ("a", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V", "")]
		public unsafe void A (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0)
		{
			if (id_a_Lcom_google_android_apps_auto_sdk_MenuAdapter_ == IntPtr.Zero)
				id_a_Lcom_google_android_apps_auto_sdk_MenuAdapter_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_Lcom_google_android_apps_auto_sdk_MenuAdapter_, __args);
			} finally {
			}
		}

		static IntPtr id_a_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='a' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("a", "(I)Lcom/google/android/apps/auto/sdk/MenuAdapter;", "")]
		public unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter A (int p0)
		{
			if (id_a_I == IntPtr.Zero)
				id_a_I = JNIEnv.GetMethodID (class_ref, "a", "(I)Lcom/google/android/apps/auto/sdk/MenuAdapter;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_a_ILcom_google_android_apps_auto_sdk_MenuAdapter_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='a' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='com.google.android.apps.auto.sdk.MenuAdapter']]"
		[Register ("a", "(ILcom/google/android/apps/auto/sdk/MenuAdapter;)V", "")]
		public unsafe void A (int p0, global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p1)
		{
			if (id_a_ILcom_google_android_apps_auto_sdk_MenuAdapter_ == IntPtr.Zero)
				id_a_ILcom_google_android_apps_auto_sdk_MenuAdapter_ = JNIEnv.GetMethodID (class_ref, "a", "(ILcom/google/android/apps/auto/sdk/MenuAdapter;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a_ILcom_google_android_apps_auto_sdk_MenuAdapter_, __args);
			} finally {
			}
		}

		static Delegate cb_getMenuItem_I;
#pragma warning disable 0169
		static Delegate GetGetMenuItem_IHandler ()
		{
			if (cb_getMenuItem_I == null)
				cb_getMenuItem_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetMenuItem_I);
			return cb_getMenuItem_I;
		}

		static IntPtr n_GetMenuItem_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetMenuItem (p0));
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItem' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getMenuItem", "(I)Lcom/google/android/apps/auto/sdk/MenuItem;", "GetGetMenuItem_IHandler")]
		public abstract global::Com.Google.Android.Apps.Auto.Sdk.MenuItem GetMenuItem (int p0);

		static Delegate cb_hideLoadingIndicator;
#pragma warning disable 0169
		static Delegate GetHideLoadingIndicatorHandler ()
		{
			if (cb_hideLoadingIndicator == null)
				cb_hideLoadingIndicator = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideLoadingIndicator);
			return cb_hideLoadingIndicator;
		}

		static void n_HideLoadingIndicator (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideLoadingIndicator ();
		}
#pragma warning restore 0169

		static IntPtr id_hideLoadingIndicator;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='hideLoadingIndicator' and count(parameter)=0]"
		[Register ("hideLoadingIndicator", "()V", "GetHideLoadingIndicatorHandler")]
		public virtual unsafe void HideLoadingIndicator ()
		{
			if (id_hideLoadingIndicator == IntPtr.Zero)
				id_hideLoadingIndicator = JNIEnv.GetMethodID (class_ref, "hideLoadingIndicator", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideLoadingIndicator);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideLoadingIndicator", "()V"));
			} finally {
			}
		}

		static Delegate cb_notifyDataSetChanged;
#pragma warning disable 0169
		static Delegate GetNotifyDataSetChangedHandler ()
		{
			if (cb_notifyDataSetChanged == null)
				cb_notifyDataSetChanged = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_NotifyDataSetChanged);
			return cb_notifyDataSetChanged;
		}

		static void n_NotifyDataSetChanged (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.NotifyDataSetChanged ();
		}
#pragma warning restore 0169

		static IntPtr id_notifyDataSetChanged;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='notifyDataSetChanged' and count(parameter)=0]"
		[Register ("notifyDataSetChanged", "()V", "GetNotifyDataSetChangedHandler")]
		public virtual unsafe void NotifyDataSetChanged ()
		{
			if (id_notifyDataSetChanged == IntPtr.Zero)
				id_notifyDataSetChanged = JNIEnv.GetMethodID (class_ref, "notifyDataSetChanged", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_notifyDataSetChanged);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "notifyDataSetChanged", "()V"));
			} finally {
			}
		}

		static Delegate cb_notifyItemChanged_I;
#pragma warning disable 0169
		static Delegate GetNotifyItemChanged_IHandler ()
		{
			if (cb_notifyItemChanged_I == null)
				cb_notifyItemChanged_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_NotifyItemChanged_I);
			return cb_notifyItemChanged_I;
		}

		static void n_NotifyItemChanged_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.NotifyItemChanged (p0);
		}
#pragma warning restore 0169

		static IntPtr id_notifyItemChanged_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='notifyItemChanged' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("notifyItemChanged", "(I)V", "GetNotifyItemChanged_IHandler")]
		public virtual unsafe void NotifyItemChanged (int p0)
		{
			if (id_notifyItemChanged_I == IntPtr.Zero)
				id_notifyItemChanged_I = JNIEnv.GetMethodID (class_ref, "notifyItemChanged", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_notifyItemChanged_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "notifyItemChanged", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_;
#pragma warning disable 0169
		static Delegate GetOnAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_Handler ()
		{
			if (cb_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_ == null)
				cb_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_);
			return cb_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_;
		}

		static void n_OnAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback p0 = (global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnAttach (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onAttach' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuAdapterCallback']]"
		[Register ("onAttach", "(Lcom/google/android/apps/auto/sdk/MenuAdapterCallback;)V", "GetOnAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_Handler")]
		public virtual unsafe void OnAttach (global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback p0)
		{
			if (id_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_ == IntPtr.Zero)
				id_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_ = JNIEnv.GetMethodID (class_ref, "onAttach", "(Lcom/google/android/apps/auto/sdk/MenuAdapterCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAttach_Lcom_google_android_apps_auto_sdk_MenuAdapterCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onAttach", "(Lcom/google/android/apps/auto/sdk/MenuAdapterCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onDetach;
#pragma warning disable 0169
		static Delegate GetOnDetachHandler ()
		{
			if (cb_onDetach == null)
				cb_onDetach = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDetach);
			return cb_onDetach;
		}

		static void n_OnDetach (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDetach ();
		}
#pragma warning restore 0169

		static IntPtr id_onDetach;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onDetach' and count(parameter)=0]"
		[Register ("onDetach", "()V", "GetOnDetachHandler")]
		public virtual unsafe void OnDetach ()
		{
			if (id_onDetach == IntPtr.Zero)
				id_onDetach = JNIEnv.GetMethodID (class_ref, "onDetach", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDetach);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onDetach", "()V"));
			} finally {
			}
		}

		static Delegate cb_onEnter;
#pragma warning disable 0169
		static Delegate GetOnEnterHandler ()
		{
			if (cb_onEnter == null)
				cb_onEnter = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnEnter);
			return cb_onEnter;
		}

		static void n_OnEnter (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnEnter ();
		}
#pragma warning restore 0169

		static IntPtr id_onEnter;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onEnter' and count(parameter)=0]"
		[Register ("onEnter", "()V", "GetOnEnterHandler")]
		public virtual unsafe void OnEnter ()
		{
			if (id_onEnter == IntPtr.Zero)
				id_onEnter = JNIEnv.GetMethodID (class_ref, "onEnter", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onEnter);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onEnter", "()V"));
			} finally {
			}
		}

		static Delegate cb_onExit;
#pragma warning disable 0169
		static Delegate GetOnExitHandler ()
		{
			if (cb_onExit == null)
				cb_onExit = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnExit);
			return cb_onExit;
		}

		static void n_OnExit (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnExit ();
		}
#pragma warning restore 0169

		static IntPtr id_onExit;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onExit' and count(parameter)=0]"
		[Register ("onExit", "()V", "GetOnExitHandler")]
		public virtual unsafe void OnExit ()
		{
			if (id_onExit == IntPtr.Zero)
				id_onExit = JNIEnv.GetMethodID (class_ref, "onExit", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onExit);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onExit", "()V"));
			} finally {
			}
		}

		static Delegate cb_onLoadSubmenu_I;
#pragma warning disable 0169
		static Delegate GetOnLoadSubmenu_IHandler ()
		{
			if (cb_onLoadSubmenu_I == null)
				cb_onLoadSubmenu_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_OnLoadSubmenu_I);
			return cb_onLoadSubmenu_I;
		}

		static IntPtr n_OnLoadSubmenu_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OnLoadSubmenu (p0));
		}
#pragma warning restore 0169

		static IntPtr id_onLoadSubmenu_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onLoadSubmenu' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onLoadSubmenu", "(I)Lcom/google/android/apps/auto/sdk/MenuAdapter;", "GetOnLoadSubmenu_IHandler")]
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter OnLoadSubmenu (int p0)
		{
			if (id_onLoadSubmenu_I == IntPtr.Zero)
				id_onLoadSubmenu_I = JNIEnv.GetMethodID (class_ref, "onLoadSubmenu", "(I)Lcom/google/android/apps/auto/sdk/MenuAdapter;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onLoadSubmenu_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onLoadSubmenu", "(I)Lcom/google/android/apps/auto/sdk/MenuAdapter;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_onMenuItemClicked_I;
#pragma warning disable 0169
		static Delegate GetOnMenuItemClicked_IHandler ()
		{
			if (cb_onMenuItemClicked_I == null)
				cb_onMenuItemClicked_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnMenuItemClicked_I);
			return cb_onMenuItemClicked_I;
		}

		static void n_OnMenuItemClicked_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnMenuItemClicked (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onMenuItemClicked_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='onMenuItemClicked' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onMenuItemClicked", "(I)V", "GetOnMenuItemClicked_IHandler")]
		public virtual unsafe void OnMenuItemClicked (int p0)
		{
			if (id_onMenuItemClicked_I == IntPtr.Zero)
				id_onMenuItemClicked_I = JNIEnv.GetMethodID (class_ref, "onMenuItemClicked", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onMenuItemClicked_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onMenuItemClicked", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_showLoadingIndicator;
#pragma warning disable 0169
		static Delegate GetShowLoadingIndicatorHandler ()
		{
			if (cb_showLoadingIndicator == null)
				cb_showLoadingIndicator = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowLoadingIndicator);
			return cb_showLoadingIndicator;
		}

		static void n_ShowLoadingIndicator (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowLoadingIndicator ();
		}
#pragma warning restore 0169

		static IntPtr id_showLoadingIndicator;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='showLoadingIndicator' and count(parameter)=0]"
		[Register ("showLoadingIndicator", "()V", "GetShowLoadingIndicatorHandler")]
		public virtual unsafe void ShowLoadingIndicator ()
		{
			if (id_showLoadingIndicator == IntPtr.Zero)
				id_showLoadingIndicator = JNIEnv.GetMethodID (class_ref, "showLoadingIndicator", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showLoadingIndicator);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showLoadingIndicator", "()V"));
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuAdapter", DoNotGenerateAcw=true)]
	internal partial class MenuAdapterInvoker : MenuAdapter {

		public MenuAdapterInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (MenuAdapterInvoker); }
		}

		static IntPtr id_getMenuItemCount;
		public override unsafe int MenuItemCount {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItemCount' and count(parameter)=0]"
			[Register ("getMenuItemCount", "()I", "GetGetMenuItemCountHandler")]
			get {
				if (id_getMenuItemCount == IntPtr.Zero)
					id_getMenuItemCount = JNIEnv.GetMethodID (class_ref, "getMenuItemCount", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMenuItemCount);
				} finally {
				}
			}
		}

		static IntPtr id_getMenuItem_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuAdapter']/method[@name='getMenuItem' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getMenuItem", "(I)Lcom/google/android/apps/auto/sdk/MenuItem;", "GetGetMenuItem_IHandler")]
		public override unsafe global::Com.Google.Android.Apps.Auto.Sdk.MenuItem GetMenuItem (int p0)
		{
			if (id_getMenuItem_I == IntPtr.Zero)
				id_getMenuItem_I = JNIEnv.GetMethodID (class_ref, "getMenuItem", "(I)Lcom/google/android/apps/auto/sdk/MenuItem;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuItem> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getMenuItem_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}

}
