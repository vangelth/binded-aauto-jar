using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']"
	[Register ("com/google/android/gms/car/CarInfoManager$CarInfo", "", "Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker")]
	public partial interface ICarInfoManagerCarInfo : IJavaObject {

		string DisplayName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getDisplayName' and count(parameter)=0]"
			[Register ("getDisplayName", "()Ljava/lang/String;", "GetGetDisplayNameHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		int DriverPosition {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getDriverPosition' and count(parameter)=0]"
			[Register ("getDriverPosition", "()I", "GetGetDriverPositionHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string HeadUnitMake {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitMake' and count(parameter)=0]"
			[Register ("getHeadUnitMake", "()Ljava/lang/String;", "GetGetHeadUnitMakeHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string HeadUnitModel {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitModel' and count(parameter)=0]"
			[Register ("getHeadUnitModel", "()Ljava/lang/String;", "GetGetHeadUnitModelHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		int HeadUnitProtocolMajorVersionNumber {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitProtocolMajorVersionNumber' and count(parameter)=0]"
			[Register ("getHeadUnitProtocolMajorVersionNumber", "()I", "GetGetHeadUnitProtocolMajorVersionNumberHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		int HeadUnitProtocolMinorVersionNumber {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitProtocolMinorVersionNumber' and count(parameter)=0]"
			[Register ("getHeadUnitProtocolMinorVersionNumber", "()I", "GetGetHeadUnitProtocolMinorVersionNumberHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string HeadUnitSoftwareBuild {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitSoftwareBuild' and count(parameter)=0]"
			[Register ("getHeadUnitSoftwareBuild", "()Ljava/lang/String;", "GetGetHeadUnitSoftwareBuildHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string HeadUnitSoftwareVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getHeadUnitSoftwareVersion' and count(parameter)=0]"
			[Register ("getHeadUnitSoftwareVersion", "()Ljava/lang/String;", "GetGetHeadUnitSoftwareVersionHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsCanPlayNativeMediaDuringVr {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='isCanPlayNativeMediaDuringVr' and count(parameter)=0]"
			[Register ("isCanPlayNativeMediaDuringVr", "()Z", "GetIsCanPlayNativeMediaDuringVrHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsHideBatteryLevel {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='isHideBatteryLevel' and count(parameter)=0]"
			[Register ("isHideBatteryLevel", "()Z", "GetIsHideBatteryLevelHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsHidePhoneSignal {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='isHidePhoneSignal' and count(parameter)=0]"
			[Register ("isHidePhoneSignal", "()Z", "GetIsHidePhoneSignalHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsHideProjectedClock {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='isHideProjectedClock' and count(parameter)=0]"
			[Register ("isHideProjectedClock", "()Z", "GetIsHideProjectedClockHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string Manufacturer {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getManufacturer' and count(parameter)=0]"
			[Register ("getManufacturer", "()Ljava/lang/String;", "GetGetManufacturerHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string Model {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getModel' and count(parameter)=0]"
			[Register ("getModel", "()Ljava/lang/String;", "GetGetModelHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string ModelYear {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getModelYear' and count(parameter)=0]"
			[Register ("getModelYear", "()Ljava/lang/String;", "GetGetModelYearHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string Nickname {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getNickname' and count(parameter)=0]"
			[Register ("getNickname", "()Ljava/lang/String;", "GetGetNicknameHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

		string VehicleId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarInfo']/method[@name='getVehicleId' and count(parameter)=0]"
			[Register ("getVehicleId", "()Ljava/lang/String;", "GetGetVehicleIdHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarInfoInvoker, AndroidAutoBindingJar")] get;
		}

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarInfoManager$CarInfo", DoNotGenerateAcw=true)]
	internal class ICarInfoManagerCarInfoInvoker : global::Java.Lang.Object, ICarInfoManagerCarInfo {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarInfoManager$CarInfo");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarInfoManagerCarInfoInvoker); }
		}

		IntPtr class_ref;

		public static ICarInfoManagerCarInfo GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarInfoManagerCarInfo> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarInfoManager.CarInfo"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarInfoManagerCarInfoInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getDisplayName;
#pragma warning disable 0169
		static Delegate GetGetDisplayNameHandler ()
		{
			if (cb_getDisplayName == null)
				cb_getDisplayName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDisplayName);
			return cb_getDisplayName;
		}

		static IntPtr n_GetDisplayName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.DisplayName);
		}
#pragma warning restore 0169

		IntPtr id_getDisplayName;
		public unsafe string DisplayName {
			get {
				if (id_getDisplayName == IntPtr.Zero)
					id_getDisplayName = JNIEnv.GetMethodID (class_ref, "getDisplayName", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getDisplayName), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getDriverPosition;
#pragma warning disable 0169
		static Delegate GetGetDriverPositionHandler ()
		{
			if (cb_getDriverPosition == null)
				cb_getDriverPosition = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetDriverPosition);
			return cb_getDriverPosition;
		}

		static int n_GetDriverPosition (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DriverPosition;
		}
#pragma warning restore 0169

		IntPtr id_getDriverPosition;
		public unsafe int DriverPosition {
			get {
				if (id_getDriverPosition == IntPtr.Zero)
					id_getDriverPosition = JNIEnv.GetMethodID (class_ref, "getDriverPosition", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getDriverPosition);
			}
		}

		static Delegate cb_getHeadUnitMake;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitMakeHandler ()
		{
			if (cb_getHeadUnitMake == null)
				cb_getHeadUnitMake = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadUnitMake);
			return cb_getHeadUnitMake;
		}

		static IntPtr n_GetHeadUnitMake (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadUnitMake);
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitMake;
		public unsafe string HeadUnitMake {
			get {
				if (id_getHeadUnitMake == IntPtr.Zero)
					id_getHeadUnitMake = JNIEnv.GetMethodID (class_ref, "getHeadUnitMake", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitMake), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getHeadUnitModel;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitModelHandler ()
		{
			if (cb_getHeadUnitModel == null)
				cb_getHeadUnitModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadUnitModel);
			return cb_getHeadUnitModel;
		}

		static IntPtr n_GetHeadUnitModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadUnitModel);
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitModel;
		public unsafe string HeadUnitModel {
			get {
				if (id_getHeadUnitModel == IntPtr.Zero)
					id_getHeadUnitModel = JNIEnv.GetMethodID (class_ref, "getHeadUnitModel", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitModel), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getHeadUnitProtocolMajorVersionNumber;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitProtocolMajorVersionNumberHandler ()
		{
			if (cb_getHeadUnitProtocolMajorVersionNumber == null)
				cb_getHeadUnitProtocolMajorVersionNumber = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHeadUnitProtocolMajorVersionNumber);
			return cb_getHeadUnitProtocolMajorVersionNumber;
		}

		static int n_GetHeadUnitProtocolMajorVersionNumber (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HeadUnitProtocolMajorVersionNumber;
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitProtocolMajorVersionNumber;
		public unsafe int HeadUnitProtocolMajorVersionNumber {
			get {
				if (id_getHeadUnitProtocolMajorVersionNumber == IntPtr.Zero)
					id_getHeadUnitProtocolMajorVersionNumber = JNIEnv.GetMethodID (class_ref, "getHeadUnitProtocolMajorVersionNumber", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitProtocolMajorVersionNumber);
			}
		}

		static Delegate cb_getHeadUnitProtocolMinorVersionNumber;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitProtocolMinorVersionNumberHandler ()
		{
			if (cb_getHeadUnitProtocolMinorVersionNumber == null)
				cb_getHeadUnitProtocolMinorVersionNumber = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHeadUnitProtocolMinorVersionNumber);
			return cb_getHeadUnitProtocolMinorVersionNumber;
		}

		static int n_GetHeadUnitProtocolMinorVersionNumber (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HeadUnitProtocolMinorVersionNumber;
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitProtocolMinorVersionNumber;
		public unsafe int HeadUnitProtocolMinorVersionNumber {
			get {
				if (id_getHeadUnitProtocolMinorVersionNumber == IntPtr.Zero)
					id_getHeadUnitProtocolMinorVersionNumber = JNIEnv.GetMethodID (class_ref, "getHeadUnitProtocolMinorVersionNumber", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitProtocolMinorVersionNumber);
			}
		}

		static Delegate cb_getHeadUnitSoftwareBuild;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitSoftwareBuildHandler ()
		{
			if (cb_getHeadUnitSoftwareBuild == null)
				cb_getHeadUnitSoftwareBuild = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadUnitSoftwareBuild);
			return cb_getHeadUnitSoftwareBuild;
		}

		static IntPtr n_GetHeadUnitSoftwareBuild (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadUnitSoftwareBuild);
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitSoftwareBuild;
		public unsafe string HeadUnitSoftwareBuild {
			get {
				if (id_getHeadUnitSoftwareBuild == IntPtr.Zero)
					id_getHeadUnitSoftwareBuild = JNIEnv.GetMethodID (class_ref, "getHeadUnitSoftwareBuild", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitSoftwareBuild), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getHeadUnitSoftwareVersion;
#pragma warning disable 0169
		static Delegate GetGetHeadUnitSoftwareVersionHandler ()
		{
			if (cb_getHeadUnitSoftwareVersion == null)
				cb_getHeadUnitSoftwareVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHeadUnitSoftwareVersion);
			return cb_getHeadUnitSoftwareVersion;
		}

		static IntPtr n_GetHeadUnitSoftwareVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.HeadUnitSoftwareVersion);
		}
#pragma warning restore 0169

		IntPtr id_getHeadUnitSoftwareVersion;
		public unsafe string HeadUnitSoftwareVersion {
			get {
				if (id_getHeadUnitSoftwareVersion == IntPtr.Zero)
					id_getHeadUnitSoftwareVersion = JNIEnv.GetMethodID (class_ref, "getHeadUnitSoftwareVersion", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getHeadUnitSoftwareVersion), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_isCanPlayNativeMediaDuringVr;
#pragma warning disable 0169
		static Delegate GetIsCanPlayNativeMediaDuringVrHandler ()
		{
			if (cb_isCanPlayNativeMediaDuringVr == null)
				cb_isCanPlayNativeMediaDuringVr = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCanPlayNativeMediaDuringVr);
			return cb_isCanPlayNativeMediaDuringVr;
		}

		static bool n_IsCanPlayNativeMediaDuringVr (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsCanPlayNativeMediaDuringVr;
		}
#pragma warning restore 0169

		IntPtr id_isCanPlayNativeMediaDuringVr;
		public unsafe bool IsCanPlayNativeMediaDuringVr {
			get {
				if (id_isCanPlayNativeMediaDuringVr == IntPtr.Zero)
					id_isCanPlayNativeMediaDuringVr = JNIEnv.GetMethodID (class_ref, "isCanPlayNativeMediaDuringVr", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isCanPlayNativeMediaDuringVr);
			}
		}

		static Delegate cb_isHideBatteryLevel;
#pragma warning disable 0169
		static Delegate GetIsHideBatteryLevelHandler ()
		{
			if (cb_isHideBatteryLevel == null)
				cb_isHideBatteryLevel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsHideBatteryLevel);
			return cb_isHideBatteryLevel;
		}

		static bool n_IsHideBatteryLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsHideBatteryLevel;
		}
#pragma warning restore 0169

		IntPtr id_isHideBatteryLevel;
		public unsafe bool IsHideBatteryLevel {
			get {
				if (id_isHideBatteryLevel == IntPtr.Zero)
					id_isHideBatteryLevel = JNIEnv.GetMethodID (class_ref, "isHideBatteryLevel", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isHideBatteryLevel);
			}
		}

		static Delegate cb_isHidePhoneSignal;
#pragma warning disable 0169
		static Delegate GetIsHidePhoneSignalHandler ()
		{
			if (cb_isHidePhoneSignal == null)
				cb_isHidePhoneSignal = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsHidePhoneSignal);
			return cb_isHidePhoneSignal;
		}

		static bool n_IsHidePhoneSignal (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsHidePhoneSignal;
		}
#pragma warning restore 0169

		IntPtr id_isHidePhoneSignal;
		public unsafe bool IsHidePhoneSignal {
			get {
				if (id_isHidePhoneSignal == IntPtr.Zero)
					id_isHidePhoneSignal = JNIEnv.GetMethodID (class_ref, "isHidePhoneSignal", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isHidePhoneSignal);
			}
		}

		static Delegate cb_isHideProjectedClock;
#pragma warning disable 0169
		static Delegate GetIsHideProjectedClockHandler ()
		{
			if (cb_isHideProjectedClock == null)
				cb_isHideProjectedClock = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsHideProjectedClock);
			return cb_isHideProjectedClock;
		}

		static bool n_IsHideProjectedClock (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsHideProjectedClock;
		}
#pragma warning restore 0169

		IntPtr id_isHideProjectedClock;
		public unsafe bool IsHideProjectedClock {
			get {
				if (id_isHideProjectedClock == IntPtr.Zero)
					id_isHideProjectedClock = JNIEnv.GetMethodID (class_ref, "isHideProjectedClock", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isHideProjectedClock);
			}
		}

		static Delegate cb_getManufacturer;
#pragma warning disable 0169
		static Delegate GetGetManufacturerHandler ()
		{
			if (cb_getManufacturer == null)
				cb_getManufacturer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetManufacturer);
			return cb_getManufacturer;
		}

		static IntPtr n_GetManufacturer (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Manufacturer);
		}
#pragma warning restore 0169

		IntPtr id_getManufacturer;
		public unsafe string Manufacturer {
			get {
				if (id_getManufacturer == IntPtr.Zero)
					id_getManufacturer = JNIEnv.GetMethodID (class_ref, "getManufacturer", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getManufacturer), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getModel;
#pragma warning disable 0169
		static Delegate GetGetModelHandler ()
		{
			if (cb_getModel == null)
				cb_getModel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModel);
			return cb_getModel;
		}

		static IntPtr n_GetModel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Model);
		}
#pragma warning restore 0169

		IntPtr id_getModel;
		public unsafe string Model {
			get {
				if (id_getModel == IntPtr.Zero)
					id_getModel = JNIEnv.GetMethodID (class_ref, "getModel", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModel), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getModelYear;
#pragma warning disable 0169
		static Delegate GetGetModelYearHandler ()
		{
			if (cb_getModelYear == null)
				cb_getModelYear = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetModelYear);
			return cb_getModelYear;
		}

		static IntPtr n_GetModelYear (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ModelYear);
		}
#pragma warning restore 0169

		IntPtr id_getModelYear;
		public unsafe string ModelYear {
			get {
				if (id_getModelYear == IntPtr.Zero)
					id_getModelYear = JNIEnv.GetMethodID (class_ref, "getModelYear", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getModelYear), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getNickname;
#pragma warning disable 0169
		static Delegate GetGetNicknameHandler ()
		{
			if (cb_getNickname == null)
				cb_getNickname = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetNickname);
			return cb_getNickname;
		}

		static IntPtr n_GetNickname (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Nickname);
		}
#pragma warning restore 0169

		IntPtr id_getNickname;
		public unsafe string Nickname {
			get {
				if (id_getNickname == IntPtr.Zero)
					id_getNickname = JNIEnv.GetMethodID (class_ref, "getNickname", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getNickname), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getVehicleId;
#pragma warning disable 0169
		static Delegate GetGetVehicleIdHandler ()
		{
			if (cb_getVehicleId == null)
				cb_getVehicleId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVehicleId);
			return cb_getVehicleId;
		}

		static IntPtr n_GetVehicleId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.VehicleId);
		}
#pragma warning restore 0169

		IntPtr id_getVehicleId;
		public unsafe string VehicleId {
			get {
				if (id_getVehicleId == IntPtr.Zero)
					id_getVehicleId = JNIEnv.GetMethodID (class_ref, "getVehicleId", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getVehicleId), JniHandleOwnership.TransferLocalRef);
			}
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']"
	[Register ("com/google/android/gms/car/CarInfoManager$CarUiInfo", "", "Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker")]
	public partial interface ICarInfoManagerCarUiInfo : IJavaObject {

		bool HasDpad {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='hasDpad' and count(parameter)=0]"
			[Register ("hasDpad", "()Z", "GetHasDpadHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool HasRotaryController {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='hasRotaryController' and count(parameter)=0]"
			[Register ("hasRotaryController", "()Z", "GetHasRotaryControllerHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool HasSearchButton {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='hasSearchButton' and count(parameter)=0]"
			[Register ("hasSearchButton", "()Z", "GetHasSearchButtonHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool HasTouchpadForUiNavigation {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='hasTouchpadForUiNavigation' and count(parameter)=0]"
			[Register ("hasTouchpadForUiNavigation", "()Z", "GetHasTouchpadForUiNavigationHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool HasTouchscreen {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='hasTouchscreen' and count(parameter)=0]"
			[Register ("hasTouchscreen", "()Z", "GetHasTouchscreenHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsTouchpadUiAbsolute {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='isTouchpadUiAbsolute' and count(parameter)=0]"
			[Register ("isTouchpadUiAbsolute", "()Z", "GetIsTouchpadUiAbsoluteHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		int TouchscreenType {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='getTouchscreenType' and count(parameter)=0]"
			[Register ("getTouchscreenType", "()I", "GetGetTouchscreenTypeHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager.CarUiInfo']/method[@name='getTouchpadDimensions' and count(parameter)=0]"
		[Register ("getTouchpadDimensions", "()[I", "GetGetTouchpadDimensionsHandler:Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfoInvoker, AndroidAutoBindingJar")]
		int[] GetTouchpadDimensions ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarInfoManager$CarUiInfo", DoNotGenerateAcw=true)]
	internal class ICarInfoManagerCarUiInfoInvoker : global::Java.Lang.Object, ICarInfoManagerCarUiInfo {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarInfoManager$CarUiInfo");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarInfoManagerCarUiInfoInvoker); }
		}

		IntPtr class_ref;

		public static ICarInfoManagerCarUiInfo GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarInfoManagerCarUiInfo> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarInfoManager.CarUiInfo"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarInfoManagerCarUiInfoInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_hasDpad;
#pragma warning disable 0169
		static Delegate GetHasDpadHandler ()
		{
			if (cb_hasDpad == null)
				cb_hasDpad = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasDpad);
			return cb_hasDpad;
		}

		static bool n_HasDpad (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasDpad;
		}
#pragma warning restore 0169

		IntPtr id_hasDpad;
		public unsafe bool HasDpad {
			get {
				if (id_hasDpad == IntPtr.Zero)
					id_hasDpad = JNIEnv.GetMethodID (class_ref, "hasDpad", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasDpad);
			}
		}

		static Delegate cb_hasRotaryController;
#pragma warning disable 0169
		static Delegate GetHasRotaryControllerHandler ()
		{
			if (cb_hasRotaryController == null)
				cb_hasRotaryController = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasRotaryController);
			return cb_hasRotaryController;
		}

		static bool n_HasRotaryController (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasRotaryController;
		}
#pragma warning restore 0169

		IntPtr id_hasRotaryController;
		public unsafe bool HasRotaryController {
			get {
				if (id_hasRotaryController == IntPtr.Zero)
					id_hasRotaryController = JNIEnv.GetMethodID (class_ref, "hasRotaryController", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasRotaryController);
			}
		}

		static Delegate cb_hasSearchButton;
#pragma warning disable 0169
		static Delegate GetHasSearchButtonHandler ()
		{
			if (cb_hasSearchButton == null)
				cb_hasSearchButton = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasSearchButton);
			return cb_hasSearchButton;
		}

		static bool n_HasSearchButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasSearchButton;
		}
#pragma warning restore 0169

		IntPtr id_hasSearchButton;
		public unsafe bool HasSearchButton {
			get {
				if (id_hasSearchButton == IntPtr.Zero)
					id_hasSearchButton = JNIEnv.GetMethodID (class_ref, "hasSearchButton", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasSearchButton);
			}
		}

		static Delegate cb_hasTouchpadForUiNavigation;
#pragma warning disable 0169
		static Delegate GetHasTouchpadForUiNavigationHandler ()
		{
			if (cb_hasTouchpadForUiNavigation == null)
				cb_hasTouchpadForUiNavigation = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTouchpadForUiNavigation);
			return cb_hasTouchpadForUiNavigation;
		}

		static bool n_HasTouchpadForUiNavigation (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTouchpadForUiNavigation;
		}
#pragma warning restore 0169

		IntPtr id_hasTouchpadForUiNavigation;
		public unsafe bool HasTouchpadForUiNavigation {
			get {
				if (id_hasTouchpadForUiNavigation == IntPtr.Zero)
					id_hasTouchpadForUiNavigation = JNIEnv.GetMethodID (class_ref, "hasTouchpadForUiNavigation", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTouchpadForUiNavigation);
			}
		}

		static Delegate cb_hasTouchscreen;
#pragma warning disable 0169
		static Delegate GetHasTouchscreenHandler ()
		{
			if (cb_hasTouchscreen == null)
				cb_hasTouchscreen = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasTouchscreen);
			return cb_hasTouchscreen;
		}

		static bool n_HasTouchscreen (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasTouchscreen;
		}
#pragma warning restore 0169

		IntPtr id_hasTouchscreen;
		public unsafe bool HasTouchscreen {
			get {
				if (id_hasTouchscreen == IntPtr.Zero)
					id_hasTouchscreen = JNIEnv.GetMethodID (class_ref, "hasTouchscreen", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasTouchscreen);
			}
		}

		static Delegate cb_isTouchpadUiAbsolute;
#pragma warning disable 0169
		static Delegate GetIsTouchpadUiAbsoluteHandler ()
		{
			if (cb_isTouchpadUiAbsolute == null)
				cb_isTouchpadUiAbsolute = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsTouchpadUiAbsolute);
			return cb_isTouchpadUiAbsolute;
		}

		static bool n_IsTouchpadUiAbsolute (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsTouchpadUiAbsolute;
		}
#pragma warning restore 0169

		IntPtr id_isTouchpadUiAbsolute;
		public unsafe bool IsTouchpadUiAbsolute {
			get {
				if (id_isTouchpadUiAbsolute == IntPtr.Zero)
					id_isTouchpadUiAbsolute = JNIEnv.GetMethodID (class_ref, "isTouchpadUiAbsolute", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isTouchpadUiAbsolute);
			}
		}

		static Delegate cb_getTouchscreenType;
#pragma warning disable 0169
		static Delegate GetGetTouchscreenTypeHandler ()
		{
			if (cb_getTouchscreenType == null)
				cb_getTouchscreenType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTouchscreenType);
			return cb_getTouchscreenType;
		}

		static int n_GetTouchscreenType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TouchscreenType;
		}
#pragma warning restore 0169

		IntPtr id_getTouchscreenType;
		public unsafe int TouchscreenType {
			get {
				if (id_getTouchscreenType == IntPtr.Zero)
					id_getTouchscreenType = JNIEnv.GetMethodID (class_ref, "getTouchscreenType", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getTouchscreenType);
			}
		}

		static Delegate cb_getTouchpadDimensions;
#pragma warning disable 0169
		static Delegate GetGetTouchpadDimensionsHandler ()
		{
			if (cb_getTouchpadDimensions == null)
				cb_getTouchpadDimensions = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTouchpadDimensions);
			return cb_getTouchpadDimensions;
		}

		static IntPtr n_GetTouchpadDimensions (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetTouchpadDimensions ());
		}
#pragma warning restore 0169

		IntPtr id_getTouchpadDimensions;
		public unsafe int[] GetTouchpadDimensions ()
		{
			if (id_getTouchpadDimensions == IntPtr.Zero)
				id_getTouchpadDimensions = JNIEnv.GetMethodID (class_ref, "getTouchpadDimensions", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getTouchpadDimensions), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager']"
	[Register ("com/google/android/gms/car/CarInfoManager", "", "Com.Google.Android.Gms.Car.ICarInfoManagerInvoker")]
	public partial interface ICarInfoManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager']/method[@name='loadCarInfo' and count(parameter)=0]"
		[Register ("loadCarInfo", "()Lcom/google/android/gms/car/CarInfoManager$CarInfo;", "GetLoadCarInfoHandler:Com.Google.Android.Gms.Car.ICarInfoManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo LoadCarInfo ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarInfoManager']/method[@name='loadCarUiInfo' and count(parameter)=0]"
		[Register ("loadCarUiInfo", "()Lcom/google/android/gms/car/CarInfoManager$CarUiInfo;", "GetLoadCarUiInfoHandler:Com.Google.Android.Gms.Car.ICarInfoManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo LoadCarUiInfo ();

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarInfoManager", DoNotGenerateAcw=true)]
	internal class ICarInfoManagerInvoker : global::Java.Lang.Object, ICarInfoManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarInfoManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarInfoManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarInfoManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarInfoManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarInfoManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarInfoManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_loadCarInfo;
#pragma warning disable 0169
		static Delegate GetLoadCarInfoHandler ()
		{
			if (cb_loadCarInfo == null)
				cb_loadCarInfo = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_LoadCarInfo);
			return cb_loadCarInfo;
		}

		static IntPtr n_LoadCarInfo (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LoadCarInfo ());
		}
#pragma warning restore 0169

		IntPtr id_loadCarInfo;
		public unsafe global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo LoadCarInfo ()
		{
			if (id_loadCarInfo == IntPtr.Zero)
				id_loadCarInfo = JNIEnv.GetMethodID (class_ref, "loadCarInfo", "()Lcom/google/android/gms/car/CarInfoManager$CarInfo;");
			return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarInfo> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_loadCarInfo), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_loadCarUiInfo;
#pragma warning disable 0169
		static Delegate GetLoadCarUiInfoHandler ()
		{
			if (cb_loadCarUiInfo == null)
				cb_loadCarUiInfo = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_LoadCarUiInfo);
			return cb_loadCarUiInfo;
		}

		static IntPtr n_LoadCarUiInfo (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarInfoManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LoadCarUiInfo ());
		}
#pragma warning restore 0169

		IntPtr id_loadCarUiInfo;
		public unsafe global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo LoadCarUiInfo ()
		{
			if (id_loadCarUiInfo == IntPtr.Zero)
				id_loadCarUiInfo = JNIEnv.GetMethodID (class_ref, "loadCarUiInfo", "()Lcom/google/android/gms/car/CarInfoManager$CarUiInfo;");
			return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarInfoManagerCarUiInfo> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_loadCarUiInfo), JniHandleOwnership.TransferLocalRef);
		}

	}

}
