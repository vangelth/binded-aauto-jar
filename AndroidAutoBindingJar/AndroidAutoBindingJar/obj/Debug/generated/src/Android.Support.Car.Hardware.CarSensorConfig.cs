using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Hardware {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']"
	[global::Android.Runtime.Register ("android/support/car/hardware/CarSensorConfig", DoNotGenerateAcw=true)]
	public partial class CarSensorConfig : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/field[@name='WHEEL_TICK_DISTANCE_FRONT_LEFT_UM_PER_TICK']"
		[Register ("WHEEL_TICK_DISTANCE_FRONT_LEFT_UM_PER_TICK")]
		public const string WheelTickDistanceFrontLeftUmPerTick = (string) "android.car.wheelTickDistanceFrontLeftUmPerTick";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/field[@name='WHEEL_TICK_DISTANCE_FRONT_RIGHT_UM_PER_TICK']"
		[Register ("WHEEL_TICK_DISTANCE_FRONT_RIGHT_UM_PER_TICK")]
		public const string WheelTickDistanceFrontRightUmPerTick = (string) "android.car.wheelTickDistanceFrontRightUmPerTick";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/field[@name='WHEEL_TICK_DISTANCE_REAR_LEFT_UM_PER_TICK']"
		[Register ("WHEEL_TICK_DISTANCE_REAR_LEFT_UM_PER_TICK")]
		public const string WheelTickDistanceRearLeftUmPerTick = (string) "android.car.wheelTickDistanceRearLeftUmPerTick";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/field[@name='WHEEL_TICK_DISTANCE_REAR_RIGHT_UM_PER_TICK']"
		[Register ("WHEEL_TICK_DISTANCE_REAR_RIGHT_UM_PER_TICK")]
		public const string WheelTickDistanceRearRightUmPerTick = (string) "android.car.wheelTickDistanceRearRightUmPerTick";

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/field[@name='WHEEL_TICK_DISTANCE_SUPPORTED_WHEELS']"
		[Register ("WHEEL_TICK_DISTANCE_SUPPORTED_WHEELS")]
		public const string WheelTickDistanceSupportedWheels = (string) "android.car.wheelTickDistanceSupportedWhheels";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/hardware/CarSensorConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarSensorConfig); }
		}

		protected CarSensorConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_ILandroid_os_Bundle_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/constructor[@name='CarSensorConfig' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Bundle']]"
		[Register (".ctor", "(ILandroid/os/Bundle;)V", "")]
		public unsafe CarSensorConfig (int p0, global::Android.OS.Bundle p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (CarSensorConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(ILandroid/os/Bundle;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(ILandroid/os/Bundle;)V", __args);
					return;
				}

				if (id_ctor_ILandroid_os_Bundle_ == IntPtr.Zero)
					id_ctor_ILandroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "<init>", "(ILandroid/os/Bundle;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_ILandroid_os_Bundle_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_ILandroid_os_Bundle_, __args);
			} finally {
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetType);
			return cb_getType;
		}

		static int n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Hardware.CarSensorConfig __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Type;
		}
#pragma warning restore 0169

		static IntPtr id_getType;
		public virtual unsafe int Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()I", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getType);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getType", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getInt_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetInt_Ljava_lang_String_Handler ()
		{
			if (cb_getInt_Ljava_lang_String_ == null)
				cb_getInt_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int>) n_GetInt_Ljava_lang_String_);
			return cb_getInt_Ljava_lang_String_;
		}

		static int n_GetInt_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Hardware.CarSensorConfig __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Hardware.CarSensorConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.GetInt (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getInt_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.hardware']/class[@name='CarSensorConfig']/method[@name='getInt' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getInt", "(Ljava/lang/String;)I", "GetGetInt_Ljava_lang_String_Handler")]
		public virtual unsafe int GetInt (string p0)
		{
			if (id_getInt_Ljava_lang_String_ == IntPtr.Zero)
				id_getInt_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getInt", "(Ljava/lang/String;)I");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getInt_Ljava_lang_String_, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getInt", "(Ljava/lang/String;)I"), __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
