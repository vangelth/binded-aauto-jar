using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']"
	[global::Android.Runtime.Register ("com/google/android/a/cgizmo", DoNotGenerateAcw=true)]
	public partial class Cgizmo : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/a/cgizmo", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Cgizmo); }
		}

		protected Cgizmo (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a_Landroid_os_Parcel_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=1 and parameter[1][@type='android.os.Parcel']]"
		[Register ("a", "(Landroid/os/Parcel;)Z", "")]
		public static unsafe bool A (global::Android.OS.Parcel p0)
		{
			if (id_a_Landroid_os_Parcel_ == IntPtr.Zero)
				id_a_Landroid_os_Parcel_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				bool __ret = JNIEnv.CallStaticBooleanMethod  (class_ref, id_a_Landroid_os_Parcel_, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_a_Landroid_os_Parcel_Landroid_os_IInterface_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='android.os.IInterface']]"
		[Register ("a", "(Landroid/os/Parcel;Landroid/os/IInterface;)V", "")]
		public static unsafe void A (global::Android.OS.Parcel p0, global::Android.OS.IInterface p1)
		{
			if (id_a_Landroid_os_Parcel_Landroid_os_IInterface_ == IntPtr.Zero)
				id_a_Landroid_os_Parcel_Landroid_os_IInterface_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;Landroid/os/IInterface;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_a_Landroid_os_Parcel_Landroid_os_IInterface_, __args);
			} finally {
			}
		}

		static IntPtr id_a_Landroid_os_Parcel_Landroid_os_Parcelable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='android.os.Parcelable']]"
		[Register ("a", "(Landroid/os/Parcel;Landroid/os/Parcelable;)V", "")]
		public static unsafe void A (global::Android.OS.Parcel p0, global::Android.OS.IParcelable p1)
		{
			if (id_a_Landroid_os_Parcel_Landroid_os_Parcelable_ == IntPtr.Zero)
				id_a_Landroid_os_Parcel_Landroid_os_Parcelable_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;Landroid/os/Parcelable;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_a_Landroid_os_Parcel_Landroid_os_Parcelable_, __args);
			} finally {
			}
		}

		static IntPtr id_a_Landroid_os_Parcel_Landroid_os_Parcelable_Creator_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='android.os.Parcelable.Creator&lt;T&gt;']]"
		[Register ("a", "(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;", "")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"T extends android.os.Parcelable"})]
		public static unsafe global::Java.Lang.Object A (global::Android.OS.Parcel p0, global::Android.OS.IParcelableCreator p1)
		{
			if (id_a_Landroid_os_Parcel_Landroid_os_Parcelable_Creator_ == IntPtr.Zero)
				id_a_Landroid_os_Parcel_Landroid_os_Parcelable_Creator_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				global::Java.Lang.Object __ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallStaticObjectMethod  (class_ref, id_a_Landroid_os_Parcel_Landroid_os_Parcelable_Creator_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_a_Landroid_os_Parcel_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='boolean']]"
		[Register ("a", "(Landroid/os/Parcel;Z)V", "")]
		public static unsafe void A (global::Android.OS.Parcel p0, bool p1)
		{
			if (id_a_Landroid_os_Parcel_Z == IntPtr.Zero)
				id_a_Landroid_os_Parcel_Z = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;Z)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_a_Landroid_os_Parcel_Z, __args);
			} finally {
			}
		}

		static IntPtr id_a_Landroid_os_Parcel_Ljava_lang_CharSequence_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='a' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='java.lang.CharSequence']]"
		[Register ("a", "(Landroid/os/Parcel;Ljava/lang/CharSequence;)V", "")]
		public static unsafe void A (global::Android.OS.Parcel p0, global::Java.Lang.ICharSequence p1)
		{
			if (id_a_Landroid_os_Parcel_Ljava_lang_CharSequence_ == IntPtr.Zero)
				id_a_Landroid_os_Parcel_Ljava_lang_CharSequence_ = JNIEnv.GetStaticMethodID (class_ref, "a", "(Landroid/os/Parcel;Ljava/lang/CharSequence;)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_a_Landroid_os_Parcel_Ljava_lang_CharSequence_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		public static void A (global::Android.OS.Parcel p0, string p1)
		{
			global::Java.Lang.String jls_p1 = p1 == null ? null : new global::Java.Lang.String (p1);
			A (p0, jls_p1);
			jls_p1?.Dispose ();
		}

		static IntPtr id_b_Landroid_os_Parcel_Landroid_os_Parcelable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.a']/class[@name='cgizmo']/method[@name='b' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='android.os.Parcelable']]"
		[Register ("b", "(Landroid/os/Parcel;Landroid/os/Parcelable;)V", "")]
		public static unsafe void B (global::Android.OS.Parcel p0, global::Android.OS.IParcelable p1)
		{
			if (id_b_Landroid_os_Parcel_Landroid_os_Parcelable_ == IntPtr.Zero)
				id_b_Landroid_os_Parcel_Landroid_os_Parcelable_ = JNIEnv.GetStaticMethodID (class_ref, "b", "(Landroid/os/Parcel;Landroid/os/Parcelable;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_b_Landroid_os_Parcel_Landroid_os_Parcelable_, __args);
			} finally {
			}
		}

	}
}
