using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Navigation {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationStatusManagerEmbedded", DoNotGenerateAcw=true)]
	public partial class CarNavigationStatusManagerEmbedded : global::Android.Support.Car.Navigation.CarNavigationStatusManager {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/navigation/CarNavigationStatusManagerEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarNavigationStatusManagerEmbedded); }
		}

		protected CarNavigationStatusManagerEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Ljava_lang_Object_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/constructor[@name='CarNavigationStatusManagerEmbedded' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register (".ctor", "(Ljava/lang/Object;)V", "")]
		public unsafe CarNavigationStatusManagerEmbedded (global::Java.Lang.Object p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (CarNavigationStatusManagerEmbedded)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Ljava/lang/Object;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Ljava/lang/Object;)V", __args);
					return;
				}

				if (id_ctor_Ljava_lang_Object_ == IntPtr.Zero)
					id_ctor_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/lang/Object;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_lang_Object_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Ljava_lang_Object_, __args);
			} finally {
			}
		}

		static Delegate cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
#pragma warning disable 0169
		static Delegate GetAddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_Handler ()
		{
			if (cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ == null)
				cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_);
			return cb_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
		}

		static void n_AddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0 = (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback)global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationStatusManager.CarNavigationCallback']]"
		[Register ("addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V", "GetAddListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_Handler")]
		public override unsafe void AddListener (global::Android.Support.Car.Navigation.CarNavigationStatusManager.ICarNavigationCallback p0)
		{
			if (id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ == IntPtr.Zero)
				id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_addListener_Landroid_support_car_navigation_CarNavigationStatusManager_CarNavigationCallback_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addListener", "(Landroid/support/car/navigation/CarNavigationStatusManager$CarNavigationCallback;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onCarDisconnected;
#pragma warning disable 0169
		static Delegate GetOnCarDisconnectedHandler ()
		{
			if (cb_onCarDisconnected == null)
				cb_onCarDisconnected = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnCarDisconnected);
			return cb_onCarDisconnected;
		}

		static void n_OnCarDisconnected (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnCarDisconnected ();
		}
#pragma warning restore 0169

		static IntPtr id_onCarDisconnected;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='onCarDisconnected' and count(parameter)=0]"
		[Register ("onCarDisconnected", "()V", "GetOnCarDisconnectedHandler")]
		public override unsafe void OnCarDisconnected ()
		{
			if (id_onCarDisconnected == IntPtr.Zero)
				id_onCarDisconnected = JNIEnv.GetMethodID (class_ref, "onCarDisconnected", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCarDisconnected);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onCarDisconnected", "()V"));
			} finally {
			}
		}

		static Delegate cb_removeListener;
#pragma warning disable 0169
		static Delegate GetRemoveListenerHandler ()
		{
			if (cb_removeListener == null)
				cb_removeListener = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_RemoveListener);
			return cb_removeListener;
		}

		static void n_RemoveListener (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener ();
		}
#pragma warning restore 0169

		static IntPtr id_removeListener;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='removeListener' and count(parameter)=0]"
		[Register ("removeListener", "()V", "GetRemoveListenerHandler")]
		public override unsafe void RemoveListener ()
		{
			if (id_removeListener == IntPtr.Zero)
				id_removeListener = JNIEnv.GetMethodID (class_ref, "removeListener", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_removeListener);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "()V"));
			} finally {
			}
		}

		static Delegate cb_sendEvent_ILandroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetSendEvent_ILandroid_os_Bundle_Handler ()
		{
			if (cb_sendEvent_ILandroid_os_Bundle_ == null)
				cb_sendEvent_ILandroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_SendEvent_ILandroid_os_Bundle_);
			return cb_sendEvent_ILandroid_os_Bundle_;
		}

		static void n_SendEvent_ILandroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p1 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SendEvent (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_sendEvent_ILandroid_os_Bundle_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='sendEvent' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.os.Bundle']]"
		[Register ("sendEvent", "(ILandroid/os/Bundle;)V", "GetSendEvent_ILandroid_os_Bundle_Handler")]
		public override unsafe void SendEvent (int p0, global::Android.OS.Bundle p1)
		{
			if (id_sendEvent_ILandroid_os_Bundle_ == IntPtr.Zero)
				id_sendEvent_ILandroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "sendEvent", "(ILandroid/os/Bundle;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendEvent_ILandroid_os_Bundle_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "sendEvent", "(ILandroid/os/Bundle;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_sendNavigationStatus_I;
#pragma warning disable 0169
		static Delegate GetSendNavigationStatus_IHandler ()
		{
			if (cb_sendNavigationStatus_I == null)
				cb_sendNavigationStatus_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SendNavigationStatus_I);
			return cb_sendNavigationStatus_I;
		}

		static void n_SendNavigationStatus_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationStatus (p0);
		}
#pragma warning restore 0169

		static IntPtr id_sendNavigationStatus_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='sendNavigationStatus' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("sendNavigationStatus", "(I)V", "GetSendNavigationStatus_IHandler")]
		public override unsafe void SendNavigationStatus (int p0)
		{
			if (id_sendNavigationStatus_I == IntPtr.Zero)
				id_sendNavigationStatus_I = JNIEnv.GetMethodID (class_ref, "sendNavigationStatus", "(I)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationStatus_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "sendNavigationStatus", "(I)V"), __args);
			} finally {
			}
		}

		static Delegate cb_sendNavigationTurnDistanceEvent_IIII;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnDistanceEvent_IIIIHandler ()
		{
			if (cb_sendNavigationTurnDistanceEvent_IIII == null)
				cb_sendNavigationTurnDistanceEvent_IIII = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, int, int, int>) n_SendNavigationTurnDistanceEvent_IIII);
			return cb_sendNavigationTurnDistanceEvent_IIII;
		}

		static void n_SendNavigationTurnDistanceEvent_IIII (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2, int p3)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnDistanceEvent (p0, p1, p2, p3);
		}
#pragma warning restore 0169

		static IntPtr id_sendNavigationTurnDistanceEvent_IIII;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='sendNavigationTurnDistanceEvent' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("sendNavigationTurnDistanceEvent", "(IIII)V", "GetSendNavigationTurnDistanceEvent_IIIIHandler")]
		public override unsafe void SendNavigationTurnDistanceEvent (int p0, int p1, int p2, int p3)
		{
			if (id_sendNavigationTurnDistanceEvent_IIII == IntPtr.Zero)
				id_sendNavigationTurnDistanceEvent_IIII = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnDistanceEvent", "(IIII)V");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnDistanceEvent_IIII, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "sendNavigationTurnDistanceEvent", "(IIII)V"), __args);
			} finally {
			}
		}

		static Delegate cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_IHandler ()
		{
			if (cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I == null)
				cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr, int, int, IntPtr, int>) n_SendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I);
			return cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
		}

		static void n_SendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, int p2, int p3, IntPtr native_p4, int p5)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p1 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p1, JniHandleOwnership.DoNotTransfer);
			global::Android.Graphics.Bitmap p4 = global::Java.Lang.Object.GetObject<global::Android.Graphics.Bitmap> (native_p4, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnEvent (p0, p1, p2, p3, p4, p5);
		}
#pragma warning restore 0169

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='sendNavigationTurnEvent' and count(parameter)=6 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='android.graphics.Bitmap'] and parameter[6][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_IHandler")]
		public override unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, global::Android.Graphics.Bitmap p4, int p5)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [6];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);
				__args [5] = new JValue (p5);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_IILandroid_graphics_Bitmap_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;IILandroid/graphics/Bitmap;I)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
#pragma warning disable 0169
		static Delegate GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IIIHandler ()
		{
			if (cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III == null)
				cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr, int, int, int>) n_SendNavigationTurnEvent_ILjava_lang_CharSequence_III);
			return cb_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
		}

		static void n_SendNavigationTurnEvent_ILjava_lang_CharSequence_III (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1, int p2, int p3, int p4)
		{
			global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationStatusManagerEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence p1 = global::Java.Lang.Object.GetObject<global::Java.Lang.ICharSequence> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SendNavigationTurnEvent (p0, p1, p2, p3, p4);
		}
#pragma warning restore 0169

		static IntPtr id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationStatusManagerEmbedded']/method[@name='sendNavigationTurnEvent' and count(parameter)=5 and parameter[1][@type='int'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int'] and parameter[4][@type='int'] and parameter[5][@type='int']]"
		[Register ("sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V", "GetSendNavigationTurnEvent_ILjava_lang_CharSequence_IIIHandler")]
		public override unsafe void SendNavigationTurnEvent (int p0, global::Java.Lang.ICharSequence p1, int p2, int p3, int p4)
		{
			if (id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III == IntPtr.Zero)
				id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III = JNIEnv.GetMethodID (class_ref, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (p4);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_sendNavigationTurnEvent_ILjava_lang_CharSequence_III, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "sendNavigationTurnEvent", "(ILjava/lang/CharSequence;III)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

	}
}
