using System;

namespace Android.Support.Car.Annotation {

	[global::Android.Runtime.Annotation ("android.support.car.annotation.ValueTypeDef")]
	public partial class ValueTypeDefAttribute : Attribute
	{
		[global::Android.Runtime.Register ("type")]
		public global::Java.Lang.Class Type { get; set; }

	}
}
