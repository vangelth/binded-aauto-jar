using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Nav {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/NavigationProviderService", DoNotGenerateAcw=true)]
	public abstract partial class NavigationProviderService : global::Android.App.Service {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/field[@name='NAV_PROVIDER_CATEGORY']"
		[Register ("NAV_PROVIDER_CATEGORY")]
		public const string NavProviderCategory = (string) "com.google.android.car.category.NAVIGATION_PROVIDER";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/nav/NavigationProviderService", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (NavigationProviderService); }
		}

		protected NavigationProviderService (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/constructor[@name='NavigationProviderService' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe NavigationProviderService ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (NavigationProviderService)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getClientConfig;
#pragma warning disable 0169
		static Delegate GetGetClientConfigHandler ()
		{
			if (cb_getClientConfig == null)
				cb_getClientConfig = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetClientConfig);
			return cb_getClientConfig;
		}

		static IntPtr n_GetClientConfig (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ClientConfig);
		}
#pragma warning restore 0169

		static IntPtr id_getClientConfig;
		public virtual unsafe global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig ClientConfig {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='getClientConfig' and count(parameter)=0]"
			[Register ("getClientConfig", "()Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;", "GetGetClientConfigHandler")]
			get {
				if (id_getClientConfig == IntPtr.Zero)
					id_getClientConfig = JNIEnv.GetMethodID (class_ref, "getClientConfig", "()Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getClientConfig), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationClientConfig> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getClientConfig", "()Lcom/google/android/apps/auto/sdk/nav/NavigationClientConfig;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getMaxSupportedVersion;
#pragma warning disable 0169
		static Delegate GetGetMaxSupportedVersionHandler ()
		{
			if (cb_getMaxSupportedVersion == null)
				cb_getMaxSupportedVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMaxSupportedVersion);
			return cb_getMaxSupportedVersion;
		}

		static int n_GetMaxSupportedVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MaxSupportedVersion;
		}
#pragma warning restore 0169

		protected abstract int MaxSupportedVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='getMaxSupportedVersion' and count(parameter)=0]"
			[Register ("getMaxSupportedVersion", "()I", "GetGetMaxSupportedVersionHandler")] get;
		}

		static Delegate cb_getMinSupportedVersion;
#pragma warning disable 0169
		static Delegate GetGetMinSupportedVersionHandler ()
		{
			if (cb_getMinSupportedVersion == null)
				cb_getMinSupportedVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMinSupportedVersion);
			return cb_getMinSupportedVersion;
		}

		static int n_GetMinSupportedVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MinSupportedVersion;
		}
#pragma warning restore 0169

		protected abstract int MinSupportedVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='getMinSupportedVersion' and count(parameter)=0]"
			[Register ("getMinSupportedVersion", "()I", "GetGetMinSupportedVersionHandler")] get;
		}

		static Delegate cb_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
#pragma warning disable 0169
		static Delegate GetOnAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_Handler ()
		{
			if (cb_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ == null)
				cb_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_);
			return cb_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
		}

		static void n_OnAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnAndroidAutoStart (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='onAndroidAutoStart' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.nav.ClientMode']]"
		[Register ("onAndroidAutoStart", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V", "GetOnAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_Handler")]
		protected virtual unsafe void OnAndroidAutoStart (global::Com.Google.Android.Apps.Auto.Sdk.Nav.ClientMode p0)
		{
			if (id_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ == IntPtr.Zero)
				id_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_ = JNIEnv.GetMethodID (class_ref, "onAndroidAutoStart", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAndroidAutoStart_Lcom_google_android_apps_auto_sdk_nav_ClientMode_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onAndroidAutoStart", "(Lcom/google/android/apps/auto/sdk/nav/ClientMode;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onAndroidAutoStop;
#pragma warning disable 0169
		static Delegate GetOnAndroidAutoStopHandler ()
		{
			if (cb_onAndroidAutoStop == null)
				cb_onAndroidAutoStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnAndroidAutoStop);
			return cb_onAndroidAutoStop;
		}

		static void n_OnAndroidAutoStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnAndroidAutoStop ();
		}
#pragma warning restore 0169

		static IntPtr id_onAndroidAutoStop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='onAndroidAutoStop' and count(parameter)=0]"
		[Register ("onAndroidAutoStop", "()V", "GetOnAndroidAutoStopHandler")]
		protected virtual unsafe void OnAndroidAutoStop ()
		{
			if (id_onAndroidAutoStop == IntPtr.Zero)
				id_onAndroidAutoStop = JNIEnv.GetMethodID (class_ref, "onAndroidAutoStop", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAndroidAutoStop);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onAndroidAutoStop", "()V"));
			} finally {
			}
		}

		static Delegate cb_onBind_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnBind_Landroid_content_Intent_Handler ()
		{
			if (cb_onBind_Landroid_content_Intent_ == null)
				cb_onBind_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnBind_Landroid_content_Intent_);
			return cb_onBind_Landroid_content_Intent_;
		}

		static IntPtr n_OnBind_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.Nav.NavigationProviderService> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.OnBind (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_onBind_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='onBind' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;", "GetOnBind_Landroid_content_Intent_Handler")]
		public override unsafe global::Android.OS.IBinder OnBind (global::Android.Content.Intent p0)
		{
			if (id_onBind_Landroid_content_Intent_ == IntPtr.Zero)
				id_onBind_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Android.OS.IBinder __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onBind_Landroid_content_Intent_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/nav/NavigationProviderService", DoNotGenerateAcw=true)]
	internal partial class NavigationProviderServiceInvoker : NavigationProviderService {

		public NavigationProviderServiceInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (NavigationProviderServiceInvoker); }
		}

		static IntPtr id_getMaxSupportedVersion;
		protected override unsafe int MaxSupportedVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='getMaxSupportedVersion' and count(parameter)=0]"
			[Register ("getMaxSupportedVersion", "()I", "GetGetMaxSupportedVersionHandler")]
			get {
				if (id_getMaxSupportedVersion == IntPtr.Zero)
					id_getMaxSupportedVersion = JNIEnv.GetMethodID (class_ref, "getMaxSupportedVersion", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMaxSupportedVersion);
				} finally {
				}
			}
		}

		static IntPtr id_getMinSupportedVersion;
		protected override unsafe int MinSupportedVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.nav']/class[@name='NavigationProviderService']/method[@name='getMinSupportedVersion' and count(parameter)=0]"
			[Register ("getMinSupportedVersion", "()I", "GetGetMinSupportedVersionHandler")]
			get {
				if (id_getMinSupportedVersion == IntPtr.Zero)
					id_getMinSupportedVersion = JNIEnv.GetMethodID (class_ref, "getMinSupportedVersion", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMinSupportedVersion);
				} finally {
				}
			}
		}

	}

}
