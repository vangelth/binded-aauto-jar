using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.Service.A.A {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/service/a/a/b", DoNotGenerateAcw=true)]
	public sealed partial class B : global::Android.Support.Car.Media.CarAudioRecord {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/service/a/a/b", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (B); }
		}

		internal B (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getAudioSessionId;
		public override unsafe int AudioSessionId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='getAudioSessionId' and count(parameter)=0]"
			[Register ("getAudioSessionId", "()I", "GetGetAudioSessionIdHandler")]
			get {
				if (id_getAudioSessionId == IntPtr.Zero)
					id_getAudioSessionId = JNIEnv.GetMethodID (class_ref, "getAudioSessionId", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioSessionId);
				} finally {
				}
			}
		}

		static IntPtr id_getBufferSize;
		public override unsafe int BufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='getBufferSize' and count(parameter)=0]"
			[Register ("getBufferSize", "()I", "GetGetBufferSizeHandler")]
			get {
				if (id_getBufferSize == IntPtr.Zero)
					id_getBufferSize = JNIEnv.GetMethodID (class_ref, "getBufferSize", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getBufferSize);
				} finally {
				}
			}
		}

		static IntPtr id_getRecordingState;
		public override unsafe int RecordingState {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='getRecordingState' and count(parameter)=0]"
			[Register ("getRecordingState", "()I", "GetGetRecordingStateHandler")]
			get {
				if (id_getRecordingState == IntPtr.Zero)
					id_getRecordingState = JNIEnv.GetMethodID (class_ref, "getRecordingState", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getRecordingState);
				} finally {
				}
			}
		}

		static IntPtr id_getState;
		public override unsafe int State {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='getState' and count(parameter)=0]"
			[Register ("getState", "()I", "GetGetStateHandler")]
			get {
				if (id_getState == IntPtr.Zero)
					id_getState = JNIEnv.GetMethodID (class_ref, "getState", "()I");
				try {
					return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getState);
				} finally {
				}
			}
		}

		static IntPtr id_read_arrayBII;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='read' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("read", "([BII)I", "")]
		public override sealed unsafe int Read (byte[] p0, int p1, int p2)
		{
			if (id_read_arrayBII == IntPtr.Zero)
				id_read_arrayBII = JNIEnv.GetMethodID (class_ref, "read", "([BII)I");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				int __ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_read_arrayBII, __args);
				return __ret;
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static IntPtr id_release;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "")]
		public override sealed unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
			} finally {
			}
		}

		static IntPtr id_startRecording;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='startRecording' and count(parameter)=0]"
		[Register ("startRecording", "()V", "")]
		public override sealed unsafe void StartRecording ()
		{
			if (id_startRecording == IntPtr.Zero)
				id_startRecording = JNIEnv.GetMethodID (class_ref, "startRecording", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startRecording);
			} finally {
			}
		}

		static IntPtr id_stop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.service.a.a']/class[@name='b']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "")]
		public override sealed unsafe void Stop ()
		{
			if (id_stop == IntPtr.Zero)
				id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stop);
			} finally {
			}
		}

	}
}
