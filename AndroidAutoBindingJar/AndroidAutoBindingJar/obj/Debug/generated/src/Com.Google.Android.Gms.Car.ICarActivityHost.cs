using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']"
	[Register ("com/google/android/gms/car/CarActivityHost$HostedCarActivity", "", "Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker")]
	public partial interface ICarActivityHostHostedCarActivity : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='attach' and count(parameter)=1 and parameter[1][@type='com.google.android.gms.car.CarActivityHost']]"
		[Register ("attach", "(Lcom/google/android/gms/car/CarActivityHost;)V", "GetAttach_Lcom_google_android_gms_car_CarActivityHost_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void Attach (global::Com.Google.Android.Gms.Car.ICarActivityHost p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='dump' and count(parameter)=3 and parameter[1][@type='java.io.FileDescriptor'] and parameter[2][@type='java.io.PrintWriter'] and parameter[3][@type='java.lang.String[]']]"
		[Register ("dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V", "GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void Dump (global::Java.IO.FileDescriptor p0, global::Java.IO.PrintWriter p1, string[] p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onAccessibilityScanRequested' and count(parameter)=1 and parameter[1][@type='android.os.IBinder']]"
		[Register ("onAccessibilityScanRequested", "(Landroid/os/IBinder;)V", "GetOnAccessibilityScanRequested_Landroid_os_IBinder_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnAccessibilityScanRequested (global::Android.OS.IBinder p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onConfigurationChanged' and count(parameter)=1 and parameter[1][@type='android.content.res.Configuration']]"
		[Register ("onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnConfigurationChanged (global::Android.Content.Res.Configuration p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onCreate' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onCreate", "(Landroid/os/Bundle;)V", "GetOnCreate_Landroid_os_Bundle_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnCreate (global::Android.OS.Bundle p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onDestroy' and count(parameter)=0]"
		[Register ("onDestroy", "()V", "GetOnDestroyHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnDestroy ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onFrameRateChange' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onFrameRateChange", "(I)V", "GetOnFrameRateChange_IHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnFrameRateChange (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onKeyDown' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyDown", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyDown_ILandroid_view_KeyEvent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		bool OnKeyDown (int p0, global::Android.Views.KeyEvent p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onKeyLongPress' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyLongPress", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyLongPress_ILandroid_view_KeyEvent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		bool OnKeyLongPress (int p0, global::Android.Views.KeyEvent p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onKeyUp' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='android.view.KeyEvent']]"
		[Register ("onKeyUp", "(ILandroid/view/KeyEvent;)Z", "GetOnKeyUp_ILandroid_view_KeyEvent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		bool OnKeyUp (int p0, global::Android.Views.KeyEvent p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onLowMemory' and count(parameter)=0]"
		[Register ("onLowMemory", "()V", "GetOnLowMemoryHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnLowMemory ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onNewIntent' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("onNewIntent", "(Landroid/content/Intent;)V", "GetOnNewIntent_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnNewIntent (global::Android.Content.Intent p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onPause' and count(parameter)=0]"
		[Register ("onPause", "()V", "GetOnPauseHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnPause ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onPostResume' and count(parameter)=0]"
		[Register ("onPostResume", "()V", "GetOnPostResumeHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnPostResume ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onPowerStateChange' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("onPowerStateChange", "(I)V", "GetOnPowerStateChange_IHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnPowerStateChange (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onRestoreInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onRestoreInstanceState", "(Landroid/os/Bundle;)V", "GetOnRestoreInstanceState_Landroid_os_Bundle_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnRestoreInstanceState (global::Android.OS.Bundle p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onResume' and count(parameter)=0]"
		[Register ("onResume", "()V", "GetOnResumeHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnResume ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onRetainNonConfigurationInstance' and count(parameter)=0]"
		[Register ("onRetainNonConfigurationInstance", "()Ljava/lang/Object;", "GetOnRetainNonConfigurationInstanceHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		global::Java.Lang.Object OnRetainNonConfigurationInstance ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onSaveInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onSaveInstanceState", "(Landroid/os/Bundle;)V", "GetOnSaveInstanceState_Landroid_os_Bundle_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnSaveInstanceState (global::Android.OS.Bundle p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onStart' and count(parameter)=0]"
		[Register ("onStart", "()V", "GetOnStartHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnStart ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onStop' and count(parameter)=0]"
		[Register ("onStop", "()V", "GetOnStopHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnStop ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='onWindowFocusChanged' and count(parameter)=2 and parameter[1][@type='boolean'] and parameter[2][@type='boolean']]"
		[Register ("onWindowFocusChanged", "(ZZ)V", "GetOnWindowFocusChanged_ZZHandler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void OnWindowFocusChanged (bool p0, bool p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost.HostedCarActivity']/method[@name='setContext' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("setContext", "(Landroid/content/Context;)V", "GetSetContext_Landroid_content_Context_Handler:Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivityInvoker, AndroidAutoBindingJar")]
		void SetContext (global::Android.Content.Context p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarActivityHost$HostedCarActivity", DoNotGenerateAcw=true)]
	internal class ICarActivityHostHostedCarActivityInvoker : global::Java.Lang.Object, ICarActivityHostHostedCarActivity {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarActivityHost$HostedCarActivity");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarActivityHostHostedCarActivityInvoker); }
		}

		IntPtr class_ref;

		public static ICarActivityHostHostedCarActivity GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarActivityHostHostedCarActivity> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarActivityHost.HostedCarActivity"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarActivityHostHostedCarActivityInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_attach_Lcom_google_android_gms_car_CarActivityHost_;
#pragma warning disable 0169
		static Delegate GetAttach_Lcom_google_android_gms_car_CarActivityHost_Handler ()
		{
			if (cb_attach_Lcom_google_android_gms_car_CarActivityHost_ == null)
				cb_attach_Lcom_google_android_gms_car_CarActivityHost_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Attach_Lcom_google_android_gms_car_CarActivityHost_);
			return cb_attach_Lcom_google_android_gms_car_CarActivityHost_;
		}

		static void n_Attach_Lcom_google_android_gms_car_CarActivityHost_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Gms.Car.ICarActivityHost p0 = (global::Com.Google.Android.Gms.Car.ICarActivityHost)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attach (p0);
		}
#pragma warning restore 0169

		IntPtr id_attach_Lcom_google_android_gms_car_CarActivityHost_;
		public unsafe void Attach (global::Com.Google.Android.Gms.Car.ICarActivityHost p0)
		{
			if (id_attach_Lcom_google_android_gms_car_CarActivityHost_ == IntPtr.Zero)
				id_attach_Lcom_google_android_gms_car_CarActivityHost_ = JNIEnv.GetMethodID (class_ref, "attach", "(Lcom/google/android/gms/car/CarActivityHost;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_attach_Lcom_google_android_gms_car_CarActivityHost_, __args);
		}

		static Delegate cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetDump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_Handler ()
		{
			if (cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == null)
				cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_);
			return cb_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		}

		static void n_Dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, IntPtr native_p2)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.FileDescriptor p0 = global::Java.Lang.Object.GetObject<global::Java.IO.FileDescriptor> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.IO.PrintWriter p1 = global::Java.Lang.Object.GetObject<global::Java.IO.PrintWriter> (native_p1, JniHandleOwnership.DoNotTransfer);
			string[] p2 = (string[]) JNIEnv.GetArray (native_p2, JniHandleOwnership.DoNotTransfer, typeof (string));
			__this.Dump (p0, p1, p2);
			if (p2 != null)
				JNIEnv.CopyArray (p2, native_p2);
		}
#pragma warning restore 0169

		IntPtr id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_;
		public unsafe void Dump (global::Java.IO.FileDescriptor p0, global::Java.IO.PrintWriter p1, string[] p2)
		{
			if (id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ == IntPtr.Zero)
				id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "dump", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V");
			IntPtr native_p2 = JNIEnv.NewArray (p2);
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (native_p2);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_dump_Ljava_io_FileDescriptor_Ljava_io_PrintWriter_arrayLjava_lang_String_, __args);
			if (p2 != null) {
				JNIEnv.CopyArray (native_p2, p2);
				JNIEnv.DeleteLocalRef (native_p2);
			}
		}

		static Delegate cb_onAccessibilityScanRequested_Landroid_os_IBinder_;
#pragma warning disable 0169
		static Delegate GetOnAccessibilityScanRequested_Landroid_os_IBinder_Handler ()
		{
			if (cb_onAccessibilityScanRequested_Landroid_os_IBinder_ == null)
				cb_onAccessibilityScanRequested_Landroid_os_IBinder_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnAccessibilityScanRequested_Landroid_os_IBinder_);
			return cb_onAccessibilityScanRequested_Landroid_os_IBinder_;
		}

		static void n_OnAccessibilityScanRequested_Landroid_os_IBinder_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.IBinder p0 = (global::Android.OS.IBinder)global::Java.Lang.Object.GetObject<global::Android.OS.IBinder> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnAccessibilityScanRequested (p0);
		}
#pragma warning restore 0169

		IntPtr id_onAccessibilityScanRequested_Landroid_os_IBinder_;
		public unsafe void OnAccessibilityScanRequested (global::Android.OS.IBinder p0)
		{
			if (id_onAccessibilityScanRequested_Landroid_os_IBinder_ == IntPtr.Zero)
				id_onAccessibilityScanRequested_Landroid_os_IBinder_ = JNIEnv.GetMethodID (class_ref, "onAccessibilityScanRequested", "(Landroid/os/IBinder;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onAccessibilityScanRequested_Landroid_os_IBinder_, __args);
		}

		static Delegate cb_onConfigurationChanged_Landroid_content_res_Configuration_;
#pragma warning disable 0169
		static Delegate GetOnConfigurationChanged_Landroid_content_res_Configuration_Handler ()
		{
			if (cb_onConfigurationChanged_Landroid_content_res_Configuration_ == null)
				cb_onConfigurationChanged_Landroid_content_res_Configuration_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnConfigurationChanged_Landroid_content_res_Configuration_);
			return cb_onConfigurationChanged_Landroid_content_res_Configuration_;
		}

		static void n_OnConfigurationChanged_Landroid_content_res_Configuration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Res.Configuration p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Res.Configuration> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnConfigurationChanged (p0);
		}
#pragma warning restore 0169

		IntPtr id_onConfigurationChanged_Landroid_content_res_Configuration_;
		public unsafe void OnConfigurationChanged (global::Android.Content.Res.Configuration p0)
		{
			if (id_onConfigurationChanged_Landroid_content_res_Configuration_ == IntPtr.Zero)
				id_onConfigurationChanged_Landroid_content_res_Configuration_ = JNIEnv.GetMethodID (class_ref, "onConfigurationChanged", "(Landroid/content/res/Configuration;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onConfigurationChanged_Landroid_content_res_Configuration_, __args);
		}

		static Delegate cb_onCreate_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnCreate_Landroid_os_Bundle_Handler ()
		{
			if (cb_onCreate_Landroid_os_Bundle_ == null)
				cb_onCreate_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnCreate_Landroid_os_Bundle_);
			return cb_onCreate_Landroid_os_Bundle_;
		}

		static void n_OnCreate_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnCreate (p0);
		}
#pragma warning restore 0169

		IntPtr id_onCreate_Landroid_os_Bundle_;
		public unsafe void OnCreate (global::Android.OS.Bundle p0)
		{
			if (id_onCreate_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onCreate_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onCreate", "(Landroid/os/Bundle;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCreate_Landroid_os_Bundle_, __args);
		}

		static Delegate cb_onDestroy;
#pragma warning disable 0169
		static Delegate GetOnDestroyHandler ()
		{
			if (cb_onDestroy == null)
				cb_onDestroy = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnDestroy);
			return cb_onDestroy;
		}

		static void n_OnDestroy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnDestroy ();
		}
#pragma warning restore 0169

		IntPtr id_onDestroy;
		public unsafe void OnDestroy ()
		{
			if (id_onDestroy == IntPtr.Zero)
				id_onDestroy = JNIEnv.GetMethodID (class_ref, "onDestroy", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onDestroy);
		}

		static Delegate cb_onFrameRateChange_I;
#pragma warning disable 0169
		static Delegate GetOnFrameRateChange_IHandler ()
		{
			if (cb_onFrameRateChange_I == null)
				cb_onFrameRateChange_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnFrameRateChange_I);
			return cb_onFrameRateChange_I;
		}

		static void n_OnFrameRateChange_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnFrameRateChange (p0);
		}
#pragma warning restore 0169

		IntPtr id_onFrameRateChange_I;
		public unsafe void OnFrameRateChange (int p0)
		{
			if (id_onFrameRateChange_I == IntPtr.Zero)
				id_onFrameRateChange_I = JNIEnv.GetMethodID (class_ref, "onFrameRateChange", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onFrameRateChange_I, __args);
		}

		static Delegate cb_onKeyDown_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyDown_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyDown_ILandroid_view_KeyEvent_ == null)
				cb_onKeyDown_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyDown_ILandroid_view_KeyEvent_);
			return cb_onKeyDown_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyDown_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyDown (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onKeyDown_ILandroid_view_KeyEvent_;
		public unsafe bool OnKeyDown (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyDown_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyDown_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyDown", "(ILandroid/view/KeyEvent;)Z");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyDown_ILandroid_view_KeyEvent_, __args);
			return __ret;
		}

		static Delegate cb_onKeyLongPress_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyLongPress_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyLongPress_ILandroid_view_KeyEvent_ == null)
				cb_onKeyLongPress_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyLongPress_ILandroid_view_KeyEvent_);
			return cb_onKeyLongPress_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyLongPress_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyLongPress (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onKeyLongPress_ILandroid_view_KeyEvent_;
		public unsafe bool OnKeyLongPress (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyLongPress_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyLongPress_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyLongPress", "(ILandroid/view/KeyEvent;)Z");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyLongPress_ILandroid_view_KeyEvent_, __args);
			return __ret;
		}

		static Delegate cb_onKeyUp_ILandroid_view_KeyEvent_;
#pragma warning disable 0169
		static Delegate GetOnKeyUp_ILandroid_view_KeyEvent_Handler ()
		{
			if (cb_onKeyUp_ILandroid_view_KeyEvent_ == null)
				cb_onKeyUp_ILandroid_view_KeyEvent_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr, bool>) n_OnKeyUp_ILandroid_view_KeyEvent_);
			return cb_onKeyUp_ILandroid_view_KeyEvent_;
		}

		static bool n_OnKeyUp_ILandroid_view_KeyEvent_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.KeyEvent p1 = global::Java.Lang.Object.GetObject<global::Android.Views.KeyEvent> (native_p1, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnKeyUp (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_onKeyUp_ILandroid_view_KeyEvent_;
		public unsafe bool OnKeyUp (int p0, global::Android.Views.KeyEvent p1)
		{
			if (id_onKeyUp_ILandroid_view_KeyEvent_ == IntPtr.Zero)
				id_onKeyUp_ILandroid_view_KeyEvent_ = JNIEnv.GetMethodID (class_ref, "onKeyUp", "(ILandroid/view/KeyEvent;)Z");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onKeyUp_ILandroid_view_KeyEvent_, __args);
			return __ret;
		}

		static Delegate cb_onLowMemory;
#pragma warning disable 0169
		static Delegate GetOnLowMemoryHandler ()
		{
			if (cb_onLowMemory == null)
				cb_onLowMemory = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnLowMemory);
			return cb_onLowMemory;
		}

		static void n_OnLowMemory (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnLowMemory ();
		}
#pragma warning restore 0169

		IntPtr id_onLowMemory;
		public unsafe void OnLowMemory ()
		{
			if (id_onLowMemory == IntPtr.Zero)
				id_onLowMemory = JNIEnv.GetMethodID (class_ref, "onLowMemory", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onLowMemory);
		}

		static Delegate cb_onNewIntent_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnNewIntent_Landroid_content_Intent_Handler ()
		{
			if (cb_onNewIntent_Landroid_content_Intent_ == null)
				cb_onNewIntent_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnNewIntent_Landroid_content_Intent_);
			return cb_onNewIntent_Landroid_content_Intent_;
		}

		static void n_OnNewIntent_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnNewIntent (p0);
		}
#pragma warning restore 0169

		IntPtr id_onNewIntent_Landroid_content_Intent_;
		public unsafe void OnNewIntent (global::Android.Content.Intent p0)
		{
			if (id_onNewIntent_Landroid_content_Intent_ == IntPtr.Zero)
				id_onNewIntent_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onNewIntent", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onNewIntent_Landroid_content_Intent_, __args);
		}

		static Delegate cb_onPause;
#pragma warning disable 0169
		static Delegate GetOnPauseHandler ()
		{
			if (cb_onPause == null)
				cb_onPause = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPause);
			return cb_onPause;
		}

		static void n_OnPause (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPause ();
		}
#pragma warning restore 0169

		IntPtr id_onPause;
		public unsafe void OnPause ()
		{
			if (id_onPause == IntPtr.Zero)
				id_onPause = JNIEnv.GetMethodID (class_ref, "onPause", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPause);
		}

		static Delegate cb_onPostResume;
#pragma warning disable 0169
		static Delegate GetOnPostResumeHandler ()
		{
			if (cb_onPostResume == null)
				cb_onPostResume = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnPostResume);
			return cb_onPostResume;
		}

		static void n_OnPostResume (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPostResume ();
		}
#pragma warning restore 0169

		IntPtr id_onPostResume;
		public unsafe void OnPostResume ()
		{
			if (id_onPostResume == IntPtr.Zero)
				id_onPostResume = JNIEnv.GetMethodID (class_ref, "onPostResume", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPostResume);
		}

		static Delegate cb_onPowerStateChange_I;
#pragma warning disable 0169
		static Delegate GetOnPowerStateChange_IHandler ()
		{
			if (cb_onPowerStateChange_I == null)
				cb_onPowerStateChange_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnPowerStateChange_I);
			return cb_onPowerStateChange_I;
		}

		static void n_OnPowerStateChange_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnPowerStateChange (p0);
		}
#pragma warning restore 0169

		IntPtr id_onPowerStateChange_I;
		public unsafe void OnPowerStateChange (int p0)
		{
			if (id_onPowerStateChange_I == IntPtr.Zero)
				id_onPowerStateChange_I = JNIEnv.GetMethodID (class_ref, "onPowerStateChange", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onPowerStateChange_I, __args);
		}

		static Delegate cb_onRestoreInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnRestoreInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onRestoreInstanceState_Landroid_os_Bundle_ == null)
				cb_onRestoreInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnRestoreInstanceState_Landroid_os_Bundle_);
			return cb_onRestoreInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnRestoreInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnRestoreInstanceState (p0);
		}
#pragma warning restore 0169

		IntPtr id_onRestoreInstanceState_Landroid_os_Bundle_;
		public unsafe void OnRestoreInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onRestoreInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onRestoreInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onRestoreInstanceState", "(Landroid/os/Bundle;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onRestoreInstanceState_Landroid_os_Bundle_, __args);
		}

		static Delegate cb_onResume;
#pragma warning disable 0169
		static Delegate GetOnResumeHandler ()
		{
			if (cb_onResume == null)
				cb_onResume = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnResume);
			return cb_onResume;
		}

		static void n_OnResume (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnResume ();
		}
#pragma warning restore 0169

		IntPtr id_onResume;
		public unsafe void OnResume ()
		{
			if (id_onResume == IntPtr.Zero)
				id_onResume = JNIEnv.GetMethodID (class_ref, "onResume", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onResume);
		}

		static Delegate cb_onRetainNonConfigurationInstance;
#pragma warning disable 0169
		static Delegate GetOnRetainNonConfigurationInstanceHandler ()
		{
			if (cb_onRetainNonConfigurationInstance == null)
				cb_onRetainNonConfigurationInstance = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_OnRetainNonConfigurationInstance);
			return cb_onRetainNonConfigurationInstance;
		}

		static IntPtr n_OnRetainNonConfigurationInstance (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OnRetainNonConfigurationInstance ());
		}
#pragma warning restore 0169

		IntPtr id_onRetainNonConfigurationInstance;
		public unsafe global::Java.Lang.Object OnRetainNonConfigurationInstance ()
		{
			if (id_onRetainNonConfigurationInstance == IntPtr.Zero)
				id_onRetainNonConfigurationInstance = JNIEnv.GetMethodID (class_ref, "onRetainNonConfigurationInstance", "()Ljava/lang/Object;");
			return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onRetainNonConfigurationInstance), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_onSaveInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnSaveInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onSaveInstanceState_Landroid_os_Bundle_ == null)
				cb_onSaveInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnSaveInstanceState_Landroid_os_Bundle_);
			return cb_onSaveInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnSaveInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnSaveInstanceState (p0);
		}
#pragma warning restore 0169

		IntPtr id_onSaveInstanceState_Landroid_os_Bundle_;
		public unsafe void OnSaveInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onSaveInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onSaveInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onSaveInstanceState", "(Landroid/os/Bundle;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSaveInstanceState_Landroid_os_Bundle_, __args);
		}

		static Delegate cb_onStart;
#pragma warning disable 0169
		static Delegate GetOnStartHandler ()
		{
			if (cb_onStart == null)
				cb_onStart = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStart);
			return cb_onStart;
		}

		static void n_OnStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStart ();
		}
#pragma warning restore 0169

		IntPtr id_onStart;
		public unsafe void OnStart ()
		{
			if (id_onStart == IntPtr.Zero)
				id_onStart = JNIEnv.GetMethodID (class_ref, "onStart", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStart);
		}

		static Delegate cb_onStop;
#pragma warning disable 0169
		static Delegate GetOnStopHandler ()
		{
			if (cb_onStop == null)
				cb_onStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnStop);
			return cb_onStop;
		}

		static void n_OnStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnStop ();
		}
#pragma warning restore 0169

		IntPtr id_onStop;
		public unsafe void OnStop ()
		{
			if (id_onStop == IntPtr.Zero)
				id_onStop = JNIEnv.GetMethodID (class_ref, "onStop", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onStop);
		}

		static Delegate cb_onWindowFocusChanged_ZZ;
#pragma warning disable 0169
		static Delegate GetOnWindowFocusChanged_ZZHandler ()
		{
			if (cb_onWindowFocusChanged_ZZ == null)
				cb_onWindowFocusChanged_ZZ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool, bool>) n_OnWindowFocusChanged_ZZ);
			return cb_onWindowFocusChanged_ZZ;
		}

		static void n_OnWindowFocusChanged_ZZ (IntPtr jnienv, IntPtr native__this, bool p0, bool p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnWindowFocusChanged (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_onWindowFocusChanged_ZZ;
		public unsafe void OnWindowFocusChanged (bool p0, bool p1)
		{
			if (id_onWindowFocusChanged_ZZ == IntPtr.Zero)
				id_onWindowFocusChanged_ZZ = JNIEnv.GetMethodID (class_ref, "onWindowFocusChanged", "(ZZ)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onWindowFocusChanged_ZZ, __args);
		}

		static Delegate cb_setContext_Landroid_content_Context_;
#pragma warning disable 0169
		static Delegate GetSetContext_Landroid_content_Context_Handler ()
		{
			if (cb_setContext_Landroid_content_Context_ == null)
				cb_setContext_Landroid_content_Context_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContext_Landroid_content_Context_);
			return cb_setContext_Landroid_content_Context_;
		}

		static void n_SetContext_Landroid_content_Context_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHostHostedCarActivity> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Context p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Context> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetContext (p0);
		}
#pragma warning restore 0169

		IntPtr id_setContext_Landroid_content_Context_;
		public unsafe void SetContext (global::Android.Content.Context p0)
		{
			if (id_setContext_Landroid_content_Context_ == IntPtr.Zero)
				id_setContext_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "setContext", "(Landroid/content/Context;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContext_Landroid_content_Context_, __args);
		}

	}


	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']"
	[Register ("com/google/android/gms/car/CarActivityHost", "", "Com.Google.Android.Gms.Car.ICarActivityHostInvoker")]
	public partial interface ICarActivityHost : IJavaObject {

		global::Java.Lang.Object CarActivityService {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getCarActivityService' and count(parameter)=0]"
			[Register ("getCarActivityService", "()Ljava/lang/Object;", "GetGetCarActivityServiceHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		bool HasWindowFocus {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='hasWindowFocus' and count(parameter)=0]"
			[Register ("hasWindowFocus", "()Z", "GetHasWindowFocusHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		global::Com.Google.Android.Gms.Car.Input.IInputManager InputManager {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getInputManager' and count(parameter)=0]"
			[Register ("getInputManager", "()Lcom/google/android/gms/car/input/InputManager;", "GetGetInputManagerHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		global::Android.Content.Intent Intent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getIntent' and count(parameter)=0]"
			[Register ("getIntent", "()Landroid/content/Intent;", "GetGetIntentHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='setIntent' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
			[Register ("setIntent", "(Landroid/content/Intent;)V", "GetSetIntent_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] set;
		}

		bool IsFinishing {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='isFinishing' and count(parameter)=0]"
			[Register ("isFinishing", "()Z", "GetIsFinishingHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		bool IsStopped {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='isStopped' and count(parameter)=0]"
			[Register ("isStopped", "()Z", "GetIsStoppedHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		global::Android.Views.LayoutInflater LayoutInflater {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getLayoutInflater' and count(parameter)=0]"
			[Register ("getLayoutInflater", "()Landroid/view/LayoutInflater;", "GetGetLayoutInflaterHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		int MaxFrameRate {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getMaxFrameRate' and count(parameter)=0]"
			[Register ("getMaxFrameRate", "()I", "GetGetMaxFrameRateHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		global::Java.Lang.Object NonConfigurationInstance {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getNonConfigurationInstance' and count(parameter)=0]"
			[Register ("getNonConfigurationInstance", "()Ljava/lang/Object;", "GetGetNonConfigurationInstanceHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		int PowerState {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getPowerState' and count(parameter)=0]"
			[Register ("getPowerState", "()I", "GetGetPowerStateHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		global::Android.Views.Window Window {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getWindow' and count(parameter)=0]"
			[Register ("getWindow", "()Landroid/view/Window;", "GetGetWindowHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='enableWindowTransparency' and count(parameter)=0]"
		[Register ("enableWindowTransparency", "()V", "GetEnableWindowTransparencyHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void EnableWindowTransparency ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='findViewById' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("findViewById", "(I)Landroid/view/View;", "GetFindViewById_IHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		global::Android.Views.View FindViewById (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='finish' and count(parameter)=0]"
		[Register ("finish", "()V", "GetFinishHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void Finish ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='getCarManager' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;", "GetGetCarManager_Ljava_lang_String_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		global::Java.Lang.Object GetCarManager (string p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='onRestoreInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onRestoreInstanceState", "(Landroid/os/Bundle;)V", "GetOnRestoreInstanceState_Landroid_os_Bundle_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void OnRestoreInstanceState (global::Android.OS.Bundle p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='onSaveInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Bundle']]"
		[Register ("onSaveInstanceState", "(Landroid/os/Bundle;)V", "GetOnSaveInstanceState_Landroid_os_Bundle_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void OnSaveInstanceState (global::Android.OS.Bundle p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='setContentView' and count(parameter)=1 and parameter[1][@type='android.view.View']]"
		[Register ("setContentView", "(Landroid/view/View;)V", "GetSetContentView_Landroid_view_View_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void SetContentView (global::Android.Views.View p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='setContentView' and count(parameter)=2 and parameter[1][@type='android.view.View'] and parameter[2][@type='android.view.ViewGroup.LayoutParams']]"
		[Register ("setContentView", "(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V", "GetSetContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void SetContentView (global::Android.Views.View p0, global::Android.Views.ViewGroup.LayoutParams p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='setContentView' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setContentView", "(I)V", "GetSetContentView_IHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void SetContentView (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='setIgnoreConfigChanges' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("setIgnoreConfigChanges", "(I)V", "GetSetIgnoreConfigChanges_IHandler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void SetIgnoreConfigChanges (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarActivityHost']/method[@name='startCarProjectionActivity' and count(parameter)=1 and parameter[1][@type='android.content.Intent']]"
		[Register ("startCarProjectionActivity", "(Landroid/content/Intent;)V", "GetStartCarProjectionActivity_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarActivityHostInvoker, AndroidAutoBindingJar")]
		void StartCarProjectionActivity (global::Android.Content.Intent p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarActivityHost", DoNotGenerateAcw=true)]
	internal class ICarActivityHostInvoker : global::Java.Lang.Object, ICarActivityHost {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarActivityHost");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarActivityHostInvoker); }
		}

		IntPtr class_ref;

		public static ICarActivityHost GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarActivityHost> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarActivityHost"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarActivityHostInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getCarActivityService;
#pragma warning disable 0169
		static Delegate GetGetCarActivityServiceHandler ()
		{
			if (cb_getCarActivityService == null)
				cb_getCarActivityService = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCarActivityService);
			return cb_getCarActivityService;
		}

		static IntPtr n_GetCarActivityService (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CarActivityService);
		}
#pragma warning restore 0169

		IntPtr id_getCarActivityService;
		public unsafe global::Java.Lang.Object CarActivityService {
			get {
				if (id_getCarActivityService == IntPtr.Zero)
					id_getCarActivityService = JNIEnv.GetMethodID (class_ref, "getCarActivityService", "()Ljava/lang/Object;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarActivityService), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_hasWindowFocus;
#pragma warning disable 0169
		static Delegate GetHasWindowFocusHandler ()
		{
			if (cb_hasWindowFocus == null)
				cb_hasWindowFocus = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasWindowFocus);
			return cb_hasWindowFocus;
		}

		static bool n_HasWindowFocus (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasWindowFocus;
		}
#pragma warning restore 0169

		IntPtr id_hasWindowFocus;
		public unsafe bool HasWindowFocus {
			get {
				if (id_hasWindowFocus == IntPtr.Zero)
					id_hasWindowFocus = JNIEnv.GetMethodID (class_ref, "hasWindowFocus", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_hasWindowFocus);
			}
		}

		static Delegate cb_getInputManager;
#pragma warning disable 0169
		static Delegate GetGetInputManagerHandler ()
		{
			if (cb_getInputManager == null)
				cb_getInputManager = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetInputManager);
			return cb_getInputManager;
		}

		static IntPtr n_GetInputManager (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.InputManager);
		}
#pragma warning restore 0169

		IntPtr id_getInputManager;
		public unsafe global::Com.Google.Android.Gms.Car.Input.IInputManager InputManager {
			get {
				if (id_getInputManager == IntPtr.Zero)
					id_getInputManager = JNIEnv.GetMethodID (class_ref, "getInputManager", "()Lcom/google/android/gms/car/input/InputManager;");
				return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.Input.IInputManager> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getInputManager), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getIntent;
#pragma warning disable 0169
		static Delegate GetGetIntentHandler ()
		{
			if (cb_getIntent == null)
				cb_getIntent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIntent);
			return cb_getIntent;
		}

		static IntPtr n_GetIntent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Intent);
		}
#pragma warning restore 0169

		static Delegate cb_setIntent_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetSetIntent_Landroid_content_Intent_Handler ()
		{
			if (cb_setIntent_Landroid_content_Intent_ == null)
				cb_setIntent_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIntent_Landroid_content_Intent_);
			return cb_setIntent_Landroid_content_Intent_;
		}

		static void n_SetIntent_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Intent = p0;
		}
#pragma warning restore 0169

		IntPtr id_getIntent;
		IntPtr id_setIntent_Landroid_content_Intent_;
		public unsafe global::Android.Content.Intent Intent {
			get {
				if (id_getIntent == IntPtr.Zero)
					id_getIntent = JNIEnv.GetMethodID (class_ref, "getIntent", "()Landroid/content/Intent;");
				return global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getIntent), JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (id_setIntent_Landroid_content_Intent_ == IntPtr.Zero)
					id_setIntent_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "setIntent", "(Landroid/content/Intent;)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (value);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setIntent_Landroid_content_Intent_, __args);
			}
		}

		static Delegate cb_isFinishing;
#pragma warning disable 0169
		static Delegate GetIsFinishingHandler ()
		{
			if (cb_isFinishing == null)
				cb_isFinishing = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsFinishing);
			return cb_isFinishing;
		}

		static bool n_IsFinishing (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsFinishing;
		}
#pragma warning restore 0169

		IntPtr id_isFinishing;
		public unsafe bool IsFinishing {
			get {
				if (id_isFinishing == IntPtr.Zero)
					id_isFinishing = JNIEnv.GetMethodID (class_ref, "isFinishing", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isFinishing);
			}
		}

		static Delegate cb_isStopped;
#pragma warning disable 0169
		static Delegate GetIsStoppedHandler ()
		{
			if (cb_isStopped == null)
				cb_isStopped = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsStopped);
			return cb_isStopped;
		}

		static bool n_IsStopped (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsStopped;
		}
#pragma warning restore 0169

		IntPtr id_isStopped;
		public unsafe bool IsStopped {
			get {
				if (id_isStopped == IntPtr.Zero)
					id_isStopped = JNIEnv.GetMethodID (class_ref, "isStopped", "()Z");
				return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isStopped);
			}
		}

		static Delegate cb_getLayoutInflater;
#pragma warning disable 0169
		static Delegate GetGetLayoutInflaterHandler ()
		{
			if (cb_getLayoutInflater == null)
				cb_getLayoutInflater = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLayoutInflater);
			return cb_getLayoutInflater;
		}

		static IntPtr n_GetLayoutInflater (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LayoutInflater);
		}
#pragma warning restore 0169

		IntPtr id_getLayoutInflater;
		public unsafe global::Android.Views.LayoutInflater LayoutInflater {
			get {
				if (id_getLayoutInflater == IntPtr.Zero)
					id_getLayoutInflater = JNIEnv.GetMethodID (class_ref, "getLayoutInflater", "()Landroid/view/LayoutInflater;");
				return global::Java.Lang.Object.GetObject<global::Android.Views.LayoutInflater> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getLayoutInflater), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getMaxFrameRate;
#pragma warning disable 0169
		static Delegate GetGetMaxFrameRateHandler ()
		{
			if (cb_getMaxFrameRate == null)
				cb_getMaxFrameRate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMaxFrameRate);
			return cb_getMaxFrameRate;
		}

		static int n_GetMaxFrameRate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MaxFrameRate;
		}
#pragma warning restore 0169

		IntPtr id_getMaxFrameRate;
		public unsafe int MaxFrameRate {
			get {
				if (id_getMaxFrameRate == IntPtr.Zero)
					id_getMaxFrameRate = JNIEnv.GetMethodID (class_ref, "getMaxFrameRate", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMaxFrameRate);
			}
		}

		static Delegate cb_getNonConfigurationInstance;
#pragma warning disable 0169
		static Delegate GetGetNonConfigurationInstanceHandler ()
		{
			if (cb_getNonConfigurationInstance == null)
				cb_getNonConfigurationInstance = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetNonConfigurationInstance);
			return cb_getNonConfigurationInstance;
		}

		static IntPtr n_GetNonConfigurationInstance (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.NonConfigurationInstance);
		}
#pragma warning restore 0169

		IntPtr id_getNonConfigurationInstance;
		public unsafe global::Java.Lang.Object NonConfigurationInstance {
			get {
				if (id_getNonConfigurationInstance == IntPtr.Zero)
					id_getNonConfigurationInstance = JNIEnv.GetMethodID (class_ref, "getNonConfigurationInstance", "()Ljava/lang/Object;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getNonConfigurationInstance), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getPowerState;
#pragma warning disable 0169
		static Delegate GetGetPowerStateHandler ()
		{
			if (cb_getPowerState == null)
				cb_getPowerState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetPowerState);
			return cb_getPowerState;
		}

		static int n_GetPowerState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.PowerState;
		}
#pragma warning restore 0169

		IntPtr id_getPowerState;
		public unsafe int PowerState {
			get {
				if (id_getPowerState == IntPtr.Zero)
					id_getPowerState = JNIEnv.GetMethodID (class_ref, "getPowerState", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getPowerState);
			}
		}

		static Delegate cb_getWindow;
#pragma warning disable 0169
		static Delegate GetGetWindowHandler ()
		{
			if (cb_getWindow == null)
				cb_getWindow = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetWindow);
			return cb_getWindow;
		}

		static IntPtr n_GetWindow (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Window);
		}
#pragma warning restore 0169

		IntPtr id_getWindow;
		public unsafe global::Android.Views.Window Window {
			get {
				if (id_getWindow == IntPtr.Zero)
					id_getWindow = JNIEnv.GetMethodID (class_ref, "getWindow", "()Landroid/view/Window;");
				return global::Java.Lang.Object.GetObject<global::Android.Views.Window> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getWindow), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_enableWindowTransparency;
#pragma warning disable 0169
		static Delegate GetEnableWindowTransparencyHandler ()
		{
			if (cb_enableWindowTransparency == null)
				cb_enableWindowTransparency = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_EnableWindowTransparency);
			return cb_enableWindowTransparency;
		}

		static void n_EnableWindowTransparency (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EnableWindowTransparency ();
		}
#pragma warning restore 0169

		IntPtr id_enableWindowTransparency;
		public unsafe void EnableWindowTransparency ()
		{
			if (id_enableWindowTransparency == IntPtr.Zero)
				id_enableWindowTransparency = JNIEnv.GetMethodID (class_ref, "enableWindowTransparency", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_enableWindowTransparency);
		}

		static Delegate cb_findViewById_I;
#pragma warning disable 0169
		static Delegate GetFindViewById_IHandler ()
		{
			if (cb_findViewById_I == null)
				cb_findViewById_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_FindViewById_I);
			return cb_findViewById_I;
		}

		static IntPtr n_FindViewById_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FindViewById (p0));
		}
#pragma warning restore 0169

		IntPtr id_findViewById_I;
		public unsafe global::Android.Views.View FindViewById (int p0)
		{
			if (id_findViewById_I == IntPtr.Zero)
				id_findViewById_I = JNIEnv.GetMethodID (class_ref, "findViewById", "(I)Landroid/view/View;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return global::Java.Lang.Object.GetObject<global::Android.Views.View> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_findViewById_I, __args), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_finish;
#pragma warning disable 0169
		static Delegate GetFinishHandler ()
		{
			if (cb_finish == null)
				cb_finish = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Finish);
			return cb_finish;
		}

		static void n_Finish (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Finish ();
		}
#pragma warning restore 0169

		IntPtr id_finish;
		public unsafe void Finish ()
		{
			if (id_finish == IntPtr.Zero)
				id_finish = JNIEnv.GetMethodID (class_ref, "finish", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_finish);
		}

		static Delegate cb_getCarManager_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetCarManager_Ljava_lang_String_Handler ()
		{
			if (cb_getCarManager_Ljava_lang_String_ == null)
				cb_getCarManager_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetCarManager_Ljava_lang_String_);
			return cb_getCarManager_Ljava_lang_String_;
		}

		static IntPtr n_GetCarManager_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.GetCarManager (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getCarManager_Ljava_lang_String_;
		public unsafe global::Java.Lang.Object GetCarManager (string p0)
		{
			if (id_getCarManager_Ljava_lang_String_ == IntPtr.Zero)
				id_getCarManager_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getCarManager", "(Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			global::Java.Lang.Object __ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarManager_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

		static Delegate cb_onRestoreInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnRestoreInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onRestoreInstanceState_Landroid_os_Bundle_ == null)
				cb_onRestoreInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnRestoreInstanceState_Landroid_os_Bundle_);
			return cb_onRestoreInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnRestoreInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnRestoreInstanceState (p0);
		}
#pragma warning restore 0169

		IntPtr id_onRestoreInstanceState_Landroid_os_Bundle_;
		public unsafe void OnRestoreInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onRestoreInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onRestoreInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onRestoreInstanceState", "(Landroid/os/Bundle;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onRestoreInstanceState_Landroid_os_Bundle_, __args);
		}

		static Delegate cb_onSaveInstanceState_Landroid_os_Bundle_;
#pragma warning disable 0169
		static Delegate GetOnSaveInstanceState_Landroid_os_Bundle_Handler ()
		{
			if (cb_onSaveInstanceState_Landroid_os_Bundle_ == null)
				cb_onSaveInstanceState_Landroid_os_Bundle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnSaveInstanceState_Landroid_os_Bundle_);
			return cb_onSaveInstanceState_Landroid_os_Bundle_;
		}

		static void n_OnSaveInstanceState_Landroid_os_Bundle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Bundle p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnSaveInstanceState (p0);
		}
#pragma warning restore 0169

		IntPtr id_onSaveInstanceState_Landroid_os_Bundle_;
		public unsafe void OnSaveInstanceState (global::Android.OS.Bundle p0)
		{
			if (id_onSaveInstanceState_Landroid_os_Bundle_ == IntPtr.Zero)
				id_onSaveInstanceState_Landroid_os_Bundle_ = JNIEnv.GetMethodID (class_ref, "onSaveInstanceState", "(Landroid/os/Bundle;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSaveInstanceState_Landroid_os_Bundle_, __args);
		}

		static Delegate cb_setContentView_Landroid_view_View_;
#pragma warning disable 0169
		static Delegate GetSetContentView_Landroid_view_View_Handler ()
		{
			if (cb_setContentView_Landroid_view_View_ == null)
				cb_setContentView_Landroid_view_View_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentView_Landroid_view_View_);
			return cb_setContentView_Landroid_view_View_;
		}

		static void n_SetContentView_Landroid_view_View_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetContentView (p0);
		}
#pragma warning restore 0169

		IntPtr id_setContentView_Landroid_view_View_;
		public unsafe void SetContentView (global::Android.Views.View p0)
		{
			if (id_setContentView_Landroid_view_View_ == IntPtr.Zero)
				id_setContentView_Landroid_view_View_ = JNIEnv.GetMethodID (class_ref, "setContentView", "(Landroid/view/View;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContentView_Landroid_view_View_, __args);
		}

		static Delegate cb_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_;
#pragma warning disable 0169
		static Delegate GetSetContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_Handler ()
		{
			if (cb_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_ == null)
				cb_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_SetContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_);
			return cb_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_;
		}

		static void n_SetContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.View p0 = global::Java.Lang.Object.GetObject<global::Android.Views.View> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Views.ViewGroup.LayoutParams p1 = global::Java.Lang.Object.GetObject<global::Android.Views.ViewGroup.LayoutParams> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.SetContentView (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_;
		public unsafe void SetContentView (global::Android.Views.View p0, global::Android.Views.ViewGroup.LayoutParams p1)
		{
			if (id_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_ == IntPtr.Zero)
				id_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_ = JNIEnv.GetMethodID (class_ref, "setContentView", "(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContentView_Landroid_view_View_Landroid_view_ViewGroup_LayoutParams_, __args);
		}

		static Delegate cb_setContentView_I;
#pragma warning disable 0169
		static Delegate GetSetContentView_IHandler ()
		{
			if (cb_setContentView_I == null)
				cb_setContentView_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetContentView_I);
			return cb_setContentView_I;
		}

		static void n_SetContentView_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetContentView (p0);
		}
#pragma warning restore 0169

		IntPtr id_setContentView_I;
		public unsafe void SetContentView (int p0)
		{
			if (id_setContentView_I == IntPtr.Zero)
				id_setContentView_I = JNIEnv.GetMethodID (class_ref, "setContentView", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setContentView_I, __args);
		}

		static Delegate cb_setIgnoreConfigChanges_I;
#pragma warning disable 0169
		static Delegate GetSetIgnoreConfigChanges_IHandler ()
		{
			if (cb_setIgnoreConfigChanges_I == null)
				cb_setIgnoreConfigChanges_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetIgnoreConfigChanges_I);
			return cb_setIgnoreConfigChanges_I;
		}

		static void n_SetIgnoreConfigChanges_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetIgnoreConfigChanges (p0);
		}
#pragma warning restore 0169

		IntPtr id_setIgnoreConfigChanges_I;
		public unsafe void SetIgnoreConfigChanges (int p0)
		{
			if (id_setIgnoreConfigChanges_I == IntPtr.Zero)
				id_setIgnoreConfigChanges_I = JNIEnv.GetMethodID (class_ref, "setIgnoreConfigChanges", "(I)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setIgnoreConfigChanges_I, __args);
		}

		static Delegate cb_startCarProjectionActivity_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetStartCarProjectionActivity_Landroid_content_Intent_Handler ()
		{
			if (cb_startCarProjectionActivity_Landroid_content_Intent_ == null)
				cb_startCarProjectionActivity_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_StartCarProjectionActivity_Landroid_content_Intent_);
			return cb_startCarProjectionActivity_Landroid_content_Intent_;
		}

		static void n_StartCarProjectionActivity_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Gms.Car.ICarActivityHost __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityHost> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.StartCarProjectionActivity (p0);
		}
#pragma warning restore 0169

		IntPtr id_startCarProjectionActivity_Landroid_content_Intent_;
		public unsafe void StartCarProjectionActivity (global::Android.Content.Intent p0)
		{
			if (id_startCarProjectionActivity_Landroid_content_Intent_ == IntPtr.Zero)
				id_startCarProjectionActivity_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "startCarProjectionActivity", "(Landroid/content/Intent;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startCarProjectionActivity_Landroid_content_Intent_, __args);
		}

	}

}
