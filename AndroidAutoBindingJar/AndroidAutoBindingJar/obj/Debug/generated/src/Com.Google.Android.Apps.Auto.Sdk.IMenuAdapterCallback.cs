using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']"
	[Register ("com/google/android/apps/auto/sdk/MenuAdapterCallback", "", "Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker")]
	public partial interface IMenuAdapterCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='activateAlphaJumpKeyboard' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.google.android.apps.auto.sdk.AlphaJumpKeyItem&gt;']]"
		[Register ("activateAlphaJumpKeyboard", "(Ljava/util/List;)V", "GetActivateAlphaJumpKeyboard_Ljava_util_List_Handler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void ActivateAlphaJumpKeyboard (global::System.Collections.Generic.IList<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='disableAlphaJump' and count(parameter)=0]"
		[Register ("disableAlphaJump", "()V", "GetDisableAlphaJumpHandler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void DisableAlphaJump ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='enableAlphaJump' and count(parameter)=0]"
		[Register ("enableAlphaJump", "()V", "GetEnableAlphaJumpHandler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void EnableAlphaJump ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='hideLoadingIndicator' and count(parameter)=0]"
		[Register ("hideLoadingIndicator", "()V", "GetHideLoadingIndicatorHandler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void HideLoadingIndicator ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='notifyDataSetChanged' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuAdapter']]"
		[Register ("notifyDataSetChanged", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V", "GetNotifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_Handler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void NotifyDataSetChanged (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='notifyItemChanged' and count(parameter)=2 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuAdapter'] and parameter[2][@type='int']]"
		[Register ("notifyItemChanged", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;I)V", "GetNotifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_IHandler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void NotifyItemChanged (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0, int p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/interface[@name='MenuAdapterCallback']/method[@name='showLoadingIndicator' and count(parameter)=0]"
		[Register ("showLoadingIndicator", "()V", "GetShowLoadingIndicatorHandler:Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallbackInvoker, AndroidAutoBindingJar")]
		void ShowLoadingIndicator ();

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuAdapterCallback", DoNotGenerateAcw=true)]
	internal class IMenuAdapterCallbackInvoker : global::Java.Lang.Object, IMenuAdapterCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuAdapterCallback");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IMenuAdapterCallbackInvoker); }
		}

		IntPtr class_ref;

		public static IMenuAdapterCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IMenuAdapterCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.MenuAdapterCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IMenuAdapterCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_activateAlphaJumpKeyboard_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetActivateAlphaJumpKeyboard_Ljava_util_List_Handler ()
		{
			if (cb_activateAlphaJumpKeyboard_Ljava_util_List_ == null)
				cb_activateAlphaJumpKeyboard_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_ActivateAlphaJumpKeyboard_Ljava_util_List_);
			return cb_activateAlphaJumpKeyboard_Ljava_util_List_;
		}

		static void n_ActivateAlphaJumpKeyboard_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ActivateAlphaJumpKeyboard (p0);
		}
#pragma warning restore 0169

		IntPtr id_activateAlphaJumpKeyboard_Ljava_util_List_;
		public unsafe void ActivateAlphaJumpKeyboard (global::System.Collections.Generic.IList<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem> p0)
		{
			if (id_activateAlphaJumpKeyboard_Ljava_util_List_ == IntPtr.Zero)
				id_activateAlphaJumpKeyboard_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "activateAlphaJumpKeyboard", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Com.Google.Android.Apps.Auto.Sdk.AlphaJumpKeyItem>.ToLocalJniHandle (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_activateAlphaJumpKeyboard_Ljava_util_List_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
		}

		static Delegate cb_disableAlphaJump;
#pragma warning disable 0169
		static Delegate GetDisableAlphaJumpHandler ()
		{
			if (cb_disableAlphaJump == null)
				cb_disableAlphaJump = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_DisableAlphaJump);
			return cb_disableAlphaJump;
		}

		static void n_DisableAlphaJump (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.DisableAlphaJump ();
		}
#pragma warning restore 0169

		IntPtr id_disableAlphaJump;
		public unsafe void DisableAlphaJump ()
		{
			if (id_disableAlphaJump == IntPtr.Zero)
				id_disableAlphaJump = JNIEnv.GetMethodID (class_ref, "disableAlphaJump", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_disableAlphaJump);
		}

		static Delegate cb_enableAlphaJump;
#pragma warning disable 0169
		static Delegate GetEnableAlphaJumpHandler ()
		{
			if (cb_enableAlphaJump == null)
				cb_enableAlphaJump = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_EnableAlphaJump);
			return cb_enableAlphaJump;
		}

		static void n_EnableAlphaJump (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EnableAlphaJump ();
		}
#pragma warning restore 0169

		IntPtr id_enableAlphaJump;
		public unsafe void EnableAlphaJump ()
		{
			if (id_enableAlphaJump == IntPtr.Zero)
				id_enableAlphaJump = JNIEnv.GetMethodID (class_ref, "enableAlphaJump", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_enableAlphaJump);
		}

		static Delegate cb_hideLoadingIndicator;
#pragma warning disable 0169
		static Delegate GetHideLoadingIndicatorHandler ()
		{
			if (cb_hideLoadingIndicator == null)
				cb_hideLoadingIndicator = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideLoadingIndicator);
			return cb_hideLoadingIndicator;
		}

		static void n_HideLoadingIndicator (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideLoadingIndicator ();
		}
#pragma warning restore 0169

		IntPtr id_hideLoadingIndicator;
		public unsafe void HideLoadingIndicator ()
		{
			if (id_hideLoadingIndicator == IntPtr.Zero)
				id_hideLoadingIndicator = JNIEnv.GetMethodID (class_ref, "hideLoadingIndicator", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideLoadingIndicator);
		}

		static Delegate cb_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
#pragma warning disable 0169
		static Delegate GetNotifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_Handler ()
		{
			if (cb_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_ == null)
				cb_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_NotifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_);
			return cb_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
		}

		static void n_NotifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.NotifyDataSetChanged (p0);
		}
#pragma warning restore 0169

		IntPtr id_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
		public unsafe void NotifyDataSetChanged (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0)
		{
			if (id_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_ == IntPtr.Zero)
				id_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_ = JNIEnv.GetMethodID (class_ref, "notifyDataSetChanged", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_notifyDataSetChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_, __args);
		}

		static Delegate cb_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I;
#pragma warning disable 0169
		static Delegate GetNotifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_IHandler ()
		{
			if (cb_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I == null)
				cb_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_NotifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I);
			return cb_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I;
		}

		static void n_NotifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.NotifyItemChanged (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I;
		public unsafe void NotifyItemChanged (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0, int p1)
		{
			if (id_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I == IntPtr.Zero)
				id_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I = JNIEnv.GetMethodID (class_ref, "notifyItemChanged", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;I)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_notifyItemChanged_Lcom_google_android_apps_auto_sdk_MenuAdapter_I, __args);
		}

		static Delegate cb_showLoadingIndicator;
#pragma warning disable 0169
		static Delegate GetShowLoadingIndicatorHandler ()
		{
			if (cb_showLoadingIndicator == null)
				cb_showLoadingIndicator = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowLoadingIndicator);
			return cb_showLoadingIndicator;
		}

		static void n_ShowLoadingIndicator (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.IMenuAdapterCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowLoadingIndicator ();
		}
#pragma warning restore 0169

		IntPtr id_showLoadingIndicator;
		public unsafe void ShowLoadingIndicator ()
		{
			if (id_showLoadingIndicator == IntPtr.Zero)
				id_showLoadingIndicator = JNIEnv.GetMethodID (class_ref, "showLoadingIndicator", "()V");
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showLoadingIndicator);
		}

	}

}
