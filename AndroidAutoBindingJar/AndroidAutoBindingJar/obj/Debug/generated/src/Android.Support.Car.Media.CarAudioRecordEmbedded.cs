using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Media {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']"
	[global::Android.Runtime.Register ("android/support/car/media/CarAudioRecordEmbedded", DoNotGenerateAcw=true)]
	public partial class CarAudioRecordEmbedded : global::Android.Support.Car.Media.CarAudioRecord {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/media/CarAudioRecordEmbedded", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarAudioRecordEmbedded); }
		}

		protected CarAudioRecordEmbedded (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getAudioSessionId;
#pragma warning disable 0169
		static Delegate GetGetAudioSessionIdHandler ()
		{
			if (cb_getAudioSessionId == null)
				cb_getAudioSessionId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetAudioSessionId);
			return cb_getAudioSessionId;
		}

		static int n_GetAudioSessionId (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AudioSessionId;
		}
#pragma warning restore 0169

		static IntPtr id_getAudioSessionId;
		public override unsafe int AudioSessionId {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='getAudioSessionId' and count(parameter)=0]"
			[Register ("getAudioSessionId", "()I", "GetGetAudioSessionIdHandler")]
			get {
				if (id_getAudioSessionId == IntPtr.Zero)
					id_getAudioSessionId = JNIEnv.GetMethodID (class_ref, "getAudioSessionId", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioSessionId);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAudioSessionId", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getBufferSize;
#pragma warning disable 0169
		static Delegate GetGetBufferSizeHandler ()
		{
			if (cb_getBufferSize == null)
				cb_getBufferSize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetBufferSize);
			return cb_getBufferSize;
		}

		static int n_GetBufferSize (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.BufferSize;
		}
#pragma warning restore 0169

		static IntPtr id_getBufferSize;
		public override unsafe int BufferSize {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='getBufferSize' and count(parameter)=0]"
			[Register ("getBufferSize", "()I", "GetGetBufferSizeHandler")]
			get {
				if (id_getBufferSize == IntPtr.Zero)
					id_getBufferSize = JNIEnv.GetMethodID (class_ref, "getBufferSize", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getBufferSize);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBufferSize", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getRecordingState;
#pragma warning disable 0169
		static Delegate GetGetRecordingStateHandler ()
		{
			if (cb_getRecordingState == null)
				cb_getRecordingState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetRecordingState);
			return cb_getRecordingState;
		}

		static int n_GetRecordingState (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.RecordingState;
		}
#pragma warning restore 0169

		static IntPtr id_getRecordingState;
		public override unsafe int RecordingState {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='getRecordingState' and count(parameter)=0]"
			[Register ("getRecordingState", "()I", "GetGetRecordingStateHandler")]
			get {
				if (id_getRecordingState == IntPtr.Zero)
					id_getRecordingState = JNIEnv.GetMethodID (class_ref, "getRecordingState", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getRecordingState);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRecordingState", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getState;
#pragma warning disable 0169
		static Delegate GetGetStateHandler ()
		{
			if (cb_getState == null)
				cb_getState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetState);
			return cb_getState;
		}

		static int n_GetState (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.State;
		}
#pragma warning restore 0169

		static IntPtr id_getState;
		public override unsafe int State {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='getState' and count(parameter)=0]"
			[Register ("getState", "()I", "GetGetStateHandler")]
			get {
				if (id_getState == IntPtr.Zero)
					id_getState = JNIEnv.GetMethodID (class_ref, "getState", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getState);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getState", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_read_arrayBII;
#pragma warning disable 0169
		static Delegate GetRead_arrayBIIHandler ()
		{
			if (cb_read_arrayBII == null)
				cb_read_arrayBII = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, int, int>) n_Read_arrayBII);
			return cb_read_arrayBII;
		}

		static int n_Read_arrayBII (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1, int p2)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			byte[] p0 = (byte[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (byte));
			int __ret = __this.Read (p0, p1, p2);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_read_arrayBII;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='read' and count(parameter)=3 and parameter[1][@type='byte[]'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("read", "([BII)I", "GetRead_arrayBIIHandler")]
		public override unsafe int Read (byte[] p0, int p1, int p2)
		{
			if (id_read_arrayBII == IntPtr.Zero)
				id_read_arrayBII = JNIEnv.GetMethodID (class_ref, "read", "([BII)I");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				int __ret;
				if (((object) this).GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_read_arrayBII, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "read", "([BII)I"), __args);
				return __ret;
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static Delegate cb_release;
#pragma warning disable 0169
		static Delegate GetReleaseHandler ()
		{
			if (cb_release == null)
				cb_release = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Release);
			return cb_release;
		}

		static void n_Release (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Release ();
		}
#pragma warning restore 0169

		static IntPtr id_release;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='release' and count(parameter)=0]"
		[Register ("release", "()V", "GetReleaseHandler")]
		public override unsafe void Release ()
		{
			if (id_release == IntPtr.Zero)
				id_release = JNIEnv.GetMethodID (class_ref, "release", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_release);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "release", "()V"));
			} finally {
			}
		}

		static Delegate cb_startRecording;
#pragma warning disable 0169
		static Delegate GetStartRecordingHandler ()
		{
			if (cb_startRecording == null)
				cb_startRecording = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartRecording);
			return cb_startRecording;
		}

		static void n_StartRecording (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartRecording ();
		}
#pragma warning restore 0169

		static IntPtr id_startRecording;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='startRecording' and count(parameter)=0]"
		[Register ("startRecording", "()V", "GetStartRecordingHandler")]
		public override unsafe void StartRecording ()
		{
			if (id_startRecording == IntPtr.Zero)
				id_startRecording = JNIEnv.GetMethodID (class_ref, "startRecording", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_startRecording);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startRecording", "()V"));
			} finally {
			}
		}

		static Delegate cb_stop;
#pragma warning disable 0169
		static Delegate GetStopHandler ()
		{
			if (cb_stop == null)
				cb_stop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Stop);
			return cb_stop;
		}

		static void n_Stop (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Media.CarAudioRecordEmbedded __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Media.CarAudioRecordEmbedded> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Stop ();
		}
#pragma warning restore 0169

		static IntPtr id_stop;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.media']/class[@name='CarAudioRecordEmbedded']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "GetStopHandler")]
		public override unsafe void Stop ()
		{
			if (id_stop == IntPtr.Zero)
				id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_stop);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stop", "()V"));
			} finally {
			}
		}

	}
}
