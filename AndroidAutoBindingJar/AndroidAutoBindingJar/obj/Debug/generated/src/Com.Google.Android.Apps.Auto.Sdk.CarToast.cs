using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarToast']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/CarToast", DoNotGenerateAcw=true)]
	public partial class CarToast : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/CarToast", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarToast); }
		}

		protected CarToast (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_makeText_Landroid_content_Context_II;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarToast']/method[@name='makeText' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("makeText", "(Landroid/content/Context;II)Landroid/widget/Toast;", "")]
		public static unsafe global::Android.Widget.Toast MakeText (global::Android.Content.Context p0, int p1, int p2)
		{
			if (id_makeText_Landroid_content_Context_II == IntPtr.Zero)
				id_makeText_Landroid_content_Context_II = JNIEnv.GetStaticMethodID (class_ref, "makeText", "(Landroid/content/Context;II)Landroid/widget/Toast;");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				global::Android.Widget.Toast __ret = global::Java.Lang.Object.GetObject<global::Android.Widget.Toast> (JNIEnv.CallStaticObjectMethod  (class_ref, id_makeText_Landroid_content_Context_II, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_makeText_Landroid_content_Context_Ljava_lang_CharSequence_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarToast']/method[@name='makeText' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='java.lang.CharSequence'] and parameter[3][@type='int']]"
		[Register ("makeText", "(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;", "")]
		public static unsafe global::Android.Widget.Toast MakeText (global::Android.Content.Context p0, global::Java.Lang.ICharSequence p1, int p2)
		{
			if (id_makeText_Landroid_content_Context_Ljava_lang_CharSequence_I == IntPtr.Zero)
				id_makeText_Landroid_content_Context_Ljava_lang_CharSequence_I = JNIEnv.GetStaticMethodID (class_ref, "makeText", "(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;");
			IntPtr native_p1 = CharSequence.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (p2);
				global::Android.Widget.Toast __ret = global::Java.Lang.Object.GetObject<global::Android.Widget.Toast> (JNIEnv.CallStaticObjectMethod  (class_ref, id_makeText_Landroid_content_Context_Ljava_lang_CharSequence_I, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		public static global::Android.Widget.Toast MakeText (global::Android.Content.Context p0, string p1, int p2)
		{
			global::Java.Lang.String jls_p1 = p1 == null ? null : new global::Java.Lang.String (p1);
			global::Android.Widget.Toast __result = MakeText (p0, jls_p1, p2);
			var __rsval = __result;
			jls_p1?.Dispose ();
			return __rsval;
		}

	}
}
