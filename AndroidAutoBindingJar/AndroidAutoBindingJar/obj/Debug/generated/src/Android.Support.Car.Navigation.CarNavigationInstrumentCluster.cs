using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Android.Support.Car.Navigation {

	// Metadata.xml XPath class reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']"
	[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationInstrumentCluster", DoNotGenerateAcw=true)]
	public partial class CarNavigationInstrumentCluster : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/field[@name='CLUSTER_TYPE_CUSTOM_IMAGES_SUPPORTED']"
		[Register ("CLUSTER_TYPE_CUSTOM_IMAGES_SUPPORTED")]
		public const int ClusterTypeCustomImagesSupported = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/field[@name='CLUSTER_TYPE_IMAGE_CODES_ONLY']"
		[Register ("CLUSTER_TYPE_IMAGE_CODES_ONLY")]
		public const int ClusterTypeImageCodesOnly = (int) 2;
		// Metadata.xml XPath interface reference: path="/api/package[@name='android.support.car.navigation']/interface[@name='CarNavigationInstrumentCluster.ClusterType']"
		[Register ("android/support/car/navigation/CarNavigationInstrumentCluster$ClusterType", "", "Android.Support.Car.Navigation.CarNavigationInstrumentCluster/IClusterTypeInvoker")]
		public partial interface IClusterType : global::Java.Lang.Annotation.IAnnotation {

		}

		[global::Android.Runtime.Register ("android/support/car/navigation/CarNavigationInstrumentCluster$ClusterType", DoNotGenerateAcw=true)]
		internal class IClusterTypeInvoker : global::Java.Lang.Object, IClusterType {

			static IntPtr java_class_ref = JNIEnv.FindClass ("android/support/car/navigation/CarNavigationInstrumentCluster$ClusterType");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IClusterTypeInvoker); }
			}

			IntPtr class_ref;

			public static IClusterType GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IClusterType> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "android.support.car.navigation.CarNavigationInstrumentCluster.ClusterType"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IClusterTypeInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_annotationType;
#pragma warning disable 0169
			static Delegate GetAnnotationTypeHandler ()
			{
				if (cb_annotationType == null)
					cb_annotationType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_AnnotationType);
				return cb_annotationType;
			}

			static IntPtr n_AnnotationType (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.ToLocalJniHandle (__this.AnnotationType ());
			}
#pragma warning restore 0169

			IntPtr id_annotationType;
			public unsafe global::Java.Lang.Class AnnotationType ()
			{
				if (id_annotationType == IntPtr.Zero)
					id_annotationType = JNIEnv.GetMethodID (class_ref, "annotationType", "()Ljava/lang/Class;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_annotationType), JniHandleOwnership.TransferLocalRef);
			}

			static Delegate cb_equals_Ljava_lang_Object_;
#pragma warning disable 0169
			static Delegate GetEquals_Ljava_lang_Object_Handler ()
			{
				if (cb_equals_Ljava_lang_Object_ == null)
					cb_equals_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_Equals_Ljava_lang_Object_);
				return cb_equals_Ljava_lang_Object_;
			}

			static bool n_Equals_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_obj)
			{
				global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Java.Lang.Object obj = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_obj, JniHandleOwnership.DoNotTransfer);
				bool __ret = __this.Equals (obj);
				return __ret;
			}
#pragma warning restore 0169

			IntPtr id_equals_Ljava_lang_Object_;
			public unsafe global::System.Boolean Equals (global::Java.Lang.Object obj)
			{
				if (id_equals_Ljava_lang_Object_ == IntPtr.Zero)
					id_equals_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "equals", "(Ljava/lang/Object;)Z");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (obj);
				global::System.Boolean __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_equals_Ljava_lang_Object_, __args);
				return __ret;
			}

			static Delegate cb_hashCode;
#pragma warning disable 0169
			static Delegate GetGetHashCodeHandler ()
			{
				if (cb_hashCode == null)
					cb_hashCode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetHashCode);
				return cb_hashCode;
			}

			static int n_GetHashCode (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.GetHashCode ();
			}
#pragma warning restore 0169

			IntPtr id_hashCode;
			public unsafe global::System.Int32 GetHashCode ()
			{
				if (id_hashCode == IntPtr.Zero)
					id_hashCode = JNIEnv.GetMethodID (class_ref, "hashCode", "()I");
				return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_hashCode);
			}

			static Delegate cb_toString;
#pragma warning disable 0169
			static Delegate GetToStringHandler ()
			{
				if (cb_toString == null)
					cb_toString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_ToString);
				return cb_toString;
			}

			static IntPtr n_ToString (IntPtr jnienv, IntPtr native__this)
			{
				global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster.IClusterType> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.ToString ());
			}
#pragma warning restore 0169

			IntPtr id_toString;
			public unsafe global::System.String ToString ()
			{
				if (id_toString == IntPtr.Zero)
					id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			}

		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("android/support/car/navigation/CarNavigationInstrumentCluster", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarNavigationInstrumentCluster); }
		}

		protected CarNavigationInstrumentCluster (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_support_car_navigation_CarNavigationInstrumentCluster_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/constructor[@name='CarNavigationInstrumentCluster' and count(parameter)=1 and parameter[1][@type='android.support.car.navigation.CarNavigationInstrumentCluster']]"
		[Register (".ctor", "(Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V", "")]
		public unsafe CarNavigationInstrumentCluster (global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (CarNavigationInstrumentCluster)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V", __args);
					return;
				}

				if (id_ctor_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ == IntPtr.Zero)
					id_ctor_Landroid_support_car_navigation_CarNavigationInstrumentCluster_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/support/car/navigation/CarNavigationInstrumentCluster;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_support_car_navigation_CarNavigationInstrumentCluster_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_support_car_navigation_CarNavigationInstrumentCluster_, __args);
			} finally {
			}
		}

		static Delegate cb_getExtra;
#pragma warning disable 0169
		static Delegate GetGetExtraHandler ()
		{
			if (cb_getExtra == null)
				cb_getExtra = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetExtra);
			return cb_getExtra;
		}

		static IntPtr n_GetExtra (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Extra);
		}
#pragma warning restore 0169

		static IntPtr id_getExtra;
		public virtual unsafe global::Android.OS.Bundle Extra {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getExtra' and count(parameter)=0]"
			[Register ("getExtra", "()Landroid/os/Bundle;", "GetGetExtraHandler")]
			get {
				if (id_getExtra == IntPtr.Zero)
					id_getExtra = JNIEnv.GetMethodID (class_ref, "getExtra", "()Landroid/os/Bundle;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getExtra), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.OS.Bundle> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExtra", "()Landroid/os/Bundle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getImageColorDepthBits;
#pragma warning disable 0169
		static Delegate GetGetImageColorDepthBitsHandler ()
		{
			if (cb_getImageColorDepthBits == null)
				cb_getImageColorDepthBits = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageColorDepthBits);
			return cb_getImageColorDepthBits;
		}

		static int n_GetImageColorDepthBits (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageColorDepthBits;
		}
#pragma warning restore 0169

		static IntPtr id_getImageColorDepthBits;
		public virtual unsafe int ImageColorDepthBits {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getImageColorDepthBits' and count(parameter)=0]"
			[Register ("getImageColorDepthBits", "()I", "GetGetImageColorDepthBitsHandler")]
			get {
				if (id_getImageColorDepthBits == IntPtr.Zero)
					id_getImageColorDepthBits = JNIEnv.GetMethodID (class_ref, "getImageColorDepthBits", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageColorDepthBits);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageColorDepthBits", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getImageHeight;
#pragma warning disable 0169
		static Delegate GetGetImageHeightHandler ()
		{
			if (cb_getImageHeight == null)
				cb_getImageHeight = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageHeight);
			return cb_getImageHeight;
		}

		static int n_GetImageHeight (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageHeight;
		}
#pragma warning restore 0169

		static IntPtr id_getImageHeight;
		public virtual unsafe int ImageHeight {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getImageHeight' and count(parameter)=0]"
			[Register ("getImageHeight", "()I", "GetGetImageHeightHandler")]
			get {
				if (id_getImageHeight == IntPtr.Zero)
					id_getImageHeight = JNIEnv.GetMethodID (class_ref, "getImageHeight", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageHeight);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageHeight", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getImageWidth;
#pragma warning disable 0169
		static Delegate GetGetImageWidthHandler ()
		{
			if (cb_getImageWidth == null)
				cb_getImageWidth = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetImageWidth);
			return cb_getImageWidth;
		}

		static int n_GetImageWidth (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ImageWidth;
		}
#pragma warning restore 0169

		static IntPtr id_getImageWidth;
		public virtual unsafe int ImageWidth {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getImageWidth' and count(parameter)=0]"
			[Register ("getImageWidth", "()I", "GetGetImageWidthHandler")]
			get {
				if (id_getImageWidth == IntPtr.Zero)
					id_getImageWidth = JNIEnv.GetMethodID (class_ref, "getImageWidth", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getImageWidth);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getImageWidth", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getMinIntervalMillis;
#pragma warning disable 0169
		static Delegate GetGetMinIntervalMillisHandler ()
		{
			if (cb_getMinIntervalMillis == null)
				cb_getMinIntervalMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMinIntervalMillis);
			return cb_getMinIntervalMillis;
		}

		static int n_GetMinIntervalMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MinIntervalMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getMinIntervalMillis;
		public virtual unsafe int MinIntervalMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getMinIntervalMillis' and count(parameter)=0]"
			[Register ("getMinIntervalMillis", "()I", "GetGetMinIntervalMillisHandler")]
			get {
				if (id_getMinIntervalMillis == IntPtr.Zero)
					id_getMinIntervalMillis = JNIEnv.GetMethodID (class_ref, "getMinIntervalMillis", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMinIntervalMillis);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMinIntervalMillis", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetType);
			return cb_getType;
		}

		static int n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Type;
		}
#pragma warning restore 0169

		static IntPtr id_getType;
		public virtual unsafe int Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()I", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getType);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getType", "()I"));
				} finally {
				}
			}
		}

		static IntPtr id_createCluster_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='createCluster' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("createCluster", "(I)Landroid/support/car/navigation/CarNavigationInstrumentCluster;", "")]
		public static unsafe global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster CreateCluster (int p0)
		{
			if (id_createCluster_I == IntPtr.Zero)
				id_createCluster_I = JNIEnv.GetStaticMethodID (class_ref, "createCluster", "(I)Landroid/support/car/navigation/CarNavigationInstrumentCluster;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (JNIEnv.CallStaticObjectMethod  (class_ref, id_createCluster_I, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_createCustomImageCluster_IIII;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='createCustomImageCluster' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register ("createCustomImageCluster", "(IIII)Landroid/support/car/navigation/CarNavigationInstrumentCluster;", "")]
		public static unsafe global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster CreateCustomImageCluster (int p0, int p1, int p2, int p3)
		{
			if (id_createCustomImageCluster_IIII == IntPtr.Zero)
				id_createCustomImageCluster_IIII = JNIEnv.GetStaticMethodID (class_ref, "createCustomImageCluster", "(IIII)Landroid/support/car/navigation/CarNavigationInstrumentCluster;");
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				return global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (JNIEnv.CallStaticObjectMethod  (class_ref, id_createCustomImageCluster_IIII, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_supportsCustomImages;
#pragma warning disable 0169
		static Delegate GetSupportsCustomImagesHandler ()
		{
			if (cb_supportsCustomImages == null)
				cb_supportsCustomImages = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_SupportsCustomImages);
			return cb_supportsCustomImages;
		}

		static bool n_SupportsCustomImages (IntPtr jnienv, IntPtr native__this)
		{
			global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster __this = global::Java.Lang.Object.GetObject<global::Android.Support.Car.Navigation.CarNavigationInstrumentCluster> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SupportsCustomImages ();
		}
#pragma warning restore 0169

		static IntPtr id_supportsCustomImages;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.support.car.navigation']/class[@name='CarNavigationInstrumentCluster']/method[@name='supportsCustomImages' and count(parameter)=0]"
		[Register ("supportsCustomImages", "()Z", "GetSupportsCustomImagesHandler")]
		public virtual unsafe bool SupportsCustomImages ()
		{
			if (id_supportsCustomImages == IntPtr.Zero)
				id_supportsCustomImages = JNIEnv.GetMethodID (class_ref, "supportsCustomImages", "()Z");
			try {

				if (((object) this).GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_supportsCustomImages);
				else
					return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "supportsCustomImages", "()Z"));
			} finally {
			}
		}

	}
}
