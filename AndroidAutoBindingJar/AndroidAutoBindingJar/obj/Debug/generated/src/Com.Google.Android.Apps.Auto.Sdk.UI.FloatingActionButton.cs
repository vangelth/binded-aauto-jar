using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk.UI {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/FloatingActionButton", DoNotGenerateAcw=true)]
	public partial class FloatingActionButton : global::Android.Widget.ImageButton, global::Android.Widget.ICheckable {

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='FloatingActionButton.OnCheckedChangeListener']"
		[Register ("com/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener", "", "Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton/IOnCheckedChangeListenerInvoker")]
		public partial interface IOnCheckedChangeListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/interface[@name='FloatingActionButton.OnCheckedChangeListener']/method[@name='onCheckedChanged' and count(parameter)=2 and parameter[1][@type='com.google.android.apps.auto.sdk.ui.FloatingActionButton'] and parameter[2][@type='boolean']]"
			[Register ("onCheckedChanged", "(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton;Z)V", "GetOnCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_ZHandler:Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton/IOnCheckedChangeListenerInvoker, AndroidAutoBindingJar")]
			void OnCheckedChanged (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0, bool p1);

		}

		[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener", DoNotGenerateAcw=true)]
		internal class IOnCheckedChangeListenerInvoker : global::Java.Lang.Object, IOnCheckedChangeListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnCheckedChangeListenerInvoker); }
			}

			IntPtr class_ref;

			public static IOnCheckedChangeListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnCheckedChangeListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.google.android.apps.auto.sdk.ui.FloatingActionButton.OnCheckedChangeListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnCheckedChangeListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z;
#pragma warning disable 0169
			static Delegate GetOnCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_ZHandler ()
			{
				if (cb_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z == null)
					cb_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, bool>) n_OnCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z);
				return cb_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z;
			}

			static void n_OnCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, bool p1)
			{
				global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.OnCheckedChanged (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z;
			public unsafe void OnCheckedChanged (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0, bool p1)
			{
				if (id_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z == IntPtr.Zero)
					id_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z = JNIEnv.GetMethodID (class_ref, "onCheckedChanged", "(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton;Z)V");
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_Z, __args);
			}

		}

		public partial class CheckedChangeEventArgs : global::System.EventArgs {

			public CheckedChangeEventArgs (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0, bool p1)
			{
				this.p0 = p0;
				this.p1 = p1;
			}

			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0;
			public global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton P0 {
				get { return p0; }
			}

			bool p1;
			public bool P1 {
				get { return p1; }
			}
		}

		[global::Android.Runtime.Register ("mono/com/google/android/apps/auto/sdk/ui/FloatingActionButton_OnCheckedChangeListenerImplementor")]
		internal sealed partial class IOnCheckedChangeListenerImplementor : global::Java.Lang.Object, IOnCheckedChangeListener {

			object sender;

			public IOnCheckedChangeListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/google/android/apps/auto/sdk/ui/FloatingActionButton_OnCheckedChangeListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<CheckedChangeEventArgs> Handler;
#pragma warning restore 0649

			public void OnCheckedChanged (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton p0, bool p1)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new CheckedChangeEventArgs (p0, p1));
			}

			internal static bool __IsEmpty (IOnCheckedChangeListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/ui/FloatingActionButton", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (FloatingActionButton); }
		}

		protected FloatingActionButton (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/constructor[@name='FloatingActionButton' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register (".ctor", "(Landroid/content/Context;)V", "")]
		public unsafe FloatingActionButton (global::Android.Content.Context p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (FloatingActionButton)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/constructor[@name='FloatingActionButton' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "")]
		public unsafe FloatingActionButton (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (FloatingActionButton)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/constructor[@name='FloatingActionButton' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "")]
		public unsafe FloatingActionButton (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (FloatingActionButton)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/constructor[@name='FloatingActionButton' and count(parameter)=4 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int'] and parameter[4][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", "")]
		public unsafe FloatingActionButton (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2, int p3)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				if (((object) this).GetType () != typeof (FloatingActionButton)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_II, __args);
			} finally {
			}
		}

		static Delegate cb_isChecked;
#pragma warning disable 0169
		static Delegate GetIsCheckedHandler ()
		{
			if (cb_isChecked == null)
				cb_isChecked = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsChecked);
			return cb_isChecked;
		}

		static bool n_IsChecked (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Checked;
		}
#pragma warning restore 0169

		static Delegate cb_setChecked_Z;
#pragma warning disable 0169
		static Delegate GetSetChecked_ZHandler ()
		{
			if (cb_setChecked_Z == null)
				cb_setChecked_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetChecked_Z);
			return cb_setChecked_Z;
		}

		static void n_SetChecked_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Checked = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isChecked;
		static IntPtr id_setChecked_Z;
		public virtual unsafe bool Checked {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='isChecked' and count(parameter)=0]"
			[Register ("isChecked", "()Z", "GetIsCheckedHandler")]
			get {
				if (id_isChecked == IntPtr.Zero)
					id_isChecked = JNIEnv.GetMethodID (class_ref, "isChecked", "()Z");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isChecked);
					else
						return JNIEnv.CallNonvirtualBooleanMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isChecked", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='setChecked' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setChecked", "(Z)V", "GetSetChecked_ZHandler")]
			set {
				if (id_setChecked_Z == IntPtr.Zero)
					id_setChecked_Z = JNIEnv.GetMethodID (class_ref, "setChecked", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setChecked_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setChecked", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_onRestoreInstanceState_Landroid_os_Parcelable_;
#pragma warning disable 0169
		static Delegate GetOnRestoreInstanceState_Landroid_os_Parcelable_Handler ()
		{
			if (cb_onRestoreInstanceState_Landroid_os_Parcelable_ == null)
				cb_onRestoreInstanceState_Landroid_os_Parcelable_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnRestoreInstanceState_Landroid_os_Parcelable_);
			return cb_onRestoreInstanceState_Landroid_os_Parcelable_;
		}

		static void n_OnRestoreInstanceState_Landroid_os_Parcelable_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.IParcelable p0 = (global::Android.OS.IParcelable)global::Java.Lang.Object.GetObject<global::Android.OS.IParcelable> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnRestoreInstanceState (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onRestoreInstanceState_Landroid_os_Parcelable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='onRestoreInstanceState' and count(parameter)=1 and parameter[1][@type='android.os.Parcelable']]"
		[Register ("onRestoreInstanceState", "(Landroid/os/Parcelable;)V", "GetOnRestoreInstanceState_Landroid_os_Parcelable_Handler")]
		public virtual unsafe void OnRestoreInstanceState (global::Android.OS.IParcelable p0)
		{
			if (id_onRestoreInstanceState_Landroid_os_Parcelable_ == IntPtr.Zero)
				id_onRestoreInstanceState_Landroid_os_Parcelable_ = JNIEnv.GetMethodID (class_ref, "onRestoreInstanceState", "(Landroid/os/Parcelable;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onRestoreInstanceState_Landroid_os_Parcelable_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onRestoreInstanceState", "(Landroid/os/Parcelable;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onSaveInstanceState;
#pragma warning disable 0169
		static Delegate GetOnSaveInstanceStateHandler ()
		{
			if (cb_onSaveInstanceState == null)
				cb_onSaveInstanceState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_OnSaveInstanceState);
			return cb_onSaveInstanceState;
		}

		static IntPtr n_OnSaveInstanceState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OnSaveInstanceState ());
		}
#pragma warning restore 0169

		static IntPtr id_onSaveInstanceState;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='onSaveInstanceState' and count(parameter)=0]"
		[Register ("onSaveInstanceState", "()Landroid/os/Parcelable;", "GetOnSaveInstanceStateHandler")]
		public virtual unsafe global::Android.OS.IParcelable OnSaveInstanceState ()
		{
			if (id_onSaveInstanceState == IntPtr.Zero)
				id_onSaveInstanceState = JNIEnv.GetMethodID (class_ref, "onSaveInstanceState", "()Landroid/os/Parcelable;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelable> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_onSaveInstanceState), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelable> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onSaveInstanceState", "()Landroid/os/Parcelable;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_;
#pragma warning disable 0169
		static Delegate GetSetOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_Handler ()
		{
			if (cb_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_ == null)
				cb_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_);
			return cb_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_;
		}

		static void n_SetOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener p0 = (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener)global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOnCheckedChangeListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='setOnCheckedChangeListener' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.ui.FloatingActionButton.OnCheckedChangeListener']]"
		[Register ("setOnCheckedChangeListener", "(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener;)V", "GetSetOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_Handler")]
		public virtual unsafe void SetOnCheckedChangeListener (global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener p0)
		{
			if (id_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_ == IntPtr.Zero)
				id_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_ = JNIEnv.GetMethodID (class_ref, "setOnCheckedChangeListener", "(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setOnCheckedChangeListener_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_OnCheckedChangeListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOnCheckedChangeListener", "(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton$OnCheckedChangeListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_toggle;
#pragma warning disable 0169
		static Delegate GetToggleHandler ()
		{
			if (cb_toggle == null)
				cb_toggle = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Toggle);
			return cb_toggle;
		}

		static void n_Toggle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Toggle ();
		}
#pragma warning restore 0169

		static IntPtr id_toggle;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk.ui']/class[@name='FloatingActionButton']/method[@name='toggle' and count(parameter)=0]"
		[Register ("toggle", "()V", "GetToggleHandler")]
		public virtual unsafe void Toggle ()
		{
			if (id_toggle == IntPtr.Zero)
				id_toggle = JNIEnv.GetMethodID (class_ref, "toggle", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_toggle);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "toggle", "()V"));
			} finally {
			}
		}

#region "Event implementation for Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener"
		public event EventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.CheckedChangeEventArgs> CheckedChange {
			add {
				global::Java.Interop.EventHelper.AddEventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener, global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListenerImplementor>(
						ref weak_implementor_SetOnCheckedChangeListener,
						__CreateIOnCheckedChangeListenerImplementor,
						SetOnCheckedChangeListener,
						__h => __h.Handler += value);
			}
			remove {
				global::Java.Interop.EventHelper.RemoveEventHandler<global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListener, global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListenerImplementor>(
						ref weak_implementor_SetOnCheckedChangeListener,
						global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListenerImplementor.__IsEmpty,
						__v => SetOnCheckedChangeListener (null),
						__h => __h.Handler -= value);
			}
		}

		WeakReference weak_implementor_SetOnCheckedChangeListener;

		global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListenerImplementor __CreateIOnCheckedChangeListenerImplementor ()
		{
			return new global::Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton.IOnCheckedChangeListenerImplementor (this);
		}
#endregion
	}
}
