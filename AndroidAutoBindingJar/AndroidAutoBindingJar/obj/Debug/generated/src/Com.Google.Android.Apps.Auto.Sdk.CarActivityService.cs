using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivityService']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/CarActivityService", DoNotGenerateAcw=true)]
	public abstract partial class CarActivityService : global::Com.Google.Android.Gms.Car.D {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/CarActivityService", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarActivityService); }
		}

		protected CarActivityService (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivityService']/constructor[@name='CarActivityService' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CarActivityService ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (CarActivityService)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		// skipped generating property CarActivity because its Java method declaration is variant that we cannot represent in C#
	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/CarActivityService", DoNotGenerateAcw=true)]
	internal partial class CarActivityServiceInvoker : CarActivityService {

		public CarActivityServiceInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CarActivityServiceInvoker); }
		}

		static IntPtr id_getCarActivity;
		public override unsafe global::Java.Lang.Class CarActivity {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='CarActivityService']/method[@name='getCarActivity' and count(parameter)=0]"
			[Register ("getCarActivity", "()Ljava/lang/Class;", "GetGetCarActivityHandler")]
			get {
				if (id_getCarActivity == IntPtr.Zero)
					id_getCarActivity = JNIEnv.GetMethodID (class_ref, "getCarActivity", "()Ljava/lang/Class;");
				try {
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarActivity), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}

}
