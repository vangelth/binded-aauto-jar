using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']"
	[Register ("com/google/android/gms/car/CarAudioManager", "", "Com.Google.Android.Gms.Car.ICarAudioManagerInvoker")]
	public partial interface ICarAudioManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='createCarAudioRecord' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("createCarAudioRecord", "(III)Lcom/google/android/gms/car/CarAudioRecord;", "GetCreateCarAudioRecord_IIIHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.ICarAudioRecord CreateCarAudioRecord (int p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioRecordConfigurations' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioRecordConfigurations", "(I)[Lcom/google/android/gms/car/CarAudioConfig;", "GetGetAudioRecordConfigurations_IHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.CarAudioConfig[] GetAudioRecordConfigurations (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioRecordMinBufferSize' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register ("getAudioRecordMinBufferSize", "(II)I", "GetGetAudioRecordMinBufferSize_IIHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		int GetAudioRecordMinBufferSize (int p0, int p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioRecordStreams' and count(parameter)=0]"
		[Register ("getAudioRecordStreams", "()[I", "GetGetAudioRecordStreamsHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		int[] GetAudioRecordStreams ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioTrackConfigurations' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getAudioTrackConfigurations", "(I)[Lcom/google/android/gms/car/CarAudioConfig;", "GetGetAudioTrackConfigurations_IHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.CarAudioConfig[] GetAudioTrackConfigurations (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioTrackMinBufferSize' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register ("getAudioTrackMinBufferSize", "(II)I", "GetGetAudioTrackMinBufferSize_IIHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		int GetAudioTrackMinBufferSize (int p0, int p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getAudioTrackStreams' and count(parameter)=0]"
		[Register ("getAudioTrackStreams", "()[I", "GetGetAudioTrackStreamsHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		int[] GetAudioTrackStreams ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='getCarAudioTrack' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("getCarAudioTrack", "(III)Lcom/google/android/gms/car/CarAudioTrack;", "GetGetCarAudioTrack_IIIHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		global::Com.Google.Android.Gms.Car.CarAudioTrack GetCarAudioTrack (int p0, int p1, int p2);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='isAudioRecordStreamSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isAudioRecordStreamSupported", "(I)Z", "GetIsAudioRecordStreamSupported_IHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		bool IsAudioRecordStreamSupported (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='isAudioTrackStreamSupported' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("isAudioTrackStreamSupported", "(I)Z", "GetIsAudioTrackStreamSupported_IHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		bool IsAudioTrackStreamSupported (int p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='waitForPlayback' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register ("waitForPlayback", "(J)Z", "GetWaitForPlayback_JHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		bool WaitForPlayback (long p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/interface[@name='CarAudioManager']/method[@name='waitForSilence' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register ("waitForSilence", "(J)Z", "GetWaitForSilence_JHandler:Com.Google.Android.Gms.Car.ICarAudioManagerInvoker, AndroidAutoBindingJar")]
		bool WaitForSilence (long p0);

	}

	[global::Android.Runtime.Register ("com/google/android/gms/car/CarAudioManager", DoNotGenerateAcw=true)]
	internal class ICarAudioManagerInvoker : global::Java.Lang.Object, ICarAudioManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/google/android/gms/car/CarAudioManager");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ICarAudioManagerInvoker); }
		}

		IntPtr class_ref;

		public static ICarAudioManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICarAudioManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.google.android.gms.car.CarAudioManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICarAudioManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_createCarAudioRecord_III;
#pragma warning disable 0169
		static Delegate GetCreateCarAudioRecord_IIIHandler ()
		{
			if (cb_createCarAudioRecord_III == null)
				cb_createCarAudioRecord_III = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int, IntPtr>) n_CreateCarAudioRecord_III);
			return cb_createCarAudioRecord_III;
		}

		static IntPtr n_CreateCarAudioRecord_III (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CreateCarAudioRecord (p0, p1, p2));
		}
#pragma warning restore 0169

		IntPtr id_createCarAudioRecord_III;
		public unsafe global::Com.Google.Android.Gms.Car.ICarAudioRecord CreateCarAudioRecord (int p0, int p1, int p2)
		{
			if (id_createCarAudioRecord_III == IntPtr.Zero)
				id_createCarAudioRecord_III = JNIEnv.GetMethodID (class_ref, "createCarAudioRecord", "(III)Lcom/google/android/gms/car/CarAudioRecord;");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioRecord> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_createCarAudioRecord_III, __args), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_getAudioRecordConfigurations_I;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordConfigurations_IHandler ()
		{
			if (cb_getAudioRecordConfigurations_I == null)
				cb_getAudioRecordConfigurations_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetAudioRecordConfigurations_I);
			return cb_getAudioRecordConfigurations_I;
		}

		static IntPtr n_GetAudioRecordConfigurations_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetAudioRecordConfigurations (p0));
		}
#pragma warning restore 0169

		IntPtr id_getAudioRecordConfigurations_I;
		public unsafe global::Com.Google.Android.Gms.Car.CarAudioConfig[] GetAudioRecordConfigurations (int p0)
		{
			if (id_getAudioRecordConfigurations_I == IntPtr.Zero)
				id_getAudioRecordConfigurations_I = JNIEnv.GetMethodID (class_ref, "getAudioRecordConfigurations", "(I)[Lcom/google/android/gms/car/CarAudioConfig;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return (global::Com.Google.Android.Gms.Car.CarAudioConfig[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordConfigurations_I, __args), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Google.Android.Gms.Car.CarAudioConfig));
		}

		static Delegate cb_getAudioRecordMinBufferSize_II;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordMinBufferSize_IIHandler ()
		{
			if (cb_getAudioRecordMinBufferSize_II == null)
				cb_getAudioRecordMinBufferSize_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int>) n_GetAudioRecordMinBufferSize_II);
			return cb_getAudioRecordMinBufferSize_II;
		}

		static int n_GetAudioRecordMinBufferSize_II (IntPtr jnienv, IntPtr native__this, int p0, int p1)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.GetAudioRecordMinBufferSize (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_getAudioRecordMinBufferSize_II;
		public unsafe int GetAudioRecordMinBufferSize (int p0, int p1)
		{
			if (id_getAudioRecordMinBufferSize_II == IntPtr.Zero)
				id_getAudioRecordMinBufferSize_II = JNIEnv.GetMethodID (class_ref, "getAudioRecordMinBufferSize", "(II)I");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordMinBufferSize_II, __args);
		}

		static Delegate cb_getAudioRecordStreams;
#pragma warning disable 0169
		static Delegate GetGetAudioRecordStreamsHandler ()
		{
			if (cb_getAudioRecordStreams == null)
				cb_getAudioRecordStreams = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAudioRecordStreams);
			return cb_getAudioRecordStreams;
		}

		static IntPtr n_GetAudioRecordStreams (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetAudioRecordStreams ());
		}
#pragma warning restore 0169

		IntPtr id_getAudioRecordStreams;
		public unsafe int[] GetAudioRecordStreams ()
		{
			if (id_getAudioRecordStreams == IntPtr.Zero)
				id_getAudioRecordStreams = JNIEnv.GetMethodID (class_ref, "getAudioRecordStreams", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioRecordStreams), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

		static Delegate cb_getAudioTrackConfigurations_I;
#pragma warning disable 0169
		static Delegate GetGetAudioTrackConfigurations_IHandler ()
		{
			if (cb_getAudioTrackConfigurations_I == null)
				cb_getAudioTrackConfigurations_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetAudioTrackConfigurations_I);
			return cb_getAudioTrackConfigurations_I;
		}

		static IntPtr n_GetAudioTrackConfigurations_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetAudioTrackConfigurations (p0));
		}
#pragma warning restore 0169

		IntPtr id_getAudioTrackConfigurations_I;
		public unsafe global::Com.Google.Android.Gms.Car.CarAudioConfig[] GetAudioTrackConfigurations (int p0)
		{
			if (id_getAudioTrackConfigurations_I == IntPtr.Zero)
				id_getAudioTrackConfigurations_I = JNIEnv.GetMethodID (class_ref, "getAudioTrackConfigurations", "(I)[Lcom/google/android/gms/car/CarAudioConfig;");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return (global::Com.Google.Android.Gms.Car.CarAudioConfig[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioTrackConfigurations_I, __args), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Google.Android.Gms.Car.CarAudioConfig));
		}

		static Delegate cb_getAudioTrackMinBufferSize_II;
#pragma warning disable 0169
		static Delegate GetGetAudioTrackMinBufferSize_IIHandler ()
		{
			if (cb_getAudioTrackMinBufferSize_II == null)
				cb_getAudioTrackMinBufferSize_II = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int>) n_GetAudioTrackMinBufferSize_II);
			return cb_getAudioTrackMinBufferSize_II;
		}

		static int n_GetAudioTrackMinBufferSize_II (IntPtr jnienv, IntPtr native__this, int p0, int p1)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.GetAudioTrackMinBufferSize (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_getAudioTrackMinBufferSize_II;
		public unsafe int GetAudioTrackMinBufferSize (int p0, int p1)
		{
			if (id_getAudioTrackMinBufferSize_II == IntPtr.Zero)
				id_getAudioTrackMinBufferSize_II = JNIEnv.GetMethodID (class_ref, "getAudioTrackMinBufferSize", "(II)I");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getAudioTrackMinBufferSize_II, __args);
		}

		static Delegate cb_getAudioTrackStreams;
#pragma warning disable 0169
		static Delegate GetGetAudioTrackStreamsHandler ()
		{
			if (cb_getAudioTrackStreams == null)
				cb_getAudioTrackStreams = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAudioTrackStreams);
			return cb_getAudioTrackStreams;
		}

		static IntPtr n_GetAudioTrackStreams (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetAudioTrackStreams ());
		}
#pragma warning restore 0169

		IntPtr id_getAudioTrackStreams;
		public unsafe int[] GetAudioTrackStreams ()
		{
			if (id_getAudioTrackStreams == IntPtr.Zero)
				id_getAudioTrackStreams = JNIEnv.GetMethodID (class_ref, "getAudioTrackStreams", "()[I");
			return (int[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getAudioTrackStreams), JniHandleOwnership.TransferLocalRef, typeof (int));
		}

		static Delegate cb_getCarAudioTrack_III;
#pragma warning disable 0169
		static Delegate GetGetCarAudioTrack_IIIHandler ()
		{
			if (cb_getCarAudioTrack_III == null)
				cb_getCarAudioTrack_III = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int, IntPtr>) n_GetCarAudioTrack_III);
			return cb_getCarAudioTrack_III;
		}

		static IntPtr n_GetCarAudioTrack_III (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetCarAudioTrack (p0, p1, p2));
		}
#pragma warning restore 0169

		IntPtr id_getCarAudioTrack_III;
		public unsafe global::Com.Google.Android.Gms.Car.CarAudioTrack GetCarAudioTrack (int p0, int p1, int p2)
		{
			if (id_getCarAudioTrack_III == IntPtr.Zero)
				id_getCarAudioTrack_III = JNIEnv.GetMethodID (class_ref, "getCarAudioTrack", "(III)Lcom/google/android/gms/car/CarAudioTrack;");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			return global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.CarAudioTrack> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getCarAudioTrack_III, __args), JniHandleOwnership.TransferLocalRef);
		}

		static Delegate cb_isAudioRecordStreamSupported_I;
#pragma warning disable 0169
		static Delegate GetIsAudioRecordStreamSupported_IHandler ()
		{
			if (cb_isAudioRecordStreamSupported_I == null)
				cb_isAudioRecordStreamSupported_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_IsAudioRecordStreamSupported_I);
			return cb_isAudioRecordStreamSupported_I;
		}

		static bool n_IsAudioRecordStreamSupported_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsAudioRecordStreamSupported (p0);
		}
#pragma warning restore 0169

		IntPtr id_isAudioRecordStreamSupported_I;
		public unsafe bool IsAudioRecordStreamSupported (int p0)
		{
			if (id_isAudioRecordStreamSupported_I == IntPtr.Zero)
				id_isAudioRecordStreamSupported_I = JNIEnv.GetMethodID (class_ref, "isAudioRecordStreamSupported", "(I)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isAudioRecordStreamSupported_I, __args);
		}

		static Delegate cb_isAudioTrackStreamSupported_I;
#pragma warning disable 0169
		static Delegate GetIsAudioTrackStreamSupported_IHandler ()
		{
			if (cb_isAudioTrackStreamSupported_I == null)
				cb_isAudioTrackStreamSupported_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, bool>) n_IsAudioTrackStreamSupported_I);
			return cb_isAudioTrackStreamSupported_I;
		}

		static bool n_IsAudioTrackStreamSupported_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsAudioTrackStreamSupported (p0);
		}
#pragma warning restore 0169

		IntPtr id_isAudioTrackStreamSupported_I;
		public unsafe bool IsAudioTrackStreamSupported (int p0)
		{
			if (id_isAudioTrackStreamSupported_I == IntPtr.Zero)
				id_isAudioTrackStreamSupported_I = JNIEnv.GetMethodID (class_ref, "isAudioTrackStreamSupported", "(I)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_isAudioTrackStreamSupported_I, __args);
		}

		static Delegate cb_waitForPlayback_J;
#pragma warning disable 0169
		static Delegate GetWaitForPlayback_JHandler ()
		{
			if (cb_waitForPlayback_J == null)
				cb_waitForPlayback_J = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long, bool>) n_WaitForPlayback_J);
			return cb_waitForPlayback_J;
		}

		static bool n_WaitForPlayback_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.WaitForPlayback (p0);
		}
#pragma warning restore 0169

		IntPtr id_waitForPlayback_J;
		public unsafe bool WaitForPlayback (long p0)
		{
			if (id_waitForPlayback_J == IntPtr.Zero)
				id_waitForPlayback_J = JNIEnv.GetMethodID (class_ref, "waitForPlayback", "(J)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_waitForPlayback_J, __args);
		}

		static Delegate cb_waitForSilence_J;
#pragma warning disable 0169
		static Delegate GetWaitForSilence_JHandler ()
		{
			if (cb_waitForSilence_J == null)
				cb_waitForSilence_J = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long, bool>) n_WaitForSilence_J);
			return cb_waitForSilence_J;
		}

		static bool n_WaitForSilence_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Google.Android.Gms.Car.ICarAudioManager __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarAudioManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.WaitForSilence (p0);
		}
#pragma warning restore 0169

		IntPtr id_waitForSilence_J;
		public unsafe bool WaitForSilence (long p0)
		{
			if (id_waitForSilence_J == IntPtr.Zero)
				id_waitForSilence_J = JNIEnv.GetMethodID (class_ref, "waitForSilence", "(J)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			return JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_waitForSilence_J, __args);
		}

	}

}
