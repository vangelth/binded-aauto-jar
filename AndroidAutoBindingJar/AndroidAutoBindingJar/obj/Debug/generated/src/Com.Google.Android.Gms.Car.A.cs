using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Gms.Car {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='a']"
	[global::Android.Runtime.Register ("com/google/android/gms/car/a", DoNotGenerateAcw=true)]
	public sealed partial class A : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/gms/car/a", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (A); }
		}

		internal A (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='a']/constructor[@name='a' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe A ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (A)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_a_Landroid_content_Context_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='a']/method[@name='a' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("a", "(Landroid/content/Context;)Lcom/google/android/gms/car/CarActivityServiceProxy;", "")]
		public unsafe global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy InvokeA (global::Android.Content.Context p0)
		{
			if (id_a_Landroid_content_Context_ == IntPtr.Zero)
				id_a_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "a", "(Landroid/content/Context;)Lcom/google/android/gms/car/CarActivityServiceProxy;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy __ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarActivityServiceProxy> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_Landroid_content_Context_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_a_Landroid_content_Context_Lcom_google_android_gms_car_CarApiConnection_ApiConnectionCallback_Landroid_os_Looper_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.gms.car']/class[@name='a']/method[@name='a' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='com.google.android.gms.car.CarApiConnection.ApiConnectionCallback'] and parameter[3][@type='android.os.Looper']]"
		[Register ("a", "(Landroid/content/Context;Lcom/google/android/gms/car/CarApiConnection$ApiConnectionCallback;Landroid/os/Looper;)Lcom/google/android/gms/car/CarApiConnection;", "")]
		public unsafe global::Com.Google.Android.Gms.Car.ICarApiConnection InvokeA (global::Android.Content.Context p0, global::Com.Google.Android.Gms.Car.ICarApiConnectionApiConnectionCallback p1, global::Android.OS.Looper p2)
		{
			if (id_a_Landroid_content_Context_Lcom_google_android_gms_car_CarApiConnection_ApiConnectionCallback_Landroid_os_Looper_ == IntPtr.Zero)
				id_a_Landroid_content_Context_Lcom_google_android_gms_car_CarApiConnection_ApiConnectionCallback_Landroid_os_Looper_ = JNIEnv.GetMethodID (class_ref, "a", "(Landroid/content/Context;Lcom/google/android/gms/car/CarApiConnection$ApiConnectionCallback;Landroid/os/Looper;)Lcom/google/android/gms/car/CarApiConnection;");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				global::Com.Google.Android.Gms.Car.ICarApiConnection __ret = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Gms.Car.ICarApiConnection> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_a_Landroid_content_Context_Lcom_google_android_gms_car_CarApiConnection_ApiConnectionCallback_Landroid_os_Looper_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}
}
