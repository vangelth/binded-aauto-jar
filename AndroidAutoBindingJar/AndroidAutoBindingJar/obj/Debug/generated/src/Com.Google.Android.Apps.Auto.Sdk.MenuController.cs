using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuController']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/MenuController", DoNotGenerateAcw=true)]
	public partial class MenuController : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/MenuController", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (MenuController); }
		}

		protected MenuController (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuController']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()V", "")]
		public unsafe void A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()V");
			try {
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_a);
			} finally {
			}
		}

		static Delegate cb_hideMenuButton;
#pragma warning disable 0169
		static Delegate GetHideMenuButtonHandler ()
		{
			if (cb_hideMenuButton == null)
				cb_hideMenuButton = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_HideMenuButton);
			return cb_hideMenuButton;
		}

		static void n_HideMenuButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideMenuButton ();
		}
#pragma warning restore 0169

		static IntPtr id_hideMenuButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuController']/method[@name='hideMenuButton' and count(parameter)=0]"
		[Register ("hideMenuButton", "()V", "GetHideMenuButtonHandler")]
		public virtual unsafe void HideMenuButton ()
		{
			if (id_hideMenuButton == IntPtr.Zero)
				id_hideMenuButton = JNIEnv.GetMethodID (class_ref, "hideMenuButton", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_hideMenuButton);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hideMenuButton", "()V"));
			} finally {
			}
		}

		static Delegate cb_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
#pragma warning disable 0169
		static Delegate GetSetRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_Handler ()
		{
			if (cb_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_ == null)
				cb_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_);
			return cb_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
		}

		static void n_SetRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetRootMenuAdapter (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuController']/method[@name='setRootMenuAdapter' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.MenuAdapter']]"
		[Register ("setRootMenuAdapter", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V", "GetSetRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_Handler")]
		public virtual unsafe void SetRootMenuAdapter (global::Com.Google.Android.Apps.Auto.Sdk.MenuAdapter p0)
		{
			if (id_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_ == IntPtr.Zero)
				id_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_ = JNIEnv.GetMethodID (class_ref, "setRootMenuAdapter", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setRootMenuAdapter_Lcom_google_android_apps_auto_sdk_MenuAdapter_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRootMenuAdapter", "(Lcom/google/android/apps/auto/sdk/MenuAdapter;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_showMenuButton;
#pragma warning disable 0169
		static Delegate GetShowMenuButtonHandler ()
		{
			if (cb_showMenuButton == null)
				cb_showMenuButton = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ShowMenuButton);
			return cb_showMenuButton;
		}

		static void n_ShowMenuButton (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.MenuController __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.MenuController> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ShowMenuButton ();
		}
#pragma warning restore 0169

		static IntPtr id_showMenuButton;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='MenuController']/method[@name='showMenuButton' and count(parameter)=0]"
		[Register ("showMenuButton", "()V", "GetShowMenuButtonHandler")]
		public virtual unsafe void ShowMenuButton ()
		{
			if (id_showMenuButton == IntPtr.Zero)
				id_showMenuButton = JNIEnv.GetMethodID (class_ref, "showMenuButton", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_showMenuButton);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "showMenuButton", "()V"));
			} finally {
			}
		}

	}
}
