using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Google.Android.Apps.Auto.Sdk {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']"
	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/SearchCallback", DoNotGenerateAcw=true)]
	public abstract partial class SearchCallback : global::Java.Lang.Object {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/google/android/apps/auto/sdk/SearchCallback", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (SearchCallback); }
		}

		protected SearchCallback (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/constructor[@name='SearchCallback' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe SearchCallback ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				if (((object) this).GetType () != typeof (SearchCallback)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_;
#pragma warning disable 0169
		static Delegate GetOnSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_Handler ()
		{
			if (cb_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_ == null)
				cb_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_);
			return cb_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_;
		}

		static void n_OnSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Google.Android.Apps.Auto.Sdk.SearchItem p0 = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchItem> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnSearchItemSelected (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchItemSelected' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.SearchItem']]"
		[Register ("onSearchItemSelected", "(Lcom/google/android/apps/auto/sdk/SearchItem;)V", "GetOnSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_Handler")]
		public abstract void OnSearchItemSelected (global::Com.Google.Android.Apps.Auto.Sdk.SearchItem p0);

		static Delegate cb_onSearchStart;
#pragma warning disable 0169
		static Delegate GetOnSearchStartHandler ()
		{
			if (cb_onSearchStart == null)
				cb_onSearchStart = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnSearchStart);
			return cb_onSearchStart;
		}

		static void n_OnSearchStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnSearchStart ();
		}
#pragma warning restore 0169

		static IntPtr id_onSearchStart;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchStart' and count(parameter)=0]"
		[Register ("onSearchStart", "()V", "GetOnSearchStartHandler")]
		public virtual unsafe void OnSearchStart ()
		{
			if (id_onSearchStart == IntPtr.Zero)
				id_onSearchStart = JNIEnv.GetMethodID (class_ref, "onSearchStart", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSearchStart);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onSearchStart", "()V"));
			} finally {
			}
		}

		static Delegate cb_onSearchStop;
#pragma warning disable 0169
		static Delegate GetOnSearchStopHandler ()
		{
			if (cb_onSearchStop == null)
				cb_onSearchStop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_OnSearchStop);
			return cb_onSearchStop;
		}

		static void n_OnSearchStop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.OnSearchStop ();
		}
#pragma warning restore 0169

		static IntPtr id_onSearchStop;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchStop' and count(parameter)=0]"
		[Register ("onSearchStop", "()V", "GetOnSearchStopHandler")]
		public virtual unsafe void OnSearchStop ()
		{
			if (id_onSearchStop == IntPtr.Zero)
				id_onSearchStop = JNIEnv.GetMethodID (class_ref, "onSearchStop", "()V");
			try {

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSearchStop);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onSearchStop", "()V"));
			} finally {
			}
		}

		static Delegate cb_onSearchSubmitted_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetOnSearchSubmitted_Ljava_lang_String_Handler ()
		{
			if (cb_onSearchSubmitted_Ljava_lang_String_ == null)
				cb_onSearchSubmitted_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_OnSearchSubmitted_Ljava_lang_String_);
			return cb_onSearchSubmitted_Ljava_lang_String_;
		}

		static bool n_OnSearchSubmitted_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.OnSearchSubmitted (p0);
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchSubmitted' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("onSearchSubmitted", "(Ljava/lang/String;)Z", "GetOnSearchSubmitted_Ljava_lang_String_Handler")]
		public abstract bool OnSearchSubmitted (string p0);

		static Delegate cb_onSearchTextChanged_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetOnSearchTextChanged_Ljava_lang_String_Handler ()
		{
			if (cb_onSearchTextChanged_Ljava_lang_String_ == null)
				cb_onSearchTextChanged_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnSearchTextChanged_Ljava_lang_String_);
			return cb_onSearchTextChanged_Ljava_lang_String_;
		}

		static void n_OnSearchTextChanged_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback __this = global::Java.Lang.Object.GetObject<global::Com.Google.Android.Apps.Auto.Sdk.SearchCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnSearchTextChanged (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchTextChanged' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("onSearchTextChanged", "(Ljava/lang/String;)V", "GetOnSearchTextChanged_Ljava_lang_String_Handler")]
		public abstract void OnSearchTextChanged (string p0);

	}

	[global::Android.Runtime.Register ("com/google/android/apps/auto/sdk/SearchCallback", DoNotGenerateAcw=true)]
	internal partial class SearchCallbackInvoker : SearchCallback {

		public SearchCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (SearchCallbackInvoker); }
		}

		static IntPtr id_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchItemSelected' and count(parameter)=1 and parameter[1][@type='com.google.android.apps.auto.sdk.SearchItem']]"
		[Register ("onSearchItemSelected", "(Lcom/google/android/apps/auto/sdk/SearchItem;)V", "GetOnSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_Handler")]
		public override unsafe void OnSearchItemSelected (global::Com.Google.Android.Apps.Auto.Sdk.SearchItem p0)
		{
			if (id_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_ == IntPtr.Zero)
				id_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_ = JNIEnv.GetMethodID (class_ref, "onSearchItemSelected", "(Lcom/google/android/apps/auto/sdk/SearchItem;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSearchItemSelected_Lcom_google_android_apps_auto_sdk_SearchItem_, __args);
			} finally {
			}
		}

		static IntPtr id_onSearchSubmitted_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchSubmitted' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("onSearchSubmitted", "(Ljava/lang/String;)Z", "GetOnSearchSubmitted_Ljava_lang_String_Handler")]
		public override unsafe bool OnSearchSubmitted (string p0)
		{
			if (id_onSearchSubmitted_Ljava_lang_String_ == IntPtr.Zero)
				id_onSearchSubmitted_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "onSearchSubmitted", "(Ljava/lang/String;)Z");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				bool __ret = JNIEnv.CallBooleanMethod (((global::Java.Lang.Object) this).Handle, id_onSearchSubmitted_Ljava_lang_String_, __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_onSearchTextChanged_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.google.android.apps.auto.sdk']/class[@name='SearchCallback']/method[@name='onSearchTextChanged' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("onSearchTextChanged", "(Ljava/lang/String;)V", "GetOnSearchTextChanged_Ljava_lang_String_Handler")]
		public override unsafe void OnSearchTextChanged (string p0)
		{
			if (id_onSearchTextChanged_Ljava_lang_String_ == IntPtr.Zero)
				id_onSearchTextChanged_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "onSearchTextChanged", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onSearchTextChanged_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}

}
