package md501853634b6e183c569823b9112e6b4b9;


public class CarService
	extends com.google.android.apps.auto.sdk.CarActivityService
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_getCarActivity:()Ljava/lang/Class;:GetGetCarActivityHandler\n" +
			"";
		mono.android.Runtime.register ("AndroidAutoTest.CarService, AndroidAutoTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarService.class, __md_methods);
	}


	public CarService ()
	{
		super ();
		if (getClass () == CarService.class)
			mono.android.TypeManager.Activate ("AndroidAutoTest.CarService, AndroidAutoTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public java.lang.Class getCarActivity ()
	{
		return n_getCarActivity ();
	}

	private native java.lang.Class n_getCarActivity ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
