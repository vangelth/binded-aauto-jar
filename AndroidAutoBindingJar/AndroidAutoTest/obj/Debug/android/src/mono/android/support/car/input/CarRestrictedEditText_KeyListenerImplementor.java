package mono.android.support.car.input;


public class CarRestrictedEditText_KeyListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.support.car.input.CarRestrictedEditText.KeyListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCloseKeyboard:()V:GetOnCloseKeyboardHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onCommitText:(Ljava/lang/String;)V:GetOnCommitText_Ljava_lang_String_Handler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onDelete:()V:GetOnDeleteHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onKeyDown:(I)V:GetOnKeyDown_IHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onKeyUp:(I)V:GetOnKeyUp_IHandler:Android.Support.Car.Input.CarRestrictedEditText/IKeyListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Android.Support.Car.Input.CarRestrictedEditText+IKeyListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarRestrictedEditText_KeyListenerImplementor.class, __md_methods);
	}


	public CarRestrictedEditText_KeyListenerImplementor ()
	{
		super ();
		if (getClass () == CarRestrictedEditText_KeyListenerImplementor.class)
			mono.android.TypeManager.Activate ("Android.Support.Car.Input.CarRestrictedEditText+IKeyListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCloseKeyboard ()
	{
		n_onCloseKeyboard ();
	}

	private native void n_onCloseKeyboard ();


	public void onCommitText (java.lang.String p0)
	{
		n_onCommitText (p0);
	}

	private native void n_onCommitText (java.lang.String p0);


	public void onDelete ()
	{
		n_onDelete ();
	}

	private native void n_onDelete ();


	public void onKeyDown (int p0)
	{
		n_onKeyDown (p0);
	}

	private native void n_onKeyDown (int p0);


	public void onKeyUp (int p0)
	{
		n_onKeyUp (p0);
	}

	private native void n_onKeyUp (int p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
