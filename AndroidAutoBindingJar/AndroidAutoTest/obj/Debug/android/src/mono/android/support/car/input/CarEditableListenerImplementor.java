package mono.android.support.car.input;


public class CarEditableListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.support.car.input.CarEditableListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onUpdateSelection:(IIII)V:GetOnUpdateSelection_IIIIHandler:Android.Support.Car.Input.ICarEditableListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Android.Support.Car.Input.ICarEditableListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarEditableListenerImplementor.class, __md_methods);
	}


	public CarEditableListenerImplementor ()
	{
		super ();
		if (getClass () == CarEditableListenerImplementor.class)
			mono.android.TypeManager.Activate ("Android.Support.Car.Input.ICarEditableListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onUpdateSelection (int p0, int p1, int p2, int p3)
	{
		n_onUpdateSelection (p0, p1, p2, p3);
	}

	private native void n_onUpdateSelection (int p0, int p1, int p2, int p3);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
