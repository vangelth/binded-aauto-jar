package mono.android.support.car.hardware;


public class CarSensorManager_OnSensorChangedListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.support.car.hardware.CarSensorManager.OnSensorChangedListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onSensorChanged:(Landroid/support/car/hardware/CarSensorManager;Landroid/support/car/hardware/CarSensorEvent;)V:GetOnSensorChanged_Landroid_support_car_hardware_CarSensorManager_Landroid_support_car_hardware_CarSensorEvent_Handler:Android.Support.Car.Hardware.CarSensorManager/IOnSensorChangedListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Android.Support.Car.Hardware.CarSensorManager+IOnSensorChangedListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarSensorManager_OnSensorChangedListenerImplementor.class, __md_methods);
	}


	public CarSensorManager_OnSensorChangedListenerImplementor ()
	{
		super ();
		if (getClass () == CarSensorManager_OnSensorChangedListenerImplementor.class)
			mono.android.TypeManager.Activate ("Android.Support.Car.Hardware.CarSensorManager+IOnSensorChangedListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onSensorChanged (android.support.car.hardware.CarSensorManager p0, android.support.car.hardware.CarSensorEvent p1)
	{
		n_onSensorChanged (p0, p1);
	}

	private native void n_onSensorChanged (android.support.car.hardware.CarSensorManager p0, android.support.car.hardware.CarSensorEvent p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
