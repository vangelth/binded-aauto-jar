package mono.android.support.car;


public class CarAppFocusManager_OnAppFocusChangedListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.support.car.CarAppFocusManager.OnAppFocusChangedListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onAppFocusChanged:(Landroid/support/car/CarAppFocusManager;IZ)V:GetOnAppFocusChanged_Landroid_support_car_CarAppFocusManager_IZHandler:Android.Support.Car.CarAppFocusManager/IOnAppFocusChangedListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Android.Support.Car.CarAppFocusManager+IOnAppFocusChangedListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarAppFocusManager_OnAppFocusChangedListenerImplementor.class, __md_methods);
	}


	public CarAppFocusManager_OnAppFocusChangedListenerImplementor ()
	{
		super ();
		if (getClass () == CarAppFocusManager_OnAppFocusChangedListenerImplementor.class)
			mono.android.TypeManager.Activate ("Android.Support.Car.CarAppFocusManager+IOnAppFocusChangedListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onAppFocusChanged (android.support.car.CarAppFocusManager p0, int p1, boolean p2)
	{
		n_onAppFocusChanged (p0, p1, p2);
	}

	private native void n_onAppFocusChanged (android.support.car.CarAppFocusManager p0, int p1, boolean p2);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
