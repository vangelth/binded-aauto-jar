package mono.com.google.android.gms.car;


public class CarNavigationStatusManager_CarNavigationStatusListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarNavigationStatusManager.CarNavigationStatusListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onStart:(IIIII)V:GetOnStart_IIIIIHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onStop:()V:GetOnStopHandler:Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarNavigationStatusManager_CarNavigationStatusListenerImplementor.class, __md_methods);
	}


	public CarNavigationStatusManager_CarNavigationStatusListenerImplementor ()
	{
		super ();
		if (getClass () == CarNavigationStatusManager_CarNavigationStatusListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.ICarNavigationStatusManagerCarNavigationStatusListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onStart (int p0, int p1, int p2, int p3, int p4)
	{
		n_onStart (p0, p1, p2, p3, p4);
	}

	private native void n_onStart (int p0, int p1, int p2, int p3, int p4);


	public void onStop ()
	{
		n_onStop ();
	}

	private native void n_onStop ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
