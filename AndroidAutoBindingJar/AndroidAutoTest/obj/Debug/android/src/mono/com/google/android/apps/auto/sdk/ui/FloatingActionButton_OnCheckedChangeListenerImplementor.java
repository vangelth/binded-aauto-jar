package mono.com.google.android.apps.auto.sdk.ui;


public class FloatingActionButton_OnCheckedChangeListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.apps.auto.sdk.ui.FloatingActionButton.OnCheckedChangeListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCheckedChanged:(Lcom/google/android/apps/auto/sdk/ui/FloatingActionButton;Z)V:GetOnCheckedChanged_Lcom_google_android_apps_auto_sdk_ui_FloatingActionButton_ZHandler:Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton/IOnCheckedChangeListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton+IOnCheckedChangeListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", FloatingActionButton_OnCheckedChangeListenerImplementor.class, __md_methods);
	}


	public FloatingActionButton_OnCheckedChangeListenerImplementor ()
	{
		super ();
		if (getClass () == FloatingActionButton_OnCheckedChangeListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Apps.Auto.Sdk.UI.FloatingActionButton+IOnCheckedChangeListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCheckedChanged (com.google.android.apps.auto.sdk.ui.FloatingActionButton p0, boolean p1)
	{
		n_onCheckedChanged (p0, p1);
	}

	private native void n_onCheckedChanged (com.google.android.apps.auto.sdk.ui.FloatingActionButton p0, boolean p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
