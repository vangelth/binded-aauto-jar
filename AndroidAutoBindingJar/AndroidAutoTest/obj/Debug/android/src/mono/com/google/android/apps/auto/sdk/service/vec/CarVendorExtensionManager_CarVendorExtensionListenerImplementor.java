package mono.com.google.android.apps.auto.sdk.service.vec;


public class CarVendorExtensionManager_CarVendorExtensionListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.apps.auto.sdk.service.vec.CarVendorExtensionManager.CarVendorExtensionListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onData:(Lcom/google/android/apps/auto/sdk/service/vec/CarVendorExtensionManager;[B)V:GetOnData_Lcom_google_android_apps_auto_sdk_service_vec_CarVendorExtensionManager_arrayBHandler:Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManagerCarVendorExtensionListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManagerCarVendorExtensionListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarVendorExtensionManager_CarVendorExtensionListenerImplementor.class, __md_methods);
	}


	public CarVendorExtensionManager_CarVendorExtensionListenerImplementor ()
	{
		super ();
		if (getClass () == CarVendorExtensionManager_CarVendorExtensionListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Apps.Auto.Sdk.Service.Vec.ICarVendorExtensionManagerCarVendorExtensionListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onData (com.google.android.apps.auto.sdk.service.vec.CarVendorExtensionManager p0, byte[] p1)
	{
		n_onData (p0, p1);
	}

	private native void n_onData (com.google.android.apps.auto.sdk.service.vec.CarVendorExtensionManager p0, byte[] p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
