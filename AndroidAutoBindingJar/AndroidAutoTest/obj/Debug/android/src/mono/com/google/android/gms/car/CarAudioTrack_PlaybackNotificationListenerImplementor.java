package mono.com.google.android.gms.car;


public class CarAudioTrack_PlaybackNotificationListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarAudioTrack.PlaybackNotificationListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onPeriodicNotification:(Lcom/google/android/gms/car/CarAudioTrack;)V:GetOnPeriodicNotification_Lcom_google_android_gms_car_CarAudioTrack_Handler:Com.Google.Android.Gms.Car.CarAudioTrack/IPlaybackNotificationListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onPlayError:(Lcom/google/android/gms/car/CarAudioTrack;I)V:GetOnPlayError_Lcom_google_android_gms_car_CarAudioTrack_IHandler:Com.Google.Android.Gms.Car.CarAudioTrack/IPlaybackNotificationListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.CarAudioTrack+IPlaybackNotificationListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarAudioTrack_PlaybackNotificationListenerImplementor.class, __md_methods);
	}


	public CarAudioTrack_PlaybackNotificationListenerImplementor ()
	{
		super ();
		if (getClass () == CarAudioTrack_PlaybackNotificationListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.CarAudioTrack+IPlaybackNotificationListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onPeriodicNotification (com.google.android.gms.car.CarAudioTrack p0)
	{
		n_onPeriodicNotification (p0);
	}

	private native void n_onPeriodicNotification (com.google.android.gms.car.CarAudioTrack p0);


	public void onPlayError (com.google.android.gms.car.CarAudioTrack p0, int p1)
	{
		n_onPlayError (p0, p1);
	}

	private native void n_onPlayError (com.google.android.gms.car.CarAudioTrack p0, int p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
