package mono.com.google.android.gms.car;


public class CarVendorExtensionManager_CarVendorExtensionListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarVendorExtensionManager.CarVendorExtensionListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onData:([B)V:GetOnData_arrayBHandler:Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarVendorExtensionManager_CarVendorExtensionListenerImplementor.class, __md_methods);
	}


	public CarVendorExtensionManager_CarVendorExtensionListenerImplementor ()
	{
		super ();
		if (getClass () == CarVendorExtensionManager_CarVendorExtensionListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.ICarVendorExtensionManagerCarVendorExtensionListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onData (byte[] p0)
	{
		n_onData (p0);
	}

	private native void n_onData (byte[] p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
