package mono.com.google.android.gms.car;


public class CarMessageManager_CarMessageListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarMessageManager.CarMessageListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onIntegerMessage:(III)V:GetOnIntegerMessage_IIIHandler:Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onOwnershipLost:(I)V:GetOnOwnershipLost_IHandler:Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarMessageManager_CarMessageListenerImplementor.class, __md_methods);
	}


	public CarMessageManager_CarMessageListenerImplementor ()
	{
		super ();
		if (getClass () == CarMessageManager_CarMessageListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.ICarMessageManagerCarMessageListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onIntegerMessage (int p0, int p1, int p2)
	{
		n_onIntegerMessage (p0, p1, p2);
	}

	private native void n_onIntegerMessage (int p0, int p1, int p2);


	public void onOwnershipLost (int p0)
	{
		n_onOwnershipLost (p0);
	}

	private native void n_onOwnershipLost (int p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
