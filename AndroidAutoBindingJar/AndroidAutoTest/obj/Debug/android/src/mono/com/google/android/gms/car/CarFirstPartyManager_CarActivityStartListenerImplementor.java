package mono.com.google.android.gms.car;


public class CarFirstPartyManager_CarActivityStartListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarFirstPartyManager.CarActivityStartListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onActivityStarted:(Landroid/content/Intent;)V:GetOnActivityStarted_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerInvoker, AndroidAutoBindingJar\n" +
			"n_onNewActivityRequest:(Landroid/content/Intent;)V:GetOnNewActivityRequest_Landroid_content_Intent_Handler:Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarFirstPartyManager_CarActivityStartListenerImplementor.class, __md_methods);
	}


	public CarFirstPartyManager_CarActivityStartListenerImplementor ()
	{
		super ();
		if (getClass () == CarFirstPartyManager_CarActivityStartListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.ICarFirstPartyManagerCarActivityStartListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onActivityStarted (android.content.Intent p0)
	{
		n_onActivityStarted (p0);
	}

	private native void n_onActivityStarted (android.content.Intent p0);


	public void onNewActivityRequest (android.content.Intent p0)
	{
		n_onNewActivityRequest (p0);
	}

	private native void n_onNewActivityRequest (android.content.Intent p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
