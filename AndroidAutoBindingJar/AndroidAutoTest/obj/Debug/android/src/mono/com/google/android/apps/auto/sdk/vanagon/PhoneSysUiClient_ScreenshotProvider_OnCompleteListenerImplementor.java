package mono.com.google.android.apps.auto.sdk.vanagon;


public class PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.apps.auto.sdk.vanagon.PhoneSysUiClient.ScreenshotProvider.OnCompleteListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onScreenshotComplete:(Landroid/graphics/Bitmap;)V:GetOnScreenshotComplete_Landroid_graphics_Bitmap_Handler:Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient/IScreenshotProviderOnCompleteListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient+IScreenshotProviderOnCompleteListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor.class, __md_methods);
	}


	public PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor ()
	{
		super ();
		if (getClass () == PhoneSysUiClient_ScreenshotProvider_OnCompleteListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Apps.Auto.Sdk.Vanagon.PhoneSysUiClient+IScreenshotProviderOnCompleteListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onScreenshotComplete (android.graphics.Bitmap p0)
	{
		n_onScreenshotComplete (p0);
	}

	private native void n_onScreenshotComplete (android.graphics.Bitmap p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
