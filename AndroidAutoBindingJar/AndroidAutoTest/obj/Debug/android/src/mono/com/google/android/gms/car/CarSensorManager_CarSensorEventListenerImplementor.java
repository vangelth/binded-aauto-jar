package mono.com.google.android.gms.car;


public class CarSensorManager_CarSensorEventListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.gms.car.CarSensorManager.CarSensorEventListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onSensorChanged:(IJ[F[B)V:GetOnSensorChanged_IJarrayFarrayBHandler:Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CarSensorManager_CarSensorEventListenerImplementor.class, __md_methods);
	}


	public CarSensorManager_CarSensorEventListenerImplementor ()
	{
		super ();
		if (getClass () == CarSensorManager_CarSensorEventListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Gms.Car.ICarSensorManagerCarSensorEventListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onSensorChanged (int p0, long p1, float[] p2, byte[] p3)
	{
		n_onSensorChanged (p0, p1, p2, p3);
	}

	private native void n_onSensorChanged (int p0, long p1, float[] p2, byte[] p3);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
