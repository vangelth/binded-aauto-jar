package mono.com.google.android.apps.auto.sdk.ui;


public class PagedScrollBarView_PaginationListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.apps.auto.sdk.ui.PagedScrollBarView.PaginationListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onPaginate:(I)V:GetOnPaginate_IHandler:Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView/IPaginationListenerInvoker, AndroidAutoBindingJar\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView+IPaginationListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", PagedScrollBarView_PaginationListenerImplementor.class, __md_methods);
	}


	public PagedScrollBarView_PaginationListenerImplementor ()
	{
		super ();
		if (getClass () == PagedScrollBarView_PaginationListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Apps.Auto.Sdk.UI.PagedScrollBarView+IPaginationListenerImplementor, AndroidAutoBindingJar, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onPaginate (int p0)
	{
		n_onPaginate (p0);
	}

	private native void n_onPaginate (int p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
