﻿using Android.App;
using Android.Widget;
using Android.OS;

using Com.Google.Android.Apps.Auto.Sdk;

namespace AndroidAutoTest
{
    [Activity(Label = "AndroidAutoTest", MainLauncher = true)]
    public class MainActivity : CarActivity
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
        }
    }
}

